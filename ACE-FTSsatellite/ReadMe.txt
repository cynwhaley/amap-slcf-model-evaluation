Sampling models
1) Run SampleModelAsACE_v3.py
	-Must modify for different models (instructions within the script)
-Model files must be within the same folder and be named with the form model-name_*details*_year_gas-key.nc where gas-key is the key to get the VMR in the netCDF file.
-Must have ACE-FTS and ACE-FTS quality flag file in the same folder.
-If the models have corresponding pressure files, they must have the form model-name_*details*_year_air-pressure.nc
	-Python packages: numpy, netCDF4, os, datetime, time, copy, math, scipy
	-Calls DataFiltering (no additional packages required)
	-Calls GetPressureProfiles (additional Python packages: csv)
-If using MATCH-SALSA, EMEP-MSC-W, or CESM, need the files with the coefficients and to update the paths to these files within the script
	-Saves one .mat file with the sampled values in the folder specified by the user

Most likely errors: files in wrong folders, files with wrong naming scheme

Plotting
1) Run ModelDifferencePlots.py
-Makes vertically resolved daily time series plots that compare the relative differences between models.
-Python packages: sys, os, numpy, matplotlib, datetime, colorcet, copy, scipy, pcandas
-Calls DataFiltering (additional Python packages: netCDF4)

2) ScatterModelMeasPlots.py
-Makes correlation plots between each model and ACE-FTS values for input year ranges, pressure levels, and latitudes. Each season is shown by a colour.
-Prints the number of measurements, Pearson correlation coefficient, mean absolute difference, and mean relative difference for each year/pressure level combo.
-Python packages: matplotlib, numpy, scipy, copy, os
-Calls DataFiltering (additional Python packages: netCDF4)

3) TaylorDiagram.py
-Makes one figure for each range of years and pressure level, one subplot per gas.
-Python packages: matplotlib, numpy, pandas, datetime, scipy, skill_metrics, calendar, copy
-Calls DataFiltering (additional Python packages: netCDF4)


