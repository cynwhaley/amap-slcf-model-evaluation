import numpy as np
import scipy.io as spio
from netCDF4 import Dataset
from datetime import datetime as dt
from copy import copy

def FilterACEMeas(ace_vmr,ace_qf,max_qf):
    occs_4to7=np.any(np.isin(ace_qf,[4,5,6,7]),axis=1)
    ace_vmr[occs_4to7,:]=float('nan')
    ace_vmr[ace_qf>max_qf]=float('nan')
    return ace_vmr

def MatchQFProfiles(ace_fname,qf_fname):
    """
    This function takes the orbit and sunset/sunrise values from the ACE-FTS 
    file and the corresponding flag file and matches the profiles.
    """
    if ace_fname.endswith('.nc'):
        nc_ace=Dataset(ace_fname)
        orbit_ace=nc_ace.variables['orbit'][:].data
        sssr_ace=nc_ace.variables['sunset_sunrise'][:].data
    elif ace_fname.endswith('.mat'):
        mat_ace=spio.loadmat(ace_fname,squeeze_me=True,variable_names=['orbit','sunset_sunrise'])
        orbit_ace=mat_ace['orbit']
        sssr_ace=mat_ace['sunset_sunrise']
    nc_qf=Dataset(qf_fname)

    
    orbit_qf=nc_qf.variables['orbit'][:].data
    sssr_qf=nc_qf.variables['sunset_sunrise'][:].data

    orbit_qf=orbit_qf.astype('float')
    sssr_qf=sssr_qf.astype('float')

    #if I don't do this then the inputs get changed in the main script
    orbit_ace_c=copy(orbit_ace)
    orbit_qf_c=copy(orbit_qf)

    #give a unique value for each orbit/sssr combo
    orbit_ace_c[sssr_ace==1]=orbit_ace[sssr_ace==1]+0.25
    orbit_ace_c[sssr_ace==2]=orbit_ace[sssr_ace==2]+0.5
    orbit_ace_c[sssr_ace==3]=orbit_ace[sssr_ace==3]+0.75

    orbit_qf_c[sssr_qf==1]=orbit_qf[sssr_qf==1]+0.25
    orbit_qf_c[sssr_qf==2]=orbit_qf[sssr_qf==2]+0.5
    orbit_qf_c[sssr_qf==3]=orbit_qf[sssr_qf==3]+0.75

    [_,ind_ace,ind_qf]=np.intersect1d(orbit_ace_c,orbit_qf_c,return_indices='True')

    return [ind_ace,ind_qf]

def MatchProfiles(fname1,fname2):   
    """
    This function takes in data from DMP files, ACE-FTS files, flag files, and sampled 
    model files and matches up profiles. This allows me to use files that weren't
    produced at the same time. This only compares two files.
    
    -Input-
    
    fname1: string
        filename (with path) of first file
    fname2: string
        filename (with path) of second file
    orbit: boolean
        If True, use the orbit number instead of the date to match profiles.
        THIS DOES NOT WORK YET.
        
    -Output-
    
    inds1: array
        indices of the matched up profiles in the first file
    inds2: array
        indices of the matched up profiles in the second file
    """
    #get year, month, day, and hour arrays from each file
    if fname1.endswith('.nc') or fname1.endswith('.nc4'):
        nc=Dataset(fname1)
        try:
            years1=nc.variables['year'][:].data
            months1=nc.variables['month'][:].data
            days1=nc.variables['day'][:].data
            hours1=nc.variables['hour'][:].data
        except KeyError: #this will happen if it's a DMP file
            dates=[x for x in nc.variables['Date']]
            years1=np.array([x[:4] for x in dates]).astype('float32')
            months1=np.array([x[4:6] for x in dates]).astype('float32')
            days1=np.array([x[6:8] for x in dates]).astype('float32')
            hours1=np.array([x for x in nc.variables['Hour'][:]]).astype('float32')
        nc.close()
            
    elif fname1.endswith('.mat'):
        mat=spio.loadmat(fname1,variable_names=['year','month','day','hour'])
        years1=np.squeeze(mat['year'])
        months1=np.squeeze(mat['month'])
        days1=np.squeeze(mat['day'])
        hours1=np.squeeze(mat['hour'])
    else:
        # print('Cannot read file 1.')
        return [-1,-1]
    
    if fname2.endswith('.nc') or fname2.endswith('.nc4'):
        nc=Dataset(fname2)
        try:
            years2=nc.variables['year'][:].data
            months2=nc.variables['month'][:].data
            days2=nc.variables['day'][:].data
            hours2=nc.variables['hour'][:].data
        except KeyError: #this will happen if it's a DMP file
            dates=[x for x in nc.variables['Date']]
            years2=np.array([x[:4] for x in dates]).astype('float32')
            months2=np.array([x[4:6] for x in dates]).astype('float32')
            days2=np.array([x[6:8] for x in dates]).astype('float32')
            hours2=np.array([x for x in nc.variables['Hour'][:]]).astype('float32')
        nc.close()
    elif fname2.endswith('.mat'):
        mat=spio.loadmat(fname2,variable_names=['year','month','day','hour'])
        years2=np.squeeze(mat['year'])
        months2=np.squeeze(mat['month'])
        days2=np.squeeze(mat['day'])
        hours2=np.squeeze(mat['hour'])
    else:
        print('Cannot read file 1.')
        return [-1,-1]
        
    #convert the hours as decimals to hours and minutes
    minutes1=(hours1-hours1.astype('int'))*60
    minutes2=(hours2-hours2.astype('int'))*60
    hours1=hours1.astype('int')
    hours2=hours2.astype('int')
    
    #get a list of datetimes for each file
    dts1=[]
    for i in range(len(years1)):
        dts1.append(dt(int(years1[i]),int(months1[i]),int(days1[i]),int(hours1[i]),int(minutes1[i])))
    dts1=np.array(dts1)
    
    dts2=[]
    for i in range(len(years2)):
        dts2.append(dt(int(years2[i]),int(months2[i]),int(days2[i]),int(hours2[i]),int(minutes2[i])))
    dts2=np.array(dts2)

    #match up the datetimes and get the indices that match them for each file    
    [_,inds1,inds2]=np.intersect1d(dts1,dts2,return_indices='True')
    
    return [inds1,inds2]


def MatchProfilesND(fnames):   
    """
    This function takes in data from DMP files, ACE-FTS files, flag files, and sampled 
    model files and matches up profiles. This allows me to use files that weren't
    produced at the same time.

    This extends MatchProfiles to allow for comparison between multiple files.
    """
    ref_inds=[]
    test_inds=[]

    ref_file=fnames[0]
    
    #match it with the other files
    for f in fnames:
        [inds1,inds2]=MatchProfiles(fnames[0],f)
        ref_inds.append(inds1)
        test_inds.append(inds2)

    #find the intersection of all the references indices
    ref_intersection=list(set(ref_inds[0]).intersection(*ref_inds))

    all_inds=[]
    #get where these indices are found
    for i in range(len(ref_inds)):
        ref_match=np.where(np.isin(ref_inds[i],ref_intersection))[0]
        test_match=test_inds[i][ref_match]
        all_inds.append(test_match)

    return all_inds

