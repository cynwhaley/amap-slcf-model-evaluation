import matplotlib.pyplot as plt
import numpy as np
from os.path import isdir
from os import mkdir
import scipy.io as spio
from copy import copy
from scipy.stats import pearsonr
import matplotlib.patches as mpl_patches
import matplotlib.lines as mlines
from DataFiltering import MatchProfiles, MatchQFProfiles, FilterACEMeas

model_folder='Data/AMAP/AMAP sampled/' #folder where the sampled model output is saved
ace_folder='Data/ACE-FTS/' #folder where the ACE data is saved

save_folder='AMAP project/New figures/Scatter/'

#models to include (must be written the same way as in filenames)
models=['CESM','CMAM','GEOS-Chem','MRI-ESM2','UKESM1']

wanted_yrs=[[2008,2009],[2014,2015]]
gases=['O3','CH4','CO','NOx']

lat_range=[60,90] #range of latitudes to include

p=np.array([100,150]) #pressure levels to plot in hPa
p_units='hPa'
alt_inds=np.arange(0,150)

if not isdir(save_folder):
    mkdir(save_folder)

#find the best units (e.g. ppbv vs pptv) for each gas/level combo
unit_dict=dict()
for g in gases:
    g_dict=dict()

    mat=spio.loadmat(model_folder+'MRI-ESM2_ACEFTS_locations_'+g+'.mat',squeeze_me=True)
    vmr=mat[g]

    #filter with quality flags
    vmr=FilterACEMeas(mat[g],mat['quality_flag'],1)
    ace_p=mat['pressure']*1013.25
        
    for level in p: #loop through pressure levels
        #find closest ACE altitude for this level (this is approximate)
        p_diffs=np.abs(ace_p-level)
        alt_ind=np.argmin(np.nanmean(p_diffs,axis=0))
        
        exp=np.abs(np.log10(np.nanmedian(vmr[:,alt_ind])))
        if exp<8:
            g_dict[str(level)]=[1e9,'ppbv']
        else:
            g_dict[str(level)]=[1e12,'pptv']
            
    unit_dict[g]=g_dict

print('Got units.')

#loop through gases
for g in gases:
    #get subscripted chem formula for plot labels
    g_latex=''.join(['$_{%d}$' % (int(x),) if x.isdigit() else x for x in list(g)])
    for i in range(len(wanted_yrs)):
        
        #initialize a dictionary to hold the interpolated model values
        m_vmr_dict=dict()
        #keep track of which models have values at each pressure
        num_models_p=np.zeros(len(p))
        #now loop through models and get values for each of them
        for j in range(len(models)):        
            #load in the model VMRs
            try:
                fname=model_folder+models[j]+'_ACEFTS_locations_'+g+'.mat'
                mat=spio.loadmat(fname, squeeze_me=True)  
            except FileNotFoundError:
                continue
    
            #take out information from the desired latitudes in the Arctic
            time_inds=np.where(np.isin(mat['year'],wanted_yrs[i]))[0]
            lat_inds=np.where(np.logical_and(mat['latitude']>=lat_range[0],mat['latitude']<=lat_range[1]))[0]
            
            right_inds=list(set(time_inds).intersection(lat_inds))
            
            if len(right_inds)==0: #if there are no measurements, skip this model
                continue
            
            years=mat['year'][right_inds]
            months=mat['month'][right_inds]
            ace_vmrs=mat[g][right_inds][:,alt_inds]
            m_vmrs=mat['sampled_vmr'][right_inds][:,alt_inds]
            
            #get the pressure in hPa, logged and multiplied (for easier interpolation)
            p_vals_a=10*np.log(mat['pressure'][right_inds][:,alt_inds]*1013.25) #convert to hPa
            p_vals_m=copy(p_vals_a)
                
            #filter the data with quality flags
            qfs=mat['quality_flag'][right_inds][:,alt_inds]    
            m_vmrs=FilterACEMeas(m_vmrs,qfs,1)
            
            #initialize an array of NaNs to hold the interpolated ACE values
            ace_interp=np.empty((len(ace_vmrs),len(p)))
            ace_interp[:]=float('nan')           
            m_interp=copy(ace_interp)
        
            #interpolate all the profiles onto the wanted levels
            for k in range(len(ace_vmrs)): #loop through profiles
        		#remove NaN values
                valid_a=np.where(np.logical_and(~np.isnan(ace_vmrs[k]),~np.isnan(p_vals_a[k])))[0]
                valid_m=np.where(np.logical_and(~np.isnan(ace_vmrs[k]),~np.isnan(p_vals_m[k])))[0]
        
                if len(valid_a)>3 and len(valid_m)>3:
                	#put heights in order
                    order_a=np.argsort(p_vals_a[k][valid_a])
                    order_m=np.argsort(p_vals_m[k][valid_m])
                
                    p_vals_sorted_a=p_vals_a[k][order_a]
                    p_vals_sorted_m=p_vals_m[k][order_m]
                    
                    ace_sorted=ace_vmrs[k][order_a]
                    m_sorted=m_vmrs[k][order_m]
  
                    p_for_interp=10*np.log(p)
                
                    ace_interp[k,:]=np.interp([p_for_interp],p_vals_sorted_a,ace_sorted,\
                					left=float('nan'),right=float('nan'))
                    m_interp[k,:]=np.interp([p_for_interp],p_vals_sorted_m,m_sorted,\
                					left=float('nan'),right=float('nan'))
            
            m_vmr_dict[models[j]]=[ace_interp,m_interp,months]
            num_models_p=num_models_p+np.any(~np.isnan(m_interp),axis=0)
            print('Done '+models[j])

        #now make a figure for each pressure level
        for j in range(len(p)):
            
            #get the units
            unit_mult=unit_dict[g][str(p[j])][0]
            unit_name=unit_dict[g][str(p[j])][1]
            
            num_rows=int(num_models_p[j]**0.5) 
            if num_rows==0:
                continue
            num_cols=int(np.ceil(num_models_p[j]/float(num_rows)))
            
            f,square_axes=plt.subplots(num_rows,num_cols,figsize=(11,8.5),sharex=True,sharey=True)
            if not type(square_axes)==np.ndarray: #I think this is if there's only one subplot
                #make the one axis into an array of length 1 to avoid errors later
                square_axes=np.array([square_axes])
            subplot_count=0 #keep track of how many subplots are used
            
            #loop through the models
            for key in list(m_vmr_dict.keys()):
                #take out the ACE values
                ace_vmrs=m_vmr_dict[key][0][:,j]*unit_mult
                #take out model values
                m_vmrs=m_vmr_dict[key][1][:,j]*unit_mult
                
                #find non-NaN values (needed for calculating R)
                valid=np.where(np.logical_and(~np.isnan(m_vmrs),~np.isnan(ace_vmrs)))[0]
                if ~np.any(valid):
                    continue #move on if there are no valid comparisons
                
                print(key+': '+str(p[j]))
                print('$N=%d$' % (np.sum(~np.isnan(m_vmrs+ace_vmrs))), 
                     '$R=%.2f$' % (pearsonr(ace_vmrs[valid],m_vmrs[valid])[0], ),
                     '$\mu_{abs}=%.2f$%s' % (np.nanmean(m_vmrs-ace_vmrs),unit_name),
                     '$\mu_{rel}=%.2f$%%' % (100*np.nanmean((m_vmrs-ace_vmrs)/ace_vmrs)))

                print('-----')
                
                mos=m_vmr_dict[key][2]
                
                #divide into seasons
                djf_inds=np.where(np.isin(mos,[12,1,2]))[0]
                mam_inds=np.where(np.isin(mos,[3,4,5]))[0]
                jja_inds=np.where(np.isin(mos,[6,7,8]))[0]
                son_inds=np.where(np.isin(mos,[9,10,11]))[0]
                
                #get the axes to plot on
                s_ax=square_axes.flatten()[subplot_count] 
                
                #plot each season with a different colour
                s_ax.scatter(ace_vmrs[djf_inds],m_vmrs[djf_inds],label='DJF',s=2,c='r')
                s_ax.scatter(ace_vmrs[mam_inds],m_vmrs[mam_inds],label='MAM',s=2,c='b')
                s_ax.scatter(ace_vmrs[jja_inds],m_vmrs[jja_inds],label='JJA',s=2,c='g')
                s_ax.scatter(ace_vmrs[son_inds],m_vmrs[son_inds],label='SON',s=2,c='y')
                
                subplot_count+=1
                s_ax.set_title(key)
                
                #add in all the text
                #put pressure level, years, and gas in first set of axes
                if subplot_count==1:
                    labels=[str(wanted_yrs[i][0])+'-'+str(wanted_yrs[i][1])+', '\
                            +str(p[j])+p_units+', 60-90$^\circ$N']
                    handles=[mpl_patches.Rectangle((0,0),1,1,fc="white",ec="white",lw=0,alpha=0)]*5
                else:
                    labels=[]
                    handles=[mpl_patches.Rectangle((0,0),1,1,fc="white",ec="white",lw=0,alpha=0)]*4
                                 
                labels=labels+['$N=%d$' % (np.sum(~np.isnan(m_vmrs+ace_vmrs))), 
                         '$R=%.2f$' % (pearsonr(ace_vmrs[valid],m_vmrs[valid])[0], ),
                         '$\mu_{abs}=%d$%s' % (np.nanmean(m_vmrs-ace_vmrs),unit_name),
                         '$\mu_{rel}=%d$%%' % (100*np.nanmean((m_vmrs-ace_vmrs)/ace_vmrs))]
                
                s_ax.legend(handles,labels,loc='best', fontsize='small',
                          framealpha=0.9,handlelength=0,handletextpad=0)
                
            f.subplots_adjust(left=0.1,bottom=0.1,wspace=0)
            #x label
            f.text(0.5,0.05,'ACE '+g_latex+' ['+unit_name+']',ha='center',va='top')
            #y label
            f.text(0.05,0.5,'Model '+g_latex+' ['+unit_name+']',ha='right',va='center',rotation='vertical')
            
            #replace legend of last subplot
            s_ax=square_axes.flatten()[subplot_count-1]
            handles=handles+[mlines.Line2D([0],[0],marker='o',color=x,markersize=4,linestyle='') for x in ['r','b','g','y']]
            labels=labels+['DJF','MAM','JJA','SON']
            s_ax.legend(handles,labels,loc='best',fontsize='small',fancybox=True,ncol=2,\
                          framealpha=0.9)
                
            #remove unused axes
            for ax in square_axes.flatten()[subplot_count:]:
                ax.remove()
                
            #add perfect correlation line
            x_lims=s_ax.get_xlim()
            y_lims=s_ax.get_ylim()
            low_val=np.min((x_lims[0],y_lims[0]))
            high_val=np.max((x_lims[1],y_lims[1]))
            for s_ax in square_axes.flatten():
                s_ax.plot([low_val,high_val],[low_val,high_val],'k')
            s_ax.set_xlim(x_lims)
            s_ax.set_ylim(y_lims)
                    
            fig_name='scatter_'+g+'_60-90N_'+str(wanted_yrs[i][0])+'-'+str(wanted_yrs[i][1])+'_'+str(p[j])+p_units

            f.savefig(save_folder+fig_name+'.png')
            f.savefig(save_folder+fig_name+'.eps')
            
            plt.close()