import numpy as np
from netCDF4 import Dataset, num2date
from os.path import basename, isfile
from os import listdir
from datetime import datetime as dt, timedelta
from time import time as tm
from copy import copy
from math import radians, cos, sin, asin, sqrt
import scipy.io as spio
from scipy import interpolate

from GetPressureProfiles import GetPressureProfiles
from DataFiltering import MatchQFProfiles
"""
Important notes about units:
	
	1) All times should be immediately converted to seconds
	2) All longitudes should be put on a 0-360 degree scale
	3) All pressures should be immediately converted to atm
	4) All VMRs should be immediately converted to molecules/molecule of air

"""

#======================ONLY CHANGE WHAT'S IN THIS BOX==========================
#folder that contains the model output files
model_folder='Data/CMAM39/Model output/'

#model filename (needs to be a list for later commands to work)
#name any of the model files that will be used. Must be within model_folder
model_fnames=['UKESM1_type0_3hourly_2008_o3-volume-mixing-ratio.nc']

#starting date of model (can be anything for MATCH-SALSA and WRF-CHEM)
# model_start=dt(2007,1,1) #CESM 2008-2009
# model_start=dt(2013,1,1) #CESM 2014-2015
model_start=dt(1860,1,1) #CMAM, DEHM, MATCH
# model_start=dt(1970,1,1) #EMEP-MSC-W, UKESM1
# model_start=dt(2014,1,1) #GEOS-Chem

#model keys - varies depending on model
time_key='time'
lat_key='latitude' 
lon_key='longitude'

#model units
time_units=3600*24. #conversion factor to get to seconds
vmr_units=1e-9 #conversion factor to get to ppv

#all ACE years up to 2018
years_to_sample=np.arange(2004,2019)

#ACE data filename - if sampling NOx, use the NO ACE file
ACE_fname="Data/ACE-FTS/ACEFTS_L2_v4p1_O3.nc"

#ACE tangent height
tangent_height=30. #30 km, also index 30 in the altitude list

#folder to save to
save_folder='Data/AMAP/AMAP sampled/'
#==============================================================================

#===================CONVERT TIMES FROM MATCH-SALSA AND WRF-CHEM=========================
#MATCH-SALSA gives its times in YYYYMMDD. To avoid changing the code that follows,
#I simply convert it to the units we normally see in model files. The user can input
#any model start time and time units and it will work.

def ConvertTimes(model_name,model_times,model_start,time_units):
	"""
	Convert times to the common format. Only needed for MATCH-SALSA and
	WRF-CHEM (so far), since they used different formats from all the others.
	"""
	if model_name=='MATCH-SALSA':
		model_times=model_times.astype('str').tolist()
		years=np.array([int(x[:4]) for x in model_times])
		months=np.array([int(x[4:6]) for x in model_times])
		days=np.array([int(x[6:].split('.')[0]) for x in model_times])
		hours=np.array([int(float('0.'+x[6:].split('.')[1])*24.) for x in model_times])
	elif model_name=='WRF-CHEM':
		model_times=model_times.astype('|S1')
		years=np.array([int(x[:4].tostring().decode('utf-8')) for x in model_times])
		months=np.array([int(x[5:7].tostring().decode('utf-8')) for x in model_times])
		days=np.array([int(x[8:10].tostring().decode('utf-8')) for x in model_times])
		hours=np.array([int(x[11:13].tostring().decode('utf-8')) for x in model_times])
	else:
		return model_times

	days_since_start=[]
	for i in range(len(model_times)):
		time_passed=(dt(years[i],months[i],days[i],hours[i])-model_start).total_seconds()/time_units
		days_since_start.append(time_passed)

	return np.array(days_since_start)

#==============================================================================

def haversine(lon1,lat1,lon2,lat2):
	"""
	Calculate the great circle distance between two points 
	on the earth (specified in decimal degrees).
	
	I took this from the internet.
	"""
	# convert decimal degrees to radians 
	[lon1,lat1,lon2,lat2]=map(radians,[lon1,lat1,lon2,lat2])

	# haversine formula 
	dlon=lon2-lon1 
	dlat=lat2-lat1 
	h=sin(dlat/2)**2+cos(lat1)*cos(lat2)*sin(dlon/2)**2
	c=2*asin(sqrt(h)) 
	r=6378.1 # Radius of earth in km
	return c*r

#To understand the script more easily, start reading from the bottom of this function.
	
def SampleACEYear(model_year,model_fname,model_start,ACE_nc,GLC_nc,ACE_alts,ACE_yrs,NOx):
	t0=tm()
	#get the name of the species in the model
	file_no_path=basename(model_fname) #get just the file name (without path)
	file_no_ext=file_no_path.split('.')[0] #get file name without extension
	species=file_no_ext.split('_')[-1].replace('-','_') #get the species name from end of filename
	model_name=file_no_ext.split('_')[0]

	#for WRF-CHEM and GEM-MACH, since the files are divided into months, also need
	#to make sure the month is correct
	if model_name in ['GEM-MACH','WRF-CHEM']:
		#get the month (this assumes the files are all named correctly)
		mo=int(file_no_ext.split('_')[-2].split('-')[1])
		print(mo)
		ACE_mos=ACE_nc.variables['month'][:].data
		occultations=np.where(np.logical_and(ACE_yrs==model_year,ACE_mos==mo))[0]
		if model_name=='GEM-MACH': #need to change the start time
			model_start=model_start.replace(month=int(mo),hour=2)
	else:
		#get the occultations that are in the correct year
		occultations=np.where(ACE_yrs==model_year)[0]

	#ACE time data
	ACE_yrs=ACE_yrs[occultations]
	ACE_mos=ACE_nc.variables['month'][occultations].data
	ACE_dys=ACE_nc.variables['day'][occultations].data
	ACE_hrs=ACE_nc.variables['hour'][occultations].data
	
	#ACE location data
	ACE_ps=ACE_nc.variables['pressure'][occultations,:].data
	ACE_lats=GLC_nc.variables['GLC_latitude'][occultations].data
	ACE_lons=GLC_nc.variables['GLC_longitude'][occultations].data
	ACE_tan_lats=ACE_nc.variables['latitude'][occultations].data
	ACE_tan_lons=ACE_nc.variables['longitude'][occultations].data

	#wrap the longitudes that are out of range
	ACE_lons[ACE_lons<-180.]=ACE_lons[ACE_lons<-180.]+360.
	ACE_lons[ACE_lons>180.]=ACE_lons[ACE_lons>180.]-360.
	
	#don't go through profiles where the latitudes couldn't be retrieved
	valid_rows=np.where(~np.all(np.isnan(ACE_lats),axis=1))[0]
	invalid_rows=np.where(np.all(np.isnan(ACE_lats),axis=1))[0]

	for i in invalid_rows:
		ACE_lats[i,:]=ACE_tan_lats[i]
		ACE_lons[i,:]=ACE_tan_lons[i]
		   
	#load in the model output, without casting to numpy arrays yet	
	#load all the data in the model file
	model_nc=Dataset(model_fname)
	if NOx:
		model_nc2=Dataset(model_fname.replace('.nc','2.nc'))
	
	#get the model file basename (i.e. remove the folders from the beginning)
	model_basename=model_fname.split('/')[-1]
	#see if the model file has a corresponding pressure file (e.g. UKESM1)
	model_p_basename=model_basename.replace(species.replace('_','-'),'air-pressure')
	model_p_fname=model_fname.replace(model_basename,'')+model_p_basename

	if model_name=='GEOS-CHEM':
		model_p_fname=model_fname
	if isfile(model_p_fname):
		p_file=True
		if model_name=='UKESM1':
			air_pressure_key='air_pressure'
		elif model_name in ['MATCH','MATCH-SALSA','WRF-CHEM']:
			air_pressure_key='pres'
		elif model_name in ['CESM','EMEP-MSCW']:
			air_pressure_key='ps'
		elif model_name=='GEM-MACH':
			air_pressure_key='P'
		elif model_name in ['MRI-ESM','GEOS-CHEM']:
			air_pressure_key='nope' #this won't be right for any of them

	else:
		p_file=False
		
	#take one time stamp from the model on either side of this year
	
	#get the minimum and maximum times in this year
	first_time=(dt(model_year,1,1,0)-model_start).total_seconds()/time_units
	last_time=(dt(model_year,12,31,23)-model_start).total_seconds()/time_units

	#time stamps for the model in hours, but need to convert so it includes leap years
	#if the calendar is not provided in the file, might have to input it manually (so far, I've only found this
	#for UKESM1 2008, which uses gregorian)
	model_times=num2date(model_nc.variables[time_key][:].data,\
		model_nc.variables[time_key].units,calendar=model_nc.variables[time_key].calendar)
	model_times=np.array([(dt(x.year,x.month,x.day,x.hour)\
		-model_start).total_seconds()/time_units for x in model_times])

	#this is really only necessary for models that don't have the standard time
	#format; this function does nothing for others (I just wanted to remove if statements)
	model_times=ConvertTimes(model_name,model_times,model_start,time_units)
	
	model_lats=model_nc.variables[lat_key][:].data
	model_lons=model_nc.variables[lon_key][:].data

	lat_spacing=np.nanmax(np.diff(model_lats)) #max distance (degrees) between adjacent model latitudes
	lon_spacing=np.nanmax(np.diff(model_lons)) #max distance (degrees) between adjacent model longitudes
	
	#convert the model longitudes to [-180, 180]
	model_lons[model_lons>180.]=model_lons[model_lons>180.]-360.
	 
	#model VMR (and maybe pressure) values
	model_vmrs=model_nc.variables[species]
	if NOx:
		model_vmrs=model_nc2.variables[species+'2']
	if p_file:
		model_p_nc=Dataset(model_p_fname)
		try:
			model_p=model_p_nc.variables[air_pressure_key]
		except KeyError:
			if model_name=='MRI-ESM':
				model_p=[model_p_nc.variables['a'],model_p_nc.variables['p0'],\
						model_p_nc.variables['b'],model_p_nc.variables['ps']] 
			elif model_name=='GEOS-CHEM':
				model_p=[model_p_nc.variables['Ap'],model_p_nc.variables['Bp'],\
						model_p_nc.variables['psfc']]

	else:
		model_p=-1
	
	#create an array of nan values that is occultations x altitudes
	sampled_vmr=np.empty((len(ACE_yrs),len(ACE_alts)))
	sampled_vmr[:]=float('nan')
	sampled_vmr_nn=copy(sampled_vmr)
	sampled_lat=copy(sampled_vmr)
	sampled_lon=copy(sampled_vmr)
	sampled_t=np.empty(len(ACE_yrs))
	sampled_t[:]=float('nan')
	
	time_for_profile=[]
	
	#loop through valid ACE occultations
	for i in range(len(ACE_lats)):
	# for i in [0,1]:
		t00=tm()

		#for GEM-MACH, skip all occultations south of 60N to save time
		if model_name=='GEM-MACH' and ACE_lats[i,30]<60:
			continue

		#get the number of days/hours (depending on model) since start date for 
		#the ACE occultation (considers leap years)
		ace_t=(dt(model_year,ACE_mos[i],ACE_dys[i],ACE_hrs[i])-model_start).total_seconds()/time_units

		#find the 2 model times that are closest
		model_t_ind=np.argmin(np.abs(model_times-ace_t))
		model_nc_used=model_nc
		model_p_var_used=model_p

		sampled_t[i]=model_times[model_t_ind]
		tdiffs_order=np.argsort(np.abs(model_times-ace_t))
		model_t_inds=np.sort(tdiffs_order[:2])

		if not p_file:
			model_p_var_used=-1

		#figure out which model profiles will be needed that are "close enough"
		all_grid_coords=[]
		all_grid_lat_inds=[]
		all_grid_lon_inds=[]

		for j in np.arange(0,len(ACE_alts)): #loop through levels
			#get ACE coordinates
			ace_lat=ACE_lats[i,j]
			ace_lon=ACE_lons[i,j]
			
			#find the four closest gridpoints (two closest lats, two closest lons)
			#latitudes
			latdiffs_order=np.argsort(np.abs(ace_lat-model_lats))
			grid_lat_inds=np.sort(latdiffs_order[:2])
			#longitudes
			londiffs_order=np.argsort(np.abs(ace_lon-model_lons))
			#need to do the following because longitudes wrap
			if ace_lon>model_lons[-1] and ace_lon<0.: #if the measurement is beyond the easternmost longitude
				grid_lon_inds=np.array([len(model_lons)-1,0]) #set the "high" longitude to -180 (i.e. 180)
			else: #otherwise, find the two closest longitudes
				grid_lon_inds=np.sort(londiffs_order[:2])

			#save the indices for later use (when doing the bilinear interpolation)
			all_grid_lat_inds.append(grid_lat_inds)
			all_grid_lon_inds.append(grid_lon_inds)
			
			#save the closest model latitude/longitude
			sampled_lat[i,j]=model_lats[latdiffs_order[0]]
			sampled_lon[i,j]=model_lons[londiffs_order[0]]

			#get all coordinate combinations (there will be four)
			all_grid_coords.extend(product(grid_lat_inds,grid_lon_inds))

		#convert coordinates to a numpy array and find unique ones
		all_grid_coords=np.unique(np.array(all_grid_coords),axis=0)
		
		#remove coordinates that are too far away
		#find which model locations are within ~the ACE drift of the ACE profile 
		#(i.e. which model locations are "close enough")
		#first determine the ACE drift in latitude and longitude
		drift_lat=np.nanmax(ACE_lats[i,:])-np.nanmin(ACE_lats[i,:])
		drift_lon=np.nanmax(ACE_lons[i,:])-np.nanmin(ACE_lons[i,:])
		#add the resolution of the latitude/longitude grid (~4 degrees) in the model, 
		#and multiply by 2
		is_grid_sz_lat=2*(drift_lat+lat_spacing)
		is_grid_sz_lon=2*(drift_lon+lon_spacing)
		
		#get the lat and lon indices of coordinates that are close enough to the ACE point
		close_lat_indices=np.where(np.abs(model_lats-ACE_lats[i,30])<=is_grid_sz_lat/2.)[0]
		close_lon_indices=np.where(np.abs(model_lons-ACE_lons[i,30])<=is_grid_sz_lon/2.)[0]

		#find the closest longitude in the model
		m_lon_ind=np.argmin(np.abs(model_lons-ACE_lons[i,int(tangent_height)]))
		model_lon=model_lons[m_lon_ind]

		if model_lon<is_grid_sz_lon/2.-180.: #if grid box straddles the IDL
			#e.g. if it's -177 and the isolated grid size is 5, need 2 degree from 
			#other side, i.e. anything greater than 178
			tacked_on=is_grid_sz_lon/2.-180.-model_lon #number of degrees to tack on
			tacked_on_inds=np.where(model_lons>180.-tacked_on)[0]
			close_lon_indices=np.append(close_lon_indices,tacked_on_inds)
			
		if model_lon>180.-is_grid_sz_lon/2.: #if grid box straddles the IDL on the other side 
			#e.g. if it's 177, need 2 degrees from other side, i.e. anything less than -178
			tacked_on=model_lon-(180.-is_grid_sz_lon/2.)
			tacked_on_inds=np.where(model_lon<-180.+tacked_on)[0]
			close_lon_indices=np.append(close_lon_indices,tacked_on_inds)

		#I want all lat indices to be found in grid_lat_indices
		#and all lon indices to be found in grid_lon_indices
		close_lat=np.isin(all_grid_coords[:,0],close_lat_indices)
		close_lon=np.isin(all_grid_coords[:,1],close_lon_indices)
		#keep coordinates where both the lats and lons are close enough
		all_grid_coords=all_grid_coords[np.logical_and(close_lat,close_lon)]
			  
		#compute vertical interpolation   
		ACE_p=ACE_ps[i,:] #I converted above, outside the loop
		tp_vmr=np.empty((len(model_lons),len(model_lats),len(ACE_alts)))
		tp_vmr[:]=float('nan')

		#loop through the coordinates that we will need and interpolate in time AND pressure
		for (lat,lon) in all_grid_coords:
			#take out the model VMR profiles and corresponding pressure profiles
			model_profiles=model_vmrs[model_t_inds,:,lat,lon].data
			if NOx:
				model_profiles=model_profiles+model_vmrs2[model_t_inds,:,lat,lon].data
			model_p_profiles=GetPressureProfiles(model_name,model_nc_used,model_p_var_used,model_t_inds,lat,lon)

			#these should be the same size
			model_profile=np.empty(model_profiles.shape[1])
			log_model_p=np.empty(model_p_profiles.shape[1])
			#interpolate in time first by looping through levels
			for level in range(len(model_profile)):
				t_series_vmr=model_profiles[:,level] #take out a time series to interpolate
				model_profile[level]=np.interp(ace_t,model_times[model_t_inds],t_series_vmr)
				#also interpolate pressures
				t_series_p=10*np.log(model_p_profiles[:,level])
				log_model_p[level]=np.interp(ace_t,model_times[model_t_inds],t_series_p)

			log_ACE_p=10*np.log(ACE_p)
			order=np.argsort(log_model_p) #interpolation function needs increasing order
			tp_vmr[lon,lat,:]=np.interp(log_ACE_p,log_model_p[order],model_profile[order],\
				  left=float('nan'),right=float('nan'))
					
		#find the right gridboxes for this profile
		tpll_vmr=np.empty(len(ACE_alts))
		tpll_vmr[:]=float('nan')
		tpll_vmr_nn=copy(tpll_vmr)
		
		for j in np.arange(0,len(ACE_alts)):
			
			ace_lat=ACE_lats[i,j]
			ace_lon=ACE_lons[i,j]
			
			grid_lat_inds=all_grid_lat_inds[j]
			grid_lon_inds=all_grid_lon_inds[j]
			
			#get the lat and lon values of the closest gridpoints
			grid_lons=model_lons[grid_lon_inds]
			grid_lats=model_lats[grid_lat_inds]
			
			#get the model VMR values at these gridpoints
			model_vals=tp_vmr[np.ix_(grid_lon_inds,grid_lat_inds)][:,:,j]
			
			#calculate the side lengths of each rectangle created by the four gridpoints
			#and the ACE point
			tl=haversine(grid_lons[0],grid_lats[1],ace_lon,grid_lats[1])
			tr=haversine(ace_lon,grid_lats[1],grid_lons[1],grid_lats[1])
			rt=haversine(grid_lons[1],grid_lats[1],grid_lons[1],ace_lat)
			rb=haversine(grid_lons[1],ace_lat,grid_lons[1],grid_lats[0])
			br=haversine(grid_lons[1],grid_lats[0],ace_lon,grid_lats[0])
			bl=haversine(ace_lon,grid_lats[0],grid_lons[0],grid_lats[0])
			lb=haversine(grid_lons[0],grid_lats[0],grid_lons[0],ace_lat)
			lt=haversine(grid_lons[0],ace_lat,grid_lons[0],grid_lats[1])
			
			#calculate the area of each rectangle and the total area
			a_tl=lt*tl
			a_tr=tr*rt
			a_br=rb*br
			a_bl=bl*lb
	
			tarea=a_tl+a_tr+a_br+a_bl
	
			#multiply VMR value by area of rectangle diagonally across from it, add results, divide by total area
			tpll_vmr[j]=(a_tl*model_vals[1,0]+a_tr*model_vals[0,0]+a_br*model_vals[0,1]+a_bl*model_vals[1,1])/tarea
			#save the VMR values from the closest model gridpoint (nn=nearest neighbour)
			tpll_vmr_nn[j]=tp_vmr[londiffs_order[0],latdiffs_order[0],j]
			
		sampled_vmr[i,:]=tpll_vmr
		sampled_vmr_nn[i,:]=tpll_vmr_nn
		
		time_for_profile.append(tm()-t00)
		
		if i%1000==0:
			print(str(i)+'/'+str(len(ACE_lats)))
	
	print(str(model_year)+' done: '+str(tm()-t0)+' seconds')
	
	return [sampled_vmr,sampled_vmr_nn,sampled_lat,sampled_lon,sampled_t,occultations]

#load in the ACE information
#get the GLC filename
GLC_fname=ACE_fname.replace(ACE_fname.split('_')[-1],'GLC.nc')
try:
	ACE_nc=Dataset(ACE_fname)
except FileNotFoundError:
	ACE_nc=Dataset(ACE_fname.replace('NOx','NO'))
	ACE_nc2=Dataset(ACE_fname.replace('NOx','NO2'))
GLC_nc=Dataset(GLC_fname)
ACE_alts=ACE_nc.variables['altitude'][:].data
ACE_yrs=ACE_nc.variables['year'][:].data
species=basename(ACE_fname).split('.')[0].split('_')[-1].replace('-','_')

if species=='NOx':
	NOx=True
else:
	NOx=False

#initialize variables for saving sampling output
tot_sampled_vmr=np.zeros(150)
tot_sampled_vmr_nn=np.zeros(150)
tot_sampled_lat=np.zeros(150)
tot_sampled_lon=np.zeros(150)
tot_sampled_t=[]
occultation_indices=[]

for yr in years_to_sample:
	print(yr)
	#determine the model name and the species wanted using the first filename
	file_no_path=basename(model_fnames[0]) #get the first file name without path
	file_no_ext=file_no_path.split('.')[0] #get file name without extension
	model_name=file_no_ext.split('_')[0]
	model_species=file_no_ext.split('_')[-1]
	model_yrs_in_file=file_no_ext.split('_')[-2].split('-')

	#need to find the file that contains this year
	#list all files in this directory
	all_files=listdir(model_folder)
	#find the one(s) that contains the model name, species, and year we want
	#there should only be multiple files if there is more than one model file per year
	if NOx:
		model_fnames_to_use=sorted([f for f in all_files if model_name in f and ('no' in f or 'NO' in f)\
		 and 'no2' not in f and 'NO2' not in f and str(yr) in f]) 
	else:
		print(model_species)
		model_fnames_to_use=sorted([model_folder+f for f in all_files if f.split('_')[0]==model_name\
		 and f.split('.')[0].split('_')[-1]==model_species in f and str(yr) in f])
		if 'no' in model_species or 'NO' in model_species: #if we want NO file
			#make sure we don't include NO2 files
			model_fnames_to_use=sorted([x for x in model_fnames if 'no2' not in x and 'NO2' not in x])
		print(model_fnames_to_use) #print to make sure we're using the right model files

	for fname in model_fnames_to_use:
		print(fname)
		[sampled_vmr,sampled_vmr_nn,sampled_lat,sampled_lon,sampled_t,occ_inds]=\
		SampleACEYear(yr,fname,model_start,ACE_nc,GLC_nc,ACE_alts,ACE_yrs,NOx)
		print(np.nanmax(sampled_vmr))
	
		tot_sampled_vmr=np.vstack((tot_sampled_vmr,sampled_vmr))
		tot_sampled_vmr_nn=np.vstack((tot_sampled_vmr_nn,sampled_vmr_nn))
		tot_sampled_lat=np.vstack((tot_sampled_lat,sampled_lat))
		tot_sampled_lon=np.vstack((tot_sampled_lon,sampled_lon))
		tot_sampled_t.extend(sampled_t)
		occultation_indices.extend(occ_inds)
	
tot_sampled_t=np.array(tot_sampled_t)
occultation_indices=np.array(occultation_indices)
	
data=dict()
if save_diff:
	data['sampled_vmr']=ACE_nc.variables['temperature'][occultation_indices].data-tot_sampled_vmr[1:,:]*vmr_units
else:
	data['sampled_vmr']=tot_sampled_vmr[1:,:]*vmr_units

print(np.nanmax(data['sampled_vmr']))
data['sampled_vmr_nn']=tot_sampled_vmr_nn[1:,:]*vmr_units
data['sampled_lat']=tot_sampled_lat[1:,:]
data['sampled_lon']=tot_sampled_lon[1:,:]

data['latitude']=GLC_nc.variables['GLC_latitude'][occultation_indices,30].data
data['longitude']=GLC_nc.variables['GLC_longitude'][occultation_indices,30].data

ACE_variables=['year','month','day','hour','orbit','sunset_sunrise','pressure','temperature','beta_angle',species,species+'_error']

for var in ACE_variables:
	data[var]=ACE_nc.variables[var][occultation_indices].data
		
data['altitude']=ACE_alts 

#calculate percent differences and save those too
data['diff']=data['sampled_vmr']-data[species]
data['percdiff']=(data['sampled_vmr']-data[species])/data[species]*100.

print('Saving')
spio.savemat(save_folder+'/'+model_name+'_ACEFTS_v4p1_locations_'+species+'.mat',data)
print('Saved')

#add the quality flags - this used to be in another script, which is why the output saved
#and then reloaded. 

print('Adding quality flags...')
model_fname=save_folder+'/'+model_name+'_ACEFTS_v4p1_locations_'+species+'.mat'

if NOx: #if the gas is NOx, we have to combine the NO and NO2 quality flags
	#quality flag filenames for NO and NO2
	ace_qf_fname1=ACE_fname.replace('v4p1','v4p1_flags')
	ace_qf_fname2=ACE_fname.replace('v4p1_NO','v4p1_flags_NO2')

	#make sure the profiles line up with the quality flag profiles
	[mod_inds,qf_inds]=MatchQFProfiles(model_fname,ace_qf_fname1)

	nc_qf_no=Dataset(ace_qf_fname1)
	nc_qf_no2=Dataset(ace_qf_fname2)

	qf_no=nc_qf_no.variables['quality_flag'][qf_inds].data
	qf_no2=nc_qf_no2.variables['quality_flag'][qf_inds].data

	#make a dummy array of "VMRs" to filter
	no_test=np.ones(qf_no.shape)
	no2_test=np.ones(qf_no2.shape)

	#filter each one with the quality flags (some will now be NaN)
	no_test=FilterACEMeas(no_test,qf_no,1)
	no2_test=FilterACEMeas(no2_test,qf_no2,1)

	#sum them - measurements with a valid NO and NO2 value will be 2,
	#otherwise they will be NaN
	test_sum=no_test+no2_test

	#make a new quality flag array (start with 0s, i.e. everything is usable)
	qf_to_add=np.zeros(no_test.shape)
	#replace locations with either NO or NO2 as NaN with a 9 (i.e. unusable)
	qf_to_add[np.isnan(test_sum)]=9

else:
	ace_qf_fname=ACE_fname.replace('v4p1','v4p1_flags')

	[mod_inds,qf_inds]=MatchQFProfiles(model_fname,ace_qf_fname)

	nc_qf=Dataset(ace_qf_fname)

	#just use the quality flags directly from the file
	qf_to_add=nc_qf['quality_flag'][qf_inds].data

#load the model mat file into a dictionary
mat=spio.loadmat(model_fname,squeeze_me=True)

#make sure only the profiles that match with the quality flag file are used
for k in list(mat.keys())[3:]:
	if k!='altitude':
		mat[k]=mat[k][mod_inds]

#replace QFs
qf_to_add[mat[g][mod_inds]==-999]=9 #sometimes the quality flags are wrong...
mat['quality_flag']=qf_to_add

#save the mat file
spio.savemat(model_fname,mat)


