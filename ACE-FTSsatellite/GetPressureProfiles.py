from netCDF4 import Dataset
import numpy as np
import csv
from os.path import basename
from time import time as tm

"""
Return pressure profiles in atm.
"""

def GetPressureProfiles(model_name,nc_main,p_var,time_ind,lat_ind,lon_ind):
    """
    -Input-

    model_name : string
        The name of the model being used. Necessary because models require 
        different methods for determining the pressure profiles.
    nc_main : NETCDF4 dataset
        NETCDF4 dataset (result of Dataset(netcdf filename)) that contains
        the model output
    p_var : NETCDF4 variable or list of NETCDF4 variables
        Depending on the model, this could be the NETCDF4 variable containing
        the pressures (or surface pressures), or a list of variables containing
        the components necessary to calculate the pressures.
    time_ind : list or integer
        Time indices of wanted pressure profiles.
    lat_ind : list or integer
        Latitude indices of wanted pressure profiles.
    lon_ind : list or integer
        Longitude indices of wanted pressure profiles.
    """
    #determine how many indices are being used along each dimension
    try:
        t_len=len(time_ind)
    except TypeError:
        t_len=1
        
    try:
        lat_len=len(lat_ind)
    except TypeError:
        lat_len=1
        
    try:
        lon_len=len(lon_ind)
    except TypeError:
        lon_len=1

    #calculate total number of profiles
    num_profiles=t_len*lat_len*lon_len  
    #================================DEHM======================================
    if model_name=='DEHM':
        ptop=10000
        lev=nc_main.variables['lev'][:].data #pressure dimension
        
        #get surface pressure at this time, latitude, and longitude
        ps=nc_main.variables['ps'][time_ind,lat_ind,lon_ind].data
        
        #tile everything so that the multiplication works        
        ps=np.transpose(np.tile(ps,(len(lev),1)))
        lev=np.tile(lev,(num_profiles,1))
        
        #calculate the pressure profiles
        p=np.squeeze(ptop+lev*(ps-ptop))
        return p/101325.
    
    #===========================MATCH, UKESM1, WRF-CHEM=========================
    elif model_name in ['MATCH','UKESM1','WRF-CHEM']:         
        #take the pressure profile
        p=p_var[time_ind,:,lat_ind,lon_ind].data
        return p/101325.
    
    #================================CMAM======================================
    elif model_name=='CMAM30-SD':       
        p=np.squeeze(np.tile(nc_main.variables['plev'][:].data,(num_profiles,1)))
        return p/101325

    elif model_name=='CMAM' or model_name=='CMAM39-SD':
        ap=nc_main.variables['ap'][:].data
        b=nc_main.variables['b'][:].data
        ps=nc_main.variables['ps'][time_ind,lat_ind,lon_ind].data

        #tile everything so that the multiplication works        
        ps=np.transpose(np.tile(ps,(len(ap),1)))
        ap=np.tile(ap,(num_profiles,1))
        b=np.tile(b,(num_profiles,1))
        
        #calculate the pressure profiles
        p=np.squeeze(ap+b*ps)
        return p/101325.

    #===============================MRI-ESM===================================
    elif model_name=='MRI-ESM': 
        #p variable is given as [a,p0,b,ps]
        a=p_var[0][:].data
        p0=p_var[1][:].data
        b=p_var[2][:].data
        ps=p_var[3][time_ind,lat_ind,lon_ind].data

        ps=np.transpose(np.tile(ps,(len(b),1)))
        b=np.tile(b,(num_profiles,1))
        a=np.tile(a,(num_profiles,1))

        #calculate the pressure profiles
        p=np.squeeze(p0*a+b*ps)
        return p/101325

    #=============================MATCH-SALSA==================================
    elif model_name=='MATCH-SALSA':
        ps=p_var[time_ind,lat_ind,lon_ind].data #get the surface pressures
        #import the coefficients
        coeffs=[]
        with open('Data/AMAP/MATCH-SALSA_coefficients.csv',encoding='utf-8-sig') as csv_file:
            csv_reader=csv.reader(csv_file,delimiter=',')
            line_count=0
            for row in csv_reader:
                coeffs.append([float(x) for x in row])

        coeffs=np.array(coeffs)

        ps=np.transpose(np.tile(ps,(len(coeffs),1)))
        a=np.tile(coeffs[:,0],(num_profiles,1))
        b=np.tile(coeffs[:,1],(num_profiles,1))
        
        p=np.squeeze(coeffs[::-1,0]+coeffs[::-1,1]*ps)
        return p/101325

    #==============================EMEP-MSCW===================================
    elif model_name=='EMEP-MSCW':
        ps=p_var[time_ind,lat_ind,lon_ind].data #get the surface pressures

        coeffs=[]
        with open('EMEP-MSCW_coefficients.csv',encoding='utf-8-sig') as csv_file:
            csv_reader=csv.reader(csv_file,delimiter=',')
            line_count=0
            for row in csv_reader:
                coeffs.append([float(x) for x in row])

        coeffs=np.array(coeffs)

        ps=np.transpose(np.tile(ps,(len(coeffs),1)))/100.
        a=np.tile(coeffs[:,0],(num_profiles,1))
        b=np.tile(coeffs[:,1],(num_profiles,1))
        
        p=np.squeeze(a+b*ps)
        return p/1013.25

    #===============================GEOS-CHEM==================================
    if model_name=='GEOS-CHEM':
        ap=p_var[0][:].data
        bp=p_var[1][:].data
        ps=p_var[2][time_ind,lat_ind,lon_ind].data

        ps=np.transpose(np.tile(ps,(len(bp),1)))
        bp=np.tile(bp,(num_profiles,1))
        ap=np.tile(ap,(num_profiles,1))

        p_edges=np.squeeze(ap+bp*ps)
        #these are the edges, so take the log middles
        log_p_edges=np.log10(p_edges)
        p=np.transpose(np.array([np.power(10,(log_p_edges[:,i]+log_p_edges[:,i+1])/2) for i in range(log_p_edges.shape[1]-1)]))
        return p/1013.25

    #================================GEM-MACH==================================
    if model_name=='GEM-MACH':
        #take the pressure profile
        p=p_var[time_ind,1:,lat_ind,lon_ind].data
        return p/101325.

    #=================================CESM=====================================
    if model_name=='CESM':
        ps=p_var[time_ind,lat_ind,lon_ind].data #get the surface pressures

        #load the a and b values
        nc_ab=Dataset('/net/corona/model_datasets/amap2020/hyam_hybm_P0_CESM.nc')
        a=nc_ab.variables['hyam'][:].data
        b=nc_ab.variables['hybm'][:].data

        ps=np.transpose(np.tile(ps,(len(a),1)))
        a=np.tile(a,(num_profiles,1))
        b=np.tile(b,(num_profiles,1))

        p=np.squeeze(a*100000+b*ps)

        return p/101325