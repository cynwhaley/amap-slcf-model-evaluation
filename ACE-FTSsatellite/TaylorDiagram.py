import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from datetime import datetime as dt
import scipy.io as spio
import skill_metrics as sm
import calendar
import matplotlib.lines as mlines
from copy import copy
from os.path import isdir
from os import mkdir

from DataFiltering import FilterACEMeas

plt.ioff()

#folder where the model output is saved
model_folder='Data/AMAP/AMAP sampled/'
#folder where the figures will be saved
save_folder='AMAP project/New figures/Taylor diagrams/All models/'

#all models=======================================================
models=['CESM','CMAM','DEHM','EMEP-MSC-W',\
'GEOS-Chem','MATCH','MATCH-SALSA','MRI-ESM2','UKESM1','WRF-CHEM']
model_colours=[(0.53,0.81,1),(0.75,0.75,0.75),(0,0,0.55),(0,1,0),\
(1,0.84,0),(0,0.39,0),(0,0,1),(1,0,0),(1,0,1),(0.8,0.75,0.44)]

#high-top models==================================================
# models=['CESM','CMAM','GEOS-Chem','MRI-ESM2','UKESM1']
# model_colours=[(0.53,0.81,1),(0.75,0.75,0.75),(1,0.84,0),(1,0,0),(1,0,1)]
#=================================================================

colour_dict=dict()
for i in range(len(models)):
    colour_dict[models[i]]=model_colours[i]
wanted_yrs=[[2008,2009],[2014,2015]]
# wanted_mos=[[12,1,2],[3,4,5],[6,7,8],[9,10,11]] #for latitudinal variation diagram
wanted_mos=[[1,2,3,4,5,6,7,8,9,10,11,12]] #for temporal variation diagram
min_lat=60 #use anything north of this latitude

gases=['O3','CH4','CO','NOx']
p=[150] #hPa, list of values

#time or latitude (if latitude, use other wanted_mos and set min_lat to -90)
variation='time'

#make list of times
wanted_times=[]
for i in wanted_yrs:
    for j in wanted_mos:
        wanted_times.append([i,j])

#these are the highest the "axes" can possibly be (they'll be smaller if
#the data allows)
theta_max=np.pi
r_max=5

all_taylor_stats=dict()

#first, interpolate everything onto the wanted pressure levels
for g in gases:
    print(g)
    gas_dict=dict()
    for i in range(len(wanted_times)):
        time_dict=dict()
        #now loop through models and get values for each of them
        for j in range(len(models)):
            model_dict=dict()
            #load in the model VMRs
            try:
                fname=model_folder+models[j]+'_ACEFTS_locations_'+g+'.mat'
                mat=spio.loadmat(fname, squeeze_me=True)  
            except FileNotFoundError: #if this model doesn't have this gas
                continue
    
            #take out information from the desired time and location
            yr_inds=np.where(np.isin(mat['year'],wanted_times[i][0]))[0]
            mo_inds=np.where(np.isin(mat['month'],wanted_times[i][1]))[0]
            time_inds=list(set(yr_inds).intersection(mo_inds))
            lat_inds=np.where(mat['latitude']>=min_lat)[0]
            right_inds=list(set(time_inds).intersection(lat_inds))
            
            if len(right_inds)==0:
                continue
            
            years=mat['year'][right_inds]
            months=mat['month'][right_inds]
            days=mat['day'][right_inds]
            hours=mat['hour'][right_inds]
            
            dts=[]
            for k in range(len(years)):
                dts.append(dt(years[k],months[k],days[k],hours[k]))
            dts=np.array(dts)
            
            ace_vmrs=mat[g][right_inds]
            m_vmrs=mat['sampled_vmr'][right_inds]
            p_log=10*np.log(mat['pressure'][right_inds]*1013.25) #convert to hPa
            
            #filter the data
            qfs=mat['quality_flag'][right_inds]    
            ace_vmrs=FilterACEMeas(ace_vmrs,qfs,1)
            m_vmrs=FilterACEMeas(m_vmrs,qfs,1)
            
            #initialize an array of NaNs to hold the interpolated ACE values
            ace_interp=np.empty((len(ace_vmrs),len(p)))
            ace_interp[:]=float('nan')           
            m_interp=copy(ace_interp)
        
            #interpolate all the profiles onto the wanted pressure levels
            for k in range(len(ace_vmrs)): #loop through profiles
        		#remove NaN values
                valid=np.where(np.logical_and(~np.isnan(ace_vmrs[k]),~np.isnan(p_log[k])))[0]
        
                if len(valid)>3:
                	#put pressures in order
                    order=np.argsort(p_log[k][valid])
                
                    p_log_sorted=p_log[k][order]
                    ace_sorted=ace_vmrs[k][order]
                    m_sorted=m_vmrs[k][order]
                
                    ace_interp[k,:]=np.interp([10.*np.log(p)],p_log_sorted,ace_sorted,\
                					left=float('nan'),right=float('nan'))
                    m_interp[k,:]=np.interp([10.*np.log(p)],p_log_sorted,m_sorted,\
                					left=float('nan'),right=float('nan'))
            
            #make a dataframe with all the model and ACE values        
            df=pd.DataFrame(dts,columns=['date'])
            for k in range(len(p)):
                df['ace_'+str(p[k])]=ace_interp[:,k]
                df['m_'+str(p[k])]=m_interp[:,k]
            df.index=df['date']
            
            if variation=='time':
                #take monthly values
                df_avg=df.resample('M').apply(lambda x: np.nanmean(x))
            elif variation=='latitude':
                #group the values by latitude (1 degree bins, hence np.floor)
                df_avg=df.groupby(np.floor(mat['latitude'][right_inds])).mean()
            
            #calculate Taylor statistics for each level
            for k in range(len(p)):
                #get the time series
                ace_series=df_avg['ace_'+str(p[k])].values
                m_series=df_avg['m_'+str(p[k])].values

                #remove NaN values
                valid=np.where(np.logical_and(~np.isnan(ace_series),~np.isnan(m_series)))[0]
                if ~np.any(valid): #if there are no values
                    continue
                ref=ace_series[valid]
                pred=m_series[valid]
            
                #calculate statistics for Taylor diagram
                taylor_stats=sm.taylor_statistics(pred,ref,'data')
                
                model_dict[p[k]]=sm.taylor_statistics(pred,ref,'data')

            if model_dict: #if not empty
                time_dict[models[j]]=model_dict
            
        if time_dict:
            gas_dict[i]=time_dict
        
    if gas_dict:
        all_taylor_stats[g]=gas_dict

#now that the Taylor statistics have been calculated, make the plots
num_rows=int(len(gases)**0.5) 
num_cols=int(np.ceil(len(gases)/float(num_rows)))

for i in range(len(wanted_times)):
    wanted_yrs=wanted_times[i][0]
    wanted_mos=wanted_times[i][1]
    for level in p:
        #make a new figure
        f=plt.figure(figsize=(11,8.5))

        r_vals=[]
        theta_vals=[]

        all_axes=[]
        labels=[]
        
        for j in range(len(gases)):
            ax=f.add_subplot(num_rows,num_cols,j+1,projection='polar')
        
            #loop through the models
            for m in models:
                #get the stats for this gas, year range, model, and level
                try:
                    taylor_stats=all_taylor_stats[gases[j]][i][m][level]
                except KeyError:
                    continue
                #plot the point for this model
                h=ax.scatter(np.arccos(taylor_stats['ccoef'][1]),\
                           taylor_stats['sdev'][1]/taylor_stats['sdev'][0],\
                           c=[colour_dict[m]],label=m,s=50)
                #save the label
                labels.append(m)
                #save the values so the limits can be set
                r_vals.append(taylor_stats['sdev'][1]/taylor_stats['sdev'][0])
                theta_vals.append(np.arccos(taylor_stats['ccoef'][1]))
                
            #plot the reference point
            ax.scatter([0],[1],c='k',s=50)
            
            all_axes.append(ax)
            
        f.tight_layout()
        f.subplots_adjust(bottom=0.15,right=0.9)
            
        #now, loop back through and format the axes
        new_r_max=np.min([np.max(np.array(r_vals)),r_max])
        new_theta_max=np.pi/2+np.pi/10
        #calculate skill grid for contours
        r,theta=np.meshgrid(np.linspace(0,new_r_max,100),np.linspace(0,new_theta_max,100))
        S=2*(1+np.cos(theta))/(r+1/r)**2
                
        for j in range(len(all_axes)):
            ax=all_axes[j]
            
            #plot the contours
            levels=np.arange(0.1,1,0.1)
            contours=ax.contour(theta,r,S,levels=levels,colors='grey')
            ax.clabel(contours,levels[::2], inline=1, fontsize=10,fmt='%1.1f')
            
            ax.set_rmax(new_r_max)
            ax.set_thetamin(0)
            ax.set_thetamax(new_theta_max*180/np.pi)  
            
            #remove gridlines
            ax.xaxis.grid(False)
            ax.yaxis.grid(False)
            #put tick labels
            #for radius ticks, do every integer
            ax.set_yticks(np.arange(0,int(new_r_max)+1))
            #for angle ticks, do every 0.2 for cos(theta)
            #also add in 0.9, 0.95, and 0.99 because 1 and 0.8 are far apart
            cos_vals=np.sort(np.append(np.round(np.arange(np.round(5*np.cos(new_theta_max))/5,1.1,0.2),1),[0.9,0.95,0.99]))
            ax.set_xticks(np.arccos(cos_vals))
            ax.set_xticklabels(cos_vals+0)
            
            #angular axis label
            box=ax.get_position()
            f.text(box.x0+box.width*0.8,box.y0+box.height*0.8,'Correlation',va='center',ha='center',rotation=-45)
            
            #gas label
            #get subscripted chem formula for plot labels
            g_latex=''.join(['$_{%d}$' % (int(x),) if x.isdigit() else x for x in list(gases[j])])
            f.text(box.x0+0.97*box.width,box.y0+0.97*box.height,g_latex)
            
        #axis labels
        f.text(0.5,0.08,'Standard deviation (normalized)',va='center',ha='center')

        #legend
        #get the legend labels
        labels=np.unique(np.array(labels))
        #make a handle for each label
        handles=[]
        for l in labels:
            handles.append(mlines.Line2D([0],[0],marker='o',color=colour_dict[l],
                                         markersize=4,linestyle=''))
            
        f.legend(handles,labels=labels,loc='center right',borderaxespad=0.5)
        
        #pressure, years, location info
        if variation=='time':
            f.text(0.97,0.97,str(wanted_yrs[0])+'-'+str(wanted_yrs[1])+'\n'\
                   +str(level)+'hPa\n 60-90$^\circ$N',ha='right',va='top')
            fig_name='taylor_monthly-t-series_60-90N_'+str(wanted_yrs[0])+'-'+str(wanted_yrs[1])+'_'+str(level)+'hPa'
        elif variation=='latitude':
            mo_letters=''.join([calendar.month_name[x][0] for x in wanted_mos])
            f.text(0.97,0.97,mo_letters+' '+str(wanted_yrs[0])+'-'+str(wanted_yrs[1])+'\n'\
                   +str(level)+'hPa',ha='right',va='top')
            fig_name='taylor_latitudinal_'+str(wanted_yrs[0])+'-'+str(wanted_yrs[1])+'_'+mo_letters+'_'+str(level)+'hPa'
        
        #save the figure
        if not isdir(save_folder):
            mkdir(save_folder)
        f.savefig(save_folder+fig_name+'.png')
        f.savefig(save_folder+fig_name+'.eps')