#add the directory containing all the sampled model results and ACE data
#to the path
import sys
sys.path.insert(1, 'Data/')

from os.path import basename, isdir, isfile
from os import mkdir
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from datetime import datetime as dt
import matplotlib.dates as mdates
import matplotlib.gridspec as gridspec
import matplotlib.colors as colors
import colorcet as cc
from copy import copy
import scipy.io as spio
from scipy import interpolate
from scipy.interpolate import interp1d
from scipy.stats import binned_statistic
import pandas as pd
from matplotlib.dates import DateFormatter

from DataFiltering import FilterACEMeas

#I took this from https://stackoverflow.com/questions/18926031/
#how-to-extract-a-subset-of-a-colormap-as-a-new-colormap-in-matplotlib
def truncate_colormap(cmap, minval=0.0, maxval=1.0, n=100):
    new_cmap = colors.LinearSegmentedColormap.from_list(
        'trunc({n},{a:.2f},{b:.2f})'.format(n=cmap.name, a=minval, b=maxval),
        cmap(np.linspace(minval, maxval, n)))
    return new_cmap

plt.ioff()

#---------------------------------INPUT----------------------------------------
#where to save the figures
save_folder='AMAP project/Test/'
#where the sampled model files are located
sampled_model_folder='Data/AMAP/AMAP sampled/'

#ACE units
ace_unit_mult=1e9 #multiply by this value to get the unit
ace_unit_name='ppbv' #name of the unit

#NOTE: when you change the models being used, check that the model counts are accurate for each
#gas below
#all models
model_names=['ACE-FTS','CESM','CMAM','DEHM','EMEP-MSC-W','GEOS-Chem','MATCH','MATCH-SALSA','MRI-ESM2','UKESM1','WRF-CHEM']
#models with higher tops
# model_names=['ACE-FTS','CESM','CMAM','GEOS-Chem','MRI-ESM2','UKESM1']

#number of levels on the colour bar (11, 13, or 15 all look fine - 15 was used for the report)
num_clr_levels=15

#range of latitudes to include
lat_range=[60,90]

#make a plot for each of these periods
year_periods=[[2008,2009],[2014,2015]]

p_edges=[0.1,1000] #hPa, pressure limits of the figure
num_p_bins=40 #number of pressure bins
p_grid=np.logspace(np.log10(p_edges[0]),np.log10(p_edges[1]),num_p_bins)

#ozone
strO3='O3'
nm_0809_O3=5 #number of (high top) models that have O3 values for 2008-2009 
nm_1415_O3=5 #number of (high top) models that have O3 values for 2014-2015

#maximum values on the colour bar (in ppbv) for each type of difference
#these must all be the same length
#a: ACE only plot (the colour bar will go from 0 to abs(max colour value))
a_abs_O3=[6500]#,6500] 
a_rel_O3=[6500]
#d: difference plots (colour bar goes from -abs(max colour value) to abs(max colour value))
d_abs_O3=[2000]#,10000]
d_rel_O3=[100]

#methane
strCH4='CH4'
nm_0809_CH4=3 #high top models
nm_1415_CH4=3 #high top models
a_abs_CH4=[1800]
a_rel_CH4=[1800]
d_abs_CH4=[500]
d_rel_CH4=[100]

#carbon monoxide
strCO='CO'
nm_0809_CO=8 #includes all models (since that's what's in the report)
nm_1415_CO=8
a_abs_CO=[200]
a_rel_CO=[200]
d_abs_CO=[200]
d_rel_CO=[200]

#NOx (NO + NO2)
strNOx='NOx'
nm_0809_NOx=8 #includes all models (since that's what's in the report)
nm_1415_NOx=8
a_abs_NOx=[10,10]
a_rel_NOx=[10,10]
d_abs_NOx=[10,1]
d_rel_NOx=[100,200]

#------------------------------------------------------------------------------
#put together the lists of gases to plot - can play with this to only do certain gases
#make sure all the same gases are used in the lines with a star above them
#e.g. I only ran O3 and CH4 together and then CO and NOx together for the report figures

#*
#absolute difference plots (gas * number of different colour bar settings)
gases_abs=[strO3]*len(a_abs_O3)+[strCH4]*len(a_abs_CH4)+[strCO]*len(a_abs_CO)+[strNOx]*len(a_abs_NOx)
#*
#relative difference plots
gases_rel=[strO3]*len(a_rel_O3)+[strCH4]*len(a_rel_CH4)+[strCO]*len(a_abs_CO)+[strNOx]*len(a_rel_NOx)

all_gases=[gases_abs,gases_rel]
#*
#number of models in each 2008-2009 absolute difference plot
nm_mdls_0809_abs=[nm_0809_O3]*len(a_abs_O3)+[nm_0809_CH4]*len(a_abs_CH4)\
+[nm_0809_CO]*len(a_abs_CO)+[nm_0809_NOx]*len(a_abs_NOx)
#*
#number of models in each 2008-2009 relative difference plot
nm_mdls_0809_rel=[nm_0809_O3]*len(a_rel_O3)+[nm_0809_CH4]*len(a_rel_CH4)\
+[nm_0809_CO]*len(a_rel_CO)+[nm_0809_NOx]*len(a_rel_NOx)

all_nm_mdls_0809=[nm_mdls_0809_abs,nm_mdls_0809_rel]
#*
#number of models in each 2014-2015 absolute difference plot
nm_mdls_1415_abs=[nm_1415_O3]*len(a_abs_O3)+[nm_1415_CH4]*len(a_abs_CH4)\
+[nm_1415_CO]*len(a_abs_CO)+[nm_1415_NOx]*len(a_abs_NOx)
#*
#number of models in each 2014-2015 relative difference plot
nm_mdls_1415_rel=[nm_1415_O3]*len(a_rel_O3)+[nm_1415_CH4]*len(a_rel_CH4)\
+[nm_1415_CO]*len(a_rel_CO)+[nm_1415_NOx]*len(a_rel_NOx)
 
all_nm_mdls_1415=[nm_mdls_1415_abs,nm_mdls_1415_rel]
#*
a_abs=a_abs_O3+a_abs_CH4+a_abs_CO+a_abs_NOx
#*
a_rel=a_rel_O3+a_rel_CH4+a_rel_CO+a_rel_NOx

all_ace_max=[a_abs,a_rel]
#*
d_abs=d_abs_O3+d_abs_CH4+d_abs_CO+d_abs_NOx
#*
d_rel=d_rel_O3+d_rel_CH4+d_rel_CO+d_rel_NOx

all_diff_max=[d_abs,d_rel]

#Shouldn't have to change anything below here.
#------------------------------------------------------------------------------
#make the discrete colour maps
a_cmap=truncate_colormap(cc.cm.fire,0.95,0.5)
#extract all colours from the map
a_cmaplist=[a_cmap(i) for i in range(a_cmap.N)]
# create the new map
a_cmap = mpl.colors.LinearSegmentedColormap.from_list('discrete_fire',a_cmaplist,a_cmap.N)

d_cmap=cc.cm.coolwarm
#extract all colours from the map
d_cmaplist=[d_cmap(i) for i in range(d_cmap.N)]
d_cmaplist[int(d_cmap.N/2)]=(1,1,1,1)
d_cmaplist[int(d_cmap.N/2-1)]=(1,1,1,1)
# create the new map
d_cmap=mpl.colors.LinearSegmentedColormap.from_list('discrete_coolwarm',d_cmaplist,d_cmap.N)

#the first one is for the ACE values, the second is for the differences
cmaps=[truncate_colormap(cc.cm.fire,0.95,0.5),cc.cm.coolwarm]

#------------------------------------------------------------------------------

#loop through variable to plot (0: absolute difference, 1: relative difference)
for var_to_plot in [0,1]:
     
    #gases
    gases=all_gases[var_to_plot]
    #number of models
    all_num_mdls=[all_nm_mdls_0809[var_to_plot],all_nm_mdls_1415[var_to_plot]]
    #ACE colourbar maximum
    ace_max=all_ace_max[var_to_plot]
    #difference colourbar maximum
    diff_max=all_diff_max[var_to_plot]
    
    #model difference units
    mdls_unit_mult=[ace_unit_mult,1][var_to_plot]
    mdls_unit_name=[ace_unit_name,'\% diff'][var_to_plot]
    
    #loop through 2008-2009 (0) and 2014-2015 (1)
    for year_period in range(len(year_periods)):
        
        num_mdls=all_num_mdls[year_period]
        
        #loop through gas, number of applicable models, ACE colour max, difference colour max
        #this is why they all have to be the same length
        for (g,nm,a,d) in zip(gases,num_mdls,ace_max,diff_max):
            print(g)
            #figure out the number of rows and columns
            num_rows=int((nm+1)**0.5) #add 1 to number of models to account for the ACE panel 
            num_cols=int(np.ceil((nm+1)/float(num_rows)))
            
            #sort out discrete colour bars
            a_bounds=np.concatenate(([-1],np.linspace(0,a,num_clr_levels),[a+1]))
            a_norm=mpl.colors.BoundaryNorm(a_bounds,a_cmap.N)
            
            d_bounds=np.linspace(-d,d,num_clr_levels)
            #get middle points
            d_bounds=np.array([(d_bounds[i]+d_bounds[i+1])/2 for i in range(len(d_bounds)-1)])
            d_bounds=np.concatenate(([d_bounds[0]-1],d_bounds,[d_bounds[-1]+1]))
            d_norm=mpl.colors.BoundaryNorm(d_bounds,d_cmap.N)

            #make a time series plot

            #create a new figure
            h,square_axes=plt.subplots(num_rows,num_cols,figsize=(11,8.5),sharex=True,sharey=True)
            first_ax=square_axes.flatten()[0]
            first_ax.invert_yaxis()
            first_ax.set_yscale('log')
            subplot_count=0
        
            for mn in model_names:
                print(mn)
                    
                if mn=='ACE-FTS':
                    #get the ACE values from any model file (I chose MRI-ESM2 because it has a file for every gas)
                    fname=sampled_model_folder+'MRI-ESM2_ACEFTS_locations_'+g+'.mat'
                    val_key=copy(g) #want to take the ACE values out of the file, the key for which is the gas formula
                    cmap=cmaps[0]
                    norm=a_norm
                    cbar_limits=[0,a]

                else:
                    fname=sampled_model_folder+mn+'_ACEFTS_locations_'+g+'.mat'
                    val_key=['diff','percdiff'][var_to_plot] #we either want the absolute difference or the relative difference
                    cmap=cmaps[1]
                    norm=d_norm
                    cbar_limits=[-d,d]
                    #check that the file exists
                    if not isfile(fname):
                        print(mn)
                        continue

                mat_mod=spio.loadmat(fname,squeeze_me=True)

                #load the time variables
                year=mat_mod['year']
                month=mat_mod['month']
                day=mat_mod['day']
                lat=mat_mod['latitude']

                #get the profiles in the right year and latitude range
                inds=np.where(np.logical_and(np.isin(year,year_periods[year_period]),\
                    np.logical_and(lat>lat_range[0],lat<lat_range[1])))[0]

                dts=np.array([dt(year[x],month[x],day[x]) for x in inds])

                mod_p=mat_mod['pressure'][inds]

                #filter the VMRs with quality flags
                mod_val=FilterACEMeas(mat_mod[val_key][inds],mat_mod['quality_flag'][inds],1)

                mod_lats=lat[inds] #need to keep track of the latitudes for binning

                #interpolate the values onto the pressure grid
                #interpolate linearly in log space
                mod_val_interp=np.empty((len(mod_val),num_p_bins))
                mod_val_interp[:]=float('nan')

                for i in range(len(mod_val)):
                    mod_p_i=np.log(mod_p[i]*1013.25) 
                    mod_val_i=mod_val[i]
                   
                    #put in ascending order and remove NaN entries
                    order=np.argsort(mod_p_i)
                    valid=np.where(~np.isnan(mod_val_i[order]))[0]

                    if len(valid)>3:
                        f=interp1d(mod_p_i[order][valid],mod_val_i[order][valid],bounds_error=False,fill_value=float('nan'))
                        mod_val_interp[i,:]=f(np.log(p_grid))               

                #make a pandas dataframe to make time sampling easier
                df=pd.DataFrame(data=mod_val_interp,index=dts)
                #resample into monthly bins
                df=df.resample('D').apply(lambda x: np.nanmean(x))
                #take a 7-day rolling average (take this out if doing a monthly time series)
                df=df.rolling(7,center=True,min_periods=1,axis=0).apply(lambda x: np.nanmean(x))

                #plot
                s_ax=square_axes.flatten()[subplot_count]
                s_ax.set_facecolor('grey')
                if var_to_plot==0:
                    s_ax.pcolormesh(df.index,p_grid,np.transpose(df.values)*ace_unit_mult,cmap=cmap,\
                        shading='nearest',vmin=cbar_limits[0],vmax=cbar_limits[-1],norm=norm)
                else:
                    s_ax.pcolormesh(df.index,p_grid,np.transpose(df.values),cmap=cmap,\
                        shading='nearest',vmin=cbar_limits[0],vmax=cbar_limits[-1],norm=norm)

                subplot_count+=1
                s_ax.set_title(mn)
                s_ax.set_ylim(np.sort(p_edges)[::-1])
            
            #add x ticks to bottom plot of each column
            for i in range(len(square_axes.flatten())):
                if i>=subplot_count: #if the axis is unused
                    print(i)
                    #add a subplot and axis ticks 
                    s_ax=square_axes.flatten()[i-num_cols]
                    s_ax.xaxis.set_tick_params(which='both', labelbottom=True)
            
            # #modify the axes/labels as needed
            for s_ax in square_axes[num_rows-1,:]:
                date_form=DateFormatter("%Y-%m")
                s_ax.xaxis.set_major_formatter(date_form)
                s_ax.set_xticks([dt(year_periods[year_period][0],1,1),dt(year_periods[year_period][0],7,1),\
                    dt(year_periods[year_period][1],1,1),dt(year_periods[year_period][1],7,1)])
            
            h.text(0.02,0.5,'Pressure [hPa]',va='center',rotation='vertical')
                    
            h.tight_layout()
            
            #make room on bottom and left for colour bars
            h.subplots_adjust(left=0.1,bottom=0.15,wspace=0)

            #get positions of bottom left and bottom right subplots
            bl_pos=square_axes[-1,0].get_position().get_points().flatten()
            br_pos=square_axes[-1,-1].get_position().get_points().flatten()

            cbar_ax1 = h.add_axes([bl_pos[0],0.08,br_pos[2]-bl_pos[0],0.01]) #make colorbar axes
            cbar_ax2=h.add_axes([bl_pos[0],0.03,br_pos[2]-bl_pos[0],0.01])
            
            if len(np.unique(a_bounds.astype('int')))==len(a_bounds): 
                #if using integers will differentiate between the colorbar edges
                a_fmt='%d' #use integers
            else:
                a_fmt='%.1f' #use decimals
            cbar_im1=mpl.colorbar.ColorbarBase(cbar_ax1,cmap=a_cmap,norm=a_norm,spacing='proportional',extend='both',
                                               ticks=a_bounds,boundaries=a_bounds,orientation='horizontal',format=a_fmt)

            h.text(0,0.08,'VMR ['+mdls_unit_name+']',va='center' )
        
            cbar_2_label=['Absolute\ndifference','Relative\ndifference'][var_to_plot]

            # if len(str(d_bounds[-1]))>2:
            print(d_bounds.astype('int'))
            if len(np.unique(d_bounds.astype('int')))==len(d_bounds):
                d_fmt='%d'
            else:
                d_fmt='%.1f'
            cbar_im2=mpl.colorbar.ColorbarBase(cbar_ax2,cmap=d_cmap,norm=d_norm,spacing='proportional',extend='both',
                                               ticks=d_bounds,boundaries=d_bounds,orientation='horizontal',format=d_fmt)

            h.text(0,0.03,cbar_2_label+'\n['+mdls_unit_name+']',va='center')
            #remove unused axes
            for ax in square_axes.flatten()[subplot_count:]:
                ax.remove()
            
            fig_name=g+'_'+str(lat_range[0])+'-'+str(lat_range[1])+'_'+str(year_periods[year_period][0])+'-'\
            +str(year_periods[year_period][1])+'_'+str(a)+'_'+str(d)

            if var_to_plot==0:
                if not isdir(save_folder+'Absolute differences'):
                    mkdir(save_folder+'Absolute differences')
                h.savefig(save_folder+'Absolute differences/'+fig_name+'.png')
                h.savefig(save_folder+'Absolute differences/'+fig_name+'.eps')
            else:
                if not isdir(save_folder+'Relative differences'):
                    mkdir(save_folder+'Relative differences')
                h.savefig(save_folder+'Relative differences/'+fig_name+'.png')
                h.savefig(save_folder+'Relative differences/'+fig_name+'.eps')
