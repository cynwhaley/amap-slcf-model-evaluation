"""
o3_surface_compare_multi_cyn.py
March 2020
Author: Tahya Weiss-Gibbons
tahya.weissgibbons@gmail.com

April-June 2020
Update by Cyndi to fix multipanel plotting, and change CO 
to O3, and add more models.

Reads in surface site data for O3 from across the globe
Compares then to multiple models sampled at the site locations


Fully fixed issues with gem-mach.
MDL_lon fix applied

"""

import re
import sys
import csv
import glob
import numpy as np
import pandas as pd
#import statistics
import matplotlib.pyplot as plt
from bisect import bisect_left
from netCDF4 import Dataset, num2date
from mpl_toolkits.basemap import Basemap
from datetime import date, datetime


def cesm_model(filtered_on_target_years = ['2014', '2015'],target_years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/CESM/'):
    mdl_files = {'o3': 'CESM_type0_o3_'+target_years[0]+'_'+target_years[1]+'_3h.nc'}

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = (mf.variables['lon'][:]).tolist()
    mdl_lat = (mf.variables['lat'][:]).tolist()
    mdl_vmro3 = mf.variables['o3'][:,31,:,:] #levels are reversed in cesm

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)

    mdl_mnth_o3 = np.zeros((12, nm_mdl_lat, nm_mdl_lon))
    mdl_annual_o3 = np.zeros((nm_mdl_lat,nm_mdl_lon))
    mnth_count = np.zeros((12, nm_mdl_lat, nm_mdl_lon))
    annual_count = np.zeros((nm_mdl_lat, nm_mdl_lon))

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in filtered_on_target_years:
            continue
        i = list(times).index(t)
        m = t.month-1
        mdl_mnth_o3[m,:,:] = np.add(mdl_vmro3[i,:,:], mdl_mnth_o3[m,:,:])
        mnth_count[m,:,:] += 1

        mdl_annual_o3[:,:] = np.add(mdl_vmro3[i,:,:], mdl_annual_o3[:,:])
        annual_count[:,:] += 1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:] ,mnth_count[:,:,:])
    mdl_annual_o3[:,:] = np.divide(mdl_annual_o3[:,:], annual_count[:,:])
    return mdl_mnth_o3, mdl_annual_o3, mdl_lat, mdl_lon, times

def cmam_model(filtered_on_target_years = ['2014', '2015'],target_years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/CMAM/'):

    mdl_files = {'o3': 'vmro3_month_CMAM_AMAP2020-SD_r1i1p1_199001-201812.nc'}

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = (mf.variables['lon'][:]).tolist()
    mdl_lat = (mf.variables['lat'][:]).tolist()
    mdl_vmro3 = mf.variables['vmro3'][:,0,:,:]*(10**9) #have to conver the units; 10**9

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)

    mdl_mnth_o3 = np.zeros((12, nm_mdl_lat, nm_mdl_lon))
    mdl_annual_o3 = np.zeros((nm_mdl_lat,nm_mdl_lon))
    mnth_count = np.zeros((12, nm_mdl_lat, nm_mdl_lon))
    annual_count = np.zeros((nm_mdl_lat, nm_mdl_lon))

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in filtered_on_target_years:
            continue
        i = list(times).index(t)
        m = t.month-1
        mdl_mnth_o3[m,:,:] = np.add(mdl_vmro3[i,:,:], mdl_mnth_o3[m,:,:])
        mnth_count[m,:,:] += 1

        mdl_annual_o3[:,:] = np.add(mdl_vmro3[i,:,:], mdl_annual_o3[:,:])
        annual_count[:,:] += 1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:] ,mnth_count[:,:,:])
    mdl_annual_o3[:,:] = np.divide(mdl_annual_o3[:,:], annual_count[:,:])
    return mdl_mnth_o3, mdl_annual_o3, mdl_lat, mdl_lon, times

def dehm_model(filtered_on_target_years = ['2014', '2015'],target_years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/DEHM/'):

    mdl_files = {'o3': 'DEHM_type0_o3_1990_2018.nc'}

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = (mf.variables['lon'][:]).tolist()
    mdl_lat = (mf.variables['lat'][:]).tolist()
    mdl_vmro3 = mf.variables['o3'][:,0,:,:]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)

    mdl_mnth_o3 = np.zeros((12, nm_mdl_lat, nm_mdl_lon))
    mdl_annual_o3 = np.zeros((nm_mdl_lat,nm_mdl_lon))
    mnth_count = np.zeros((12, nm_mdl_lat, nm_mdl_lon))
    annual_count = np.zeros((nm_mdl_lat, nm_mdl_lon))

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in filtered_on_target_years:
            continue
        i = list(times).index(t)
        m = t.month-1
        mdl_mnth_o3[m,:,:] = np.add(mdl_vmro3[i,:,:], mdl_mnth_o3[m,:,:])
        mnth_count[m,:,:] += 1

        mdl_annual_o3[:,:] = np.add(mdl_vmro3[i,:,:], mdl_annual_o3[:,:])
        annual_count[:,:] += 1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:] ,mnth_count[:,:,:])
    mdl_annual_o3[:,:] = np.divide(mdl_annual_o3[:,:], annual_count[:,:])
    return mdl_mnth_o3, mdl_annual_o3, mdl_lat, mdl_lon, times

def emep_model(filtered_on_target_years = ['2014', '2015'],target_years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/EMEP-MSCW/'):

    mdl_files = {'o3': 'EMEP-MSCW_tp0_v04_so3_month_1990_2015.nc'}

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = np.array((mf.variables['lon'][:]).tolist())
    mdl_lat = (mf.variables['lat'][:]).tolist()
    mdl_vmro3 = mf.variables['so3'][:,:,:]
    mdl_lon = [ln+360 if ln<0 else ln for ln in mdl_lon]
    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')

    mf.close()

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)

    mdl_mnth_o3 = np.zeros((12, nm_mdl_lat, nm_mdl_lon))
    mdl_annual_o3 = np.zeros((nm_mdl_lat,nm_mdl_lon))
    mnth_count = np.zeros((12, nm_mdl_lat, nm_mdl_lon))
    annual_count = np.zeros((nm_mdl_lat, nm_mdl_lon))

    times = num2date(mdl_time, units=time_units, calendar='gregorian')

    #now average over just the interested years
    for t in times:
        if str(t.year) not in filtered_on_target_years:
            continue
        i = list(times).index(t)
        m = t.month-1
        mdl_mnth_o3[m,:,:] = np.add(mdl_vmro3[i,:,:], mdl_mnth_o3[m,:,:])
        mnth_count[m,:,:] += 1
	
        mdl_annual_o3[:,:] = np.add(mdl_vmro3[i,:,:], mdl_annual_o3[:,:])
        annual_count[:,:] += 1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:], mnth_count[:,:,:])
    mdl_annual_o3[:,:] = np.divide(mdl_annual_o3[:,:], annual_count[:,:])
    return mdl_mnth_o3, mdl_annual_o3, mdl_lat, mdl_lon, times

def geoschem_model(filtered_on_target_years = ['2014', '2015'],target_years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/GEOS-CHEM/'):

    mdl_files = {'o3': 'GEOS-CHEM_type0_o3_'+target_years[0]+'_'+target_years[1]+'.nc'}

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = np.array((mf.variables['lon'][:]).tolist())
    mdl_lat = (mf.variables['lat'][:]).tolist()
    mdl_vmro3 = mf.variables['o3'][:,0,:,:]  
    mdl_lon = [ln+360 if ln<0 else ln for ln in mdl_lon]
    mdl_time = mf.variables['time'][:]
    #time_units = mf.variables['time'].getncattr('units')
    time_units = mf.variables['time'].getncattr('long_name')
    time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)

    mdl_mnth_o3 = np.zeros((12, nm_mdl_lat, nm_mdl_lon))
    mdl_annual_o3 = np.zeros((nm_mdl_lat,nm_mdl_lon))
    mnth_count = np.zeros((12, nm_mdl_lat, nm_mdl_lon))
    annual_count = np.zeros((nm_mdl_lat, nm_mdl_lon))

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in filtered_on_target_years:
            continue
        i = list(times).index(t)
        m = t.month-1
        mdl_mnth_o3[m,:,:] = np.add(mdl_vmro3[i,:,:], mdl_mnth_o3[m,:,:])
        mnth_count[m,:,:] += 1

        mdl_annual_o3[:,:] = np.add(mdl_vmro3[i,:,:], mdl_annual_o3[:,:])
        annual_count[:,:] += 1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:] ,mnth_count[:,:,:])
    mdl_annual_o3[:,:] = np.divide(mdl_annual_o3[:,:], annual_count[:,:])
    return mdl_mnth_o3, mdl_annual_o3, mdl_lat, mdl_lon, times

def gem_mach_model(filtered_on_target_years = ['2014', '2015'],target_years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/GEM-MACH/'):
    print("its here")
    mdl_files = {'O3': ['GEM-MACH_tp0_o3_monthly_201501.nc', 
    'GEM-MACH_tp0_o3_monthly_201502.nc', 
    'GEM-MACH_tp0_o3_monthly_201503.nc', 
    'GEM-MACH_tp0_o3_monthly_201504.nc', 
    'GEM-MACH_tp0_o3_monthly_201505.nc', 
    'GEM-MACH_tp0_o3_monthly_201506.nc', 
    'GEM-MACH_tp0_o3_monthly_201507.nc', 
    'GEM-MACH_tp0_o3_monthly_201508.nc', 
    'GEM-MACH_tp0_o3_monthly_201509.nc', 
    'GEM-MACH_tp0_o3_monthly_201510.nc', 
    'GEM-MACH_tp0_o3_monthly_201511.nc', 
    'GEM-MACH_tp0_o3_monthly_201512.nc',]}

    #first lets read in the info from the o3 file
    mf1 = Dataset(root_mdl+mdl_files['O3'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['O3'][1], 'r')
    mdl_lon = (mf1.variables['lon'][:]).tolist()
    mdl_lat = (mf1.variables['lat'][:]).tolist()
    mdl_vmro3 = np.concatenate((mf1.variables['O3'][:,34,:,:], mf2.variables['O3'][:,34,:,:]))
    
    mdl_time = [mf1.variables['time'].getncattr('units')[12:21],mf2.variables['time'].getncattr('units')[12:21]]
    
    mf1.close()
    mf2.close()
    #print(mdl_time)
    for month in range(2,12):
        mf = Dataset(root_mdl+mdl_files['O3'][month], 'r')
        #print(mf.variables['time'].getncattr('units')[12:21])
        mdl_vmro3 = np.concatenate((mdl_vmro3, mf.variables['O3'][:,34,:,:]))
        mdl_time.append(mf.variables['time'].getncattr('units')[12:21])
        mf.close()
        #print(mdl_time)



    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)

    mdl_mnth_o3 = np.ma.zeros((12, nm_mdl_lat, nm_mdl_lon))
    mdl_annual_o3 = np.ma.zeros((nm_mdl_lat,nm_mdl_lon))
    #mdl_annual_o3 = np.ma.masked_where(mdl_annual_o3==0,mdl_annual_o3)
    mnth_count = np.ma.zeros((12, nm_mdl_lat, nm_mdl_lon))
    annual_count = np.ma.zeros((nm_mdl_lat, nm_mdl_lon))

    #now average over just the interested years
    #we want annual and monthly
    #a masking array to cover each position that has -999 as a value
    masked_vals = np.where(mdl_vmro3==-999,1,0)
    #print(masked_vals[0])
    #mask all lats that have -999 in the location
    masked_lat = [ x[0] for x in masked_vals[0]]
    #print(masked_lat)
    #print(len(masked_lat))
    #print((mdl_lat))
    mdl_lat = np.ma.masked_array(mdl_lat, mask=masked_lat)
    #print(mdl_lat)
    mdl_vmro3 = np.ma.masked_array(mdl_vmro3, mask=masked_vals)
    #mdl_vmro3 = np.ma.masked_array(mdl_vmro3, mask=masked_vals) 
    #print((mdl_vmro3.shape))
    for t in mdl_time:
        date = str(t).split("-")
        if date[0] not in filtered_on_target_years:
            continue
        i = list(mdl_time).index(t)
        m = int(date[1])-1
        mdl_mnth_o3[m,:,:] = np.add(mdl_vmro3[i,:,:],mdl_mnth_o3[m,:,:])
        mnth_count[m,:,:] += 1

        mdl_annual_o3[:,:] = np.add(mdl_vmro3[i,:,:], mdl_annual_o3[:,:])
        annual_count[:,:] += 1
    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:], mnth_count[:,:,:])
    mdl_annual_o3[:,:] = np.divide(mdl_annual_o3[:,:], annual_count[:,:])
    #sys.exit()
    return mdl_mnth_o3, mdl_annual_o3, mdl_lat, mdl_lon, mdl_time

def giss_model(filtered_on_target_years = ['2014', '2015'],target_years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/GISS-modelE-OMA/GISS_reformatted/'):

    mdl_files = {'o3': 'GISS-modelE-OMA_type0_o3_NCEP_reformatted.nc'}

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = np.array((mf.variables['lon'][:]).tolist())
    mdl_lat = (mf.variables['lat'][:]).tolist()
    mdl_vmro3 = mf.variables['o3'][:,0,:,:]
    mdl_lon = [ln+360 if ln<0 else ln for ln in mdl_lon]
    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)

    mdl_mnth_o3 = np.zeros((12, nm_mdl_lat, nm_mdl_lon))
    mdl_annual_o3 = np.zeros((nm_mdl_lat,nm_mdl_lon))
    mnth_count = np.zeros((12, nm_mdl_lat, nm_mdl_lon))
    annual_count = np.zeros((nm_mdl_lat, nm_mdl_lon))

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in filtered_on_target_years:
            continue
        i = list(times).index(t)
        m = t.month-1
        mdl_mnth_o3[m,:,:] = np.add(mdl_vmro3[i,:,:], mdl_mnth_o3[m,:,:])
        mnth_count[m,:,:] += 1

        mdl_annual_o3[:,:] = np.add(mdl_vmro3[i,:,:], mdl_annual_o3[:,:])
        annual_count[:,:] += 1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:] ,mnth_count[:,:,:])
    mdl_annual_o3[:,:] = np.divide(mdl_annual_o3[:,:], annual_count[:,:])
    return mdl_mnth_o3, mdl_annual_o3, mdl_lat, mdl_lon, times

def matchsalsa_model(filtered_on_target_years = ['2014', '2015'],target_years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/MATCH-SALSA/'):

    mdl_files = {'o3': ['MATCH-SALSA_type0_o3s_'+target_years[0]+'.nc', 'MATCH-SALSA_type0_o3s_'+target_years[1]+'.nc']}

    #first lets read in the info from the co file
    mf1 = Dataset(root_mdl+mdl_files['o3'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['o3'][1], 'r')
    mdl_lon = (mf1.variables['lon'][:]).tolist()
    mdl_lat = (mf1.variables['lat'][:]).tolist()
    mdl_vmro3 = np.concatenate((mf1.variables['o3s'][:], mf2.variables['o3s'][:]))

    mdl_time = np.concatenate((mf1.variables['time'][:], mf2.variables['time'][:]))

    mf1.close()
    mf2.close()

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)

    mdl_mnth_o3 = np.zeros((12, nm_mdl_lat, nm_mdl_lon))
    mdl_annual_o3 = np.zeros((nm_mdl_lat,nm_mdl_lon))
    mnth_count = np.zeros((12, nm_mdl_lat, nm_mdl_lon))
    annual_count = np.zeros((nm_mdl_lat, nm_mdl_lon))

    #now average over just the interested years
    #we want annual and Monthly
    for t in mdl_time:
        if str(t)[:4] not in filtered_on_target_years:
            continue
        i = list(mdl_time).index(t)
        m = int(str(t)[4:6])-1
        mdl_mnth_o3[m,:,:] = np.add(mdl_vmro3[i,:,:], mdl_mnth_o3[m,:,:])
        mnth_count[m,:,:] += 1

        mdl_annual_o3[:,:] = np.add(mdl_vmro3[i,:,:], mdl_annual_o3[:,:])
        annual_count[:,:] += 1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:], mnth_count[:,:,:])
    mdl_annual_o3[:,:] = np.divide(mdl_annual_o3[:,:], annual_count[:,:])
    return mdl_mnth_o3, mdl_annual_o3, mdl_lat, mdl_lon, mdl_time

def match_model(filtered_on_target_years = ['2014', '2015'],target_years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/MATCH/'):

    mdl_files = {'o3': ['MATCH_Type0_o3s_'+target_years[0]+'.nc', 'MATCH_Type0_o3s_'+target_years[1]+'.nc']}

    #first lets read in the info from the co file
    mf1 = Dataset(root_mdl+mdl_files['o3'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['o3'][1], 'r')
    mdl_lon = (mf1.variables['lon'][:]).tolist()
    mdl_lat = (mf1.variables['lat'][:]).tolist()
    mdl_vmro3 = np.concatenate((mf1.variables['o3s'][:], mf2.variables['o3s'][:]))

    mdl_time = np.concatenate((mf1.variables['time'][:], mf2.variables['time'][:]))
    time_units = mf1.variables['time'].getncattr('units')
    time_cal = mf1.variables['time'].getncattr('calendar')

    mf1.close()
    mf2.close()

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)

    mdl_mnth_o3 = np.zeros((12, nm_mdl_lat, nm_mdl_lon))
    mdl_annual_o3 = np.zeros((nm_mdl_lat,nm_mdl_lon))
    mnth_count = np.zeros((12, nm_mdl_lat, nm_mdl_lon))
    annual_count = np.zeros((nm_mdl_lat, nm_mdl_lon))

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    #we want annual and Monthly
    for t in times:
        if str(t.year) not in filtered_on_target_years:
            continue
        i = list(times).index(t)
        m = t.month-1
        mdl_mnth_o3[m,:,:] = np.add(mdl_vmro3[i,:,:], mdl_mnth_o3[m,:,:])
        mnth_count[m,:,:] += 1

        mdl_annual_o3[:,:] = np.add(mdl_vmro3[i,:,:], mdl_annual_o3[:,:])
        annual_count[:,:] += 1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:], mnth_count[:,:,:])
    mdl_annual_o3[:,:] = np.divide(mdl_annual_o3[:,:], annual_count[:,:])
    return mdl_mnth_o3, mdl_annual_o3, mdl_lat, mdl_lon, times

def mri_model(filtered_on_target_years = ['2014', '2015'],target_years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/MRI-ESM/'):

    mdl_files = {'o3': 'MRI-ESM_type0_o3s_'+target_years[0]+'-'+target_years[1]+'.nc'}

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = (mf.variables['lon'][:]).tolist()
    mdl_lat = (mf.variables['lat'][:]).tolist()
    mdl_vmro3 = mf.variables['o3s'][:,:,:]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)

    mdl_mnth_o3 = np.zeros((12, nm_mdl_lat, nm_mdl_lon))
    mdl_annual_o3 = np.zeros((nm_mdl_lat,nm_mdl_lon))
    mnth_count = np.zeros((12, nm_mdl_lat, nm_mdl_lon))
    annual_count = np.zeros((nm_mdl_lat, nm_mdl_lon))

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in filtered_on_target_years:
            continue
        i = list(times).index(t)
        m = t.month-1
        mdl_mnth_o3[m,:,:] = np.add(mdl_vmro3[i,:,:], mdl_mnth_o3[m,:,:])
        mnth_count[m,:,:] += 1

        mdl_annual_o3[:,:] = np.add(mdl_vmro3[i,:,:], mdl_annual_o3[:,:])
        annual_count[:,:] += 1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:], mnth_count[:,:,:])
    mdl_annual_o3[:,:] = np.divide(mdl_annual_o3[:,:], annual_count[:,:])
    return mdl_mnth_o3, mdl_annual_o3, mdl_lat, mdl_lon, times

def oslo_model(filtered_on_target_years = ['2014', '2015'],target_years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/OsloCTM/'):

    mdl_files = {'o3': ['OsloCTM_type0_o3_monthly_'+target_years[0]+'.nc', 'OsloCTM_type0_o3_monthly_'+target_years[1]+'.nc']}

    #first lets read in the info from the o3 file
    mf1 = Dataset(root_mdl+mdl_files['o3'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['o3'][1], 'r')
    mdl_lon = (mf1.variables['lon'][:]).tolist()
    mdl_lat = (mf1.variables['lat'][:]).tolist()
    mdl_vmro3 = np.concatenate((mf1.variables['o3'][:,0,:,:], mf2.variables['o3'][:,0,:,:]))

    mdl_time = np.concatenate((mf1.variables['time'][:], mf2.variables['time'][:]))
   # weren't specified in o3 files. Taken from the pressure file, assuming the same
   # time_units = mf1.variables['time'].getncattr('units')
   # time_cal = mf1.variables['time'].getncattr('calendar')
    time_units = "days since 2001-01-01 00:00:00"
    time_cal = "Julian"
    
    mf1.close()
    mf2.close()

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)

    mdl_mnth_o3 = np.zeros((12, nm_mdl_lat, nm_mdl_lon))
    mdl_annual_o3 = np.zeros((nm_mdl_lat,nm_mdl_lon))
    mnth_count = np.zeros((12, nm_mdl_lat, nm_mdl_lon))
    annual_count = np.zeros((nm_mdl_lat, nm_mdl_lon))

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in filtered_on_target_years:
            continue
        i = list(times).index(t)
        m = t.month-1
        mdl_mnth_o3[m,:,:] = np.add(mdl_vmro3[i,:,:], mdl_mnth_o3[m,:,:])
        mnth_count[m,:,:] += 1

        mdl_annual_o3[:,:] = np.add(mdl_vmro3[i,:,:], mdl_annual_o3[:,:])
        annual_count[:,:] += 1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:], mnth_count[:,:,:])
    mdl_annual_o3[:,:] = np.divide(mdl_annual_o3[:,:], annual_count[:,:])
    return mdl_mnth_o3, mdl_annual_o3,  mdl_lat, mdl_lon, times

def ukesm_model(filtered_on_target_years = ['2014', '2015'],target_years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/UKESM1/'):

    mdl_files = {'o3': 'UKESM1_type0_monthly_ozone_volume_mixing_ratio_'+target_years[0]+'_'+target_years[1]+'.nc'}

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = (mf.variables['longitude'][:]).tolist()
    mdl_lat = (mf.variables['latitude'][:]).tolist()
    mdl_vmro3 = mf.variables['ozone_volume_mixing_ratio'][:,0,:,:]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)

    mdl_mnth_o3 = np.zeros((12, nm_mdl_lat, nm_mdl_lon))
    mdl_annual_o3 = np.zeros((nm_mdl_lat,nm_mdl_lon))
    mnth_count = np.zeros((12, nm_mdl_lat, nm_mdl_lon))
    annual_count = np.zeros((nm_mdl_lat, nm_mdl_lon))

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in filtered_on_target_years:
            continue
        i = list(times).index(t)
        m = t.month-1
        mdl_mnth_o3[m,:,:] = np.add(mdl_vmro3[i,:,:], mdl_mnth_o3[m,:,:])
        mnth_count[m,:,:] += 1

        mdl_annual_o3[:,:] = np.add(mdl_vmro3[i,:,:], mdl_annual_o3[:,:])
        annual_count[:,:] += 1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:], mnth_count[:,:,:])
    mdl_annual_o3[:,:] = np.divide(mdl_annual_o3[:,:], annual_count[:,:])
    return mdl_mnth_o3, mdl_annual_o3, mdl_lat, mdl_lon, times

def wrfchem_model(filtered_on_target_years = ['2014', '2015'],target_years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/WRF-CHEM/'):

    mdl_files = {'o3': 'WRF-CHEM_type0_o3.nc'}

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = (mf.variables['XLONG'][:]).tolist()
    mdl_lat = (mf.variables['XLAT'][:]).tolist()
    mdl_vmro3 = mf.variables['o3'][:,0,:,:]
    
    mdl_time = mf.variables['Times'][:]
#    time_units = mf.variables['time'].getncattr('units')
#    time_cal = mf.variables['time'].getncattr('calendar')

    nm_mdl_lon = mf.dimensions['west_east'].size
    nm_mdl_lat = mf.dimensions['south_north'].size

    mf.close()

    mdl_mnth_o3 = np.zeros((12, nm_mdl_lat, nm_mdl_lon))
    mdl_annual_o3 = np.zeros((nm_mdl_lat,nm_mdl_lon))
    mnth_count = np.zeros((12, nm_mdl_lat, nm_mdl_lon))
    annual_count = np.zeros((nm_mdl_lat, nm_mdl_lon))

    #convert the times
    rep = {'b': '', "'": "", ' ': '', "\n": "", '[': "", ']': ""} #for formating strings
    rep = dict((re.escape(k), v) for k, v in rep.items())
    pattern = re.compile("|".join(rep.keys()))

    times = []
    for i in range(24):
        t = np.array2string(mdl_time[i])
        new_t = pattern.sub(lambda m: rep[re.escape(m.group(0))], t)
        #now convert to a datetime object
        d = datetime.strptime(new_t, '%Y-%m-%d_%H:%M:%S')
        times.append(d)
    
    #now average over just the interested years
    for t in times:
        if str(t.year) not in filtered_on_target_years:
            continue
        i = list(times).index(t)
        m = t.month-1
        mdl_mnth_o3[m,:,:] = np.add(mdl_vmro3[i,:,:], mdl_mnth_o3[m,:,:])
        mnth_count[m,:,:] += 1

        mdl_annual_o3[:,:] = np.add(mdl_vmro3[i,:,:], mdl_annual_o3[:,:])
        annual_count[:,:] += 1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:], mnth_count[:,:,:])
    mdl_annual_o3[:,:] = np.divide(mdl_annual_o3[:,:], annual_count[:,:])
    return mdl_mnth_o3, mdl_annual_o3, mdl_lat, mdl_lon, times

#main function which reads in the site data, then calls the functions for the desired models and plots eveything
def o3_surface_compare_multi(filtered_on_target_years = ['2014', '2015'],type='global',target_years=['2014', '2015'], models=['CESM', 'CMAM', 'DEHM', 'MATCH-SALSA', 'MATCH', 'MRI-ESM2']):
    ####-----Observation Paths-----------
    root_dir_csn = '/space/hall3/sitestore/eccc/crd/ccrn/obs/amap/CSN/'

    rdir_cmdl = '/space/hall3/sitestore/eccc/crd/ccrn/obs/amap/CMDL/CMDL_sfcO3/'
    cmdl_lookup = '/space/hall3/sitestore/eccc/crd/ccrn/obs/amap/CMDL/CMDL_CO/station_lookup.csv' #

    naps_root = '/space/hall3/sitestore/eccc/crd/ccrn/obs/amap/NAPS/'
    stations_file_naps = '/space/hall3/sitestore/eccc/crd/ccrn/obs/amap/NAPS/naps_stations.csv'

    hk_dir = '/space/hall3/sitestore/eccc/crd/ccrn/obs/amap/HongKongEPA/'
    
    ebas_root = "/space/hall3/sitestore/eccc/crd/ccrn/obs/amap/EBAS/"

    eur_root =  '/space/hall3/sitestore/eccc/crd/ccrn/obs/amap/europa/o3/'
    meta_file_eur = '/space/hall3/sitestore/eccc/crd/ccrn/obs/amap/europa/metadata.csv'
    
    villium_nord_file = '/space/hall3/sitestore/eccc/crd/ccrn/obs/amap/villium_nord/Ozone_1996-2018_200504.csv'

    root_china = '/space/hall3/sitestore/eccc/crd/ccrn/obs/amap/beijing_air/'
    stat_file_china = '/space/hall3/sitestore/eccc/crd/ccrn/obs/amap/beijing_air/station_lookup.csv'

    plot_output = '/space/hall3/sitestore/eccc/crd/ccrn/users/rcw001/AMAP/png_figs/'
    plot_output_eps = "/space/hall3/sitestore/eccc/crd/ccrn/users/rcw001/AMAP/eps_figs/"

    ###------------------------------------

    ###------------------------------------


    #lets make sure everything is on the same 1x1 grid
    compare_lat = list(np.arange(-90., 91.))
    compare_lon = list(np.arange(0., 360.))

    ###------Observations----------

    #CSN
    print("Reading in CSN files")

    obs_files = []

    for yr in filtered_on_target_years:
        obs_files.append('hourly_44201_'+yr+'.csv') #44201 is the species code for ozone

    #read in the file and pull the variables interested into pandas
    frames = [pd.read_csv(root_dir_csn+of) for of in obs_files]
    csn_data = pd.concat(frames)
    csn_data = csn_data[['Latitude', 'Longitude', 'Date Local', 'Time Local', 'Date GMT', 'Time GMT', 'Sample Measurement']]

    #seperate the date and add a column for the Month
    d = csn_data['Date Local'].str.split(pat = '-', expand = True)
    csn_data['Month'] = d[1]

    #check the units and convert to ppbv if necessary
    #csn_data.loc[csn_data['Units of Measure'] == "Parts per million", 'Arithmetic Mean'] *= 10**3
    csn_data.loc[:,'Sample Measurement'] *= 10**3 #convert units
    
    #take the mean at each location for each Month
    csn_sites = csn_data.groupby(['Latitude', 'Longitude', 'Month'], as_index=False).mean()

    #now at each location, need to match to the closest Compare location and average again
    csn_lat = csn_sites['Latitude'].values
    csn_lon = csn_sites['Longitude'].values
    compare_match_lon = np.zeros(len(csn_lat))
    compare_match_lat = np.zeros(len(csn_lat))
    for i in range(len(csn_lat)):
        ln = csn_lon[i]
        lt = csn_lat[i]
        if ln < 0:
            ln = ln+360.
        k = bisect_left(compare_lon, ln)-1
        j = bisect_left(compare_lat, lt)-1
        compare_match_lon[i] = compare_lon[k]
        compare_match_lat[i] = compare_lat[j]

    csn_sites['Compare_Longitude'] = pd.Series(compare_match_lon)
    csn_sites['Compare_Latitude'] = pd.Series(compare_match_lat)
     
    csn_sites = csn_sites[['Month', 'Compare_Longitude', 'Compare_Latitude', 'Sample Measurement']]
    csn_sites_annual = csn_sites[['Compare_Longitude', 'Compare_Latitude', 'Sample Measurement']]
    csn_avg_annual = csn_sites_annual.groupby(['Compare_Longitude', 'Compare_Latitude'], as_index=False).mean()
    csn_avg_grid = csn_sites.groupby(['Month', 'Compare_Longitude', 'Compare_Latitude'], as_index=False).mean()

    # fill any missing Months with NA 
    csn_avg_grid2 = csn_avg_grid
    # add a new row with lat and lon combined so that I can figure out the unique meas sites
    csn_avg_grid2['latlon'] = csn_avg_grid['Compare_Latitude'].astype(str)+csn_avg_grid['Compare_Longitude'].astype(str)
    
    idx_m = csn_avg_grid.Month.unique()   # new index will be 1:12 for month
    idx_ll = csn_avg_grid2.latlon.unique()  # new index will have each unique meas site

 #   df = csn_sites[['Compare_Longitude','Compare_Latitude']]
 #   latlon = df.drop_duplicates()  # unique lat/lon combinations = number of sites

    csn_avg_grid2 = csn_avg_grid2.set_index(['latlon','Month'])
    # reindex will insert an NA if there's a missing month for a given meas site (and insert an NA is there's a missing meas site for a given month)
    csn_avg_grid2 = csn_avg_grid2.reindex(pd.MultiIndex.from_product([idx_ll,idx_m],names=['latlon','Month'])).fillna(-999) 
    csn_avg_grid = csn_avg_grid2.reset_index()  
 #   print('csn_avg_grid=',csn_avg_grid.head())

    #CMDL
    print("Reading in CMDL files")
    all_files = glob.glob(rdir_cmdl+"*.txt")
    val = []
    for filename in all_files:
        with open(filename, 'r') as f:
            lines = f.readlines()
        tmp = [k for k in lines if '#' not in k]
        val.extend(tmp)

    site = []
    year = []
    month = []
    value = []

    for v in val:
        t = v.split()
        site.append(t[0])
        year.append(t[1])
        month.append(t[2])
        value.append(float(t[3]))

    #now lets make a pandas dataframe

    d = {'site': site, 'year': year, 'month': month, 'value': value}

    cmdl_data = pd.DataFrame(data=d)

    #lets read in the station lookup file to get the lat lon info
    lookup_data = pd.read_csv(cmdl_lookup, sep='|')

    #filter out the years we don't want
    cmdl_data = cmdl_data.loc[(cmdl_data['year'].isin(filtered_on_target_years))]

    #merge the station location data to our main database
    cmdl_site_data = pd.merge(cmdl_data, lookup_data, how='left', left_on='site', right_on='Code')

    cmdl_site_data = cmdl_site_data[['month', 'Latitude', 'Longitude', 'value']]

    #lets take a monthly average
    cmdl_avg = cmdl_site_data.groupby(['month', 'Latitude', 'Longitude'], as_index=False).mean()

    #match to the closet Compare location at each site and average if necessary
    cmdl_lon = cmdl_avg['Longitude'].values
    cmdl_lat = cmdl_avg['Latitude'].values

    compare_lon_match = np.zeros((len(cmdl_lon)))
    compare_lat_match = np.zeros((len(cmdl_lat)))

    for i in range(len(cmdl_lon)):
        ln = cmdl_lon[i]
        lt = cmdl_lat[i]
        if ln < 0:
            ln = ln+360.
        j = bisect_left(compare_lon, ln) -1
        k = bisect_left(compare_lat, lt) -1
        compare_lon_match[i] = compare_lon[j]
        compare_lat_match[i] = compare_lat[k]

    cmdl_avg['Compare_Longitude'] = compare_lon_match
    cmdl_avg['Compare_Latitude'] = compare_lat_match

    cmdl_avg = cmdl_avg[['Compare_Longitude', 'Compare_Latitude', 'month', 'value']]

    #now drop the lower hemisphere values
    cmdl_avg = cmdl_avg[cmdl_avg['Compare_Latitude'] > 0]

    #and now lets average one more time if more than one meas in a grid square
    cmdl_avg_grid = cmdl_avg.groupby(['month', 'Compare_Latitude', 'Compare_Longitude'], as_index=False).mean()

    cmdl_avg_annual = cmdl_avg.groupby(['Compare_Latitude', 'Compare_Longitude'], as_index=False).mean()

    # fill any missing months with NA
    cmdl_avg_grid2 = cmdl_avg_grid
    # add a new row with lat and lon combined so that I can figure out the unique meas sites
    cmdl_avg_grid2['latlon'] = cmdl_avg_grid['Compare_Latitude'].astype(str)+cmdl_avg_grid['Compare_Longitude'].astype(str)

    idx_m = cmdl_avg_grid.month.unique()   # new index will be 1:12 for month
    idx_ll = cmdl_avg_grid2.latlon.unique()  # new index will have each unique meas site

    cmdl_avg_grid2 = cmdl_avg_grid2.set_index(['latlon','month'])
    # reindex will insert an NA if there's a missing month for a given meas site (and insert an NA is there's a missing meas site for a given month)
    cmdl_avg_grid2 = cmdl_avg_grid2.reindex(pd.MultiIndex.from_product([idx_ll,idx_m],names=['latlon','month'])).fillna(-999) 
    cmdl_avg_grid = cmdl_avg_grid2.reset_index()  
  #  print('cmdl_avg_grid=',cmdl_avg_grid.head())

    #europe site data

    print("Reading European site data")

    eur_sites = ['albania', 'andorra', 'austria', 'belgium', 'bulgaria', 'croatia', 'cypren', 'czech', 'denmark', 'estonia', 'finland' , 'france', 'germany', 'gibraltar', 'greece', 'hungary', 'ireland', 'italy', 'kosovo', 'latvia', 'lithuania', 'luxembourg', 'macedonia' ,'malta', 'montenegro', 'netherlands', 'norway', 'poland', 'portugal', 'romania', 'serbia', 'slovakia', 'slovenia', 'spain', 'sweden', 'switzerland', 'turkey', 'uk']

    #first lets read in the meta data

    eur_meta = pd.read_csv(meta_file_eur)
    eur_meta = eur_meta[['Countrycode', 'SamplingPoint', 'Longitude', 'Latitude']]

    frames = []

    #now lets read in all of the country data
    for site in eur_sites:
        site_dir = eur_root+site
        all_files = glob.glob(site_dir + "/*.csv")
        li = []
        for filename in all_files:
            try:
                df = pd.read_csv(filename, index_col=None, header=0, error_bad_lines=False)
            except:
                df = pd.read_csv(filename, encoding="utf-16", index_col=None, header=0, error_bad_lines=False)
            li.append(df)
        try:
            site_data = pd.concat(li, axis=0, ignore_index=True)
        except ValueError:
            print("Couldn't concatenate?")
            print(site)
            print(len(li))
            continue
        site_data = site_data[['Countrycode', 'SamplingPoint', 'Concentration', 'UnitOfMeasurement', 'DatetimeBegin', 'DatetimeEnd']]
        frames.append(site_data)

    tmp_eur_data = pd.concat(frames)

    #now merge location data to concentration data
    eur_data = pd.merge(tmp_eur_data, eur_meta, how='left', left_on=['Countrycode', 'SamplingPoint'], right_on=['Countrycode', 'SamplingPoint'])

    #look at date time columns and extract needed information

    #first here is all the begining and end date times
    ti = eur_data['DatetimeBegin'].str.split()
    te = eur_data['DatetimeEnd'].str.split()

    #extract the month
    d = ti.str[0]
    d = d.str.split(pat = '-')
    eur_data['Month'] = d.str[1]

    #and extract the year and make sure we only use target year data
    year = d.str[0]
    eur_data = eur_data.loc[(year.isin(filtered_on_target_years))]

    #should mask out any negative data
    eur_data['Concentration'] = pd.to_numeric(eur_data['Concentration'], errors='coerce') #changing any bad values to nan
    eur_data = eur_data[eur_data.Concentration > 0]

    eur_data = eur_data.dropna(subset=['UnitOfMeasurement'])

    #check the units and convert everything to ppbv
    eur_data.loc[eur_data['UnitOfMeasurement'].isin(['mg/m3']), 'Concentration'] *= 510.4
    eur_data.loc[eur_data['UnitOfMeasurement'].isin(['µg/m3']), 'Concentration'] *= .5104

    eur_data = eur_data[['Concentration', 'Month', 'Longitude', 'Latitude']]

    #now do the monthly mean
    eur_avg = eur_data.groupby(['Month', 'Longitude', 'Latitude'], as_index=False).mean()

    #now at each location, need to match to the closest Compare location and average again
    eur_lat = eur_avg['Latitude'].values
    eur_lon = eur_avg['Longitude'].values

    compare_match_lon = np.zeros(len(eur_lat))
    compare_match_lat = np.zeros(len(eur_lat))
    for i in range(len(eur_lat)):
        ln = eur_lon[i]
        lt = eur_lat[i]
        if ln < 0:
            ln = ln+360.
        k = bisect_left(compare_lon, ln)-1 #returns an insertion point, want closest index which is i-1
        j = bisect_left(compare_lat, lt)-1
        compare_match_lon[i] = compare_lon[k]
        compare_match_lat[i] = compare_lat[j]

    eur_avg['Compare_Longitude'] = pd.Series(compare_match_lon)
    eur_avg['Compare_Latitude'] = pd.Series(compare_match_lat)

    eur_avg = eur_avg[['Month', 'Compare_Longitude', 'Compare_Latitude', 'Concentration']]
    eur_avg_grid = eur_avg.groupby(['Month', 'Compare_Longitude', 'Compare_Latitude'], as_index=False).mean()    
    eur_avg_annual = eur_avg.groupby(['Compare_Latitude', 'Compare_Longitude'], as_index=False).mean()
    print(eur_avg_annual.sort_values(by='Concentration', ascending=False))
    # fill any missing months with NA
    eur_avg_grid2 = eur_avg_grid
    # add a new row with lat and lon combined so that I can figure out the unique meas sites
    eur_avg_grid2['latlon'] = eur_avg_grid['Compare_Latitude'].astype(str)+eur_avg_grid['Compare_Longitude'].astype(str)

    idx_m = eur_avg_grid.Month.unique()   # new index will be 1:12 for month
    idx_ll = eur_avg_grid2.latlon.unique()  # new index will have each unique meas site

    eur_avg_grid2 = eur_avg_grid2.set_index(['latlon','Month'])
    # reindex will insert an NA if there's a missing month for a given meas site (and insert an NA is there's a missing meas site for a given month)
    eur_avg_grid2 = eur_avg_grid2.reindex(pd.MultiIndex.from_product([idx_ll,idx_m],names=['latlon','Month'])).fillna(-999) 
    eur_avg_grid = eur_avg_grid2.reset_index() 
     
 #   print('eur_avg_grid=',eur_avg_grid.head())
    
     # add NAPS code here
     # add HongKongEPA code here (see o3_compare_cesm.py)
     # add individual and other ebas site data (see o3_compare_cesm.py)
     
    #NAPS
    print("Reading in NAPS")

    naps_files = []
    naps_lookup = {'2008': '2008_O3_v3.HLY', '2009': '2009O3.hly', '2014': '2014_O3_v3.HLY', '2015': '2015_O3_v4.HLY'}

    for yr in filtered_on_target_years:
        naps_files.append(naps_lookup[yr])

    naps_id = []
    station_name = []
    naps_lat = []
    naps_lon = []

    #read in the station ids and locations first
    with open(stations_file_naps, 'r') as sf:
        csv_reader = csv.DictReader(sf)
        for row in csv_reader:
            naps_id.append(row['NAPS_ID'])
            station_name.append(row['STATION_NAME'])
            naps_lat.append(row['Lat_Decimal'])
            naps_lon.append(row['Long_Decimal'])

    nyears = len(naps_files)
    nstat = len(naps_id)

    for i in range(len(naps_lon)):
        if naps_lon[i] == "":
            naps_lon[i] = -999.9
        else:
            naps_lon[i] = float(naps_lon[i])

        if naps_lat[i] == "":
            naps_lat[i] = -999.9
        else:
            naps_lat[i] = float(naps_lat[i])
    
    naps_lon = np.ma.masked_values(naps_lon, -999.9)
    naps_lat = np.ma.masked_values(naps_lat, -999.9)
 
    compare_lon_match = []
    compare_lat_match = []
 
    naps_mnth = []
    naps_o3 = []

    #now read in the naps data files
    for yr in range(nyears):

        obs = naps_files[yr]

        with open(naps_root+obs, 'r') as nf:
            naps_data = nf.readlines()
 
        for line in naps_data:
            nd = re.sub(r'(\d)-', r'\1 -', line)
            nd = nd.split()

            row_info = nd[0]
            #check if row is valid
            if(len(row_info) != 17):
                print("Row wrong length?")
                print(len(row_info))
                continue

            pollutant_id = row_info[0:3] #should be 007 for O3
            if pollutant_id != '007':
                print("Not right pollutant?")
                print(pollutant_id)
                break

            stat_id = row_info[3:9]
            stat_id = stat_id.lstrip("0")

            try:
                si = naps_id.index(stat_id)
        #        print('si=',si)
            except(ValueError):
                continue
        
            ndate = row_info[9:]

            #extract the month from the date

            mnth = int(ndate[4:6])
            naps_mnth.append(mnth)
            tmp = np.array(nd[1:]).astype(np.float)
            o3 = np.ma.masked_values(tmp, -999)

            #now match to the model grid
            nln = naps_lon[si]
            nlt = naps_lat[si]
            #find the closest compare point
            if nln < 0:
                nln = nln+360.
            #print("in NAPs",nln)
            k = bisect_left(compare_lon, nln)-1
            j = bisect_left(compare_lat, nlt)-1
            compare_lon_match.append(compare_lon[k])
            compare_lat_match.append(compare_lat[j])

            naps_o3.append(o3.mean())
    
    #now we can construct the series
    d = {'Compare_Latitude': compare_lat_match, 'Compare_Longitude': compare_lon_match, 'Month': naps_mnth, 'Value': naps_o3}
    naps_avg = pd.DataFrame(data=d)
    #print(naps_avg)
    naps_avg['Value'] = naps_avg.Value.astype(float)
    
    naps_avg_grid = naps_avg.groupby(['Month', 'Compare_Latitude', 'Compare_Longitude',], as_index=False).mean()
    #print(naps_avg_grid)
    naps_avg_annual = naps_avg.groupby(['Compare_Latitude', 'Compare_Longitude'], as_index=False).mean()        
    #print(naps_avg_annual)
    # fill any missing months with NA
    naps_avg_grid2 = naps_avg_grid
    # add a new row with lat and lon combined so that I can figure out the unique meas sites
    naps_avg_grid2['latlon'] = naps_avg_grid['Compare_Latitude'].astype(str)+naps_avg_grid['Compare_Longitude'].astype(str)

    idx_m = naps_avg_grid.Month.unique()   # new index will be 1:12 for month
    idx_ll = naps_avg_grid2.latlon.unique()  # new index will have each unique meas site

    naps_avg_grid2 = naps_avg_grid2.set_index(['latlon','Month'])
    # reindex will insert an NA if there's a missing month for a given meas site (and insert an NA is there's a missing meas site for a given month)
    naps_avg_grid2 = naps_avg_grid2.reindex(pd.MultiIndex.from_product([idx_ll,idx_m],names=['latlon','Month'])).fillna(-999) 
    naps_avg_grid = naps_avg_grid2.reset_index()  
    
    #print('naps_avg_grid=',naps_avg_grid.head(50))     
     
    #Indiviual and other ebas site data

    print("Reading indiviual site data")
    
    ebas_files = glob.glob(ebas_root+"*.nas")

    pat_lat = re.compile(r"\bStation latitude\w*\b")
    pat_lon = re.compile(r"\bStation longitude\w*\b")
    pat_start_date = re.compile(r"\bStartdate\w*\b")
    #regex to find unit if they exist
    pat_unit = re.compile(r"Unit:\s*\w*\b")

    li = []
    for filename in ebas_files:
        with open(filename, 'r') as df:
            try:
                l = df.readline() #first line gives number of header lines
                num_head = int(l.split()[0])
                #now want to extract what the lat and lon is for this file
                lines = df.readlines()
            except OSError:
                print("Can't read file"+ filename)
                continue
        for ln in lines:
            if re.search(pat_lat, ln):
                lat = ln.split()
            if re.search(pat_lon, ln):
                lon = ln.split()
            if re.search(pat_start_date, ln):
                start_date = ln.split()
            if re.search(pat_unit, ln):
                #print(filename,ln)	        
                unit = ln.split()
            
        lat = float(lat[2])
        lon = float(lon[2])
        start_date = start_date[1]

        	
        null_vals=lines[10].split()
        #now lets read the data into a pandas dataframe
        df = pd.read_csv(filename, index_col=None, na_values=null_vals, skiprows=num_head-1, delim_whitespace=True, error_bad_lines=False)
        #now if there is more than 3 columns, only want the first three        	        
        df = df[['starttime', 'endtime', 'O3']]
        if unit==None:
            df['Unit'] = 'µg/m3'
        else:
            unit = unit[1]
            df['Unit'] = unit
        
        df['lat'] = lat
        df['lon'] = lon
        df['start date'] = pd.Timestamp(start_date)
        li.append(df)
        unit = None
    
    
    site_data = pd.concat(li, ignore_index=True, axis=0)
    #print(site_data.head())
    #lets turn all the time and date columns into one datetime object column
    site_grouped = site_data.groupby(site_data['start date'])
    li = []
    for sd, group in site_grouped:
        days = pd.to_datetime(group['endtime'], unit='d', origin=sd)
        group['time'] = days
        li.append(group)

    ind_data = pd.concat(li)
    #lets take out the years we don't need  
    ind_data = ind_data.loc[(ind_data['time'].dt.year.isin(filtered_on_target_years))]   
    #update O3 values according to their unit
    ind_data = ind_data.reset_index(drop=True)
    #refactor values of O3 depending on unit of measurement
    ind_data.loc[ind_data['Unit'].isin(['mg/m3']), 'O3'] *= 510.4
    ind_data.loc[ind_data['Unit'].isin(['µg/m3']), 'O3'] *= .5104
    ind_data.loc[ind_data['Unit']=='ug/m3', ['O3']] *= 0.5104
    ind_data = ind_data[['time', 'lat', 'lon', 'O3','Unit']]
    

    #now match to the model grid
    nln = ind_data['lon'].values
    nlt = ind_data['lat'].values
    compare_match_lon = np.zeros(len(nln))
    compare_match_lat = np.zeros(len(nln))
    #find the closest compare point using a loop
    for i in range(len(nln)):
        ln = nln[i]
        lt = nlt[i]
        if ln < 0:
            ln = ln + 360
        k = bisect_left(compare_lon, ln)-1 #returns an insertion point, want closest index which is i-1
        j = bisect_left(compare_lat, lt)-1
        compare_match_lon[i] = compare_lon[k]
        compare_match_lat[i] = compare_lat[j]
    
    ind_avg = ind_data[['time', 'O3']]
    ind_avg['Value'] = ind_avg.O3.astype(float)
    ind_avg['Compare_Longitude'] = pd.Series(compare_match_lon)
    ind_avg['Compare_Latitude'] = pd.Series(compare_match_lat)
    
    #now take the average for each month, at each location, day and night
    ind_avg['month'] = ind_avg['time'].dt.month
    ind_avg_grid = ind_avg.groupby(['month','Compare_Longitude', 'Compare_Latitude'], as_index=False).mean()
    ind_avg_annual = ind_avg_grid.groupby(['Compare_Longitude', 'Compare_Latitude'], as_index=False).mean()
    ind_avg_grid2 = ind_avg_grid
    # add a new row with lat and lon combined so that I can figure out the unique meas sites
    ind_avg_grid2['latlon'] = ind_avg_grid['Compare_Latitude'].astype(str)+ind_avg_grid['Compare_Longitude'].astype(str)

    idx_m = ind_avg_grid.month.unique()   # new index will be 1:12 for month
    idx_ll = ind_avg_grid2.latlon.unique()  # new index will have each unique meas site

    ind_avg_grid2 = ind_avg_grid2.set_index(['latlon','month'])
    # reindex will insert an NA if there's a missing month for a given meas site (and insert an NA is there's a missing meas site for a given month)
    ind_avg_grid2 = ind_avg_grid2.reindex(pd.MultiIndex.from_product([idx_ll,idx_m],names=['latlon','Month'])).fillna(-999) 
    ind_avg_grid = ind_avg_grid2.reset_index()


    #villium nord site data
    
    #constant lat and lon for villium data
    vlat = 81.6 #81.36
    vlon = -16.7 #-16.40
    if vlon < 0:
        vlon = vlon+360
    vdata = pd.read_csv(villium_nord_file, index_col=None, header=0)
    vdata = vdata.reset_index()
    #now we only want data from the target years
    vdata["date"] = vdata['date'].astype('datetime64')
    #filter out the target years
    vdata = vdata.loc[(vdata['date'].dt.year.isin(filtered_on_target_years))]
    k = bisect_left(compare_lon, vlon)-1 #returns an insertion point, want closest index which is i-1
    j = bisect_left(compare_lat, vlat)-1
    compare_val_lon = compare_lon[k]
    compare_val_lat = compare_lat[j]
    #okay, we want to seperate by day and night
    v_avg = vdata
    v_avg = v_avg.rename(columns={'Ozon': 'Value'})
    v_avg['Value'] = v_avg.Value.astype(float)
    v_avg['Compare_Longitude'] = compare_val_lon
    v_avg['Compare_Latitude'] = compare_val_lat
    v_avg['month'] = v_avg['date'].dt.month
    v_avg_grid = v_avg.groupby(['month','Compare_Longitude', 'Compare_Latitude'], as_index=False).mean()
    v_avg_annual = v_avg_grid.groupby(['Compare_Longitude', 'Compare_Latitude'], as_index=False).mean()
    v_avg_grid2 = v_avg_grid
    # add a new row with lat and lon combined so that I can figure out the unique meas sites
    v_avg_grid2['latlon'] = v_avg_grid['Compare_Latitude'].astype(str)+v_avg_grid['Compare_Longitude'].astype(str)

    idx_m = v_avg_grid.month.unique()   # new index will be 1:12 for month
    idx_ll = v_avg_grid2.latlon.unique()  # new index will have each unique meas site

    v_avg_grid2 = v_avg_grid2.set_index(['latlon','month'])
    # reindex will insert an NA if there's a missing month for a given meas site (and insert an NA is there's a missing meas site for a given month)
    v_avg_grid2 = v_avg_grid2.reindex(pd.MultiIndex.from_product([idx_ll,idx_m],names=['latlon','Month'])).fillna(-999) 
    v_avg_grid = v_avg_grid2.reset_index()     

     
    #chinese site data

    print("Reading in Chinese site data")

    all_files = glob.glob(root_china + "csv*")
    li = []

    for filename in all_files:
        df = pd.read_csv(filename, index_col=None, header=0)
        li.append(df)

    china_data = pd.concat(li, axis=0, ignore_index=True)

    #now read in the headers csv file

    sf = pd.read_csv(stat_file_china)

    #okay get the headers and match city locations

    cities = list(china_data.columns.values)
    cities = cities[3:]

    city_lat = {}
    city_lon = {}

    for city in cities:
        entry = sf.loc[sf['CityNM'] == city]
        city_lat[city] = np.average(entry['lat'].to_numpy())
        city_lon[city] = np.average(entry['lon'].to_numpy())

    #now we want to change the organization of the dataframe
    #the column headers for cities should be row values

    china_data = china_data.melt(id_vars=['date', 'hour', 'type'], var_name="City", value_name="Value")

    #lets map the lat lon locations to the city names
    china_data['lat'] = china_data['City'].map(city_lat)
    china_data['lon'] = china_data['City'].map(city_lon)

    #lets just take the co data
    china_data = china_data.loc[china_data['type'] == 'O3']

    #we just want the target years
    china_data['date'] = china_data['date'].apply(str)
    date = china_data['date'].str
    year = date[:4]
    china_data = china_data.loc[(year.isin(filtered_on_target_years))]

    #now we need the Month for the average
    month = date[4:6]
    china_data['Month'] = month

    #convert the units
    #units are given in ug/m3

    china_data['Value'] = (china_data['Value']*0.5104)

    #get rid of the columns we no longer need

    china_data = china_data[['Value', 'lat', 'lon', 'Month']]
    china_avg = china_data.groupby(['lat', 'lon', 'Month'], as_index=False).mean()

    #lets convert to the Compare grid
    china_lat = china_avg['lat'].values
    china_lon = china_avg['lon'].values
    compare_match_lon = np.zeros(len(china_lat))
    compare_match_lat = np.zeros(len(china_lat))
    for i in range(len(china_lat)):
        ln = china_lon[i]
        lt = china_lat[i]
        if ln < 0:
            ln = ln+360.
        k = bisect_left(compare_lon, ln)-1
        j = bisect_left(compare_lat, lt)-1
        compare_match_lon[i] = compare_lon[k]
        compare_match_lat[i] = compare_lat[j]

    china_avg['Compare_Longitude'] = pd.Series(compare_match_lon)
    china_avg['Compare_Latitude'] = pd.Series(compare_match_lat)

    china_avg = china_avg[['Value', 'Compare_Longitude', 'Compare_Latitude', 'Month']]

    #and lets take Monthly averages at each location
    china_avg_grid = china_avg.groupby(['Compare_Longitude', 'Compare_Latitude', 'Month'], as_index=False).mean()
  #  print('china_avg_grid before index = ',china_avg_grid.head())
   # print('china_avg_grid shape = ',china_avg_grid.shape())
    # annual avg
    china_avg_annual = china_avg.groupby(['Compare_Longitude', 'Compare_Latitude'], as_index=False).mean()

    # fill any missing months with NA
    china_avg_grid2 = china_avg_grid
    # add a new row with lat and lon combined so that I can figure out the unique meas sites
    china_avg_grid2['latlon'] = china_avg_grid['Compare_Latitude'].astype(str)+china_avg_grid['Compare_Longitude'].astype(str)

    idx_m = china_avg_grid.Month.unique()   # new index will be 1:12 for month
    idx_ll = china_avg_grid2.latlon.unique()  # new index will have each unique meas site

    china_avg_grid2 = china_avg_grid2.set_index(['latlon','Month'])
    # reindex will insert an NA if there's a missing month for a given meas site (and insert an NA is there's a missing meas site for a given month)
    china_avg_grid2 = china_avg_grid2.reindex(pd.MultiIndex.from_product([idx_ll,idx_m],names=['latlon','Month'])).fillna(-999) 
    china_avg_grid = china_avg_grid2.reset_index()  
 #   print('china_avg_grid=',china_avg_grid.head())

    ####--------Models------------

    all_mdl_annual = {}
    all_mdl_mon = {}
    processed_models = []
    all_mdl_lon = {}
    all_mdl_lat = {}
    
    # need to add other models that provided O3: WRF-CHEM (and maybe GEM-MACH)
    for m in models:
        print(m)

        if m =='CESM':
            mdl_o3, mdl_annual, mdl_lat, mdl_lon, times = cesm_model(filtered_on_target_years = filtered_on_target_years,target_years=target_years)
        elif m == 'CMAM':
            mdl_o3, mdl_annual, mdl_lat, mdl_lon, times = cmam_model(filtered_on_target_years = filtered_on_target_years,target_years=target_years)
        elif m == 'DEHM':
            mdl_o3, mdl_annual, mdl_lat, mdl_lon, times = dehm_model(filtered_on_target_years = filtered_on_target_years,target_years=target_years)
        elif m == 'EMEP-MSC-W':
            mdl_o3, mdl_annual, mdl_lat, mdl_lon, times = emep_model(filtered_on_target_years = filtered_on_target_years,target_years=target_years)
        elif m == 'GEM-MACH':
            mdl_o3, mdl_annual, mdl_lat, mdl_lon, times = gem_mach_model(filtered_on_target_years = filtered_on_target_years,target_years=target_years)
        elif m == 'GEOS-CHEM':
            mdl_o3, mdl_annual, mdl_lat, mdl_lon, times = geoschem_model(filtered_on_target_years = filtered_on_target_years,target_years=target_years)
        elif m == 'GISS-E2.1':
            mdl_o3, mdl_annual, mdl_lat, mdl_lon, times = giss_model(filtered_on_target_years = filtered_on_target_years,target_years=target_years)
        elif m == 'MATCH-SALSA':
            mdl_o3, mdl_annual, mdl_lat, mdl_lon, times = matchsalsa_model(filtered_on_target_years = filtered_on_target_years,target_years=target_years)
        elif m == 'MATCH':
            mdl_o3, mdl_annual, mdl_lat, mdl_lon, times = match_model(filtered_on_target_years = filtered_on_target_years,target_years=target_years)
        elif m == 'MRI-ESM2':
            mdl_o3, mdl_annual, mdl_lat, mdl_lon, times = mri_model(filtered_on_target_years = filtered_on_target_years,target_years=target_years)
        elif m == 'OsloCTM':
            mdl_o3, mdl_annual, mdl_lat, mdl_lon, times = oslo_model(filtered_on_target_years = filtered_on_target_years,target_years=target_years)
        elif m == 'UKESM1':
            mdl_o3, mdl_annual, mdl_lat, mdl_lon, times = ukesm_model(filtered_on_target_years = filtered_on_target_years,target_years=target_years)
        elif m == 'WRF-Chem':
            mdl_o3, mdl_annual, mdl_lat, mdl_lon, times = wrfchem_model(filtered_on_target_years = filtered_on_target_years,target_years=target_years)
        else:
            print("Didn't recognize model "+m+", can't process files")
            continue
        #print(mdl_lon)
        processed_models.append(m)
        all_mdl_annual[m] = mdl_annual
        all_mdl_mon[m] = mdl_o3
        all_mdl_lon[m] = mdl_lon
        all_mdl_lat[m] = mdl_lat

    ###---------Plotting--------------

    csn_lat = csn_avg_annual['Compare_Latitude'].values
    csn_lon = csn_avg_annual['Compare_Longitude'].values
    csn_lat_mon = csn_avg_grid['Compare_Latitude'].values

    eur_lat = eur_avg_annual['Compare_Latitude'].values
    eur_lon = eur_avg_annual['Compare_Longitude'].values
    eur_lat_mon = eur_avg_grid['Compare_Latitude'].values
    
    naps_lat = naps_avg_annual['Compare_Latitude'].values
    naps_lon = naps_avg_annual['Compare_Longitude'].values
    naps_lat_mon = naps_avg_grid['Compare_Latitude'].values

    ind_lat = ind_avg_annual['Compare_Latitude'].values
    ind_lon = ind_avg_annual['Compare_Longitude'].values
    ind_lat_mon = ind_avg_grid['Compare_Latitude'].values
  
    china_lat = china_avg_annual['Compare_Latitude'].values
    china_lon = china_avg_annual['Compare_Longitude'].values
    china_lat_mon = china_avg_grid['Compare_Latitude'].values
    
    cmdl_lat = cmdl_avg_annual['Compare_Latitude'].values
    cmdl_lon = cmdl_avg_annual['Compare_Longitude'].values
    cmdl_lat_mon = cmdl_avg_grid['Compare_Longitude'].values
    
    vil_lat = v_avg_annual['Compare_Latitude'].values
    vil_lon = v_avg_annual['Compare_Longitude'].values
    vil_lat_mon = v_avg_grid['Compare_Longitude'].values
    
    #now for the difference maps have to match model to station locations
    # number of locations:
    csn_len = len(csn_lat)
    csn_len_mon = len(csn_lat_mon)
    eur_len = len(eur_lat)
    eur_len_mon = len(eur_lat_mon)
    naps_len = len(naps_lat)
    naps_len_mon = len(naps_lat_mon)
    ind_len = len(ind_lat)
    ind_len_mon = len(ind_lat_mon)
    china_len = len(china_lat)
    china_len_mon = len(china_lat_mon)
    cmdl_len = len(cmdl_lat)
    cmdl_len_mon = len(cmdl_lat_mon)
    vil_len = len(vil_lat)
    vil_len_mon = len(vil_lat_mon)


# np.ma.masked_all used to keep default values masked
    mdl_csn_scatter = np.ma.masked_all((len(processed_models), csn_len))
    mdl_csn_scatter_mnth = np.ma.zeros((len(processed_models), csn_len_mon))
    mdl_eur_scatter = np.ma.masked_all((len(processed_models), eur_len))
    mdl_eur_scatter_mnth = np.ma.zeros((len(processed_models), eur_len_mon))
    mdl_naps_scatter = np.ma.masked_all((len(processed_models), naps_len))
    mdl_naps_scatter_mnth = np.ma.zeros((len(processed_models), naps_len_mon))
    mdl_ind_scatter = np.ma.masked_all((len(processed_models), ind_len))
    mdl_ind_scatter_mnth = np.ma.zeros((len(processed_models), ind_len_mon))
    mdl_china_scatter = np.ma.masked_all((len(processed_models), china_len))
    mdl_china_scatter_mnth = np.ma.zeros((len(processed_models), china_len_mon))
    mdl_cmdl_scatter = np.ma.masked_all((len(processed_models), cmdl_len))
    mdl_cmdl_scatter_mnth = np.ma.zeros((len(processed_models), cmdl_len_mon))
    mdl_vil_scatter = np.ma.masked_all((len(processed_models), vil_len))
    mdl_vil_scatter_mnth = np.ma.zeros((len(processed_models), vil_len_mon))
    
    csn_dif = np.zeros((len(processed_models), csn_len))
    eur_dif = np.zeros((len(processed_models), eur_len))
    naps_dif = np.zeros((len(processed_models), naps_len))
    ind_dif = np.zeros((len(processed_models), ind_len))
    china_dif = np.zeros((len(processed_models), china_len))
    cmdl_dif = np.zeros((len(processed_models), cmdl_len))
    vil_dif = np.zeros((len(processed_models), vil_len)) 
     
    for mi in range(len(processed_models)):
        print("matching model",processed_models[mi])
        m = processed_models[mi]
        mdl_lon = all_mdl_lon[m]
        mdl_lat = all_mdl_lat[m]
        mdl_annual_o3 = all_mdl_annual[m]
        mdl_mnth_o3 = all_mdl_mon[m]
        mdl_min_lat = np.min(mdl_lat)
        mdl_max_lat = np.max(mdl_lat)
        mdl_min_lon = np.min(mdl_lon)
        mdl_max_lon = np.max(mdl_lon)
        for n in range(csn_len):
            if (csn_lat[n]<mdl_min_lat or csn_lat[n]>mdl_max_lat) or (csn_lon[n]<mdl_min_lon or csn_lon[n]>mdl_max_lon): continue
            k = np.abs(csn_lon[n]-mdl_lon).argmin()
            j = np.abs(csn_lat[n]-mdl_lat).argmin()
            #print(n,j,k,csn_lon[n],mdl_min_lon,mdl_max_lon)
            mdl_csn_scatter[mi,n] = mdl_annual_o3[j,k]
            mdl_csn_scatter_mnth[mi,(12*(n+1)-12):(12*(n+1))] = mdl_mnth_o3[:,j,k]
        for n in range(eur_len):
            if (eur_lat[n]<mdl_min_lat or eur_lat[n]>mdl_max_lat) or (eur_lon[n]<mdl_min_lon or eur_lon[n]>mdl_max_lon): continue
            k = np.abs(eur_lon[n]-mdl_lon).argmin()
            j = np.abs(eur_lat[n]-mdl_lat).argmin()
            mdl_eur_scatter[mi,n] = mdl_annual_o3[j,k]
            mdl_eur_scatter_mnth[mi,(12*(n+1)-12):(12*(n+1))] = mdl_mnth_o3[:,j,k]
	    
        for n in range(naps_len):
            if (naps_lat[n]<mdl_min_lat or naps_lat[n]>mdl_max_lat) or (naps_lon[n]<mdl_min_lon or naps_lon[n]>mdl_max_lon): continue
            k = np.abs(naps_lon[n]-mdl_lon).argmin()
            j = np.abs(naps_lat[n]-mdl_lat).argmin()
            mdl_naps_scatter[mi,n] = mdl_annual_o3[j,k]
            mdl_naps_scatter_mnth[mi,(12*(n+1)-12):(12*(n+1))] = mdl_mnth_o3[:,j,k]

        for n in range(ind_len):
            if (ind_lat[n]<mdl_min_lat or ind_lat[n]>mdl_max_lat) or (ind_lon[n]<mdl_min_lon or ind_lon[n]>mdl_max_lon): continue
            k = np.abs(ind_lon[n]-mdl_lon).argmin()
            j = np.abs(ind_lat[n]-mdl_lat).argmin()
            mdl_ind_scatter[mi,n] = mdl_annual_o3[j,k]
            mdl_ind_scatter_mnth[mi,(12*(n+1)-12):(12*(n+1))] = mdl_mnth_o3[:,j,k]
	    
        for n in range(china_len):
            if (china_lat[n]<mdl_min_lat or china_lat[n]>mdl_max_lat) or (china_lon[n]<mdl_min_lon or china_lon[n]>mdl_max_lon): continue
            k = np.abs(china_lon[n]-mdl_lon).argmin()
            j = np.abs(china_lat[n]-mdl_lat).argmin()
            #print(n,j,k,china_lat[n],mdl_lat[j-1],mdl_lat[j],mdl_lat[j+1])
            mdl_china_scatter[mi,n] = mdl_annual_o3[j,k]
            mdl_china_scatter_mnth[mi,(12*(n+1)-12):(12*(n+1))] = mdl_mnth_o3[:,j,k]
	          
        for n in range(cmdl_len):
            if (cmdl_lat[n]<mdl_min_lat or cmdl_lat[n]>mdl_max_lat) or (cmdl_lon[n]<mdl_min_lon or cmdl_lon[n]>mdl_max_lon): continue
            k = np.abs(cmdl_lon[n]-mdl_lon).argmin()
            j = np.abs(cmdl_lat[n]-mdl_lat).argmin()
            mdl_cmdl_scatter[mi,n] = mdl_annual_o3[j,k]
            mdl_cmdl_scatter_mnth[mi,(12*(n+1)-12):(12*(n+1))] = mdl_mnth_o3[:,j,k] 
	
        for n in range(vil_len):
            if (vil_lat[n]<mdl_min_lat or vil_lat[n]>mdl_max_lat) or (vil_lon[n]<mdl_min_lon or vil_lon[n]>mdl_max_lon): continue
            k = np.abs(vil_lon[n]-mdl_lon).argmin()
            j = np.abs(vil_lat[n]-mdl_lat).argmin()
            mdl_vil_scatter[mi,n] = mdl_annual_o3[j,k]
            mdl_vil_scatter_mnth[mi,(12*(n+1)-12):(12*(n+1))] = mdl_mnth_o3[:,j,k] 
	
	#and now we can calculate the difference at each location of the annual avgs
        #print(csn_lon,csn_lon)
        #print(mdl_csn_scatter[mi])
        #print(csn_avg_annual['Sample Measurement'])    
        #print(mdl_csn_scatter[mi] - csn_avg_annual['Sample Measurement'])
        
        csn_dif[mi] = mdl_csn_scatter[mi] - csn_avg_annual['Sample Measurement']
        #sys.exit()
        eur_dif[mi] = mdl_eur_scatter[mi] - eur_avg_annual['Concentration']
        naps_dif[mi] = mdl_naps_scatter[mi] - naps_avg_annual['Value']
        ind_dif[mi] = mdl_ind_scatter[mi] - ind_avg_annual['Value']
        #print(mdl_china_scatter[mi])
        china_dif[mi] = mdl_china_scatter[mi] - china_avg_annual['Value']
        #print(china_dif[mi])
        cmdl_dif[mi] = mdl_cmdl_scatter[mi] - cmdl_avg_annual['value']
        vil_dif[mi] = mdl_vil_scatter[mi] - v_avg_annual['Value']


    #now should have everything to plot the annual averages
    #lets figure out the plotting configuration
    num_plots = len(processed_models) + 1 #models plus one for the obs values
    num_rows = int(num_plots / 4)  # want 4 columns and an extra row for the colour bars 
    if (num_plots % 4) != 0: num_rows += 1

    fig, axs = plt.subplots(num_rows, 4)
    ax = axs[0,0] #observations first
    ax.set_title("Observations",fontsize=9)
    if type=='Arctic':
        obs_map = Basemap(projection='npaeqd',boundinglat=60,lon_0=0,resolution='l', ax=ax)
        size=16
        vmaxAG=40
    else:
        obs_map = Basemap(projection='merc',llcrnrlat=-45,urcrnrlat=85, llcrnrlon=-180,urcrnrlon=180,lat_ts=20,resolution='l', ax=ax)
        size=2
        vmaxAG=60
    obs_map.drawmapboundary(linewidth=0.25)
    obs_map.drawcoastlines(linewidth=0.25)
    pcm_obs = obs_map.scatter(csn_lon, csn_lat, c=csn_avg_annual['Sample Measurement'], cmap='YlOrRd', latlon=True, s=size, vmin=20, vmax=vmaxAG)
    pcm_obs = obs_map.scatter(eur_lon, eur_lat, c=eur_avg_annual['Concentration'], latlon=True, cmap='YlOrRd', s=size, vmin=20, vmax=vmaxAG)
    pcm_obs = obs_map.scatter(naps_lon, naps_lat, c=naps_avg_annual['Value'], latlon=True, cmap='YlOrRd', s=size, vmin=20, vmax=vmaxAG)
    pcm_obs = obs_map.scatter(ind_lon, ind_lat, c=ind_avg_annual['Value'], latlon=True, cmap='YlOrRd', s=size, vmin=20, vmax=vmaxAG)
    pcm_obs = obs_map.scatter(china_lon, china_lat, c=china_avg_annual['Value'], latlon=True, cmap='YlOrRd', s=size, vmin=20, vmax=vmaxAG)
    pcm_obs = obs_map.scatter(cmdl_lon, cmdl_lat, c=cmdl_avg_annual['value'], latlon=True, cmap='YlOrRd', s=size, vmin=20, vmax=vmaxAG)
    pcm_obs = obs_map.scatter(vil_lon, vil_lat, c=v_avg_annual['Value'], latlon=True, cmap='YlOrRd', s=size, vmin=20, vmax=vmaxAG)
    
    #now we can do a difference plot for each model
    c = 0
    v_min = 0
    v_max = 0
    for nr in range(num_rows):
        for nc in range(4):
            #don't plot over the measurement values
            if nr == 0 and nc == 0: continue
            #if we've plotted all the models, just need to delete any extra plots
            if c+1 >= num_plots:
                fig.delaxes(axs[nr, nc])
                continue
            ax = axs[nr,nc]
            ax.set_title(processed_models[c],fontsize=7)
            if type == 'Arctic': 
                mdl_map = Basemap(projection='npaeqd',boundinglat=60,lon_0=0,resolution='l', ax=ax)
            else:
                mdl_map = Basemap(projection='merc',llcrnrlat=-45,urcrnrlat=85, llcrnrlon=-180,urcrnrlon=180,lat_ts=20,resolution='l', ax=ax)
            mdl_map.drawmapboundary(fill_color='lightgrey',linewidth=0.25)
            mdl_map.drawcoastlines(linewidth=0.25)
            mdl_map.fillcontinents(color='lightgrey', zorder=0)
            if type == 'Arctic':
                v_min = -40   #just update this number to change v min for ticks and the scatter plot
                v_max = 40    #just update this number to change v max for ticks and the scatter plot	    
                pcm = mdl_map.scatter(csn_lon, csn_lat, c=csn_dif[c], cmap='bwr', latlon=True, s=16, vmin=v_min, vmax=v_max)
                pcm = mdl_map.scatter(eur_lon, eur_lat, c=eur_dif[c], cmap='bwr', latlon=True, s=16, vmin=v_min, vmax=v_max)
                pcm = mdl_map.scatter(naps_lon, naps_lat, c=naps_dif[c], cmap='bwr', latlon=True, s=16, vmin=v_min, vmax=v_max)
                pcm = mdl_map.scatter(ind_lon, ind_lat, c=ind_dif[c], cmap='bwr', latlon=True, s=16, vmin=v_min, vmax=v_max)
                pcm = mdl_map.scatter(china_lon, china_lat, c=china_dif[c], cmap='bwr', latlon=True, s=16, vmin=v_min, vmax=v_max)
                pcm = mdl_map.scatter(cmdl_lon, cmdl_lat, c=cmdl_dif[c], cmap='bwr', latlon=True, s=16, vmin=v_min, vmax=v_max)
                pcm = mdl_map.scatter(vil_lon, vil_lat, c=vil_dif[c], cmap='bwr', latlon=True, s=16, vmin=v_min, vmax=v_max)  
            else:
                v_min = -60   #just update this number to change v min for ticks and the scatter plot
                v_max = 60    #just update this number to change v max for ticks and the scatter plot	       
                pcm = mdl_map.scatter(csn_lon, csn_lat, c=csn_dif[c], cmap='bwr', latlon=True, s=2, vmin=v_min, vmax=v_max)
                pcm = mdl_map.scatter(eur_lon, eur_lat, c=eur_dif[c], cmap='bwr', latlon=True, s=2, vmin=v_min, vmax=v_max)
                pcm = mdl_map.scatter(naps_lon, naps_lat, c=naps_dif[c], cmap='bwr', latlon=True, s=2, vmin=v_min, vmax=v_max)
                pcm = mdl_map.scatter(ind_lon, ind_lat, c=ind_dif[c], cmap='bwr', latlon=True, s=2, vmin=v_min, vmax=v_max)
                pcm = mdl_map.scatter(china_lon, china_lat, c=china_dif[c], cmap='bwr', latlon=True, s=2, vmin=v_min, vmax=v_max)
                pcm = mdl_map.scatter(vil_lon, vil_lat, c=vil_dif[c], cmap='bwr', latlon=True, s=2, vmin=v_min, vmax=v_max)		
                pcm = mdl_map.scatter(cmdl_lon, cmdl_lat, c=cmdl_dif[c], cmap='bwr', latlon=True, s=2, vmin=v_min, vmax=v_max)	    
            c += 1
	     
    cbaxes = fig.add_axes([0.02, 0.1, 0.01, 0.75])
    cbaxes2 = fig.add_axes([0.9, 0.1, 0.01, 0.75]) 
    fig.colorbar(pcm_obs, orientation='vertical',ticks=[x for x in range(20,v_max+1,5)],cax=cbaxes,pad=0.2)
    fig.colorbar(pcm,orientation='vertical',ticks=[x for x in range(v_min,v_max+1,20)],cax=cbaxes2,pad=0.2) 
  
    fig.subplots_adjust(wspace=0)
 #   plt.tight_layout
    
    if type=='Arctic':
        if len(filtered_on_target_years)==1:
            plt.savefig(plot_output+"o3_surface_Arctic_"+filtered_on_target_years[0]+".png")
            plt.savefig(plot_output_eps+"o3_surface_Arctic_"+filtered_on_target_years[0]+".eps")
        else:
            plt.savefig(plot_output+"o3_surface_Arctic_"+filtered_on_target_years[0]+"-"+filtered_on_target_years[1]+".png")
            plt.savefig(plot_output_eps+"o3_surface_Arctic_"+filtered_on_target_years[0]+"-"+filtered_on_target_years[1]+".eps")
    else:
        if len(filtered_on_target_years)==1:
            plt.savefig(plot_output+"o3_surface_multi_"+filtered_on_target_years[0]+".png")   
            plt.savefig(plot_output_eps+"o3_surface_multi_"+filtered_on_target_years[0]+".eps")           
        else:
            plt.savefig(plot_output+"o3_surface_multi_"+filtered_on_target_years[0]+"-"+filtered_on_target_years[1]+".png")
            plt.savefig(plot_output_eps+"o3_surface_multi_"+filtered_on_target_years[0]+"-"+filtered_on_target_years[1]+".eps")
	
    ###------------Output Data to CSV File------------------
    # row-bind all measurement arrays into one: temporarily remove csv_avg
    frames = [csn_avg_annual, eur_avg_annual, china_avg_annual, naps_avg_annual, ind_avg_annual,v_avg_annual]	# removed cmdl_avg_annual
    result = pd.concat(frames)
    # want Monthly values once I matched the models Monthly too. # columns are: lat, lon, mon, value
    df1=pd.DataFrame(csn_avg_grid)
    df2=pd.DataFrame(eur_avg_grid)
    df3=pd.DataFrame(china_avg_grid)
    df4=pd.DataFrame(naps_avg_grid)
    df5=pd.DataFrame(ind_avg_grid)
    df6=pd.DataFrame(v_avg_grid)
 #   print('csn, eur, and chin dim=',len(csn_avg_grid),len(eur_avg_grid),len(china_avg_grid))
    frames_mn = [df1, df2, df3, df4, df5, df6]
    result_mnth=pd.concat(frames_mn)
    
  #  print('annual measurement array dim=',result.shape)
  #  print(result.head())
  #  print('Monthly measurement array dim=',result_mnth.shape)
  #  print(result_mnth.head())
  #  print('ann mdl_csn_scatter dim=',mdl_csn_scatter.shape)
  #  print('mon mdl_csn_scatter dim=',mdl_csn_scatter_mnth.shape)
  #  print('ann mdl_eur_scatter dim=',mdl_eur_scatter.shape)
  #  print('mon mdl_eur_scatter dim=',mdl_eur_scatter_mnth.shape)
  #  print('ann mdl_china_scatter dim=',mdl_china_scatter.shape)
  #  print('mon mdl_china_scatter dim=',mdl_china_scatter_mnth.shape)
    fr_mod = np.concatenate((mdl_csn_scatter, mdl_eur_scatter, mdl_china_scatter, mdl_naps_scatter, mdl_ind_scatter, mdl_vil_scatter),axis=1) # removed mdl_cmdl_scatter,
    fr_mon_mod = np.concatenate((mdl_csn_scatter_mnth, mdl_eur_scatter_mnth, mdl_china_scatter_mnth, mdl_naps_scatter_mnth, mdl_ind_scatter_mnth, mdl_vil_scatter_mnth),axis=1)
 #   print('ann fr_mod dim=',fr_mod.shape)
 #   print('mon fr_mon_mod dim=',fr_mon_mod.shape)
 # add the matching model columns to the meas array
    for mo in processed_models:
        i = processed_models.index(mo)
        result[mo] = fr_mod[i]
        result_mnth[mo] = fr_mon_mod[i]

    if len(filtered_on_target_years)==1:
        result.to_csv('/space/hall3/sitestore/eccc/crd/ccrn/users/rcw001/AMAP/csv_files/surface-o3-multi_annMean'+filtered_on_target_years[0]+'.csv')
        result_mnth.to_csv('/space/hall3/sitestore/eccc/crd/ccrn/users/rcw001/AMAP/csv_files/surface-o3-multi_monMean'+filtered_on_target_years[0]+'.csv')            
    else:
        result.to_csv('/space/hall3/sitestore/eccc/crd/ccrn/users/rcw001/AMAP/csv_files/surface-o3-multi_annMean'+filtered_on_target_years[0]+"-"+filtered_on_target_years[1]+'.csv')
        result_mnth.to_csv('/space/hall3/sitestore/eccc/crd/ccrn/users/rcw001/AMAP/csv_files/surface-o3-multi_monMean'+filtered_on_target_years[0]+"-"+filtered_on_target_years[1]+'.csv')  

if __name__ == "__main__":

    start = datetime.now()
    finish = datetime.now()
    print(finish-start)
    
    #for 2008-2009. without wref-chem and gem-mach. Update type as needed.  
    #o3_surface_compare_multi(type='Arctic',filtered_on_target_years=['2008','2009'],target_years=['2008', '2009'],models=['CESM','CMAM', 'DEHM','EMEP-MSC-W','GEOS-CHEM', 'GISS-E2.1', 'MATCH', 'MATCH-SALSA', 'MRI-ESM2', 'OsloCTM', 'UKESM1']) 
    
    #for 2014-2015 without gem-mach model. Update type as needed.    
    o3_surface_compare_multi(type='global',filtered_on_target_years=['2014','2015'],target_years=['2014', '2015'],models=['CESM','CMAM', 'DEHM','EMEP-MSC-W','GEOS-CHEM', 'GISS-E2.1', 'MATCH', 'MATCH-SALSA', 'MRI-ESM2', 'OsloCTM', 'UKESM1', 'WRF-Chem']) 
    
    #for only 2015 to get models including gem_mach. Update type as needed.
    
    #o3_surface_compare_multi(type='global',filtered_on_target_years=['2015'],target_years=['2014', '2015'],models=[ 'CESM','CMAM', 'DEHM','EMEP-MSC-W','GEM-MACH','GEOS-CHEM', 'GISS-E2.1','MATCH', 'MATCH-SALSA', 'MRI-ESM2', 'OsloCTM', 'UKESM1', 'WRF-Chem']) 
    finish = datetime.now()
    print(finish-start)
#, 'MRI-ESM2', 'OsloCTM', 'UKESM1', 'WRF-Chem'
