"""
tes_multi-model_co_plots.py
Author: Tahya Weiss-Gibbons
January 2020
tahya.weissgibbons@gmail.com

Updated March 2022 by Cyndi Whaley to better treat the missing data and to plot on the seas average instead of the annual avg.

Takes in a list of models and a specified time period and compares model data to TES ozone data in that time period.
"""
import os
import sys
import glob
import h5py
import re
import argparse
import numpy as np
import matplotlib.pyplot as plt
from datetime import date, datetime
from bisect import bisect_left
from netCDF4 import Dataset, num2date
from mpl_toolkits.basemap import Basemap

def cesm_model(seas=[6,7,8],years=['2014', '2015']):
    print("Reading in CESM data...")
    root_mdl ='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/CESM/'
    mdl_files = {'co': 'CESM_type0_co_'+years[0]+'_'+years[1]+'_3h.nc', 'pres': 'CESM_type0_pres_'+years[0]+'_'+years[1]+'_3h.nc'}

    #first lets read in the info from the co file
    mf = Dataset(root_mdl+mdl_files['co'], 'r')
    mdl_lon = np.array(mf.variables['lon'][:])
  #  mdl_lon = [ln+360 if ln<0 else ln for ln in mdl_lon]
    mdl_lat = np.array(mf.variables['lat'][:])
    try:
        mdl_lev = (mf.variables['lvl'][:]).tolist()
    except KeyError:
        mdl_lev = (mf.variables['lev'][:]).tolist()
    mdl_co = mf.variables['co'][:]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    #lets convert the time units
    local_time = num2date(mdl_time, units=time_units, calendar=time_cal)

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)
    nm_lev = len(mdl_lev)

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_pres = mf.variables['pres'][:]

    #check the units
    pres_units = mf.variables['pres'].getncattr('units')

    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf.close()

    #need to average both co and pressure
    mdl_mnth_co = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_pr_mnth = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))

    # just get seas (months 6,7,8)
    for t in local_time:
        if str(t.year) not in years:
            continue
        i = list(local_time).index(t)
        m = t.month
        if m in (seas):
            if m in (1,2):   
                mdl_mnth_co[m,:,:,:] = np.add(mdl_mnth_co[m, :,:,:], mdl_co[i,:,:,:])
                mdl_pr_mnth[m,:,:,:] = np.add(mdl_pr_mnth[m,:,:,:], mdl_pres[i,:,:,:])
                mnth_count[m,:,:,:] += 1                
            else:
                mdl_mnth_co[m-seas[0],:,:,:] = np.add(mdl_mnth_co[m-seas[0], :,:,:], mdl_co[i,:,:,:])
                mdl_pr_mnth[m-seas[0],:,:,:] = np.add(mdl_pr_mnth[m-seas[0],:,:,:], mdl_pres[i,:,:,:])
                mnth_count[m-seas[0],:,:,:] += 1

    mdl_mnth_co[:,:,:,:] = np.divide(mdl_mnth_co[:,:,:,:], mnth_count[:,:,:,:])
    mdl_pr_mnth[:,:,:,:] = np.divide(mdl_pr_mnth[:,:,:,:], mnth_count[:,:,:,:])
    return mdl_mnth_co, mdl_pr_mnth, local_time, mdl_lat, mdl_lon, nm_lev

def cmam_model(seas=[6,7,8],years=['2014', '2015']):
    print("Reading in CMAM data...")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/CMAM/'
    mdl_files = {'co': 'vmrco_month_CMAM_AMAP2020-SD_r1i1p1_199001-201812.nc'}

    #first lets read in the info from the co file
    mf = Dataset(root_mdl+mdl_files['co'], 'r')
    mdl_lon = np.array(mf.variables['lon'][:])
  #  mdl_lon = [ln+360 if ln<0 else ln for ln in mdl_lon]
    mdl_lat = np.array(mf.variables['lat'][:])
    mdl_lev = (mf.variables['lev'][:]).tolist()
    mdl_co = mf.variables['vmrco'][:]*(10**9) #convert the units

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    mdl_ap = mf.variables['ap'][:]
    mdl_b = mf.variables['b'][:]
    mdl_ps = mf.variables['ps'][:]

    mf.close()

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)
    nm_lev = len(mdl_lev)

    mdl_mnth_co = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_mnth_pres = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    pres_count = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #need to average both co and pressure
    mdl_mnth_co = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_pr_mnth = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    pres_count = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))

    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month
        if m in (seas):
            if m in (1,2):   
                mdl_mnth_co[m,:,:,:] = np.add(mdl_mnth_co[m, :,:,:], mdl_co[i,:,:,:])
                mnth_count[m,:,:,:] += 1
                for j in range(nm_mdl_lat):
                    for k in range(nm_mdl_lon):
                        tmp_pres = (mdl_ap + mdl_b*mdl_ps[i,j,k])*0.01 #convert to hpa
                        mdl_pr_mnth[m,:,j,k] = np.add(tmp_pres, mdl_pr_mnth[m,:,j,k])
                        pres_count[m,:,j,k] += 1
            else:
                mdl_mnth_co[m-seas[0],:,:,:] = np.add(mdl_mnth_co[m-seas[0], :,:,:], mdl_co[i,:,:,:])
                mnth_count[m-seas[0],:,:,:] += 1
                for j in range(nm_mdl_lat):
                    for k in range(nm_mdl_lon):
                        tmp_pres = (mdl_ap + mdl_b*mdl_ps[i,j,k])*0.01 #convert to hpa
                        mdl_pr_mnth[m-seas[0],:,j,k] = np.add(tmp_pres, mdl_pr_mnth[m-seas[0],:,j,k])
                        pres_count[m-seas[0],:,j,k] += 1

    mdl_mnth_co[:,:,:,:] = np.divide(mdl_mnth_co[:,:,:,:], mnth_count[:,:,:,:])
    mdl_pr_mnth[:,:,:,:] = np.divide(mdl_pr_mnth[:,:,:,:], pres_count[:,:,:,:])
    return mdl_mnth_co, mdl_pr_mnth, times, mdl_lat, mdl_lon, nm_lev

def dehm_model(seas=[6,7,8],years=['2014', '2015']):
    print("Reading in DEHM data....")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/DEHM/'
    mdl_files = {'co': 'DEHM_type0_co_1990_2018.nc', 'pres': 'DEHM_type0_pres_1990_2018.nc'}

    #first lets read in the info from the co file
    mf = Dataset(root_mdl+mdl_files['co'], 'r')
    mdl_lon = np.array(mf.variables['lon'][:])
  #  mdl_lon = [ln+360 if ln<0 else ln for ln in mdl_lon]
    mdl_lat = np.array(mf.variables['lat'][:])
    mdl_lev = (mf.variables['lev'][:]).tolist()
    mdl_co = mf.variables['co'][:]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    #lets convert the time units
    local_time = num2date(mdl_time, units=time_units, calendar=time_cal)

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)
    nm_lev = len(mdl_lev)

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_pres = mf.variables['pres'][:]

    #check the units
    pres_units = mf.variables['pres'].getncattr('units')

    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf.close()

    #need to average both co and pressure
    mdl_mnth_co = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_pr_mnth = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))

    for t in local_time:
        if str(t.year) not in years:
            continue
        i = list(local_time).index(t)
        m = t.month
        if m in (seas):
            if m in (1,2):   
                mdl_mnth_co[m,:,:,:] = np.add(mdl_mnth_co[m, :,:,:], mdl_co[i,:,:,:])
                mdl_pr_mnth[m,:,:,:] = np.add(mdl_pr_mnth[m,:,:,:], mdl_pres[i,:,:,:])
                mnth_count[m,:,:,:] += 1                
            else:
                mdl_mnth_co[m-seas[0],:,:,:] = np.add(mdl_mnth_co[m-seas[0], :,:,:], mdl_co[i,:,:,:])
                mdl_pr_mnth[m-seas[0],:,:,:] = np.add(mdl_pr_mnth[m-seas[0],:,:,:], mdl_pres[i,:,:,:])
                mnth_count[m-seas[0],:,:,:] += 1

    mdl_mnth_co[:,:,:,:] = np.divide(mdl_mnth_co[:,:,:,:], mnth_count[:,:,:,:])
    mdl_pr_mnth[:,:,:,:] = np.divide(mdl_pr_mnth[:,:,:,:], mnth_count[:,:,:,:])
    return mdl_mnth_co, mdl_pr_mnth, local_time, mdl_lat, mdl_lon, nm_lev


def emep_model(seas=[6,7,8],years=['2014', '2015']):
    print("Reading in emep data....")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/EMEP-MSCW/'
    mdl_files = {'co': 'EMEP-MSCW_tp0_v04_co_3hour_'+years[0]+'_'+years[1]+'.nc', 'pres': 'EMEP-MSCW_tp0_v04_ps_3hour_'+years[0]+'_'+years[1]+'.nc'}
    ab_file = '/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/EMEP-MSCW/emep_a_b.csv'
    
    #first lets read in the info from the co file
    mf = Dataset(root_mdl+mdl_files['co'], 'r')
    mdl_lon = np.array(mf.variables['lon'][:])
    mdl_lon = [ln+360 if ln<0 else ln for ln in mdl_lon]
    mdl_lat = np.array(mf.variables['lat'][:])
    try:
        mdl_lev = (mf.variables['lvl'][:]).tolist()
    except KeyError:
        mdl_lev = (mf.variables['lev'][:]).tolist()
    mdl_vmrco = mf.variables['co'][:]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
   # time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)
    nm_lev = len(mdl_lev)

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_ps = mf.variables['ps'][:]

    mf.close()

    #also need the a and b coordinates to calcualte the pressure at each level
    #info in csv in format lev a b
    #the pressure is from the formula a+b*ps
    with open(ab_file, 'r') as f:
        headers = f.readline()
        lines = f.readlines()

    mdl_a = []
    mdl_b =[]
    for r in lines:
        t = r.split(',')
        mdl_a.append(float(t[0].strip()))
        mdl_b.append(float(t[1].strip()))

    mdl_a = np.asarray(mdl_a)
    mdl_b = np.asarray(mdl_b)

    times = num2date(mdl_time, units=time_units, calendar='gregorian')

    mdl_mnth_co = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_mnth_pres = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    pres_count = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month
        if m in (seas):
            if m in (1,2):   
                mdl_mnth_co[m,:,:,:] = np.add(mdl_mnth_co[m, :,:,:], mdl_vmrco[i,:,:,:])
                mnth_count[m,:,:,:] += 1
                for j in range(nm_mdl_lat):
                    for k in range(nm_mdl_lon):
                        tmp = mdl_b*mdl_ps[i,j,k]
                        tmp_pres = (mdl_a + tmp)*0.01 #convert to hpa
                        mdl_mnth_pres[m,:,j,k] = np.add(tmp_pres, mdl_mnth_pres[m,:,j,k])
                        pres_count[m,:,j,k] += 1
            else:   
                mdl_mnth_co[m-seas[0],:,:,:] = np.add(mdl_mnth_co[m-seas[0], :,:,:], mdl_vmrco[i,:,:,:])
                mnth_count[m-seas[0],:,:,:] += 1
                for j in range(nm_mdl_lat):
                    for k in range(nm_mdl_lon):
                        tmp = mdl_b*mdl_ps[i,j,k]
                        tmp_pres = (mdl_a + tmp)*0.01 #convert to hpa
                        mdl_mnth_pres[m-seas[0],:,j,k] = np.add(tmp_pres, mdl_mnth_pres[m-seas[0],:,j,k])
                        pres_count[m-seas[0],:,j,k] += 1
			
    mdl_mnth_co[:,:,:,:] = np.divide(mdl_mnth_co[:,:,:,:] ,mnth_count[:,:,:,:])
    mdl_mnth_pres[:,:,:,:] = np.divide(mdl_mnth_pres[:,:,:,:], pres_count[:,:,:,:])
    
    return mdl_mnth_co, mdl_mnth_pres, times, mdl_lat, mdl_lon, nm_lev

def giss_model(seas=[6,7,8],years=['2014', '2015']):
    print("Reading in GISS-modelE-OMA data...")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/GISS-modelE-OMA/GISS_reformatted/'
    mdl_files = {'co': 'GISS-modelE-OMA_type0_co_NCEP_reformatted.nc', 'pres': 'GISS-modelE-OMA_type0_pres_NCEP_reformatted.nc'}

    #first lets read in the info from the co file
    mf = Dataset(root_mdl+mdl_files['co'], 'r')
    try:
        mdl_lon = np.array(mf.variables['lon'][:])
    except KeyError:
        mdl_lon = np.array(mf.variables['longitude'][:])
    try: 
        mdl_lat = np.array(mf.variables['lat'][:])
    except KeyError:
        mdl_lat = np.array(mf.variables['latitude'][:])
    try:
        mdl_lev = (mf.variables['lvl'][:]).tolist()
    except KeyError:
        mdl_lev = (mf.variables['lev'][:]).tolist()
    mdl_co = mf.variables['co'][:]
    mdl_lon = [ln+360 if ln<0 else ln for ln in mdl_lon]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()
    mdl_lon = np.array(mdl_lon)
    mdl_lon = [ln+360 if ln<0 else ln for ln in mdl_lon]
    #lets convert the time units
    local_time = num2date(mdl_time, units=time_units, calendar=time_cal)

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)
    nm_lev = len(mdl_lev)

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_pres = mf.variables['pres'][:]

    #check the units
    pres_units = mf.variables['pres'].getncattr('units')

    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf.close()

    #need to average both co and pressure
    mdl_mnth_co = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_pr_mnth = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))

    for t in local_time:
        if str(t.year) not in years:
            continue
        i = list(local_time).index(t)
        m = t.month
        if m in (seas):
            if m in (1,2):   
                mdl_mnth_co[m,:,:,:] = np.add(mdl_mnth_co[m, :,:,:], mdl_co[i,:,:,:])
                mdl_pr_mnth[m,:,:,:] = np.add(mdl_pr_mnth[m,:,:,:], mdl_pres[i,:,:,:])
                mnth_count[m,:,:,:] += 1                
            else:
                mdl_mnth_co[m-seas[0],:,:,:] = np.add(mdl_mnth_co[m-seas[0], :,:,:], mdl_co[i,:,:,:])
                mdl_pr_mnth[m-seas[0],:,:,:] = np.add(mdl_pr_mnth[m-seas[0],:,:,:], mdl_pres[i,:,:,:])
                mnth_count[m-seas[0],:,:,:] += 1

    mdl_mnth_co[:,:,:,:] = np.divide(mdl_mnth_co[:,:,:,:], mnth_count[:,:,:,:])
    mdl_pr_mnth[:,:,:,:] = np.divide(mdl_pr_mnth[:,:,:,:], mnth_count[:,:,:,:])
    return mdl_mnth_co, mdl_pr_mnth, local_time, mdl_lat, mdl_lon, nm_lev

def geoschem_model(seas=[6,7,8],years=['2014', '2015']):
    print("Reading in GEOS-Chem data...")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/GEOS-CHEM/'
    mdl_files = {'co': 'GEOS-CHEM_type0_co_'+years[0]+'_'+years[1]+'.nc', 'pres': 'GEOS-CHEM_type0_pres_2014_2015.nc'}

    #first lets read in the info from the co file
    mf = Dataset(root_mdl+mdl_files['co'], 'r')
    mdl_lon = np.array(mf.variables['lon'][:])
    mdl_lon = [ln+360 if ln<0 else ln for ln in mdl_lon]
    mdl_lat = np.array(mf.variables['lat'][:])
    mdl_vmrco = mf.variables['co'][:]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('long_name')
    time_cal = mf.variables['time'].getncattr('calendar')
    nm_lev = mf.dimensions['lev'].size

    mf.close()

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)

    mdl_mnth_co = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_mnth_pres = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    pmnth_count = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_pres = mf.variables['pres'][:]
    p_time = mf.variables['time'][:]
    ptime_units = mf.variables['time'].getncattr('long_name')
    ptime_cal = mf.variables['time'].getncattr('calendar')
    
    #check the units on the pressure
    pres_units = mf.variables['pres'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf.close()

    times = num2date(mdl_time, units=time_units, calendar=time_cal)
    ptimes = num2date(p_time, units=ptime_units, calendar=ptime_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month
        if m in (seas):
            if m in (1,2): 
                mdl_mnth_co[m,:,:,:] = np.add(mdl_vmrco[i,:,:,:], mdl_mnth_co[m,:,:,:])
                mnth_count[m,:,:,:] +=1
            else: 
                mdl_mnth_co[m-seas[0],:,:,:] = np.add(mdl_vmrco[i,:,:,:], mdl_mnth_co[m-seas[0],:,:,:])
                mnth_count[m-seas[0],:,:,:] +=1

    for t in ptimes:
        if str(t.year) not in years:
            continue
        i = list(ptimes).index(t) 
        m = t.month
        if m in (seas):
            if m in (1,2): 
                mdl_mnth_pres[m,:,:,:] = np.add(mdl_pres[i,:,:,:], mdl_mnth_pres[m,:,:,:])
                pmnth_count[m,:,:,:] +=1
            else: 
                mdl_mnth_pres[m-seas[0],:,:,:] = np.add(mdl_pres[i,:,:,:], mdl_mnth_pres[m-seas[0],:,:,:])
                pmnth_count[m-seas[0],:,:,:] +=1
		
    mdl_mnth_co[:,:,:,:] = np.divide(mdl_mnth_co[:,:,:,:] ,mnth_count[:,:,:,:])
    mdl_mnth_pres[:,:,:,:] = np.divide(mdl_mnth_pres[:,:,:,:], pmnth_count[:,:,:,:])
    return mdl_mnth_co, mdl_mnth_pres, times, mdl_lat, mdl_lon, nm_lev

def matchsalsa_model(seas=[6,7,8],years=['2014', '2015']):
    print("Reading in MATCH model data...")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/MATCH-SALSA/'																					      
    mdl_files = {'co': ['MATCH-SALSA_type0_co_'+years[0]+'.nc', 'MATCH-SALSA_type0_co_'+years[1]+'.nc'], 'pres': ['MATCH-SALSA_type0_pres_'+years[0]+'.nc', 'MATCH-SALSA_type0_pres_'+years[1]+'.nc']}
    match_ab_file = '/space/hall3/sitestore/eccc/crd/ccrn/users/sta109/AMAP_plots/tes_scripts/match-salsa_ab.csv'

    #first lets read in the info from the co file															          
    mf1 = Dataset(root_mdl+mdl_files['co'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['co'][1], 'r')															          
    mdl_lon = np.array(mf1.variables['lon'][:])															   
 #   mdl_lon = [ln+360 if ln<0 else ln for ln in mdl_lon]
    mdl_lat = np.array(mf1.variables['lat'][:])															          
    mdl_lev = (mf1.variables['lev'][:]).tolist()															          
    mdl_vmrco = np.concatenate((mf1.variables['co'][:], mf2.variables['co'][:]))											          
																					          
    mdl_time = np.concatenate((mf1.variables['time'][:], mf2.variables['time'][:]))															          
																					          
    mf1.close() 																			          
    mf2.close()
																					          
    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)																		          
    nm_lev = len(mdl_lev)																		         

    #now read in the data from the pressure file
    #info in csv in format lev a b
    #the pressure is from the formula a+b*ps
    with open(match_ab_file, 'r') as f:
        headers = f.readline()
        lines = f.readlines()

    mdl_a = []
    mdl_b =[]
    for r in lines:
        t = r.split('|')
        if(len(t) != 3): continue
        mdl_a.append(float(t[1].strip()))
        mdl_b.append(float(t[2].strip()))

    mdl_a = np.asarray(mdl_a)
    mdl_b = np.asarray(mdl_b)

    #now read in the surface pressure
    mf1 = Dataset(root_mdl+mdl_files['pres'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['pres'][1], 'r')

    mdl_ps = np.concatenate((mf1.variables['pres'][:], mf2.variables['pres'][:]))
    
 #   #check the units on the pressure 																	          
 #   pres_units = mf1.variables['pres'].getncattr('units')														          
 #   if pres_units == 'Pa' or pres_units == "kg m-1 s-2":														          
 #       mdl_pres = mdl_pres*0.01
    
    mf1.close()
    mf2.close()
 																					          
    mdl_mnth_co = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_mnth_pres = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    pres_count = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))	
																					          
    #now average over just the interested years 															          
    for t in mdl_time:																			              
        if str(t)[:4] not in years:																	        	  
            continue																			              
        i = list(mdl_time).index(t)																	              
        m = int(str(t)[4:6])
        if m in (seas):																			              
            if m in (1,2): 
                mdl_mnth_co[m,:,:,:] = np.add(mdl_mnth_co[m, :,:,:], mdl_vmrco[i,:,:,:])
                mnth_count[m,:,:,:] += 1
                for j in range(nm_mdl_lat):
                    for k in range(nm_mdl_lon):
                        tmp = mdl_b*mdl_ps[i,j,k]
                        tmp_pres = (mdl_a + tmp)*0.01 #convert to hpa
                        mdl_mnth_pres[m,:,j,k] = np.add(tmp_pres, mdl_mnth_pres[m,:,j,k])
                        pres_count[m,:,j,k] += 1																	        	  
            else: 
                mdl_mnth_co[m-seas[0],:,:,:] = np.add(mdl_mnth_co[m-seas[0], :,:,:], mdl_vmrco[i,:,:,:])
                mnth_count[m-seas[0],:,:,:] += 1
                for j in range(nm_mdl_lat):
                    for k in range(nm_mdl_lon):
                        tmp = mdl_b*mdl_ps[i,j,k]
                        tmp_pres = (mdl_a + tmp)*0.01 #convert to hpa
                        mdl_mnth_pres[m-seas[0],:,j,k] = np.add(tmp_pres, mdl_mnth_pres[m-seas[0],:,j,k])
                        pres_count[m-seas[0],:,j,k] += 1
																								        	      
    mdl_mnth_co[:,:,:,:] = np.divide(mdl_mnth_co[:,:,:,:] ,mnth_count[:,:,:,:]) 											        	      
    mdl_mnth_pres[:,:,:,:] = np.divide(mdl_mnth_pres[:,:,:,:], pres_count[:,:,:,:])											        	      
    return mdl_mnth_co, mdl_mnth_pres, mdl_time, mdl_lat, mdl_lon, nm_lev

def match_model(seas=[6,7,8],years=['2014', '2015']):
    print("Reading in MATCH model data...")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/MATCH/'
    mdl_files = {'co': ['MATCH_Type0_co_'+years[0]+'.nc', 'MATCH_Type0_co_'+years[1]+'.nc'], 'pres': ['MATCH_Type0_pres_'+years[0]+'.nc', 'MATCH_Type0_pres_'+years[1]+'.nc']}

    #first lets read in the info from the co file
    mf1 = Dataset(root_mdl+mdl_files['co'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['co'][1], 'r')
    mdl_lon = np.array(mf1.variables['lon'][:])
 #   mdl_lon = [ln+360 if ln<0 else ln for ln in mdl_lon]
    mdl_lat = np.array(mf1.variables['lat'][:])
    mdl_lev = (mf1.variables['lev'][:]).tolist()
    mdl_co = np.concatenate((mf1.variables['co'][:], mf2.variables['co'][:]))

    mdl_time = np.concatenate((mf1.variables['time'][:], mf2.variables['time'][:]))
    time_units = mf1.variables['time'].getncattr('units')
    time_cal = mf1.variables['time'].getncattr('calendar')

    mf1.close()
    mf2.close()

    #lets convert the time units
    local_time = num2date(mdl_time, units=time_units, calendar=time_cal)

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)
    nm_lev = len(mdl_lev)

    #now read in the data from the pressure file
    mf1 = Dataset(root_mdl+mdl_files['pres'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['pres'][1], 'r')
    mdl_pres = np.concatenate((mf1.variables['pres'][:], mf2.variables['pres'][:]))

    #check the units on the pressure 
    pres_units = mf1.variables['pres'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf1.close()
    mf2.close()

    #need to average both co and pressure
    mdl_mnth_co = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_pr_mnth = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))

    print("Monthly averaging for model data")

    for t in local_time:
        if str(t.year) not in years:
            continue
        i = list(local_time).index(t)
        m = t.month
        if m in (seas):
            if m in (1,2):   
                mdl_mnth_co[m,:,:,:] = np.add(mdl_mnth_co[m, :,:,:], mdl_co[i,:,:,:])
                mdl_pr_mnth[m,:,:,:] = np.add(mdl_pr_mnth[m,:,:,:], mdl_pres[i,:,:,:])
                mnth_count[m,:,:,:] += 1                
            else:
                mdl_mnth_co[m-seas[0],:,:,:] = np.add(mdl_mnth_co[m-seas[0], :,:,:], mdl_co[i,:,:,:])
                mdl_pr_mnth[m-seas[0],:,:,:] = np.add(mdl_pr_mnth[m-seas[0],:,:,:], mdl_pres[i,:,:,:])
                mnth_count[m-seas[0],:,:,:] += 1

    mdl_mnth_co[:,:,:,:] = np.divide(mdl_mnth_co[:,:,:,:], mnth_count[:,:,:,:])
    mdl_pr_mnth[:,:,:,:] = np.divide(mdl_pr_mnth[:,:,:,:], mnth_count[:,:,:,:])
    return mdl_mnth_co, mdl_pr_mnth, local_time, mdl_lat, mdl_lon, nm_lev

def mri_model(seas=[6,7,8],years=['2014', '2015']):
    print("Reading in MRI-ESM data...")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/MRI-ESM/'
    mdl_files = {'co': 'MRI-ESM_type0_co_'+years[0]+'-'+years[1]+'.nc', 'pres': 'MRI-ESM_type0_ps_T42L80_128x32NH_3hr_'+years[0]+'-'+years[1]+'.nc'}

    #first lets read in the info from the co file
    mf = Dataset(root_mdl+mdl_files['co'], 'r')
    mdl_lon = np.array(mf.variables['lon'][:])
 #   mdl_lon = [ln+360 if ln<0 else ln for ln in mdl_lon]
    mdl_lat = np.array(mf.variables['lat'][:])
    mdl_lev = (mf.variables['lev'][:]).tolist()
    mdl_co = mf.variables['co'][:]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    #lets convert the time units
    local_time = num2date(mdl_time, units=time_units, calendar=time_cal)

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)
    nm_lev = len(mdl_lev)

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_p0 = mf.variables['p0'][:]
    mdl_a = mf.variables['a'][:]
    mdl_b = mf.variables['b'][:]
    mdl_ps = mf.variables['ps'][:]

    mf.close()

    #need to average both co and pressure
    mdl_mnth_co = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_pr_mnth = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    pres_count = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))

    for t in local_time:
        if str(t.year) not in years:
            continue
        i = list(local_time).index(t)
        m = t.month
        if m in (seas):
            if m in (1,2): 
                mdl_mnth_co[m,:,:,:] = np.add(mdl_mnth_co[m, :,:,:], mdl_co[i,:,:,:])
                mnth_count[m,:,:,:] += 1
                for j in range(nm_mdl_lat):
                    for k in range(nm_mdl_lon):
                        tmp_pres = (mdl_a*mdl_p0 + mdl_b*mdl_ps[i,j,k])*0.01 #convert to hpa
                        mdl_pr_mnth[m,:,j,k] = np.add(tmp_pres, mdl_pr_mnth[m,:,j,k])
                        pres_count[m,:,j,k] += 1
            else: 
                mdl_mnth_co[m-seas[0],:,:,:] = np.add(mdl_mnth_co[m-seas[0], :,:,:], mdl_co[i,:,:,:])
                mnth_count[m-seas[0],:,:,:] += 1
                for j in range(nm_mdl_lat):
                    for k in range(nm_mdl_lon):
                        tmp_pres = (mdl_a*mdl_p0 + mdl_b*mdl_ps[i,j,k])*0.01 #convert to hpa
                        mdl_pr_mnth[m-seas[0],:,j,k] = np.add(tmp_pres, mdl_pr_mnth[m-seas[0],:,j,k])
                        pres_count[m-seas[0],:,j,k] += 1
			
    mdl_mnth_co[:,:,:,:] = np.divide(mdl_mnth_co[:,:,:,:], mnth_count[:,:,:,:])
    mdl_pr_mnth[:,:,:,:] = np.divide(mdl_pr_mnth[:,:,:,:], pres_count[:,:,:,:])
    return mdl_mnth_co, mdl_pr_mnth, local_time, mdl_lat, mdl_lon, nm_lev

def oslo_model(seas=[6,7,8],years=['2014', '2015']):
    print("Reading in OsloCTM model data...")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/OsloCTM/'
    mdl_files = {'co': ['OsloCTM_type0_co_monthly_'+years[0]+'.nc', 'OsloCTM_type0_co_monthly_'+years[1]+'.nc'], 'pres': ['OsloCTM_type0_pres_monthly_'+years[0]+'.nc', 'OsloCTM_type0_pres_monthly_'+years[1]+'.nc']}

    #first lets read in the info from the co file
    mf1 = Dataset(root_mdl+mdl_files['co'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['co'][1], 'r')
    mdl_lon = np.array(mf1.variables['lon'][:])
  #  mdl_lon = [ln+360 if ln<0 else ln for ln in mdl_lon]
    mdl_lat = np.array(mf1.variables['lat'][:])
    mdl_lev = (mf1.variables['lev'][:]).tolist()
    mdl_vmrco = np.concatenate((mf1.variables['co'][:], mf2.variables['co'][:]))

    mdl_time = np.concatenate((mf1.variables['time'][:], mf2.variables['time'][:]))
    #weren't specified in co files
    #taken from the pressure file, assuming the same
    time_units = "days since 2001-01-01 00:00:00"
    time_cal = "Julian"

    mf1.close()
    mf2.close()

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)
    nm_lev = len(mdl_lev)

    #now read in the data from the pressure file
    mf1 = Dataset(root_mdl+mdl_files['pres'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['pres'][1], 'r')
    mdl_pres = np.concatenate((mf1.variables['pres'][:], mf2.variables['pres'][:]))

    #check the units on the pressure 
    pres_units = mf1.variables['pres'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf1.close()
    mf2.close()

    mdl_mnth_co = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_mnth_pres = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    pres_count = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month
        if m in (seas):
            if m in (1,2):   
                mdl_mnth_co[m,:,:,:] = np.add(mdl_mnth_co[m, :,:,:], mdl_vmrco[i,:,:,:])
                mdl_mnth_pres[m,:,:,:] = np.add(mdl_mnth_pres[m,:,:,:], mdl_pres[i,:,:,:])
                mnth_count[m,:,:,:] += 1                
            else:
                mdl_mnth_co[m-seas[0],:,:,:] = np.add(mdl_mnth_co[m-seas[0], :,:,:], mdl_vmrco[i,:,:,:])
                mdl_mnth_pres[m-seas[0],:,:,:] = np.add(mdl_mnth_pres[m-seas[0],:,:,:], mdl_pres[i,:,:,:])
                mnth_count[m-seas[0],:,:,:] += 1

    mdl_mnth_co[:,:,:,:] = np.divide(mdl_mnth_co[:,:,:,:] ,mnth_count[:,:,:,:])
    mdl_mnth_pres[:,:,:,:] = np.divide(mdl_mnth_pres[:,:,:,:], mnth_count[:,:,:,:])
    return mdl_mnth_co, mdl_mnth_pres, times, mdl_lat, mdl_lon, nm_lev

def ukesm_model(seas=[6,7,8],years=['2014', '2015']):
    print("Reading in UKESM1 model data...")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/UKESM1/'
    mdl_files = {'co': 'UKESM1_type0_monthly_carbon_monoxide_volume_mixing_ratio_'+years[0]+'_'+years[1]+'.nc', 'pres': 'UKESM1_type0_monthly_air_pressure_'+years[0]+'_'+years[1]+'.nc'}

    #first lets read in the info from the co file
    mf = Dataset(root_mdl+mdl_files['co'], 'r')
    try:
        mdl_lon = np.array(mf.variables['lon'][:])
    except KeyError:
        mdl_lon = np.array(mf.variables['longitude'][:])
    try: 
        mdl_lat = np.array(mf.variables['lat'][:]).tolist()
    except KeyError:
        mdl_lat = np.array(mf.variables['latitude'][:])
    try:
        mdl_lev = (mf.variables['model_level_number'][:]).tolist()
    except KeyError:
        mdl_lev = (mf.variables['lev'][:]).tolist()
  #  mdl_lon = [ln+360 if ln<0 else ln for ln in mdl_lon]
    mdl_co = mf.variables['carbon_monoxide_volume_mixing_ratio'][:]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    #lets convert the time units
    local_time = num2date(mdl_time, units=time_units, calendar=time_cal)

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)
    nm_lev = len(mdl_lev)

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_pres = mf.variables['air_pressure'][:]

    #check the units
    pres_units = mf.variables['air_pressure'].getncattr('units')

    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf.close()

    #need to average both co and pressure
    mdl_mnth_co = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_pr_mnth = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))

    for t in local_time:
        if str(t.year) not in years:
            continue
        i = list(local_time).index(t)
        m = t.month
        if m in (seas):
            if m in (1,2):   
                mdl_mnth_co[m,:,:,:] = np.add(mdl_mnth_co[m, :,:,:], mdl_co[i,:,:,:])
                mdl_pr_mnth[m,:,:,:] = np.add(mdl_pr_mnth[m,:,:,:], mdl_pres[i,:,:,:])
                mnth_count[m,:,:,:] += 1                
            else:
                mdl_mnth_co[m-seas[0],:,:,:] = np.add(mdl_mnth_co[m-seas[0], :,:,:], mdl_co[i,:,:,:])
                mdl_pr_mnth[m-seas[0],:,:,:] = np.add(mdl_pr_mnth[m-seas[0],:,:,:], mdl_pres[i,:,:,:])
                mnth_count[m-seas[0],:,:,:] += 1

    mdl_mnth_co[:,:,:,:] = np.divide(mdl_mnth_co[:,:,:,:], mnth_count[:,:,:,:])
    mdl_pr_mnth[:,:,:,:] = np.divide(mdl_pr_mnth[:,:,:,:], mnth_count[:,:,:,:])
    return mdl_mnth_co, mdl_pr_mnth, local_time, mdl_lat, mdl_lon, nm_lev
    
def wrfchem_model(seas=[6,7,8],years=['2014', '2015']):
    print("Reading in WRF-CHEM model data...")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/WRF-CHEM/'
    mdl_files = {'co': 'WRF-CHEM_type0_co.nc', 'pres': 'WRF-CHEM_type0_pres.nc'}

    #first lets read in the info from the co file
    mf = Dataset(root_mdl+mdl_files['co'], 'r')
    mdl_lon = np.array(mf.variables['XLONG'][:])
 #   mdl_lon = [ln+360 if ln<0 else ln for ln in mdl_lon]
    mdl_lat = np.array(mf.variables['XLAT'][:])
    mdl_vmrco = mf.variables['co'][:]

    mdl_time = mf.variables['Times'][:]

    nm_mdl_lon = mf.dimensions['west_east'].size
    nm_mdl_lat = mf.dimensions['south_north'].size

    mf.close()

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_pres = mf.variables['pres'][:]
    nm_lev = mf.dimensions['bottom_top'].size

    #check the units on the pressure 
    pres_units = mf.variables['pres'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf.close()

    mdl_mnth_co = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_mnth_pres = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((3, nm_lev, nm_mdl_lat, nm_mdl_lon))

    #convert the times
    rep = {'b': '', "'": "", ' ': '', "\n": "", '[': "", ']': ""} #for formating strings
    rep = dict((re.escape(k), v) for k, v in rep.items())
    pattern = re.compile("|".join(rep.keys()))

    times = []
    for i in range(24):
        t = np.array2string(mdl_time[i])
        new_t = pattern.sub(lambda m: rep[re.escape(m.group(0))], t)
        #now convert to a datetime object
        d = datetime.strptime(new_t, '%Y-%m-%d_%H:%M:%S')
        times.append(d)
        

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month
        if m in (seas):
            if m in (1,2):   
                mdl_mnth_co[m,:,:,:] = np.add(mdl_mnth_co[m, :,:,:], mdl_vmrco[i,:,:,:])
                mdl_pr_mnth[m,:,:,:] = np.add(mdl_pr_mnth[m,:,:,:], mdl_pres[i,:,:,:])
                mnth_count[m,:,:,:] += 1                
            else:
                mdl_mnth_co[m-seas[0],:,:,:] = np.add(mdl_mnth_co[m-seas[0], :,:,:], mdl_vmrco[i,:,:,:])
                mdl_mnth_pres[m-seas[0],:,:,:] = np.add(mdl_mnth_pres[m-seas[0],:,:,:], mdl_pres[i,:,:,:])
                mnth_count[m-seas[0],:,:,:] += 1

    mdl_mnth_co[:,:,:,:] = np.divide(mdl_mnth_co[:,:,:,:] ,mnth_count[:,:,:,:])
    mdl_mnth_pres[:,:,:,:] = np.divide(mdl_mnth_pres[:,:,:,:], mnth_count[:,:,:,:])
    return mdl_mnth_co, mdl_mnth_pres, times, mdl_lat, mdl_lon, nm_lev

def multi_model_plots(seas=[6,7,8],models=['CMAM', 'DEHM', 'OsloCTM', 'UKESM'], years=['2014', '2015'], obs_dir='/space/hall3/sitestore/eccc/crd/ccrn/obs/amap/mopitt_month/'):

    plots_dir='/space/hall3/sitestore/eccc/crd/ccrn/users/rcw001/AMAP/png_figs/'
    plots_dir_eps='/space/hall3/sitestore/eccc/crd/ccrn/users/rcw001/AMAP/eps_figs/'
    
    np.seterr(divide='ignore', invalid='ignore') #we will divide by zero at some points, ignore all those warnings

    #set colours for the models for scatter plots
    colour_dict={'CESM': 'light blue', 'CanAM': 'orange', 'CMAM': 'gray', 'DEHM': 'dark blue', 'ECHAM-SALSA': 'maroon', 'EMEP-MSCW': 'green', 'GISS-E2.1': 'cyan', 'MATCH': 'dark green', 'MATCH-SALSA': 'blue', 'MRI-ESM2': 'red', 'NorESM': 'purple', 'OsloCTM': 'brown', 'UKESM1': 'magenta', 'WRF-CHEM': 'dark yellow'}

    #standardized 1x1 lat lon grid for all models and obs comparisons
    compare_lat = list(np.arange(-90., 91.))
    compare_lon = list(np.arange(0., 360.))
    nm_lat = len(compare_lat)
    nm_lon = len(compare_lon)

    #lets read in the observational data first
    print("Reading MOPITT files!")

    #pressure levels which the mopitt profile data is on (in hPa)
    pres_levels = [ 900.0, 800.0, 700.0, 600.0, 500.0, 400.0, 300.0, 200.0, 100.0]

    nmlon=360
    nmlat=180
    nmlev=len(pres_levels)

    #read in file with data file names

    #some working arrays
    obs_lon = np.zeros(nmlon)
    obs_lat = np.zeros(nmlat)

    ret_co_surface_day = np.zeros((3,nmlon,nmlat))
    ret_co_profile_day = np.zeros((3,nmlon,nmlat,nmlev))
    wrk_co_sur_ret = np.zeros((3,nmlon,nmlat))
    wrk_co_prf_ret = np.zeros((3,nmlon,nmlat,nmlev))

    apriori_co_surface = np.zeros((3,nmlon,nmlat))
    apriori_co_profile = np.zeros((3,nmlon,nmlat,nmlev))
    wrk_apr_co_sur = np.zeros((3,nmlon,nmlat))
    wrk_apr_co_prf = np.zeros((3,nmlon,nmlat,nmlev))

    avg_kernel = np.zeros((3, nmlon, nmlat, 10, 10))
    wrk_avg_kernel = np.zeros((3,nmlon,nmlat, 10, 10))
    surf_pres = np.zeros((3,nmlon,nmlat))
    wrk_surf_pres = np.zeros((3,nmlon,nmlat))
    mnth_count = np.zeros(3)

    data_files = glob.glob(obs_dir + "/*.he5")

    for data_file in data_files:
        try:
            f = h5py.File(data_file, 'r')
        except OSError:
            print(data_file)
            continue

        #extract the time from the data file name
        file_name = os.path.basename(data_file)
        time_obs = file_name.split('-')[1]
        time_obs = datetime.strptime(time_obs, "%Y%m")
        yr = time_obs.year
        mnth = time_obs.month
        ind = 0
        if mnth in (seas):
            if str(yr) not in years: continue
            # special condition for DJF (12,1,2) season
            if mnth in (1,2):   
                mnth_count[mnth] = mnth_count[mnth]+1

                obs_lat = f['HDFEOS']['GRIDS']['MOP03']['Data Fields']['Latitude'][:]
                obs_lon = f['HDFEOS']['GRIDS']['MOP03']['Data Fields']['Longitude'][:]

                wrk_co_sur_ret[mnth]= np.add(f['HDFEOS']['GRIDS']['MOP03']['Data Fields']['RetrievedCOSurfaceMixingRatioDay'][:], wrk_co_sur_ret[mnth])
                wrk_co_prf_ret[mnth] = np.add(f['HDFEOS']['GRIDS']['MOP03']['Data Fields']['RetrievedCOMixingRatioProfileDay'][:], wrk_co_prf_ret[mnth])
        
                wrk_apr_co_sur[mnth] = np.add(f['HDFEOS']['GRIDS']['MOP03']['Data Fields']['APrioriCOSurfaceMixingRatioDay'][:], wrk_apr_co_sur[mnth])
                wrk_apr_co_prf[mnth] = np.add(f['HDFEOS']['GRIDS']['MOP03']['Data Fields']['APrioriCOMixingRatioProfileDay'][:], wrk_apr_co_prf[mnth])

                wrk_avg_kernel[mnth] = np.add(f['HDFEOS']['GRIDS']['MOP03']['Data Fields']['RetrievalAveragingKernelMatrixDay'][:], wrk_avg_kernel[mnth])
                wrk_surf_pres[mnth] = np.add(f['HDFEOS']['GRIDS']['MOP03']['Data Fields']['SurfacePressureDay'][:], wrk_surf_pres[mnth])
            else:
                mnth_count[mnth-seas[0]] = mnth_count[mnth-seas[0]]+1

                obs_lat = f['HDFEOS']['GRIDS']['MOP03']['Data Fields']['Latitude'][:]
                obs_lon = f['HDFEOS']['GRIDS']['MOP03']['Data Fields']['Longitude'][:]

                wrk_co_sur_ret[mnth-seas[0]]= np.add(f['HDFEOS']['GRIDS']['MOP03']['Data Fields']['RetrievedCOSurfaceMixingRatioDay'][:], wrk_co_sur_ret[mnth-seas[0]])
                wrk_co_prf_ret[mnth-seas[0]] = np.add(f['HDFEOS']['GRIDS']['MOP03']['Data Fields']['RetrievedCOMixingRatioProfileDay'][:], wrk_co_prf_ret[mnth-seas[0]])
        
                wrk_apr_co_sur[mnth-seas[0]] = np.add(f['HDFEOS']['GRIDS']['MOP03']['Data Fields']['APrioriCOSurfaceMixingRatioDay'][:], wrk_apr_co_sur[mnth-seas[0]])
                wrk_apr_co_prf[mnth-seas[0]] = np.add(f['HDFEOS']['GRIDS']['MOP03']['Data Fields']['APrioriCOMixingRatioProfileDay'][:], wrk_apr_co_prf[mnth-seas[0]])

                wrk_avg_kernel[mnth-seas[0]] = np.add(f['HDFEOS']['GRIDS']['MOP03']['Data Fields']['RetrievalAveragingKernelMatrixDay'][:], wrk_avg_kernel[mnth-seas[0]])
                wrk_surf_pres[mnth-seas[0]] = np.add(f['HDFEOS']['GRIDS']['MOP03']['Data Fields']['SurfacePressureDay'][:], wrk_surf_pres[mnth-seas[0]])
        f.close()

    #wrk_co_sur_ret = np.ma.masked_values(wrk_co_sur_ret, -9999.)
    #wrk_co_prf_ret = np.ma.masked_values(wrk_co_prf_ret, -9999.)

    # set all -9999. to NaNs.
    wrk_co_sur_ret = np.where(wrk_co_sur_ret< -998., np.nan, wrk_co_sur_ret) 
    wrk_co_prf_ret = np.where(wrk_co_prf_ret< -998., np.nan, wrk_co_prf_ret) 
    # use the mask to mark the NaNs
    wrk_co_sur_ret = np.ma.array(wrk_co_sur_ret, mask=np.isnan(wrk_co_sur_ret))  
    wrk_co_prf_ret = np.ma.array(wrk_co_prf_ret, mask=np.isnan(wrk_co_prf_ret))  

    # Now for averaging kernel: set all -9999. to NaNs.
    wrk_avg_kernel = np.where(wrk_avg_kernel< -998., np.nan, wrk_avg_kernel) 
    # use the mask to mark the NaNs
    wrk_avg_kernel = np.ma.array(wrk_avg_kernel, mask=np.isnan(wrk_avg_kernel))  
    
    
    # compute the monthly avgs
    for m in range(3):
        ret_co_surface_day[m] = np.divide(wrk_co_sur_ret[m], mnth_count[m])
        ret_co_profile_day[m] = np.divide(wrk_co_prf_ret[m], mnth_count[m])

        apriori_co_surface[m] = np.divide(wrk_apr_co_sur[m], mnth_count[m])
        apriori_co_profile[m] = np.divide(wrk_apr_co_prf[m], mnth_count[m])

        avg_kernel[m] = np.divide(wrk_avg_kernel[m], mnth_count[m])
        surf_pres[m] = np.divide(wrk_surf_pres[m], mnth_count[m])

  #  print('before',ret_co_profile_day)
  #  ret_co_surface_day = np.ma.masked_where(-9999.,ret_co_surface_day)
  #  ret_co_profile_day = np.ma.masked_where(-9999.,ret_co_profile_day)
  #  print('ret_co_profile_day',ret_co_profile_day)
 
    #also need the seas average for mopitt
    seas_avg_mopitt_surface = np.nanmean(ret_co_surface_day, axis=0)
    seas_avg_mopitt_profile = np.nanmean(ret_co_profile_day, axis=0)
    
    print('avg_kernel at 4th vertical level:',avg_kernel[:,50:60,40:50,3])

 #   print(obs_lon) # mopitt longitudes are -180 to +180 originally.
    obs_lon = [ln+360 if ln<0  else ln for ln in obs_lon]
  #  print(obs_lon)
    
    #read in the data from each model, then interpolate
    multi_mdl_co = {}
    multi_mdl_dif = {}
    multi_mdl_seas = {}
    processed_models = []

    for m in models:
        if m == "CESM": 
            mdl_co, mdl_pres, mdl_times, mdl_lat, mdl_lon, mdl_lev = cesm_model(seas=seas,years=years)
        elif m == "CMAM": 
            mdl_co, mdl_pres, mdl_times, mdl_lat, mdl_lon, mdl_lev = cmam_model(seas=seas,years=years)
        elif m == "DEHM": 
            mdl_co, mdl_pres, mdl_times, mdl_lat, mdl_lon, mdl_lev = dehm_model(seas=seas,years=years)
        elif m == "EMEP-MSC-W": 
            mdl_co, mdl_pres, mdl_times, mdl_lat, mdl_lon, mdl_lev = emep_model(seas=seas,years=years)        
        elif m == "GEOS-Chem": 
            mdl_co, mdl_pres, mdl_times, mdl_lat, mdl_lon, mdl_lev = geoschem_model(seas=seas,years=years)
        elif m == "GISS-E2.1": 
            mdl_co, mdl_pres, mdl_times, mdl_lat, mdl_lon, mdl_lev = giss_model(seas=seas,years=years)
        elif m == "MATCH": 
            mdl_co, mdl_pres, mdl_times, mdl_lat, mdl_lon, mdl_lev = match_model(seas=seas,years=years)
        elif m == "MATCH-SALSA": 
            mdl_co, mdl_pres, mdl_times, mdl_lat, mdl_lon, mdl_lev = matchsalsa_model(seas=seas,years=years)
        elif m == "MRI-ESM2": 
            mdl_co, mdl_pres, mdl_times, mdl_lat, mdl_lon, mdl_lev = mri_model(seas=seas,years=years)
        elif m == "OsloCTM": 
            mdl_co, mdl_pres, mdl_times, mdl_lat, mdl_lon, mdl_lev = oslo_model(seas=seas,years=years)
        elif m == "UKESM1": 
            mdl_co, mdl_pres, mdl_times, mdl_lat, mdl_lon, mdl_lev = ukesm_model(seas=seas,years=years)
        elif m == "WRF-Chem": 
            mdl_co, mdl_pres, mdl_times, mdl_lat, mdl_lon, mdl_lev = wrfchem_model(seas=seas,years=years)
        else:
            print("Didn't recognize model "+m+". Can't read model files")
            continue

        #interpolate the vertical grid to the pressure levels of MOPITT
        print("Interpolating "+m)
        mdl_cp_co = np.ma.masked_all((3, nmlev+1, len(mdl_lat), len(mdl_lon)))
        obs_min_lat = np.min(obs_lat)
        obs_max_lat = np.max(obs_lat)
        obs_min_lon = np.min(obs_lon)
        obs_max_lon = np.max(obs_lon)
        for mt in range(3):
            for lt in range(len(mdl_lat)):
                for ln in range(len(mdl_lon)):
                  #  if (mdl_lat[lt]<obs_min_lat or mdl_lat[lt]>obs_max_lat) or (mdl_lon[ln]<obs_min_lon or mdl_lon[ln]>obs_max_lon): continue
                    #interpolate at the constant pressure levels
                    pr_levs = (mdl_pres[mt, :,lt,ln]).tolist()
                    co_levs = (mdl_co[mt,:,lt,ln]).tolist()
                    if m not in ("CESM","EMEP-MSC-W"):
                        pr_levs.reverse()
                        co_levs.reverse()
                    if m == "MATCH-SALSA":
                        pr_levs.reverse()  # pressure was ordered in opposite direction as O3.
                    cp_levs = np.interp(pres_levels, pr_levs, co_levs)

          #now interpolate horizontally onto mopitt horizontal grid
                    lt_obs = np.abs(obs_lat-mdl_lat[lt]).argmin()
                    ln_obs = np.abs(obs_lon-mdl_lon[ln]).argmin()
                  #  lt_obs = bisect_left(obs_lat, mdl_lat[lt])-1
                  #  ln_obs = bisect_left(obs_lon, mdl_lon[ln])-1                    
                    sp = surf_pres[mt, ln_obs, lt_obs]
                    co_sp = np.interp(sp, pr_levs, co_levs)
                    cpls = (cp_levs).tolist()
                    cpls.insert(0,co_sp)

                    mdl_cp_co[mnth,:,lt,ln] = cpls
        if np.all(np.isnan(mdl_cp_co)):
            print("Something wrong in vertical interpolation??? All nan for "+m)

	#smooth the model to the observational data
        #we're matching the model here to the observational grid
        print("Smoothing "+m)
        smooth_mdl_mnth = np.ma.masked_all((3, nmlev+1, len(obs_lat), len(obs_lon)))
        for i in range(nmlon):
            ln_tmp = obs_lon[i]
            ln = np.abs(ln_tmp-mdl_lon).argmin()	    
          #  ln = bisect_left(mdl_lon, ln_tmp)-1
            for j in range(nmlat):
                if m=='MATCH' or m=='DEHM' or m=='WRF-Chem' or m=='MATCH-SALSA':	
                    if obs_lat[j] < 0 :
                        smooth_mdl_mnth[:,:,j,i] = np.nan
                        continue
              #  lt = bisect_left(mdl_lat, obs_lat[j])-1
                lt = np.abs(obs_lat[j]-mdl_lat).argmin()
                for mt in range(3):
                    #Now taking the mdl point within the bounds, preform the smoothing operation
                    #Need to take log(vmr) for obs data
                    apr_total = np.insert(apriori_co_profile[mt,i,j], 0, apriori_co_surface[mt,i,j])
                    log_apr = np.log10(apr_total)
                    log_mdl = np.log10(mdl_cp_co[mt,:,lt,ln])
            
                    #mask the averaging kernel matrix - temporarily removed
                 #   mask_avg = np.ma.masked_where(avg_kernel[mt,i,j] < -10.0, avg_kernel[mt,i,j])
                    mask_avg = avg_kernel[mt,i,j]
                    smooth_mdl_mnth[mt,:,j,i] = log_apr+mask_avg.dot((log_mdl-log_apr))
                    smooth_mdl_mnth[mt,:,j,i] = np.power(10, smooth_mdl_mnth[mt,:,j,i])
        if np.all(np.isnan(smooth_mdl_mnth)):
            print("Smoothing didn't work??? All nan for "+m)
      #  # remove the smoothing for now:
      #  smooth_mdl_mnth = mdl_cp_co
        #the seas average
        seas_mean = np.nanmean(smooth_mdl_mnth, axis=0)

        multi_mdl_co[m] = smooth_mdl_mnth
        multi_mdl_seas[m] = seas_mean
        processed_models.append(m)


    ###------------Plotting-----------------

    #now should have all data from the models
    #lets figure out the plotting configuration
    num_plots = len(processed_models) + 1 #models plus one for the tes values
    num_cols = 4 #want 4 cols
    num_rows = int(num_plots / num_cols) 
    if (num_plots % num_cols) != 0: num_rows += 1
    print("Number of rows: "+str(num_rows))
    print("Number of plots: "+str(num_plots))

    fig = plt.figure(constrained_layout=True)

    compare_lat = list(np.arange(-90., 91.))
    compare_lon = list(np.arange(0., 360.))
    nm_lat = len(compare_lat)
    nm_lon = len(compare_lon)

    target_pres = [900.0, 600.0, 400.0] # [900.0, 600.0, 400.0]
   # lons, lats = np.meshgrid(obs_lon, obs_lat)
    lons, lats = np.meshgrid(compare_lon, compare_lat)
    test2 = np.transpose(seas_avg_mopitt_profile, [1,0,2])
    v_max = 200
    increment = 25
    for p in target_pres:
        i = pres_levels.index(p)
        #and now the seas average
        plt.clf()
        ax = fig.add_subplot(num_rows, num_cols, 1)
        ax.set_title("MOPITT",fontsize=7)
        obs_map = Basemap(projection='merc',llcrnrlat=0,urcrnrlat=85, llcrnrlon=-180,urcrnrlon=180,lat_ts=20,resolution='c')
        obs_map.drawmapboundary()
        obs_map.drawcoastlines()
        lons, lats = np.meshgrid(compare_lon, compare_lat)
     #   mask_obs = np.ma.masked_where(seas_avg_mopitt_profile[:,:,i] < 0.0, seas_avg_mopitt_profile[:,:,i])
        mask_obs = test2[:,:,i]
        pcm_mopitt = obs_map.pcolormesh(lons, lats, mask_obs, latlon=True, cmap='YlOrRd', vmin = 50, vmax = v_max) # mask_obs.T
        print('dimensions of obs=',mask_obs.shape)
        for c in range(2, num_plots+1):
            ax = fig.add_subplot(num_rows, num_cols, c)
            ax.set_title(processed_models[c-2],fontsize=7)
            mdl_map = Basemap(projection='merc',llcrnrlat=0,urcrnrlat=85, llcrnrlon=-180,urcrnrlon=180,lat_ts=20,resolution='c')
            mdl_map.drawmapboundary(fill_color='lightgrey')
            mdl_map.drawcoastlines()
            mdl_map.fillcontinents(color='lightgrey', zorder=0)
            print('model=',processed_models[c-2])
            print('dimensions of model=',multi_mdl_seas[processed_models[c-2]][i].shape)
            plot_dif = np.subtract(multi_mdl_seas[processed_models[c-2]][i], mask_obs) #multi_mdl_seas[processed_models[c-2]][i] - mask_obs # mask_obs.T
            pcm = mdl_map.pcolormesh(lons, lats, plot_dif, latlon=True, cmap='bwr', vmin=-150, vmax=150)
        cbaxes = fig.add_axes([0.02, 0.1, 0.01, 0.75])
        cbaxes2 = fig.add_axes([0.92, 0.1, 0.01, 0.75]) 
        fig.colorbar(pcm_mopitt, orientation='vertical',ticks=[t for t in range(50,v_max+1,increment)],cax=cbaxes,pad=0.2) 
        fig.colorbar(pcm, orientation='vertical',ticks=[-150, -100, -50, 0, 50, 100, 150],cax=cbaxes2,pad=0.2)
	
        fig.subplots_adjust(wspace=0)
        plt.savefig(plots_dir+"multi_mdl_mopitt_"+str(seas[0])+str(seas[1])+str(seas[2])+"_"+str(p)+"hPa_"+years[0]+"-"+years[1]+".png")
        plt.savefig(plots_dir_eps+"multi_mdl_mopitt_"+str(seas[0])+str(seas[1])+str(seas[2])+"_"+str(p)+"hPa_"+years[0]+"-"+years[1]+".eps")
            

if __name__ == "__main__":
 #   multi_model_plots(seas=[12,1,2],years =['2014', '2015'], models = ['CESM','CMAM', 'DEHM','OsloCTM', 'UKESM1'])
    multi_model_plots(seas=[3,4,5],years =['2014', '2015'], models = ['CESM','CMAM', 'DEHM','EMEP-MSC-W', 'GEOS-Chem', 'GISS-E2.1', 'MATCH', 'MATCH-SALSA', 'MRI-ESM2', 'OsloCTM', 'UKESM1','WRF-Chem'])
    #'CESM','CMAM', 'DEHM','EMEP-MSC-W', 'GEOS-CHEM', 'GISS-E2.1', 'MATCH', 'MATCH-SALSA', 'MRI-ESM2', 'OsloCTM', 'UKESM1',


    
