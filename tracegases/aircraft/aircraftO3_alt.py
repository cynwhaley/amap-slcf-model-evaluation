"""
aircraft.py
Based on TES program by
Author: Tahya Weiss-Gibbons
January 2020
tahya.weissgibbons@gmail.com
Updates by:
Author: Cyndi Whaley
Sept 2021
cynthia.whaley@ec.gc.ca

Takes in a list of models and a specified location and outputs the mmm for later comparison of model data to O3 aircraft.
"""
import os
import sys
import csv
import math
import argparse
import numpy as np
import pandas as pd
import statistics as st
import matplotlib
import matplotlib.pyplot as plt
from io import StringIO
import re
from datetime import date, datetime
from bisect import bisect_left
from netCDF4 import Dataset, num2date
from scipy.interpolate import interpn, interp2d
from mpl_toolkits.basemap import Basemap


def cesm_model(wc_lat=[74.6973],wc_lon=[-94.8297],years=['2014', '2015']):
    print("Reading in CESM data...")
    root_mdl ='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/CESM/'
    mdl_files = {'o3': 'CESM_type0_o3_'+years[0]+'_'+years[1]+'_3h.nc', 'pres': 'CESM_type0_ps_'+years[0]+'_'+years[1]+'_3h.nc', 'temp': 'CESM_type0_temp_'+years[0]+'_'+years[1]+'_3h.nc'}
    ab_file = '/space/hall3/sitestore/eccc/crd/ccrn/users/rcw001/AMAP/hyam_hybm_P0_CESM.nc'

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = np.array(mf.variables['lon'][:])
    mdl_lat = np.array(mf.variables['lat'][:])
    
    # determine the indices of the given wc_lat and wc_lon
    if wc_lon[0] < 0:
        wc_lon[0]= wc_lon[0]+360
        wc_lon[1]= wc_lon[1]+360
    latind1=np.abs(wc_lat[0]-mdl_lat).argmin()
    lonind1=np.abs(wc_lon[0]-mdl_lon).argmin()
    latind2=np.abs(wc_lat[1]-mdl_lat).argmin()
    lonind2=np.abs(wc_lon[1]-mdl_lon).argmin()
    
    try:
        mdl_lev = (mf.variables['lvl'][:]).tolist()
    except KeyError:
        mdl_lev = (mf.variables['lev'][:]).tolist()

    mdl_vmro3 = mf.variables['o3'][:,:,latind1:latind2,lonind1:lonind2]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    nm_lev = len(mdl_lev)
    nm_year = 2 

    # get a & b values to calculate 3D pressure
    mf = Dataset(ab_file,'r')
    mdl_ap = mf.variables['hyam'][:]
    mdl_b = mf.variables['hybm'][:]
    mf.close()
    
    # get surface pressure
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_ps = mf.variables['ps'][:,latind1:latind2,lonind1:lonind2]
    mf.close()

    # get temperature
    mf = Dataset(root_mdl+mdl_files['temp'], 'r')
    mdl_temp = mf.variables['temp'][:,:,latind1:latind2,lonind1:lonind2]
    mf.close()
    
    # now determine the mean O3, pres, and temp across the lat and lon range:
    mdl_temp = np.nanmean(mdl_temp,axis=2) # mean across lat
    mdl_temp = np.nanmean(mdl_temp,axis=2) # mean across lon
    mdl_ps = np.nanmean(mdl_ps,axis=1) # mean across lat
    mdl_ps = np.nanmean(mdl_ps,axis=1) # mean across lon
    mdl_vmro3 = np.nanmean(mdl_vmro3,axis=2) # mean across lat
    mdl_vmro3 = np.nanmean(mdl_vmro3,axis=2) # mean across lon


    # calculate the vertical profile of modelled pressure:
    mdl_pres = np.zeros((len(mdl_time),nm_lev))
    for i in range(len(mdl_time)):
        mdl_pres[i,:] = (mdl_ap*100000 + mdl_b*mdl_ps[i])*0.01 #convert from Pa to hPa

    mdl_mnth_o3 = np.zeros((nm_year, 12, nm_lev))
    mdl_mnth_pres = np.zeros((nm_year, 12, nm_lev))
    mdl_mnth_temp = np.zeros((nm_year, 12, nm_lev))
    mnth_count = np.zeros((nm_year, 12, nm_lev))

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year-int(years[0])
        mdl_mnth_o3[y,m,:] = np.add(mdl_vmro3[i,:], mdl_mnth_o3[y,m,:])
        mdl_mnth_pres[y,m,:] = np.add(mdl_pres[i,:], mdl_mnth_pres[y,m,:])
        mdl_mnth_temp[y,m,:] = np.add(mdl_temp[i,:], mdl_mnth_temp[y,m,:])
        mnth_count[y,m,:] +=1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:] ,mnth_count[:,:,:])
    mdl_mnth_pres[:,:,:] = np.divide(mdl_mnth_pres[:,:,:], mnth_count[:,:,:])
    mdl_mnth_temp[:,:,:] = np.divide(mdl_mnth_temp[:,:,:], mnth_count[:,:,:])
    
    return mdl_mnth_o3, mdl_mnth_pres, mdl_mnth_temp, times, nm_lev

def cmam_model(wc_lat=[74.6973],wc_lon=[-94.8297],years=['2014', '2015']):
    print("Reading in CMAM data...")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/CMAM/'
    mdl_files = {'o3': 'vmro3_month_CMAM_AMAP2020-SD_r1i1p1_199001-201812.nc','temp': 'ta_month_CMAM_AMAP2020-SD_r1i1p1_199001-201812.nc'}

    #first lets read in the info from the co file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = np.array(mf.variables['lon'][:])
    mdl_lat = np.array(mf.variables['lat'][:])
    
    # determine the indices of the given wc_lat and wc_lon
    if wc_lon[0] < 0:
        wc_lon[0]= wc_lon[0]+360
        wc_lon[1]= wc_lon[1]+360
    latind1=np.abs(wc_lat[0]-mdl_lat).argmin()
    lonind1=np.abs(wc_lon[0]-mdl_lon).argmin()
    latind2=np.abs(wc_lat[1]-mdl_lat).argmin()
    lonind2=np.abs(wc_lon[1]-mdl_lon).argmin()
    
    mdl_lev = (mf.variables['lev'][:]).tolist()
    mdl_vmro3 = mf.variables['vmro3'][:,:,latind1:latind2,lonind1:lonind2]
    co_units = mf.variables['vmro3'].getncattr('units')

    if co_units == "mole mole-1": #equivalent to volume ozone per volume air from ideal gas law
        mdl_vmro3 = mdl_vmro3*(10**9) #convert to ppbv

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    #need to find pressure from formula p = ap + b*ps at every level
    mdl_ap = mf.variables['ap'][:]
    mdl_b = mf.variables['b'][:]
    mdl_ps = mf.variables['ps'][:,latind1:latind2,lonind1:lonind2]

    mf.close()

    # get temperature
    mf = Dataset(root_mdl+mdl_files['temp'], 'r')
    mdl_temp = mf.variables['ta'][:,:,latind1:latind2,lonind1:lonind2]
    mf.close()

    # now determine the mean O3, pres, and temp across the lat and lon range:
    mdl_temp = np.nanmean(mdl_temp,axis=2) # mean across lat
    mdl_temp = np.nanmean(mdl_temp,axis=2) # mean across lon
    mdl_ps = np.nanmean(mdl_ps,axis=1) # mean across lat
    mdl_ps = np.nanmean(mdl_ps,axis=1) # mean across lon
    mdl_vmro3 = np.nanmean(mdl_vmro3,axis=2) # mean across lat
    mdl_vmro3 = np.nanmean(mdl_vmro3,axis=2) # mean across lon

    nm_lev = len(mdl_lev)
    nm_year = 2

    mdl_mnth_o3 = np.zeros((nm_year,12, nm_lev))
    mdl_mnth_pres = np.zeros((nm_year,12, nm_lev))
    mdl_mnth_temp = np.zeros((nm_year,12, nm_lev))
    mnth_count = np.zeros((nm_year,12, nm_lev))
    pres_count = np.zeros((nm_year,12, nm_lev))
    temp_count = np.zeros((nm_year,12, nm_lev))

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year-int(years[0])
        mdl_mnth_o3[y,m,:] = np.add(mdl_vmro3[i,:], mdl_mnth_o3[y,m,:])
        mnth_count[y,m,:] +=1
        mdl_mnth_temp[y,m,:] = np.add(mdl_temp[i,:], mdl_mnth_temp[y,m,:])
        temp_count[y,m,:] +=1	
        tmp_pres = (mdl_ap + mdl_b*mdl_ps[i])*0.01 #convert to hpa
        mdl_mnth_pres[y,m,:] = np.add(tmp_pres, mdl_mnth_pres[y,m,:])
        pres_count[y,m,:] += 1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:], mnth_count[:,:,:])
    mdl_mnth_pres[:,:,:] = np.divide(mdl_mnth_pres[:,:,:], pres_count[:,:,:])
    mdl_mnth_temp[:,:,:] = np.divide(mdl_mnth_temp[:,:,:], temp_count[:,:,:])

    return mdl_mnth_o3, mdl_mnth_pres, mdl_mnth_temp, times, nm_lev

def dehm_model(wc_lat=[74.6973],wc_lon=[-94.8297],years=['2014', '2015']):
    print("Reading in DEHM data....")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/DEHM/'
    mdl_files = {'o3': 'DEHM_type0_o3_1990_2018.nc', 'pres': 'DEHM_type0_pres_1990_2018.nc', 'temp':'DEHM_type0_temp_1990_2018.nc'}

    #first lets read in the info from the co file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = np.array(mf.variables['lon'][:])
    mdl_lat = np.array(mf.variables['lat'][:])
    try:
        mdl_lev = (mf.variables['lvl'][:]).tolist()
    except KeyError:
        mdl_lev = (mf.variables['lev'][:]).tolist()

    # determine the indices of the given wc_lat and wc_lon
    if wc_lon[0] < 0:
        wc_lon[0]= wc_lon[0]+360
        wc_lon[1]= wc_lon[1]+360
    latind1=np.abs(wc_lat[0]-mdl_lat).argmin()
    lonind1=np.abs(wc_lon[0]-mdl_lon).argmin()
    latind2=np.abs(wc_lat[1]-mdl_lat).argmin()
    lonind2=np.abs(wc_lon[1]-mdl_lon).argmin()
    
    mdl_vmro3 = mf.variables['o3'][:,:,latind1:latind2,lonind1:lonind2]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_pres = mf.variables['pres'][:,:,latind1:latind2,lonind1:lonind2]

    #check the units on the pressure 
    pres_units = mf.variables['pres'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf.close()

    # get temperature
    mf = Dataset(root_mdl+mdl_files['temp'], 'r')
    mdl_temp = mf.variables['temp'][:,:,latind1:latind2,lonind1:lonind2]
    mf.close()

    nm_lev = len(mdl_lev)
    nm_year = 2
    
    # now determine the mean O3, pres, and temp across the lat and lon range:
    mdl_temp = np.nanmean(mdl_temp,axis=2) # mean across lat
    mdl_temp = np.nanmean(mdl_temp,axis=2) # mean across lon
    mdl_pres = np.nanmean(mdl_pres,axis=2) # mean across lat
    mdl_pres = np.nanmean(mdl_pres,axis=2) # mean across lon
    mdl_vmro3 = np.nanmean(mdl_vmro3,axis=2) # mean across lat
    mdl_vmro3 = np.nanmean(mdl_vmro3,axis=2) # mean across lon

    mdl_mnth_o3 = np.zeros((nm_year,12, nm_lev))
    mdl_mnth_pres = np.zeros((nm_year,12, nm_lev))
    mdl_mnth_temp = np.zeros((nm_year,12, nm_lev))
    mnth_count = np.zeros((nm_year,12, nm_lev))

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year-int(years[0])
        mdl_mnth_o3[y,m,:] = np.add(mdl_vmro3[i,:], mdl_mnth_o3[y,m,:])
        mdl_mnth_pres[y,m,:] = np.add(mdl_pres[i,:], mdl_mnth_pres[y,m,:])
        mdl_mnth_temp[y,m,:] = np.add(mdl_temp[i,:], mdl_mnth_temp[y,m,:])
        mnth_count[y,m,:] +=1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:] ,mnth_count[:,:,:])
    mdl_mnth_pres[:,:,:] = np.divide(mdl_mnth_pres[:,:,:], mnth_count[:,:,:])
    mdl_mnth_temp[:,:,:] = np.divide(mdl_mnth_temp[:,:,:], mnth_count[:,:,:])

    return mdl_mnth_o3, mdl_mnth_pres, mdl_mnth_temp, times, nm_lev

def emep_model(wc_lat=[74.6973],wc_lon=[-94.8297],years=['2014', '2015']):
    print("Reading in emep data....")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/EMEP-MSCW/'
    mdl_files = {'o3': 'EMEP-MSCW_tp0_v04_o3_3hour_'+years[0]+'_'+years[1]+'.nc', 'pres': 'EMEP-MSCW_tp0_v04_ps_3hour_'+years[0]+'_'+years[1]+'.nc', 'temp': 'EMEP-MSCW_tp0_v03_temp_2014_2015.nc'}
    ab_file = '/space/hall3/sitestore/eccc/crd/ccrn/users/sta109/AMAP_plots/tes_scripts/emep_a_b.csv'
    
    #first lets read in the info from the co file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = np.array(mf.variables['lon'][:])
    mdl_lat = np.array(mf.variables['lat'][:])
    try:
        mdl_lev = (mf.variables['lvl'][:]).tolist()
    except KeyError:
        mdl_lev = (mf.variables['lev'][:]).tolist()
    
    # determine the indices of the given wc_lat and wc_lon
    if wc_lon[0] < 0:
        wc_lon[0]= wc_lon[0]+360
        wc_lon[1]= wc_lon[1]+360
    latind1=np.abs(wc_lat[0]-mdl_lat).argmin()
    lonind1=np.abs(wc_lon[0]-mdl_lon).argmin()
    latind2=np.abs(wc_lat[1]-mdl_lat).argmin()
    lonind2=np.abs(wc_lon[1]-mdl_lon).argmin()
    print('lat and lon indices are:',latind1,latind2,lonind1,lonind2)
    
    mdl_vmro3 = mf.variables['o3'][:,:,latind1:latind2,lonind1:lonind2]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
   # time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    # get temperature and its own time (ttime), which is monthly
    mf = Dataset(root_mdl+mdl_files['temp'], 'r')
    mdl_temp = mf.variables['temp'][:,:,latind1:latind2,lonind1:lonind2]
    mdl_ttime = mf.variables['time'][:]
    ttime_units = mf.variables['time'].getncattr('units')
    mf.close()

    nm_year = 2
    nm_lev = len(mdl_lev)

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_ps = mf.variables['ps'][:,latind1:latind2,lonind1:lonind2]

    mf.close()

    # now determine the mean O3, pres, and temp across the lat and lon range:
    mdl_temp = np.nanmean(mdl_temp,axis=2) # mean across lat
    mdl_temp = np.nanmean(mdl_temp,axis=2) # mean across lon
    mdl_ps = np.nanmean(mdl_ps,axis=1) # mean across lat
    mdl_ps = np.nanmean(mdl_ps,axis=1) # mean across lon
    mdl_vmro3 = np.nanmean(mdl_vmro3,axis=2) # mean across lat
    mdl_vmro3 = np.nanmean(mdl_vmro3,axis=2) # mean across lon

    #also need the a and b coordinates to calcualte the pressure at each level
    #info in csv in format lev a b
    #the pressure is from the formula a+b*ps
    with open(ab_file, 'r') as f:
        headers = f.readline()
        lines = f.readlines()

    mdl_a = []
    mdl_b =[]
    for r in lines:
        t = r.split(',')
        mdl_a.append(float(t[0].strip()))
        mdl_b.append(float(t[1].strip()))

    mdl_a = np.asarray(mdl_a)
    mdl_b = np.asarray(mdl_b)

    times = num2date(mdl_time, units=time_units, calendar='gregorian')
    ttimes = num2date(mdl_ttime, units=ttime_units, calendar='gregorian')

    mdl_mnth_o3 = np.zeros((nm_year, 12, nm_lev))
    mdl_mnth_pres = np.zeros((nm_year, 12, nm_lev))
    mdl_mnth_temp = np.zeros((nm_year, 12, nm_lev))
    mnth_count = np.zeros((nm_year,12, nm_lev))
    pres_count = np.zeros((nm_year,12, nm_lev))
    temp_count = np.zeros((nm_year,12, nm_lev))

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year-int(years[0])
        mdl_mnth_o3[y,m,:] = np.add(mdl_mnth_o3[y,m,:], mdl_vmro3[i,:])
        mnth_count[y,m,:] += 1
        tmp = mdl_b*mdl_ps[i]
        tmp_pres = (mdl_a + tmp)*0.01 #convert to hpa
        mdl_mnth_pres[y,m,:] = np.add(tmp_pres, mdl_mnth_pres[y,m,:])
        pres_count[y,m,:] += 1

    for t in ttimes:
        if str(t.year) not in years:
            continue
        i = list(ttimes).index(t)
        m = t.month-1
        yr = t.year
        y = t.year-int(years[0])
        mdl_mnth_temp[y,m,:] = np.add(mdl_mnth_temp[y,m,:], mdl_temp[i,:])
        temp_count[y,m,:] += 1
	
    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:] ,mnth_count[:,:,:])
    mdl_mnth_pres[:,:,:] = np.divide(mdl_mnth_pres[:,:,:], pres_count[:,:,:])
    mdl_mnth_temp[:,:,:] = np.divide(mdl_mnth_temp[:,:,:], temp_count[:,:,:])
    
    return mdl_mnth_o3, mdl_mnth_pres, mdl_mnth_temp, times, nm_lev
    

def geoschem_model(wc_lat=[74.6973],wc_lon=[-94.8297],years=['2014', '2015']):
    print("Reading in GEOS-Chem data...")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/GEOS-CHEM/'
    mdl_files = {'o3': 'GEOS-CHEM_type0_o3_'+years[0]+'_'+years[1]+'.nc', 'pres': 'GEOS-CHEM_type0_pres_'+years[0]+'_'+years[1]+'.nc', 'temp': 'GEOS-CHEM_type0_temp_'+years[0]+'_'+years[1]+'.nc'}

    #first lets read in the info from the co file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = np.array(mf.variables['lon'][:])
    mdl_lon = [ln+360 if ln<0 else ln for ln in mdl_lon]
    mdl_lon = np.array(mdl_lon)
    mdl_lat = np.array(mf.variables['lat'][:])
    
    # determine the indices of the given wc_lat and wc_lon
    if wc_lon[0] < 0:
        wc_lon[0]= wc_lon[0]+360
        wc_lon[1]= wc_lon[1]+360
    latind1=np.abs(wc_lat[0]-mdl_lat).argmin()
    lonind1=np.abs(wc_lon[0]-mdl_lon).argmin()
    latind2=np.abs(wc_lat[1]-mdl_lat).argmin()
    lonind2=np.abs(wc_lon[1]-mdl_lon).argmin()
    
    mdl_vmro3 = mf.variables['o3'][:,:,latind1:latind2,lonind1:lonind2]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('long_name')
    time_cal = mf.variables['time'].getncattr('calendar')
    nm_lev = mf.dimensions['lev'].size

    mf.close()
    
    nm_year = 2

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_pres = mf.variables['pres'][:,:,latind1:latind2,lonind1:lonind2]
    p_time = mf.variables['time'][:]
    ptime_units = mf.variables['time'].getncattr('long_name')
    ptime_cal = mf.variables['time'].getncattr('calendar')
    
    #check the units on the pressure
    pres_units = mf.variables['pres'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf.close()

    # get temperature
    mf = Dataset(root_mdl+mdl_files['temp'], 'r')
    mdl_temp = mf.variables['temp'][:,:,latind1:latind2,lonind1:lonind2]
    mf.close()

    # now determine the mean O3, pres, and temp across the lat and lon range:
    mdl_temp = np.nanmean(mdl_temp,axis=2) # mean across lat
    mdl_temp = np.nanmean(mdl_temp,axis=2) # mean across lon
    mdl_pres = np.nanmean(mdl_pres,axis=2) # mean across lat
    mdl_pres = np.nanmean(mdl_pres,axis=2) # mean across lon
    mdl_vmro3 = np.nanmean(mdl_vmro3,axis=2) # mean across lat
    mdl_vmro3 = np.nanmean(mdl_vmro3,axis=2) # mean across lon

    times = num2date(mdl_time, units=time_units, calendar=time_cal)
    ptimes = num2date(p_time, units=ptime_units, calendar=ptime_cal)

    mdl_mnth_o3 = np.zeros((nm_year, 12, nm_lev))
    mdl_mnth_pres = np.zeros((nm_year, 12, nm_lev))
    mdl_mnth_temp = np.zeros((nm_year, 12, nm_lev))
    mnth_count = np.zeros((nm_year, 12, nm_lev))
    pmnth_count = np.zeros((nm_year, 12, nm_lev))

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year-int(years[0])
        mdl_mnth_o3[y,m,:] = np.add(mdl_vmro3[i,:], mdl_mnth_o3[y,m,:])
        mnth_count[y,m,:] +=1

    for t in ptimes:
        if str(t.year) not in years:
            continue
        i = list(ptimes).index(t) 
        m = t.month - 1
        yr = t.year
        y = t.year-int(years[0])
        mdl_mnth_pres[y,m,:] = np.add(mdl_pres[i,:], mdl_mnth_pres[y,m,:])
        mdl_mnth_temp[y,m,:] = np.add(mdl_temp[i,:], mdl_mnth_temp[y,m,:])
        pmnth_count[y,m,:] +=1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:] ,mnth_count[:,:,:])
    mdl_mnth_pres[:,:,:] = np.divide(mdl_mnth_pres[:,:,:], pmnth_count[:,:,:])
    mdl_mnth_temp[:,:,:] = np.divide(mdl_mnth_temp[:,:,:], pmnth_count[:,:,:])

    return mdl_mnth_o3, mdl_mnth_pres, mdl_mnth_temp, times, nm_lev
    
def giss_model(wc_lat=[74.6973],wc_lon=[-94.8297],years=['2014', '2015']):
    print("Reading in GISS-E2.1 data...")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/GISS-modelE-OMA/GISS_reformatted/'
    mdl_files = {'o3': 'GISS-modelE-OMA_type0_o3_NCEP_reformatted.nc', 'pres': 'GISS-modelE-OMA_type0_pres_NCEP_reformatted.nc','temp':'GISS-modelE-OMA_type0_temp_NCEP_reformatted.nc'}

    #first lets read in the info from the co file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = np.array(mf.variables['lon'][:])
    mdl_lat = np.array(mf.variables['lat'][:])
    try:
        mdl_lev = (mf.variables['lvl'][:]).tolist()
    except KeyError:
        mdl_lev = (mf.variables['lev'][:]).tolist()

    # determine the indices of the given wc_lat and wc_lon
    if wc_lon[0] < 0:
        wc_lon[0]= wc_lon[0]+360
        wc_lon[1]= wc_lon[1]+360
    latind1=np.abs(wc_lat[0]-mdl_lat).argmin()
    lonind1=np.abs(wc_lon[0]-mdl_lon).argmin()
    latind2=np.abs(wc_lat[1]-mdl_lat).argmin()
    lonind2=np.abs(wc_lon[1]-mdl_lon).argmin()
    print('lat and lon indices are:',latind1,latind2,lonind1,lonind2)
    
    mdl_vmro3 = mf.variables['o3'][:,:,latind1:latind2,lonind1:lonind2]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    nm_year = 2
    nm_lev = len(mdl_lev)

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_pres = mf.variables['pres'][:,:,latind1:latind2,lonind1:lonind2]

    #check the units on the pressure 
    pres_units = mf.variables['pres'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf.close()

    # get temperature
    mf = Dataset(root_mdl+mdl_files['temp'], 'r')
    mdl_temp = mf.variables['temp'][:,:,latind1:latind2,lonind1:lonind2]
    mf.close()

    # now determine the mean O3, pres, and temp across the lat and lon range:
    mdl_temp = np.nanmean(mdl_temp,axis=2) # mean across lat
    mdl_temp = np.nanmean(mdl_temp,axis=2) # mean across lon
    mdl_pres = np.nanmean(mdl_pres,axis=2) # mean across lat
    mdl_pres = np.nanmean(mdl_pres,axis=2) # mean across lon
    mdl_vmro3 = np.nanmean(mdl_vmro3,axis=2) # mean across lat
    mdl_vmro3 = np.nanmean(mdl_vmro3,axis=2) # mean across lon

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    mdl_mnth_o3 = np.zeros((nm_year, 12, nm_lev))
    mdl_mnth_pres = np.zeros((nm_year, 12, nm_lev))
    mdl_mnth_temp = np.zeros((nm_year, 12, nm_lev))
    mnth_count = np.zeros((nm_year, 12, nm_lev))

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year-int(years[0])
        mdl_mnth_o3[y,m,:] = np.add(mdl_vmro3[i,:], mdl_mnth_o3[y,m,:])
        mdl_mnth_pres[y,m,:] = np.add(mdl_pres[i,:], mdl_mnth_pres[y,m,:])
        mdl_mnth_temp[y,m,:] = np.add(mdl_temp[i,:], mdl_mnth_temp[y,m,:])
        mnth_count[y,m,:] +=1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:] ,mnth_count[:,:,:])
    mdl_mnth_pres[:,:,:] = np.divide(mdl_mnth_pres[:,:,:], mnth_count[:,:,:])
    mdl_mnth_temp[:,:,:] = np.divide(mdl_mnth_temp[:,:,:], mnth_count[:,:,:])

    return mdl_mnth_o3, mdl_mnth_pres, mdl_mnth_temp, times, nm_lev

def match_model(wc_lat=[74.6973],wc_lon=[-94.8297],years=['2014', '2015']):
    print("Reading in MATCH model data...")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/MATCH/'
    mdl_files = {'o3': ['MATCH_Type0_o3_'+years[0]+'.nc', 'MATCH_Type0_o3_'+years[1]+'.nc'], 'pres': ['MATCH_Type0_pres_'+years[0]+'.nc', 'MATCH_Type0_pres_'+years[1]+'.nc'], 'temp': ['MATCH_Type0_temp_'+years[0]+'.nc', 'MATCH_Type0_temp_'+years[1]+'.nc']}

    #first lets read in the info from the co file
    mf1 = Dataset(root_mdl+mdl_files['o3'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['o3'][1], 'r')
    mdl_lon = np.array(mf1.variables['lon'][:])
    mdl_lat = np.array(mf1.variables['lat'][:])
    
    # determine the indices of the given wc_lat and wc_lon
    if wc_lon[0] < 0:
        wc_lon[0]= wc_lon[0]+360
        wc_lon[1]= wc_lon[1]+360
    latind1=np.abs(wc_lat[0]-mdl_lat).argmin()
    lonind1=np.abs(wc_lon[0]-mdl_lon).argmin()
    latind2=np.abs(wc_lat[1]-mdl_lat).argmin()
    lonind2=np.abs(wc_lon[1]-mdl_lon).argmin()
    
    mdl_lev = (mf1.variables['lev'][:]).tolist()
    mdl_vmro3 = np.concatenate((mf1.variables['o3'][:,:,latind1:latind2,lonind1:lonind2], mf2.variables['o3'][:,:,latind1:latind2,lonind1:lonind2]))

    mdl_time = np.concatenate((mf1.variables['time'][:], mf2.variables['time'][:]))
    time_units = mf1.variables['time'].getncattr('units')
    time_cal = mf1.variables['time'].getncattr('calendar')

    mf1.close()
    mf2.close()

    nm_lev = len(mdl_lev)
    nm_year = 2

    #now read in the data from the pressure file
    mf1 = Dataset(root_mdl+mdl_files['pres'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['pres'][1], 'r')
    mdl_pres = np.concatenate((mf1.variables['pres'][:,:,latind1:latind2,lonind1:lonind2], mf2.variables['pres'][:,:,latind1:latind2,lonind1:lonind2]))

    #check the units on the pressure 
    pres_units = mf1.variables['pres'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf1.close()
    mf2.close()

    # get temperature
    mf1 = Dataset(root_mdl+mdl_files['temp'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['temp'][1], 'r')
    mdl_temp = np.concatenate((mf1.variables['temp'][:,:,latind1:latind2,lonind1:lonind2], mf2.variables['temp'][:,:,latind1:latind2,lonind1:lonind2]))
    mf1.close()
    mf2.close()
    
    # now determine the mean O3, pres, and temp across the lat and lon range:
    mdl_temp = np.nanmean(mdl_temp,axis=2) # mean across lat
    mdl_temp = np.nanmean(mdl_temp,axis=2) # mean across lon
    mdl_pres = np.nanmean(mdl_pres,axis=2) # mean across lat
    mdl_pres = np.nanmean(mdl_pres,axis=2) # mean across lon
    mdl_vmro3 = np.nanmean(mdl_vmro3,axis=2) # mean across lat
    mdl_vmro3 = np.nanmean(mdl_vmro3,axis=2) # mean across lon

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    mdl_mnth_o3 = np.zeros((nm_year, 12, nm_lev))
    mdl_mnth_pres = np.zeros((nm_year, 12, nm_lev))
    mdl_mnth_temp = np.zeros((nm_year, 12, nm_lev))
    mnth_count = np.zeros((nm_year, 12, nm_lev))

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year-int(years[0])
        mdl_mnth_o3[y,m,:] = np.add(mdl_vmro3[i,:], mdl_mnth_o3[y,m,:])
        mdl_mnth_pres[y,m,:] = np.add(mdl_pres[i,:], mdl_mnth_pres[y,m,:])
        mdl_mnth_temp[y,m,:] = np.add(mdl_temp[i,:], mdl_mnth_temp[y,m,:])
        mnth_count[y,m,:] +=1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:] ,mnth_count[:,:,:])
    mdl_mnth_pres[:,:,:] = np.divide(mdl_mnth_pres[:,:,:], mnth_count[:,:,:])
    mdl_mnth_temp[:,:,:] = np.divide(mdl_mnth_temp[:,:,:], mnth_count[:,:,:])

    return mdl_mnth_o3, mdl_mnth_pres, mdl_mnth_temp, times, nm_lev

def matchsalsa_model(wc_lat=[74.6973],wc_lon=[-94.8297],years=['2014', '2015']):
    print("Reading in MATCH model data...")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/MATCH-SALSA/'																					      
    mdl_files = {'o3': ['MATCH-SALSA_type0_o3_'+years[0]+'.nc', 'MATCH-SALSA_type0_o3_'+years[1]+'.nc'], 'pres': ['MATCH-SALSA_type0_pres_'+years[0]+'.nc', 'MATCH-SALSA_type0_pres_'+years[1]+'.nc'], 'temp': ['MATCH-SALSA_type0_temp_'+years[0]+'.nc', 'MATCH-SALSA_type0_temp_'+years[1]+'.nc']}
    match_ab_file = '/space/hall3/sitestore/eccc/crd/ccrn/users/sta109/AMAP_plots/tes_scripts/match-salsa_ab.csv'

    #first lets read in the info from the co file															          
    mf1 = Dataset(root_mdl+mdl_files['o3'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['o3'][1], 'r')															          
    mdl_lon = np.array(mf1.variables['lon'][:])															   
    mdl_lat = np.array(mf1.variables['lat'][:])															          
    
    # determine the indices of the given wc_lat and wc_lon
    if wc_lon[0] < 0:
        wc_lon[0]= wc_lon[0]+360
        wc_lon[1]= wc_lon[1]+360
    latind1=np.abs(wc_lat[0]-mdl_lat).argmin()
    lonind1=np.abs(wc_lon[0]-mdl_lon).argmin()
    latind2=np.abs(wc_lat[1]-mdl_lat).argmin()
    lonind2=np.abs(wc_lon[1]-mdl_lon).argmin()
    
    mdl_lev = (mf1.variables['lev'][:]).tolist()															          
    mdl_vmro3 = np.concatenate((mf1.variables['o3'][:,:,latind1:latind2,lonind1:lonind2], mf2.variables['o3'][:,:,latind1:latind2,lonind1:lonind2]))											          
																					          
    mdl_time = np.concatenate((mf1.variables['time'][:], mf2.variables['time'][:]))															          
																					          
    mf1.close() 																			          
    mf2.close()
																					          
    nm_year = 2																		          
    nm_lev = len(mdl_lev)																		         

    #now read in the data from the pressure file
    #info in csv in format lev a b
    #the pressure is from the formula a+b*ps
    with open(match_ab_file, 'r') as f:
        headers = f.readline()
        lines = f.readlines()

    mdl_a = []
    mdl_b =[]
    for r in lines:
        t = r.split('|')
        if(len(t) != 3): continue
        mdl_a.append(float(t[1].strip()))
        mdl_b.append(float(t[2].strip()))

    mdl_a = np.asarray(mdl_a)
    mdl_b = np.asarray(mdl_b)

    #now read in the surface pressure
    mf1 = Dataset(root_mdl+mdl_files['pres'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['pres'][1], 'r')

    mdl_ps = np.concatenate((mf1.variables['pres'][:,latind1:latind2,lonind1:lonind2], mf2.variables['pres'][:,latind1:latind2,lonind1:lonind2]))
    
    mf1.close()
    mf2.close()

    # get temperature
    mf1 = Dataset(root_mdl+mdl_files['temp'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['temp'][1], 'r')
    mdl_temp = np.concatenate((mf1.variables['temp'][:,:,latind1:latind2,lonind1:lonind2], mf2.variables['temp'][:,:,latind1:latind2,lonind1:lonind2]))
    mf1.close()
    mf2.close()
    
    # now determine the mean O3, pres, and temp across the lat and lon range:
    mdl_temp = np.nanmean(mdl_temp,axis=2) # mean across lat
    mdl_temp = np.nanmean(mdl_temp,axis=2) # mean across lon
    mdl_ps = np.nanmean(mdl_ps,axis=1) # mean across lat
    mdl_ps = np.nanmean(mdl_ps,axis=1) # mean across lon
    mdl_vmro3 = np.nanmean(mdl_vmro3,axis=2) # mean across lat
    mdl_vmro3 = np.nanmean(mdl_vmro3,axis=2) # mean across lon

 																					          
    mdl_mnth_o3 = np.zeros((nm_year, 12, nm_lev))
    mdl_mnth_pres = np.zeros((nm_year, 12, nm_lev))
    mdl_mnth_temp = np.zeros((nm_year, 12, nm_lev))
    mnth_count = np.zeros((nm_year, 12, nm_lev))
																					          
    #now average over just the interested years 															          
    for t in mdl_time:																			              
        if str(t)[:4] not in years:																	        	  
            continue																			              
        i = list(mdl_time).index(t)																	              
        m = int(str(t)[4:6])-1
        yr = int(str(t)[:4])
        y = yr - int(years[0])																			              
        mdl_mnth_o3[y,m,:] = np.add(mdl_mnth_o3[y,m,:], mdl_vmro3[i,:])
        mnth_count[y,m,:] += 1
        tmp = mdl_b*mdl_ps[i]
        tmp_pres = (mdl_a + tmp)*0.01 #convert to hpa
        mdl_mnth_pres[y,m,:] = np.add(tmp_pres, mdl_mnth_pres[y,m,:])
        mdl_mnth_temp[y,m,:] = np.add(mdl_mnth_temp[y,m,:], mdl_temp[i,:])
																					        	      
    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:] ,mnth_count[:,:,:]) 											        	      
    mdl_mnth_pres[:,:,:] = np.divide(mdl_mnth_pres[:,:,:], mnth_count[:,:,:])											        	      
    mdl_mnth_temp[:,:,:] = np.divide(mdl_mnth_temp[:,:,:], mnth_count[:,:,:])											        	      

    return mdl_mnth_o3, mdl_mnth_pres, mdl_mnth_temp, mdl_time, nm_lev

def mri_model(wc_lat=[74.6973],wc_lon=[-94.8297],years=['2014', '2015']):
    print("Reading in MRI-ESM data...")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/MRI-ESM/'    
    mdl_files = {'o3': 'MRI-ESM_type0_o3_'+years[0]+'-'+years[1]+'.nc', 'pres': 'MRI-ESM_type0_ps_T42L80_128x32NH_3hr_'+years[0]+'-'+years[1]+'.nc', 'temp':'MRI-ESM_type0_temp_'+years[0]+'-'+years[1]+'.nc'}

    #first lets read in the info from the co file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = np.array(mf.variables['lon'][:])
    mdl_lat = np.array(mf.variables['lat'][:])
    mdl_lev = (mf.variables['lev'][:]).tolist()
    
    # determine the indices of the given wc_lat and wc_lon
    if wc_lon[0] < 0:
        wc_lon[0]= wc_lon[0]+360
        wc_lon[1]= wc_lon[1]+360
    latind1=np.abs(wc_lat[0]-mdl_lat).argmin()
    lonind1=np.abs(wc_lon[0]-mdl_lon).argmin()
    latind2=np.abs(wc_lat[1]-mdl_lat).argmin()
    lonind2=np.abs(wc_lon[1]-mdl_lon).argmin()
    
    mdl_vmro3 = mf.variables['o3'][:,:,latind1:latind2,lonind1:lonind2]
    co_units = mf.variables['o3'].getncattr('units')

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')
    
    mf.close()
    
    nm_year = 2
    nm_lev = len(mdl_lev)
    
    #now read in the data from the pressure file
    #need to find pressure from formula p = a*p0 + b*ps at every level
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')    
    mdl_p0 = mf.variables['p0'][:]
    mdl_a = mf.variables['a'][:]
    mdl_b = mf.variables['b'][:]
    mdl_ps = mf.variables['ps'][:,latind1:latind2,lonind1:lonind2]
    
    mf.close() 

    # get temperature
    mf = Dataset(root_mdl+mdl_files['temp'], 'r')
    mdl_temp = mf.variables['temp'][:,:,latind1:latind2,lonind1:lonind2]
    mf.close()
    
    # now determine the mean O3, pres, and temp across the lat and lon range:
    mdl_temp = np.nanmean(mdl_temp,axis=2) # mean across lat
    mdl_temp = np.nanmean(mdl_temp,axis=2) # mean across lon
    mdl_ps = np.nanmean(mdl_ps,axis=1) # mean across lat
    mdl_ps = np.nanmean(mdl_ps,axis=1) # mean across lon
    mdl_vmro3 = np.nanmean(mdl_vmro3,axis=2) # mean across lat
    mdl_vmro3 = np.nanmean(mdl_vmro3,axis=2) # mean across lon

    
    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    mdl_mnth_o3 = np.zeros((nm_year, 12, nm_lev))
    mdl_mnth_pres = np.zeros((nm_year, 12, nm_lev))
    mdl_mnth_temp = np.zeros((nm_year, 12, nm_lev))
    mnth_count = np.zeros((nm_year, 12, nm_lev))
    pres_count = np.zeros((nm_year, 12, nm_lev))

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year-int(years[0])
        mdl_mnth_o3[y,m,:] = np.add(mdl_vmro3[i,:], mdl_mnth_o3[y,m,:])
        mnth_count[y,m,:] +=1
        tmp_pres = (mdl_a*mdl_p0 + mdl_b*mdl_ps[i])*0.01 #convert to hpa
        mdl_mnth_pres[y,m,:] = np.add(tmp_pres, mdl_mnth_pres[y,m,:])
        mdl_mnth_temp[y,m,:] = np.add(mdl_temp[i,:], mdl_mnth_temp[y,m,:])
        pres_count[y,m,:] += 1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:], mnth_count[:,:,:])
    mdl_mnth_pres[:,:,:] = np.divide(mdl_mnth_pres[:,:,:], pres_count[:,:,:])
    mdl_mnth_temp[:,:,:] = np.divide(mdl_mnth_temp[:,:,:], pres_count[:,:,:])

    return mdl_mnth_o3, mdl_mnth_pres, mdl_mnth_temp, times, nm_lev
    	
def oslo_model(wc_lat=[74.6973],wc_lon=[-94.8297],years=['2014', '2015']):
    print("Reading in OsloCTM model data...")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/OsloCTM/'
    mdl_files = {'o3': ['OsloCTM_type0_o3_monthly_'+years[0]+'.nc', 'OsloCTM_type0_o3_monthly_'+years[1]+'.nc'], 'pres': ['OsloCTM_type0_pres_monthly_'+years[0]+'.nc', 'OsloCTM_type0_pres_monthly_'+years[1]+'.nc'], 'temp': ['OsloCTM_type0_temp_monthly_'+years[0]+'.nc', 'OsloCTM_type0_temp_monthly_'+years[1]+'.nc']}

    #first lets read in the info from the co file
    mf1 = Dataset(root_mdl+mdl_files['o3'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['o3'][1], 'r')
    mdl_lon = np.array(mf1.variables['lon'][:])
    mdl_lat = np.array(mf1.variables['lat'][:])
    
    # determine the indices of the given wc_lat and wc_lon
    if wc_lon[0] < 0:
        wc_lon[0]= wc_lon[0]+360
        wc_lon[1]= wc_lon[1]+360
    latind1=np.abs(wc_lat[0]-mdl_lat).argmin()
    lonind1=np.abs(wc_lon[0]-mdl_lon).argmin()
    latind2=np.abs(wc_lat[1]-mdl_lat).argmin()
    lonind2=np.abs(wc_lon[1]-mdl_lon).argmin()
    
    mdl_lev = (mf1.variables['lev'][:]).tolist()
    mdl_vmro3 = np.concatenate((mf1.variables['o3'][:,:,latind1:latind2,lonind1:lonind2], mf2.variables['o3'][:,:,latind1:latind2,lonind1:lonind2]))

    mdl_time = np.concatenate((mf1.variables['time'][:], mf2.variables['time'][:]))
    #weren't specified in co files
    #taken from the pressure file, assuming the same
    time_units = "days since 2001-01-01 00:00:00"
    time_cal = "Julian"

    mf1.close()
    mf2.close()

    nm_year = 2
    nm_lev = len(mdl_lev)

    #now read in the data from the pressure file
    mf1 = Dataset(root_mdl+mdl_files['pres'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['pres'][1], 'r')
    mdl_pres = np.concatenate((mf1.variables['pres'][:,:,latind1:latind2,lonind1:lonind2], mf2.variables['pres'][:,:,latind1:latind2,lonind1:lonind2]))

    #check the units on the pressure 
    pres_units = mf1.variables['pres'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf1.close()
    mf2.close()

    # get temperature
    mf1 = Dataset(root_mdl+mdl_files['temp'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['temp'][1], 'r')
    mdl_temp = np.concatenate((mf1.variables['temp'][:,:,latind1:latind2,lonind1:lonind2], mf2.variables['temp'][:,:,latind1:latind2,lonind1:lonind2]))
    mf1.close()
    mf2.close()

    # now determine the mean O3, pres, and temp across the lat and lon range:
    mdl_temp = np.nanmean(mdl_temp,axis=2) # mean across lat
    mdl_temp = np.nanmean(mdl_temp,axis=2) # mean across lon
    mdl_pres = np.nanmean(mdl_pres,axis=2) # mean across lat
    mdl_pres = np.nanmean(mdl_pres,axis=2) # mean across lon
    mdl_vmro3 = np.nanmean(mdl_vmro3,axis=2) # mean across lat
    mdl_vmro3 = np.nanmean(mdl_vmro3,axis=2) # mean across lon


    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    mdl_mnth_o3 = np.zeros((nm_year,12, nm_lev))
    mdl_mnth_pres = np.zeros((nm_year,12, nm_lev))
    mdl_mnth_temp = np.zeros((nm_year,12, nm_lev))
    mnth_count = np.zeros((nm_year,12, nm_lev))

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year - int(years[0])
        mdl_mnth_o3[y,m,:] = np.add(mdl_vmro3[i,:], mdl_mnth_o3[y,m,:])
        mdl_mnth_pres[y,m,:] = np.add(mdl_pres[i,:], mdl_mnth_pres[y,m,:])
        mdl_mnth_temp[y,m,:] = np.add(mdl_temp[i,:], mdl_mnth_temp[y,m,:])
        mnth_count[y,m,:] +=1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:] ,mnth_count[:,:,:])
    mdl_mnth_pres[:,:,:] = np.divide(mdl_mnth_pres[:,:,:], mnth_count[:,:,:])
    mdl_mnth_temp[:,:,:] = np.divide(mdl_mnth_temp[:,:,:], mnth_count[:,:,:])

    return mdl_mnth_o3, mdl_mnth_pres, mdl_mnth_temp, times, nm_lev

def ukesm_model(wc_lat=[74.6973],wc_lon=[-94.8297],years=['2014', '2015']):
    print("Reading in UKESM1 model data...")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/UKESM1/'
    mdl_files = {'o3': 'UKESM1_type0_monthly_ozone_volume_mixing_ratio_'+years[0]+'_'+years[1]+'.nc', 'pres': 'UKESM1_type0_monthly_air_pressure_'+years[0]+'_'+years[1]+'.nc', 'temp': 'UKESM1_type0_total_monthly_air_temperature_'+years[0]+'_'+years[1]+'.nc'}

    #first lets read in the info from the co file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = np.array(mf.variables['longitude'][:])
    mdl_lat = np.array(mf.variables['latitude'][:])
    mdl_lev = (mf.variables['level_height'][:]).tolist()
    
    # determine the indices of the given wc_lat and wc_lon
    if wc_lon[0] < 0:
        wc_lon[0]= wc_lon[0]+360
        wc_lon[1]= wc_lon[1]+360
    latind1=np.abs(wc_lat[0]-mdl_lat).argmin()
    lonind1=np.abs(wc_lon[0]-mdl_lon).argmin()
    latind2=np.abs(wc_lat[1]-mdl_lat).argmin()
    lonind2=np.abs(wc_lon[1]-mdl_lon).argmin()
    
    mdl_vmro3 = mf.variables['ozone_volume_mixing_ratio'][:,:,latind1:latind2,lonind1:lonind2]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    nm_year = 2
    nm_lev = len(mdl_lev)

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_pres = mf.variables['air_pressure'][:,:,latind1:latind2,lonind1:lonind2]

    #check the units on the pressure 
    pres_units = mf.variables['air_pressure'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf.close()

    # get temperature
    mf = Dataset(root_mdl+mdl_files['temp'], 'r')
    mdl_temp = mf.variables['air_temperature'][:,:,latind1:latind2,lonind1:lonind2]
    mf.close()

    # now determine the mean O3, pres, and temp across the lat and lon range:
    mdl_temp = np.nanmean(mdl_temp,axis=2) # mean across lat
    mdl_temp = np.nanmean(mdl_temp,axis=2) # mean across lon
    mdl_pres = np.nanmean(mdl_pres,axis=2) # mean across lat
    mdl_pres = np.nanmean(mdl_pres,axis=2) # mean across lon
    mdl_vmro3 = np.nanmean(mdl_vmro3,axis=2) # mean across lat
    mdl_vmro3 = np.nanmean(mdl_vmro3,axis=2) # mean across lon


    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    mdl_mnth_o3 = np.zeros((nm_year, 12, nm_lev))
    mdl_mnth_pres = np.zeros((nm_year, 12, nm_lev))
    mdl_mnth_temp = np.zeros((nm_year, 12, nm_lev))
    mnth_count = np.zeros((nm_year, 12, nm_lev))

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year - int(years[0])
        mdl_mnth_o3[y,m,:] = np.add(mdl_vmro3[i,:], mdl_mnth_o3[y,m,:])
        mdl_mnth_pres[y,m,:] = np.add(mdl_pres[i,:], mdl_mnth_pres[y,m,:])
        mdl_mnth_temp[y,m,:] = np.add(mdl_temp[i,:], mdl_mnth_temp[y,m,:])
        mnth_count[y,m,:] +=1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:] ,mnth_count[:,:,:])
    mdl_mnth_pres[:,:,:] = np.divide(mdl_mnth_pres[:,:,:], mnth_count[:,:,:])
    mdl_mnth_temp[:,:,:] = np.divide(mdl_mnth_temp[:,:,:], mnth_count[:,:,:])

    return mdl_mnth_o3, mdl_mnth_pres, mdl_mnth_temp, times, nm_lev

def wrfchem_model(wc_lat=[74.6973],wc_lon=[-94.8297],years=['2014', '2015']):
    print("Reading in WRF-Chem model data...")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/WRF-CHEM/'
    mdl_files = {'o3': 'WRF-CHEM_type0_o3.nc', 'pres': 'WRF-CHEM_type0_pres.nc', 'temp':'WRF-CHEM_type0_temp.nc'}

    #first lets read in the info from the co file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = np.array(mf.variables['XLONG'][:])
    mdl_lat = np.array(mf.variables['XLAT'][:])

    # determine the indices of the given wc_lat and wc_lon
    if wc_lon[0] < 0:
        wc_lon[0]= wc_lon[0]+360
        wc_lon[1]= wc_lon[1]+360
    latind1=np.abs(wc_lat[0]-mdl_lat).argmin()
    lonind1=np.abs(wc_lon[0]-mdl_lon).argmin()
    latind2=np.abs(wc_lat[1]-mdl_lat).argmin()
    lonind2=np.abs(wc_lon[1]-mdl_lon).argmin()

    mdl_vmro3 = mf.variables['o3'][:,:,latind1:latind2,lonind1:lonind2]

    mdl_time = mf.variables['Times'][:]

  #  nm_mdl_lon = mf.dimensions['west_east'].size
  #  nm_mdl_lat = mf.dimensions['south_north'].size

    mf.close()

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_pres = mf.variables['pres'][:,:,latind1:latind2,lonind1:lonind2]
    nm_lev = mf.dimensions['bottom_top'].size

    #check the units on the pressure 
    pres_units = mf.variables['pres'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf.close()

    # get temperature
    mf = Dataset(root_mdl+mdl_files['temp'], 'r')
    mdl_temp = mf.variables['temp'][:,:,latind1:latind2,lonind1:lonind2]
    mf.close()

    # now determine the mean O3, pres, and temp across the lat and lon range:
    mdl_temp = np.nanmean(mdl_temp,axis=2) # mean across lat
    mdl_temp = np.nanmean(mdl_temp,axis=2) # mean across lon
    mdl_pres = np.nanmean(mdl_pres,axis=2) # mean across lat
    mdl_pres = np.nanmean(mdl_pres,axis=2) # mean across lon
    mdl_vmro3 = np.nanmean(mdl_vmro3,axis=2) # mean across lat
    mdl_vmro3 = np.nanmean(mdl_vmro3,axis=2) # mean across lon


    nm_year = 2
    mdl_mnth_o3 = np.zeros((nm_year, 12, nm_lev))
    mdl_mnth_pres = np.zeros((nm_year, 12, nm_lev))
    mdl_mnth_temp = np.zeros((nm_year, 12, nm_lev))
    mnth_count = np.zeros((nm_year, 12, nm_lev))

    #convert the times
    rep = {'b': '', "'": "", ' ': '', "\n": "", '[': "", ']': ""} #for formating strings
    rep = dict((re.escape(k), v) for k, v in rep.items())
    pattern = re.compile("|".join(rep.keys()))

    times = []
    for i in range(24):
        t = np.array2string(mdl_time[i])
        new_t = pattern.sub(lambda m: rep[re.escape(m.group(0))], t)
        #now convert to a datetime object
        d = datetime.strptime(new_t, '%Y-%m-%d_%H:%M:%S')
        times.append(d)
        
    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year - int(years[0])
        mdl_mnth_o3[y,m,:] = np.add(mdl_vmro3[i,:], mdl_mnth_o3[y,m,:])
        mdl_mnth_pres[y,m,:] = np.add(mdl_pres[i,:], mdl_mnth_pres[y,m,:])
        mdl_mnth_temp[y,m,:] = np.add(mdl_temp[i,:], mdl_mnth_temp[y,m,:])
        mnth_count[y,m,:] +=1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:] ,mnth_count[:,:,:])
    mdl_mnth_pres[:,:,:] = np.divide(mdl_mnth_pres[:,:,:], mnth_count[:,:,:])
    mdl_mnth_temp[:,:,:] = np.divide(mdl_mnth_temp[:,:,:], mnth_count[:,:,:])

    return mdl_mnth_o3, mdl_mnth_pres, mdl_mnth_temp, times, nm_lev

def multi_model_plots(sites="Greenland",wc_lat=[75.0],wc_lon=[-45.0],years =['2014', '2015'],models=['CESM','CMAM','DEHM','EMEP-MSC-W','GEOS-Chem','GISS-E2.1','MATCH','MATCH-SALSA','MRI-ESM2','OsloCTM','UKESM1','WRF-Chem']):
	
    np.seterr(divide='ignore', invalid='ignore') #we will divide by zero at some points, ignore all those warnings

    #constant altitude (in km) levels which data is interpolated to
    calt = [0.25, 0.75, 1.25, 1.75, 2.25, 2.75, 3.25, 3.75, 4.25, 4.75, 5.25, 5.75, 6.25, 6.75, 7.25, 7.75, 8.25]
    nm_calt = len(calt)
    
    plots_dir_eps = "/space/hall3/sitestore/eccc/crd/ccrn/users/rcw001/AMAP/eps_figs/"
    plots_dir='/space/hall3/sitestore/eccc/crd/ccrn/users/rcw001/AMAP/png_figs/'
    
    #read in the data from each model, then interpolate
    multi_mdl_o3 = {}
    multi_mdl_o3_seas = {}
    multi_mdl_annual = {}
    processed_models = []

    for m in models:
        if m == "CESM": 
            mdl_o3, mdl_pres, mdl_temp, mdl_times, mdl_lev = cesm_model(wc_lat=wc_lat,wc_lon=wc_lon,years=years)
        elif m == "CMAM": 
            mdl_o3, mdl_pres, mdl_temp, mdl_times, mdl_lev = cmam_model(wc_lat=wc_lat,wc_lon=wc_lon,years=years)
        elif m == "DEHM": 
            mdl_o3, mdl_pres, mdl_temp, mdl_times, mdl_lev = dehm_model(wc_lat=wc_lat,wc_lon=wc_lon,years=years)
        elif m == "EMEP-MSC-W": 
            mdl_o3, mdl_pres, mdl_temp, mdl_times, mdl_lev = emep_model(wc_lat=wc_lat,wc_lon=wc_lon,years=years)        
        elif m == "GEOS-Chem": 
            mdl_o3, mdl_pres, mdl_temp, mdl_times, mdl_lev = geoschem_model(wc_lat=wc_lat,wc_lon=wc_lon,years=years)
        elif m == "GISS-E2.1": 
            mdl_o3, mdl_pres, mdl_temp, mdl_times, mdl_lev = giss_model(wc_lat=wc_lat,wc_lon=wc_lon,years=years)
        elif m == "MATCH": 
            mdl_o3, mdl_pres, mdl_temp, mdl_times, mdl_lev = match_model(wc_lat=wc_lat,wc_lon=wc_lon,years=years)
        elif m == "MATCH-SALSA": 
            mdl_o3, mdl_pres, mdl_temp, mdl_times, mdl_lev = matchsalsa_model(wc_lat=wc_lat,wc_lon=wc_lon,years=years)
        elif m == "MRI-ESM2": 
            mdl_o3, mdl_pres, mdl_temp, mdl_times, mdl_lev = mri_model(wc_lat=wc_lat,wc_lon=wc_lon,years=years)
        elif m == "OsloCTM": 
            mdl_o3, mdl_pres, mdl_temp, mdl_times, mdl_lev = oslo_model(wc_lat=wc_lat,wc_lon=wc_lon,years=years)
        elif m == "UKESM1": 
            mdl_o3, mdl_pres, mdl_temp, mdl_times, mdl_lev = ukesm_model(wc_lat=wc_lat,wc_lon=wc_lon,years=years)
        elif m == "WRF-Chem": 
            mdl_o3, mdl_pres, mdl_temp, mdl_times, mdl_lev = wrfchem_model(wc_lat=wc_lat,wc_lon=wc_lon,years=years)
        else:
            print("Didn't recognize model "+m+". Can't read model files")
            continue

        # first convert model pressure levels to altitude levels in km. Formula:
	# h(m)=-RT/(Mg)*ln(P/P0), M=molar mass of Earth's air (=0.2896 kg/mol)
        nm_years = 2
        mdl_alt = np.zeros((nm_years, 12, mdl_lev))
        print("Calculating altitude from model pressure and temperature for "+m)
        for mn in range(12):
            for yr in range(nm_years):
                tmp_plev = (mdl_pres[yr,mn,:]).tolist()
                tmp_temp = (mdl_temp[yr,mn,:]).tolist()
                for lev in range(len(tmp_plev)):
                    # Now convert the pressure levels to altitude (in km) levels using temperature    
                    mdl_alt[yr,mn,lev] = -1*(8.3143*tmp_temp[lev]/(0.02896*9.807)*math.log(tmp_plev[lev]/1013.5))/1000 
		    
       #  Now vertically interpolate the model profiles to calt levels
        nm_years = 2
        mdl_mnth_o3_tmp = np.zeros((nm_years, 12, mdl_lev))
        mdl_mnth_o3_int = np.zeros((nm_years, 12, len(calt)))
        print("For model "+m+" interpolate from native alt to calt (common altitude levels)")
        for mn in range(12):
            for yr in range(nm_years):
                mdl_mnth_o3_tmp[yr,mn,:] = mdl_o3[yr,mn,:]
                # Now interpolate onto calt levels    
                tmp_o3 = (mdl_mnth_o3_tmp[yr,mn,:]).tolist()
                tmp_altlev = (mdl_alt[yr, mn, :]).tolist()
                if m in ("CESM","EMEP-MSC-W"):  # "GEOS-Chem"
                    tmp_altlev.reverse()
                    tmp_o3.reverse()		    
                if m == "MATCH-SALSA":
                    tmp_altlev.reverse()  # pres & temp were ordered in opposite direction as co.
                mdl_mnth_o3_int[yr,mn,:] = np.interp(calt, tmp_altlev, tmp_o3)		    	    

     #   print('mdl_alt=',mdl_alt[0,0,:])		    
     #   print('mdl_o3=',mdl_mnth_o3_int[0,0,:])		    
			
        if np.all(np.isnan(mdl_mnth_o3_int)):
            print("Something wrong in interpolation??? All nan for "+m)

        # the annual average
        annual_tmp = np.nanmean(mdl_mnth_o3_int, axis=1) # first avg the 12 months together
        annual_mean = np.nanmean(annual_tmp, axis=0) # then avg the 2 years together.

        ###----------Seasonal Averages--------

        #lets define the time periods as atom1 (July, Aug), atom2 (Jan, Feb), atom3 (sep, oct), and atom4 (apr, may)

        #first the model averages
        atom1_mdl = np.add(mdl_mnth_o3_int[:,6,:], mdl_mnth_o3_int[:,7,:])
        atom2_mdl = np.add(mdl_mnth_o3_int[:,0,:], mdl_mnth_o3_int[:,1,:])
        atom3_mdl = np.add(mdl_mnth_o3_int[:,8,:], mdl_mnth_o3_int[:,9,:])
        atom4_mdl = np.add(mdl_mnth_o3_int[:,3,:], mdl_mnth_o3_int[:,4,:])

        atom1_mdl = np.divide(atom1_mdl, 2)
        atom2_mdl = np.divide(atom2_mdl, 2)
        atom3_mdl = np.divide(atom3_mdl, 2)
        atom4_mdl = np.divide(atom4_mdl, 2)

        seas_model = np.stack((atom1_mdl, atom2_mdl, atom3_mdl, atom4_mdl))
        seas_model = np.nanmean(seas_model, axis=1)   # get the average of the 2 years.

        multi_mdl_o3[m] = mdl_mnth_o3_int
        multi_mdl_o3_seas[m] = seas_model
        multi_mdl_annual[m] = annual_mean
        processed_models.append(m)

    ###------------Plotting-----------------
    #set colours for the models for scatter plots
    colour_dict={'CESM': 'lightblue', 'CanAM': 'orange', 'CMAM': 'gray', 'DEHM': 'darkblue', 'ECHAM-SALSA': 'maroon', 'EMEP-MSC-W': 'limegreen', 'GEOS-Chem': 'gold', 'GISS-E2.1': 'cyan', 'MATCH': 'darkgreen', 'MATCH-SALSA': 'blue', 'MRI-ESM2': 'red', 'NorESM': 'purple', 'OsloCTM': 'brown', 'UKESM1': 'magenta', 'WRF-Chem': 'darkgoldenrod'}
    seas_lookup = ['atom1(Jul,Aug)', 'atom2(Jan,Feb)', 'atom3(Sep,Oct)', 'atom4(Apr,May)']

    # plot all models as colours first:	
    for i in range(4):
        target_atom = i
      
        #first plot the profile and difference
        plt.clf()
        fig, axs = plt.subplots(1,1)
    
        axs.set_title(seas_lookup[i]+" avg O3 Profile for "+str(years[0])+" "+sites)
        axs.set_ylim(0, 9)
        axs.set_xlim(0, 200)
        axs.set_ylabel('Altitude (km)')
        axs.set_xlabel('O3 (ppbv)')
      
        c=0
        for m in models:
            mod=multi_mdl_o3_seas[m]
            mod=(mod[target_atom,:]).tolist()
            axs.plot(mod, calt, c=colour_dict[m],label=m)
            c=c+1      
        axs.legend(loc='lower right',fontsize='x-small')	
	
        fig.savefig(plots_dir+"O3aircraft_alt_"+str(years[0])+"_atom"+str(target_atom+1)+"_"+sites+".png")
        fig.savefig(plots_dir_eps+"O3aircraft_alt_"+str(years[0])+"_atom"+str(target_atom+1)+"_"+sites+".eps")
 
    
    # ---- Calculate the multi-model *median* for each vertical level and save as csv file --- #
    rows = 4	# one row per "atom" campaign
    cols = len(calt)
    arr = [[0]*cols]*rows
    n=0
    for at in range(4):
        l=0
        medi = np.array([0.0]*len(calt))
        for lev in calt:
            val=[]
            for mo in models:
                mod=multi_mdl_o3_seas[mo]   
                val=np.append(val,mod[at,l])	# the co value at yr, mn, lev for model m
            medi[l]=st.median(val)		# the multi-model median (mmm) at atom#, lev
            l=l+1
        arr[n] = medi
        n=n+1   
  #  print('arr=',arr)

    # now plot the mmm with different colours for the atom #s: 
    #set colours for the models for scatter plots
    colour_dict2={'Jul-Aug': 'orange', 'Jan-Feb': 'blue', 'Sep-Oct': 'red', 'Apr-May': 'green'}
  #  print(colour_dict2.keys())
          
    #first plot the profile and difference
    plt.clf()
    fig, axs = plt.subplots(1,1)
    
    axs.set_title("MMM O3 Profile for "+str(years[0])+" "+sites)
    axs.set_ylim(0, 9)
    axs.set_xlim(0, 200)
    axs.set_ylabel('Altitude (km)')
    axs.set_xlabel('O3 (ppbv)')

    c=0
    for m in colour_dict2.keys():
        atom=arr[c]
        atom=(atom[:]).tolist()
        axs.plot(atom, calt, c=colour_dict2[m],label=m)
        c=c+1      
        axs.legend(loc='lower right',fontsize='x-small')	
	
        fig.savefig(plots_dir+"O3aircraft_alt_"+str(years[0])+"_ALLatom_mmm"+sites+".png")
        fig.savefig(plots_dir_eps+"O3aircraft_alt_"+str(years[0])+"_ALLatom_mmm"+sites+".eps")
    
	
   # print('arr=',arr[:,:])
    # Now write data to a csv file. columns should be: year, month, O3_925, O3_850, ...O3_100.
    np.savetxt('/space/hall3/sitestore/eccc/crd/ccrn/users/rcw001/AMAP/csv_files/mmm_O3_aircraft_alt_'+sites+years[0]+"-"+years[1]+'.csv',arr,delimiter=',')  

        ####

if __name__ == "__main__":
  #  multi_model_plots(sites="Greenland",wc_lat=75.0,wc_lon=-45.0,years =['2014', '2015'], models = ['CESM','CMAM','DEHM','EMEP-MSC-W','GEOS-Chem','GISS-E2.1','MATCH','MATCH-SALSA','MRI-ESM2','OsloCTM','UKESM1','WRF-Chem'])
  #  multi_model_plots(sites="Alaska",wc_lat=[75.0],wc_lon=[-150.0],years =['2014', '2015'], models = ['CESM','CMAM','DEHM','EMEP-MSC-W','GEOS-Chem','GISS-E2.1','MATCH','MATCH-SALSA','MRI-ESM2','OsloCTM','UKESM1','WRF-Chem'])
 #   multi_model_plots(sites="Alaska",wc_lat=[60.0, 89.9],wc_lon=[-160, -140],years =['2014', '2015'], models = ['CESM','CMAM','DEHM','EMEP-MSC-W','GEOS-Chem','GISS-E2.1','MATCH','MATCH-SALSA','MRI-ESM2','OsloCTM','UKESM1','WRF-Chem'])
    multi_model_plots(sites="Alaska",wc_lat=[60.0, 89.9],wc_lon=[-160.0, -140.0],years =['2014', '2015'], models = ['EMEP-MSC-W','GISS-E2.1'])

