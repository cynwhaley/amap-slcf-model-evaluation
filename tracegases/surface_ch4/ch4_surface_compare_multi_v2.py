"""
ch4_surface_compare_multi.py
March 2020
Author: Tahya Weiss-Gibbons
tahya.weissgibbons@gmail.com

April 2020
Update by Cyndi to fix multipanel plotting

Reads in surface site data for CH4 from across the globe
Compares then to multiple models sampled at the site locations
"""

import re
import csv
import glob
import sys
import numpy as np
import pandas as pd
from datetime import date, datetime
import matplotlib.pyplot as plt
from bisect import bisect_left
from netCDF4 import Dataset, num2date
from mpl_toolkits.basemap import Basemap


def cmam_model(wc_lat=[74.6973],wc_lon=[-94.8297],target_years=['2014', '2015']):
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/CMAM/'
    mdl_files = {'ch4': 'vmrch4_month_CMAM_AMAP2020-SD_r1i1p1_199001-201812.nc'}

    #first lets read in the info from the ch4 file
    mf = Dataset(root_mdl+mdl_files['ch4'], 'r')
    mdl_lon = np.array(mf.variables['lon'][:])
    mdl_lat = np.array(mf.variables['lat'][:])

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')
    
    # determine the indices of the given wc_lat and wc_lon
    x=len(wc_lat)

    latind=np.zeros((x))
    lonind=np.zeros((x))
    mdl_vmrch4=np.zeros((len(mdl_time),x))
    for i in range(x):
        if wc_lon[i] < 0:
            wc_lon[i]= wc_lon[i]+360
        latind[i]=np.abs(wc_lat[i]-mdl_lat).argmin()
        lonind[i]=np.abs(wc_lon[i]-mdl_lon).argmin()

        mdl_vmrch4[:,i] = mf.variables['vmrch4'][:,0,latind[i],lonind[i]]*(10**9) # convert units

    mf.close()

    mdl_mnth_ch4 = np.zeros((2, 12, x))
    mnth_count = np.zeros((2, 12, x))

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in target_years:
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year-int(target_years[0])
        mdl_mnth_ch4[y,m,:] = np.add(mdl_vmrch4[i,:], mdl_mnth_ch4[y,m,:])
        mnth_count[y,m,:] += 1

    mdl_mnth_ch4[:,:,:] = np.divide(mdl_mnth_ch4[:,:,:], mnth_count[:,:,:])
    mdl_mnth_ch4=np.nanmean(mdl_mnth_ch4,axis=0) # average the 2 years together to get one avg for each month
    mdl_annual_ch4 = np.nanmean(mdl_mnth_ch4, axis=0) # average the 12 months to get annual avg
    
    # now we have mdl_mnth_ch4(m,loc) and mdl_mnth_ch4(loc)
    
    return mdl_mnth_ch4, mdl_annual_ch4, times
    

def emep_model(wc_lat=[74.6973],wc_lon=[-94.8297],target_years=['2014', '2015']):
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/EMEP-MSCW/'
    mdl_files = {'ch4': 'EMEP-MSCW_tp0_v04_ch4_3hour_'+target_years[0]+'_'+target_years[1]+'.nc'}

    #first lets read in the info from the ch4 file
    mf = Dataset(root_mdl+mdl_files['ch4'], 'r')
    mdl_lon = np.array(mf.variables['lon'][:])
    mdl_lat = np.array(mf.variables['lat'][:])
    mdl_lon = [ln+360 if ln<0 else ln for ln in mdl_lon]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')

    # determine the indices of the given wc_lat and wc_lon
    x=len(wc_lat)

    latind=np.zeros((x))
    lonind=np.zeros((x))
    mdl_vmrch4=np.zeros((len(mdl_time),x))
    for i in range(x):
        if wc_lon[i] < 0:
            wc_lon[i]= wc_lon[i]+360
        latind[i]=np.abs(wc_lat[i]-mdl_lat).argmin()
        lonind[i]=np.abs(wc_lon[i]-mdl_lon).argmin()

        mdl_vmrch4[:,i] = mf.variables['ch4'][:,0,latind[i],lonind[i]]

    mf.close()

    mdl_mnth_ch4 = np.zeros((2, 12, x))
    mnth_count = np.zeros((2, 12, x))

    times = num2date(mdl_time, units=time_units, calendar='gregorian')

    #now average over just the interested years
    for t in times:
        if str(t.year) not in target_years:
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year-int(target_years[0])
        mdl_mnth_ch4[y,m,:] = np.add(mdl_vmrch4[i,:], mdl_mnth_ch4[y,m,:])
        mnth_count[y,m,:] += 1
	
    mdl_mnth_ch4[:,:,:] = np.divide(mdl_mnth_ch4[:,:,:], mnth_count[:,:,:])
    mdl_mnth_ch4=np.nanmean(mdl_mnth_ch4,axis=0) # average the 2 years together to get one avg for each month
    mdl_annual_ch4 = np.nanmean(mdl_mnth_ch4, axis=0) # average the 12 months to get annual avg
    
    # now we have mdl_mnth_ch4(m,loc) and mdl_mnth_ch4(loc)
    
    return mdl_mnth_ch4, mdl_annual_ch4, times

def geoschem_model(wc_lat=[74.6973],wc_lon=[-94.8297],target_years=['2014', '2015']):
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/GEOS-CHEM/'
    mdl_files = {'ch4': 'GEOS-CHEM_type0_ch4_'+target_years[0]+'_'+target_years[1]+'.nc'}

    #first lets read in the info from the ch4 file
    mf = Dataset(root_mdl+mdl_files['ch4'], 'r')
    mdl_lon = np.array(mf.variables['lon'][:])
    mdl_lat = np.array(mf.variables['lat'][:])
    mdl_lon = [ln+360 if ln<0 else ln for ln in mdl_lon]

    mdl_time = mf.variables['time'][:]
    #time_units = mf.variables['time'].getncattr('units')
    time_units = mf.variables['time'].getncattr('long_name')
    time_cal = mf.variables['time'].getncattr('calendar')

    # determine the indices of the given wc_lat and wc_lon
    x=len(wc_lat)

    latind=np.zeros((x))
    lonind=np.zeros((x))
    mdl_vmrch4=np.zeros((len(mdl_time),x))
    for i in range(x):
        if wc_lon[i] < 0:
            wc_lon[i]= wc_lon[i]+360
        latind[i]=np.abs(wc_lat[i]-mdl_lat).argmin()
        lonind[i]=np.abs(wc_lon[i]-mdl_lon).argmin()

        mdl_vmrch4[:,i] = mf.variables['ch4'][:,0,latind[i],lonind[i]]  

    mf.close()

    mdl_mnth_ch4 = np.zeros((2, 12, x))
    mnth_count = np.zeros((2, 12, x))

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in target_years:
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year-int(target_years[0])
        mdl_mnth_ch4[y,m,:] = np.add(mdl_vmrch4[i,:], mdl_mnth_ch4[y,m,:])
        mnth_count[y,m,:] += 1

    mdl_mnth_ch4[:,:,:] = np.divide(mdl_mnth_ch4[:,:,:], mnth_count[:,:,:])
    mdl_mnth_ch4=np.nanmean(mdl_mnth_ch4,axis=0) # average the 2 years together to get one avg for each month
    mdl_annual_ch4 = np.nanmean(mdl_mnth_ch4, axis=0) # average the 12 months to get annual avg
    
    # now we have mdl_mnth_ch4(m,loc) and mdl_mnth_ch4(loc)
    
    return mdl_mnth_ch4, mdl_annual_ch4, times

def giss_model(wc_lat=[74.6973],wc_lon=[-94.8297],target_years=['2014', '2015']):
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/GISS-modelE-OMA/GISS_reformatted/'
    mdl_files = {'ch4': 'GISS-modelE-OMA_type0_ch4_NCEP_reformatted.nc'}

    #first lets read in the info from the ch4 file
    mf = Dataset(root_mdl+mdl_files['ch4'], 'r')
    mdl_lon = np.array(mf.variables['lon'][:])
    mdl_lat = np.array(mf.variables['lat'][:])
    mdl_lon = [ln+360 if ln<0 else ln for ln in mdl_lon]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    # determine the indices of the given wc_lat and wc_lon
    x=len(wc_lat)

    latind=np.zeros((x))
    lonind=np.zeros((x))
    mdl_vmrch4=np.zeros((len(mdl_time),x))
    for i in range(x):
        if wc_lon[i] < 0:
            wc_lon[i]= wc_lon[i]+360
        latind[i]=np.abs(wc_lat[i]-mdl_lat).argmin()
        lonind[i]=np.abs(wc_lon[i]-mdl_lon).argmin()

        mdl_vmrch4[:,i] = mf.variables['ch4'][:,0,latind[i],lonind[i]]

    mf.close()

    mdl_mnth_ch4 = np.zeros((2, 12, x))
    mnth_count = np.zeros((2, 12, x))

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in target_years:
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year-int(target_years[0])
        mdl_mnth_ch4[y,m,:] = np.add(mdl_vmrch4[i,:], mdl_mnth_ch4[y,m,:])
        mnth_count[y,m,:] += 1

    mdl_mnth_ch4[:,:,:] = np.divide(mdl_mnth_ch4[:,:,:], mnth_count[:,:,:])
    mdl_mnth_ch4=np.nanmean(mdl_mnth_ch4,axis=0) # average the 2 years together to get one avg for each month
    mdl_annual_ch4 = np.nanmean(mdl_mnth_ch4, axis=0) # average the 12 months to get annual avg
    
    # now we have mdl_mnth_ch4(m,loc) and mdl_mnth_ch4(loc)
    
    return mdl_mnth_ch4, mdl_annual_ch4,times

def mri_model(wc_lat=[74.6973],wc_lon=[-94.8297],target_years=['2014', '2015']):
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/MRI-ESM/'
    mdl_files = {'ch4': 'MRI-ESM_type0_ch4_'+target_years[0]+'-'+target_years[1]+'.nc'}

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['ch4'], 'r')
    mdl_lon = np.array(mf.variables['lon'][:])
    mdl_lat = np.array(mf.variables['lat'][:])

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    # determine the indices of the given wc_lat and wc_lon
    x=len(wc_lat)

    latind=np.zeros((x))
    lonind=np.zeros((x))
    mdl_vmrch4=np.zeros((len(mdl_time),x))
    for i in range(x):
        if wc_lon[i] < 0:
            wc_lon[i]= wc_lon[i]+360
        latind[i]=np.abs(wc_lat[i]-mdl_lat).argmin()
        lonind[i]=np.abs(wc_lon[i]-mdl_lon).argmin()
    
        mdl_vmrch4[:,i] = mf.variables['ch4'][:,0,latind[i],lonind[i]]

    mf.close()

    mdl_mnth_ch4 = np.zeros((2, 12, x))
    mnth_count = np.zeros((2, 12, x))

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in target_years:
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year-int(target_years[0])
        mdl_mnth_ch4[y,m,:] = np.add(mdl_vmrch4[i,:], mdl_mnth_ch4[y,m,:])
        mnth_count[y,m,:] += 1

    mdl_mnth_ch4[:,:,:] = np.divide(mdl_mnth_ch4[:,:,:], mnth_count[:,:,:])
    mdl_mnth_ch4=np.nanmean(mdl_mnth_ch4,axis=0) # average the 2 years together to get one avg for each month
    mdl_annual_ch4 = np.nanmean(mdl_mnth_ch4, axis=0) # average the 12 months to get annual avg
    
    # now we have mdl_mnth_ch4(m,loc) and mdl_mnth_ch4(loc)
    
    return mdl_mnth_ch4, mdl_annual_ch4, times

    
def oslo_model(wc_lat=[74.6973],wc_lon=[-94.8297],target_years=['2014', '2015']):
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/OsloCTM/'
    mdl_files = {'ch4': ['OsloCTM_type0_ch4_monthly_'+target_years[0]+'.nc', 'OsloCTM_type0_ch4_monthly_'+target_years[1]+'.nc']}

    #first lets read in the info from the o3 file
    mf1 = Dataset(root_mdl+mdl_files['ch4'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['ch4'][1], 'r')
    mdl_lon = np.array(mf1.variables['lon'][:])
    mdl_lat = np.array(mf1.variables['lat'][:])

    mdl_time = np.concatenate((mf1.variables['time'][:], mf2.variables['time'][:]))
   # weren't specified in ch4 files. Taken from the pressure file, assuming the same
   # time_units = mf1.variables['time'].getncattr('units')
   # time_cal = mf1.variables['time'].getncattr('calendar')
    time_units = "days since 2001-01-01 00:00:00"
    time_cal = "Julian"

    # determine the indices of the given wc_lat and wc_lon
    x=len(wc_lat)

    latind=np.zeros((x))
    lonind=np.zeros((x))
    mdl_vmrch4=np.zeros((len(mdl_time),x))
    for i in range(x):
        if wc_lon[i] < 0:
            wc_lon[i]= wc_lon[i]+360
        latind[i]=np.abs(wc_lat[i]-mdl_lat).argmin()
        lonind[i]=np.abs(wc_lon[i]-mdl_lon).argmin()

        mdl_vmrch4[:,i] = np.concatenate((mf1.variables['ch4'][:,0,latind[i],lonind[i]], mf2.variables['ch4'][:,0,latind[i],lonind[i]]))

    mf1.close()
    mf2.close()

    mdl_mnth_ch4 = np.zeros((2, 12, x))
    mnth_count = np.zeros((2, 12, x))

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in target_years:
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year-int(target_years[0])
        mdl_mnth_ch4[y,m,:] = np.add(mdl_vmrch4[i,:], mdl_mnth_ch4[y,m,:])
        mnth_count[y,m,:] += 1

    mdl_mnth_ch4[:,:,:] = np.divide(mdl_mnth_ch4[:,:,:], mnth_count[:,:,:])
    mdl_mnth_ch4=np.nanmean(mdl_mnth_ch4,axis=0) # average the 2 years together to get one avg for each month
    mdl_annual_ch4 = np.nanmean(mdl_mnth_ch4, axis=0) # average the 12 months to get annual avg
    
    # now we have mdl_mnth_ch4(m,loc) and mdl_mnth_ch4(loc)
    
    return mdl_mnth_ch4, mdl_annual_ch4, times

def ukesm_model(wc_lat=[74.6973],wc_lon=[-94.8297],target_years=['2014', '2015']):
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/UKESM1/'
    mdl_files = {'ch4': 'UKESM1_type0_monthly_methane_volume_mixing_ratio_'+target_years[0]+'_'+target_years[1]+'.nc'}

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['ch4'], 'r')
    mdl_lon = np.array(mf.variables['longitude'][:])
    mdl_lat = np.array(mf.variables['latitude'][:])

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    # determine the indices of the given wc_lat and wc_lon
    x=len(wc_lat)

    latind=np.zeros((x))
    lonind=np.zeros((x))
    mdl_vmrch4=np.zeros((len(mdl_time),x))
    for i in range(x):
        if wc_lon[i] < 0:
            wc_lon[i]= wc_lon[i]+360
        latind[i]=np.abs(wc_lat[i]-mdl_lat).argmin()
        lonind[i]=np.abs(wc_lon[i]-mdl_lon).argmin()

        mdl_vmrch4[:,i] = mf.variables['methane_volume_mixing_ratio'][:,0,latind[i],lonind[i]]

    mf.close()

    mdl_mnth_ch4 = np.zeros((2, 12, x))
    mnth_count = np.zeros((2, 12, x))

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in target_years:
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year-int(target_years[0])
        mdl_mnth_ch4[y,m,:] = np.add(mdl_vmrch4[i,:], mdl_mnth_ch4[y,m,:])
        mnth_count[y,m,:] += 1

    mdl_mnth_ch4[:,:,:] = np.divide(mdl_mnth_ch4[:,:,:], mnth_count[:,:,:])
    mdl_mnth_ch4=np.nanmean(mdl_mnth_ch4,axis=0) # average the 2 years together to get one avg for each month
    mdl_annual_ch4 = np.nanmean(mdl_mnth_ch4, axis=0) # average the 12 months to get annual avg
    
    # now we have mdl_mnth_ch4(m,loc) and mdl_mnth_ch4(loc)
    
    return mdl_mnth_ch4, mdl_annual_ch4, times

#main function which reads in the site data, then calls the functions for the desired models and plots eveything
def ch4_surface_compare_multi(type='global',target_years=['2014', '2015'], models=['CMAM', 'EMEP-MSC-W', 'GEOS-CHEM', 'GISS-E2.1', 'MRI-ESM2', 'OsloCTM', 'UKESM1']):
    ####-----Observation Paths-----------

    root_obs = '/space/hall3/sitestore/eccc/crd/ccrn/obs/amap/ch4/monthly/'
    alert_file = '/space/hall3/sitestore/eccc/crd/ccrn/obs/amap/Alert/Alert-CH4_GC-Monthly.DAT'

    plot_output = '/space/hall3/sitestore/eccc/crd/ccrn/users/rcw001/AMAP/png_figs/'
    plot_output_eps = '/space/hall3/sitestore/eccc/crd/ccrn/users/rcw001/AMAP/eps_figs/'
    
    ###------------------------------------

    #lets make sure everything is on the same 1x1 grid
    compare_lat = list(np.arange(-90., 91.))
    compare_lon = list(np.arange(0., 360.))

    ###------Observations----------

    print("Reading in observations")

    headers = ['site_gaw_id', 'year', 'month', 'day', 'hour', 'minute', 'second', 'year2', 'month2','day2', 'hour2', 'minute2', 'second2', 'value', 'value_unc', 'nvalue', 'latitude', 'longitude', 'altitude', 'elevation', 'intake_height', 'flask_no', 'ORG_QCflag', 'QCflag', 'instrument', 'measurement_method', 'scale']
    all_files = glob.glob(root_obs+'*.txt')
    li = []

    for filename in all_files:
        df = pd.read_csv(filename, comment='#', header=None, sep=' ', names=headers)
        li.append(df)

    site_data = pd.concat(li)

    site_data = site_data[['month', 'value', 'latitude', 'longitude']]
    site_data = site_data.mask(site_data == -999.)
    
    #take the mean at each location for each month
    ch4_sites = site_data.groupby(['latitude', 'longitude', 'month'], as_index=False).mean()
    
    #now at each location, need to match to the closest Compare location and average again
    ch4_lat = ch4_sites['latitude'].values
    ch4_lon = ch4_sites['longitude'].values
    compare_match_lon = np.zeros(len(ch4_lon))
    compare_match_lat = np.zeros(len(ch4_lat))
    for i in range(len(ch4_lat)):
        ln = ch4_lon[i]
        lt = ch4_lat[i]
        if ln < 0:
            ln = ln+360.
        k = bisect_left(compare_lon, ln)-1
        j = bisect_left(compare_lat, lt)-1
        compare_match_lon[i] = compare_lon[k]
        compare_match_lat[i] = compare_lat[j]
    
    ch4_sites['Compare Longitude'] = pd.Series(compare_match_lon)
    ch4_sites['Compare Latitude'] = pd.Series(compare_match_lat)

    ch4_sites = ch4_sites[['month', 'Compare Longitude', 'Compare Latitude', 'value']]
    ch4_sites_annual = ch4_sites[['Compare Longitude', 'Compare Latitude', 'value']]
    ch4_avg = ch4_sites.groupby(['month', 'Compare Longitude', 'Compare Latitude'], as_index=False).mean()
    ch4_avg_annual = ch4_sites_annual.groupby(['Compare Longitude', 'Compare Latitude'], as_index=False).mean()

    ch4_avg_2 = ch4_avg
    # add a new row with lat and lon combined so that I can figure out the unique meas sites
    ch4_avg_2['latlon'] = ch4_avg['Compare Latitude'].astype(str)+ch4_avg['Compare Longitude'].astype(str)

    idx_m = ch4_avg.month.unique()   # new index will be 1:12 for month
    idx_ll = ch4_avg_2.latlon.unique()  # new index will have each unique meas site

    ch4_avg_2 = ch4_avg_2.set_index(['latlon','month'])
    # reindex will insert an NA if there's a missing month for a given meas site (and insert an NA is there's a missing meas site for a given month)
    ch4_avg_2 = ch4_avg_2.reindex(pd.MultiIndex.from_product([idx_ll,idx_m],names=['latlon','Month'])).fillna(-999) 
    ch4_avg = ch4_avg_2.reset_index()
    #sys.exit()

    #alert site data

    alert_data = pd.read_csv(alert_file,sep=',')

    alert_lat = 82.5
    alert_lon = 62.34

    alert_data = alert_data[[' Year', ' Month', ' Mean']]

    #get only the target years
    tar_years = list(map(int, target_years))
    alert_data = alert_data[(alert_data[' Year'] == tar_years[0]) | (alert_data[' Year'] == tar_years[1])]

    #now do the average for each month
    alert_avg = alert_data.groupby([' Month'], as_index=False).mean()
    alert_annual_avg = alert_avg.mean()

    wc_lat=ch4_avg_annual['Compare Latitude'].values
    wc_lon=ch4_avg_annual['Compare Longitude'].values   
    
    ####--------Models------------

    all_mdl_annual = {}
    processed_models = []
    all_mdl_mon = {}
     
    print("Reading in models")   

    for m in models:
        print('processing model ',m)
        if m == 'CMAM':
            mdl_ch4, mdl_annual, times = cmam_model(wc_lat=wc_lat,wc_lon=wc_lon,target_years=target_years)
        elif m == 'EMEP-MSC-W':
            mdl_ch4, mdl_annual, times = emep_model(wc_lat=wc_lat,wc_lon=wc_lon,target_years=target_years)
        elif m == 'GEOS-Chem':
            mdl_ch4, mdl_annual, times = geoschem_model(wc_lat=wc_lat,wc_lon=wc_lon,target_years=target_years)
        elif m == 'GISS-E2.1':
            mdl_ch4, mdl_annual, times = giss_model(wc_lat=wc_lat,wc_lon=wc_lon,target_years=target_years)
        elif m == 'MRI-ESM2':
            mdl_ch4, mdl_annual, times = mri_model(wc_lat=wc_lat,wc_lon=wc_lon,target_years=target_years)
        elif m == 'OsloCTM':
            mdl_ch4, mdl_annual, times = oslo_model(wc_lat=wc_lat,wc_lon=wc_lon,target_years=target_years)
        elif m == 'UKESM1':
            mdl_ch4, mdl_annual, times = ukesm_model(wc_lat=wc_lat,wc_lon=wc_lon,target_years=target_years)
        else:
            print("Didn't recognize model "+m+", can't process files")
            continue

        processed_models.append(m)
        all_mdl_annual[m] = mdl_annual
        all_mdl_mon[m] = mdl_ch4
   
    #and now we can calculate the difference at each location
    ch4_dif_ann = np.zeros((len(processed_models), len(wc_lat)))
    for mi in range(len(processed_models)):
        m = processed_models[mi]
        ch4_dif_ann[mi,:] = all_mdl_annual[m] - ch4_avg_annual['value']
 
    ###---------Plotting--------------
## temporarily remove plotting since testing with small number of models 
 #   print("plotting") 
#
#    #now should have everything to plot the annual averages
#    #lets figure out the plotting configuration
#    num_plots = len(processed_models) + 1 #models plus one for the obs values
#    num_rows = int(num_plots / 3) #want 3 columns 
#    if (num_plots % 3) != 0: num_rows += 1
#
#    fig, axs = plt.subplots(num_rows, 3)
#    ax = axs[0,0] #observations first
#    ax.set_title("Observations",fontsize=9)
#    if type=='Arctic':
#        obs_map = Basemap(projection='npaeqd',boundinglat=60,lon_0=0,resolution='c', ax=ax)
#    else:
#        obs_map = Basemap(projection='merc',llcrnrlat=-50,urcrnrlat=85, llcrnrlon=-180,urcrnrlon=180,lat_ts=20,resolution='c', ax=ax)        
#    obs_map.drawmapboundary(linewidth=0.25)
#    obs_map.drawcoastlines(linewidth=0.25)
#    if type=='Arctic':
#        pcm_obs = obs_map.scatter(wc_lon, wc_lat, c=ch4_avg_annual['value'], cmap='YlOrRd', latlon=True, s=16, vmin=1600, vmax=2000)
#    else:
#        pcm_obs = obs_map.scatter(wc_lon, wc_lat, c=ch4_avg_annual['value'], cmap='YlOrRd', latlon=True, s=2, vmin=1500, vmax=2050)
#    
#    #now we can do a difference plot for each model
#    n = 0
#    for nr in range(num_rows):
#        for nc in range(3):
#            # skip the first panel, which is the obs
#            if nr == 0 and nc == 0: continue
#            #if we've plotted all the models, just need to delete any extra plots
#            if n+1 >= num_plots:
#                fig.delaxes(axs[nr, nc])
#                continue
#            ax = axs[nr,nc]
#            ax.set_title(processed_models[n],fontsize=9)
#            if type=='Arctic':
#                mdl_map = Basemap(projection='npaeqd',boundinglat=60,lon_0=0,resolution='c', ax=ax)
#            else:
#                mdl_map = Basemap(projection='merc',llcrnrlat=-50,urcrnrlat=85, llcrnrlon=-180,urcrnrlon=180,lat_ts=20,resolution='c', ax=ax)
#            mdl_map.drawmapboundary(fill_color='lightgrey',linewidth=0.25)
#            mdl_map.drawcoastlines(linewidth=0.25)
#            mdl_map.fillcontinents(color='lightgrey', zorder=0)
#            if type == 'Arctic':
#                pcm = mdl_map.scatter(wc_lon, wc_lat, c=ch4_dif_ann[n], cmap='bwr', latlon=True, s=8, vmin=-100, vmax=100)
#            else:
#                pcm = mdl_map.scatter(wc_lon, wc_lat, c=ch4_dif_ann[n], cmap='bwr', latlon=True, s=2, vmin=-300, vmax=300)
#            n += 1
#		    
#        cbaxes = fig.add_axes([0.02, 0.1, 0.01, 0.75])
#        cbaxes2 = fig.add_axes([0.9, 0.1, 0.01, 0.75]) 
#        if type=='Arctic':
#            fig.colorbar(pcm_obs, orientation='vertical',ticks=[1600,1700,1800,1900,2000],cax=cbaxes,pad=0.2) 
#            fig.colorbar(pcm, orientation='vertical',ticks=[-100,-75,-50,-25,0,25,50,75,100],cax=cbaxes2,pad=0.2)
#        else:
#            fig.colorbar(pcm_obs, orientation='vertical',ticks=[1500,1600,1700,1800,1900,2000],cax=cbaxes,pad=0.2) 
#            fig.colorbar(pcm, orientation='vertical',ticks=[-300,-200,-100,0,100,200,300],cax=cbaxes2,pad=0.2)
#
#    fig.subplots_adjust(wspace=0)
#   # fig.tight_layout()
#   
#    print("saving plots") 
#    if type=='Arctic':
#        plt.savefig(plot_output+"ch4_surface_ArcticV2_"+target_years[0]+"-"+target_years[1]+".png")
#        plt.savefig(plot_output_eps+"ch4_surface_ArcticV2_"+target_years[0]+"-"+target_years[1]+".eps")
#    else:
#        plt.savefig(plot_output+"ch4_surface_multiV2_"+target_years[0]+"-"+target_years[1]+".png")
#        plt.savefig(plot_output_eps+"ch4_surface_multiV2_"+target_years[0]+"-"+target_years[1]+".eps")

    ###------------Output Data to CSV File------------------
 #   print('mod annual=',all_mdl_annual)
  #  print('mod monthly=',all_mdl_mon)
  #  print('obs annual dim=',np.shape(ch4_avg_annual))
  #  print('obs monthly dim=',np.shape(ch4_avg))

 # add the matching model column to ch4_avg_annual (the meas array)
    # measurement annual array
    frames = [ch4_avg_annual]
    result = pd.concat(frames)
    
    # measurement monthly array
    df1=pd.DataFrame(ch4_avg)
    frames_mn= [df1] 
    result_mnth=pd.concat(frames_mn)
 
 #   print(all_mdl_mon.keys())
 #   print('mod monthly dim for CMAM=',np.shape(all_mdl_mon['CMAM']))
      
    # add the matching model columns to the meas array
    for mo in processed_models:
        i = processed_models.index(mo)
        result[mo] = all_mdl_annual[mo]
        temp=[]
        for loc in range(len(wc_lat)):
            temp = np.concatenate((temp,all_mdl_mon[mo][:,loc]),axis=0)
          #  print(np.shape(temp))
        result_mnth[mo]=temp
	
    result.to_csv('/space/hall3/sitestore/eccc/crd/ccrn/users/rcw001/AMAP/csv_files/surface-ch4-multiV2_annMean'+target_years[0]+"-"+target_years[1]+'.csv',index=False)
    result_mnth.to_csv('/space/hall3/sitestore/eccc/crd/ccrn/users/rcw001/AMAP/csv_files/surface-ch4-multiV2_monMean'+target_years[0]+"-"+target_years[1]+'.csv',index=False)  

if __name__ == "__main__":
    start = datetime.now()
    ch4_surface_compare_multi(type='global',target_years=['2014', '2015'], models=['CMAM', 'EMEP-MSC-W', 'GEOS-Chem', 'GISS-E2.1', 'MRI-ESM2', 'OsloCTM', 'UKESM1'])
 #   ch4_surface_compare_multi(type='global',target_years=['2014', '2015'], models=['CMAM','OsloCTM'])
    finish = datetime.now()
    print(finish-start)
