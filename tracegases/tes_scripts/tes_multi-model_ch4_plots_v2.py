"""
tes_multi-model_ch4_plots.py
Author: Tahya Weiss-Gibbons
January 2020
tahya.weissgibbons@gmail.com

Takes in a list of models and a specified time period and compares model data to TES methane data in that time period.
"""
import os
import sys
import csv
import argparse
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import re
from datetime import date, datetime
from bisect import bisect_left
from netCDF4 import Dataset, num2date
from scipy.interpolate import interpn, interp2d
from mpl_toolkits.basemap import Basemap


def cmam_model(years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/CMAM/'):
    print("Reading in CMAM data...")
    mdl_files = {'ch4': 'vmrch4_month_CMAM_AMAP2020-SD_r1i1p1_199001-201812.nc'}

    #first lets read in the info from the ch4 file
    mf = Dataset(root_mdl+mdl_files['ch4'], 'r')
    mdl_lon = (mf.variables['lon'][:]).tolist()
    mdl_lat = (mf.variables['lat'][:]).tolist()
    mdl_lev = (mf.variables['lev'][:]).tolist()
    mdl_vmrch4 = mf.variables['vmrch4'][:]
    ch4_units = mf.variables['vmrch4'].getncattr('units')

    if ch4_units == "mole mole-1": #equivalent to volume methane per volume air from ideal gas law
        mdl_vmrch4 = mdl_vmrch4*(10**9) #convert to ppbv

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    #need to find pressure from formula p = ap + b*ps at every level
    mdl_ap = mf.variables['ap'][:]
    mdl_b = mf.variables['b'][:]
    mdl_ps = mf.variables['ps'][:]

    mf.close()

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)
    nm_lev = len(mdl_lev)

    mdl_mnth_ch4 = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_mnth_pres = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    pres_count = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        mdl_mnth_ch4[m,:,:,:] = np.add(mdl_vmrch4[i,:,:,:], mdl_mnth_ch4[m,:,:,:])
        mnth_count[m,:,:,:] +=1
        for j in range(nm_mdl_lat):
            for k in range(nm_mdl_lon):
                tmp_pres = (mdl_ap + mdl_b*mdl_ps[i,j,k])*0.01 #convert to hpa
                mdl_mnth_pres[m,:, j,k] = np.add(tmp_pres, mdl_mnth_pres[m,:,j,k])
                pres_count[m,:,j,k] += 1

    mdl_mnth_ch4[:,:,:,:] = np.divide(mdl_mnth_ch4[:,:,:,:], mnth_count[:,:,:,:])
    mdl_mnth_pres[:,:,:,:] = np.divide(mdl_mnth_pres[:,:,:,:], pres_count[:,:,:,:])
    return mdl_mnth_ch4, mdl_mnth_pres, times, mdl_lat, mdl_lon, nm_lev

def emep_model(years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/EMEP-MSCW/'):
    print("Reading in emep data....")

    mdl_files = {'ch4': 'EMEP-MSCW_tp0_v04_ch4_3hour_'+years[0]+'_'+years[1]+'.nc', 'pres': 'EMEP-MSCW_tp0_v04_ps_3hour_'+years[0]+'_'+years[1]+'.nc'}
    ab_file = '/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/EMEP-MSCW/emep_a_b.csv'
    
    #first lets read in the info from the ch4 file
    mf = Dataset(root_mdl+mdl_files['ch4'], 'r')
    mdl_lon = np.array((mf.variables['lon'][:]).tolist())
    mdl_lon = [ln+360 if ln<0 else ln for ln in mdl_lon]
    mdl_lat = (mf.variables['lat'][:]).tolist()
    print(mf)
    try:
        mdl_lev = (mf.variables['lvl'][:]).tolist()
    except KeyError:
        mdl_lev = (mf.variables['lev'][:]).tolist()
    mdl_vmrch4 = mf.variables['ch4'][:]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
   # time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)
    nm_lev = len(mdl_lev)

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_ps = mf.variables['ps'][:]

    mf.close()

    #also need the a and b coordinates to calcualte the pressure at each level
    #info in csv in format lev a b
    #the pressure is from the formula a+b*ps
    with open(ab_file, 'r') as f:
        headers = f.readline()
        lines = f.readlines()

    mdl_a = []
    mdl_b =[]
    for r in lines:
        t = r.split(',')
        mdl_a.append(float(t[0].strip()))
        mdl_b.append(float(t[1].strip()))

    mdl_a = np.asarray(mdl_a)
    mdl_b = np.asarray(mdl_b)

    times = num2date(mdl_time, units=time_units, calendar='gregorian')

    mdl_mnth_ch4 = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_mnth_pres = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    pres_count = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        mdl_mnth_ch4[m,:,:,:] = np.add(mdl_mnth_ch4[m, :,:,:], mdl_vmrch4[i,:,:,:])
        mnth_count[m,:,:,:] += 1
        for j in range(nm_mdl_lat):
            for k in range(nm_mdl_lon):
                tmp = mdl_b*mdl_ps[i,j,k]
                tmp_pres = (mdl_a + tmp)*0.01 #convert to hpa
                mdl_mnth_pres[m,:,j,k] = np.add(tmp_pres, mdl_mnth_pres[m,:,j,k])
                pres_count[m,:,j,k] += 1

    mdl_mnth_ch4[:,:,:,:] = np.divide(mdl_mnth_ch4[:,:,:,:] ,mnth_count[:,:,:,:])
    mdl_mnth_pres[:,:,:,:] = np.divide(mdl_mnth_pres[:,:,:,:], mnth_count[:,:,:,:])
    
    return mdl_mnth_ch4, mdl_mnth_pres, times, mdl_lat, mdl_lon, nm_lev
    

def geoschem_model(years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/GEOS-CHEM/'):
    print("Reading in GEOS-Chem data...")

    mdl_files = {'ch4': 'GEOS-CHEM_type0_ch4_'+years[0]+'_'+years[1]+'.nc', 'pres': 'GEOS-CHEM_type0_pres_2014_2015.nc'}

    #first lets read in the info from the ch4 file
    mf = Dataset(root_mdl+mdl_files['ch4'], 'r')
    mdl_lon = np.array((mf.variables['lon'][:]).tolist())
    mdl_lon = [ln+360 if ln<0 else ln for ln in mdl_lon]
    mdl_lat = (mf.variables['lat'][:]).tolist()
    mdl_vmrch4 = mf.variables['ch4'][:]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('long_name')
    time_cal = mf.variables['time'].getncattr('calendar')
    nm_lev = mf.dimensions['lev'].size

    mf.close()

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)

    mdl_mnth_ch4 = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_mnth_pres = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_pres = mf.variables['pres'][:]
    p_time = mf.variables['time'][:]
    ptime_units = mf.variables['time'].getncattr('long_name')
    ptime_cal = mf.variables['time'].getncattr('calendar')
    
    #check the units on the pressure
    pres_units = mf.variables['pres'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf.close()

    times = num2date(mdl_time, units=time_units, calendar=time_cal)
    ptimes = num2date(p_time, units=ptime_units, calendar=ptime_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        mdl_mnth_ch4[m,:,:,:] = np.add(mdl_vmrch4[i,:,:,:], mdl_mnth_ch4[m,:,:,:])
        mnth_count[m,:,:,:] +=1

    for t in ptimes:
        if str(t.year) not in years:
            continue
        i = list(ptimes).index(t) 
        m = t.month - 1
        mdl_mnth_pres[m,:,:,:] = np.add(mdl_pres[i,:,:,:], mdl_mnth_pres[m,:,:,:])

    mdl_mnth_ch4[:,:,:,:] = np.divide(mdl_mnth_ch4[:,:,:,:] ,mnth_count[:,:,:,:])
    mdl_mnth_pres[:,:,:,:] = np.divide(mdl_mnth_pres[:,:,:,:], mnth_count[:,:,:,:])
    return mdl_mnth_ch4, mdl_mnth_pres, times, mdl_lat, mdl_lon, nm_lev
    
def giss_model(years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/GISS-modelE-OMA/GISS_reformatted/'):
    print("Reading in GISS-modelE-OMA data...")

    mdl_files = {'ch4': 'GISS-modelE-OMA_type0_ch4_NCEP_reformatted.nc', 'pres': 'GISS-modelE-OMA_type0_pres_NCEP_reformatted.nc'}

    #first lets read in the info from the ch4 file
    mf = Dataset(root_mdl+mdl_files['ch4'], 'r')
    mdl_lon = np.array((mf.variables['lon'][:]).tolist())
    mdl_lon = [ln+360 if ln<0 else ln for ln in mdl_lon]
    mdl_lat = (mf.variables['lat'][:]).tolist()
    try:
        mdl_lev = (mf.variables['lvl'][:]).tolist()
    except KeyError:
        mdl_lev = (mf.variables['lev'][:]).tolist()
    mdl_vmrch4 = mf.variables['ch4'][:]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)
    nm_lev = len(mdl_lev)

    mdl_mnth_ch4 = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_mnth_pres = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_pres = mf.variables['pres'][:]

    #check the units on the pressure 
    pres_units = mf.variables['pres'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf.close()

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        mdl_mnth_ch4[m,:,:,:] = np.add(mdl_vmrch4[i,:,:,:], mdl_mnth_ch4[m,:,:,:])
        mdl_mnth_pres[m,:,:,:] = np.add(mdl_pres[i,:,:,:], mdl_mnth_pres[m,:,:,:])
        mnth_count[m,:,:,:] +=1

    mdl_mnth_ch4[:,:,:,:] = np.divide(mdl_mnth_ch4[:,:,:,:] ,mnth_count[:,:,:,:])
    mdl_mnth_pres[:,:,:,:] = np.divide(mdl_mnth_pres[:,:,:,:], mnth_count[:,:,:,:])
    return mdl_mnth_ch4, mdl_mnth_pres, times, mdl_lat, mdl_lon, nm_lev

def mri_model(years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/MRI-ESM/'):
    print("Reading in MRI-ESM data...")
    
    mdl_files = {'ch4': 'MRI-ESM_type0_ch4_'+years[0]+'-'+years[1]+'.nc', 'pres': 'MRI-ESM_type0_ps_T42L80_128x32NH_3hr_'+years[0]+'-'+years[1]+'.nc'}

    #first lets read in the info from the ch4 file
    mf = Dataset(root_mdl+mdl_files['ch4'], 'r')
    mdl_lon = (mf.variables['lon'][:]).tolist()
    mdl_lat = (mf.variables['lat'][:]).tolist()
    mdl_lev = (mf.variables['lev'][:]).tolist()
    mdl_vmrch4 = mf.variables['ch4'][:]
    ch4_units = mf.variables['ch4'].getncattr('units')

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')
    
    mf.close()
    
    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)
    nm_lev = len(mdl_lev)
    
    mdl_mnth_ch4 = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_mnth_pres = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    pres_count = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    
    #now read in the data from the pressure file
    #need to find pressure from formula p = a*p0 + b*ps at every level
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')    
    mdl_p0 = mf.variables['p0'][:]
    mdl_a = mf.variables['a'][:]
    mdl_b = mf.variables['b'][:]
    mdl_ps = mf.variables['ps'][:]
    
    mf.close() 
    
    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        mdl_mnth_ch4[m,:,:,:] = np.add(mdl_vmrch4[i,:,:,:], mdl_mnth_ch4[m,:,:,:])
        mnth_count[m,:,:,:] +=1
        for j in range(nm_mdl_lat):
            for k in range(nm_mdl_lon):
                tmp_pres = (mdl_a*mdl_p0 + mdl_b*mdl_ps[i,j,k])*0.01 #convert to hpa
                mdl_mnth_pres[m,:, j,k] = np.add(tmp_pres, mdl_mnth_pres[m,:,j,k])
                pres_count[m,:,j,k] += 1

    mdl_mnth_ch4[:,:,:,:] = np.divide(mdl_mnth_ch4[:,:,:,:], mnth_count[:,:,:,:])
    mdl_mnth_pres[:,:,:,:] = np.divide(mdl_mnth_pres[:,:,:,:], pres_count[:,:,:,:])
    return mdl_mnth_ch4, mdl_mnth_pres, times, mdl_lat, mdl_lon, nm_lev
    	
def oslo_model(years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/OsloCTM/'):
    print("Reading in OsloCTM model data...")

    mdl_files = {'ch4': ['OsloCTM_type0_ch4_monthly_'+years[0]+'.nc', 'OsloCTM_type0_ch4_monthly_'+years[1]+'.nc'], 'pres': ['OsloCTM_type0_pres_monthly_'+years[0]+'.nc', 'OsloCTM_type0_pres_monthly_'+years[1]+'.nc']}

    #first lets read in the info from the ch4 file
    mf1 = Dataset(root_mdl+mdl_files['ch4'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['ch4'][1], 'r')
    mdl_lon = (mf1.variables['lon'][:]).tolist()
    mdl_lat = (mf1.variables['lat'][:]).tolist()
    mdl_lev = (mf1.variables['lev'][:]).tolist()
    mdl_vmrch4 = np.concatenate((mf1.variables['ch4'][:], mf2.variables['ch4'][:]))

    mdl_time = np.concatenate((mf1.variables['time'][:], mf2.variables['time'][:]))
    #weren't specified in ch4 files
    #taken from the pressure file, assuming the same
    time_units = "days since 2001-01-01 00:00:00"
    time_cal = "Julian"

    mf1.close()
    mf2.close()

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)
    nm_lev = len(mdl_lev)

    mdl_mnth_ch4 = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_mnth_pres = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))

    #now read in the data from the pressure file
    mf1 = Dataset(root_mdl+mdl_files['pres'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['pres'][1], 'r')
    mdl_pres = np.concatenate((mf1.variables['pres'][:], mf2.variables['pres'][:]))

    #check the units on the pressure 
    pres_units = mf1.variables['pres'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf1.close()
    mf2.close()

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        mdl_mnth_ch4[m,:,:,:] = np.add(mdl_vmrch4[i,:,:,:], mdl_mnth_ch4[m,:,:,:])
        mdl_mnth_pres[m,:,:,:] = np.add(mdl_pres[i,:,:,:], mdl_mnth_pres[m,:,:,:])
        mnth_count[m,:,:,:] +=1

    mdl_mnth_ch4[:,:,:,:] = np.divide(mdl_mnth_ch4[:,:,:,:] ,mnth_count[:,:,:,:])
    mdl_mnth_pres[:,:,:,:] = np.divide(mdl_mnth_pres[:,:,:,:], mnth_count[:,:,:,:])
    return mdl_mnth_ch4, mdl_mnth_pres, times, mdl_lat, mdl_lon, nm_lev

def ukesm_model(years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/UKESM1/'):
    print("Reading in UKESM1 model data...")

    mdl_files = {'ch4': 'UKESM1_type0_monthly_methane_volume_mixing_ratio_'+years[0]+'_'+years[1]+'.nc', 'pres': 'UKESM1_type0_monthly_air_pressure_'+years[0]+'_'+years[1]+'.nc'}

    #first lets read in the info from the ch4 file
    mf = Dataset(root_mdl+mdl_files['ch4'], 'r')
    mdl_lon = (mf.variables['longitude'][:]).tolist()
    mdl_lat = (mf.variables['latitude'][:]).tolist()
    mdl_lev = (mf.variables['level_height'][:]).tolist()
    mdl_vmrch4 = mf.variables['methane_volume_mixing_ratio'][:]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)
    nm_lev = len(mdl_lev)

    mdl_mnth_ch4 = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_mnth_pres = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_pres = mf.variables['air_pressure'][:]

    #check the units on the pressure 
    pres_units = mf.variables['air_pressure'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf.close()

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        mdl_mnth_ch4[m,:,:,:] = np.add(mdl_vmrch4[i,:,:,:], mdl_mnth_ch4[m,:,:,:])
        mdl_mnth_pres[m,:,:,:] = np.add(mdl_pres[i,:,:,:], mdl_mnth_pres[m,:,:,:])
        mnth_count[m,:,:,:] +=1

    mdl_mnth_ch4[:,:,:,:] = np.divide(mdl_mnth_ch4[:,:,:,:] ,mnth_count[:,:,:,:])
    mdl_mnth_pres[:,:,:,:] = np.divide(mdl_mnth_pres[:,:,:,:], mnth_count[:,:,:,:])
    return mdl_mnth_ch4, mdl_mnth_pres, times, mdl_lat, mdl_lon, nm_lev



def multi_model_plots(type='global',models=['CMAM', 'EMEP-MSCW', 'GEOS-CHEM', 'GISS-E2.1', 'MRI-ESM2', 'OsloCTM', 'UKESM1'], years=['2014', '2015'], plots_dir='/space/hall3/sitestore/eccc/crd/ccrn/users/sta109/ch4_figs/multi/', tes_root='/space/hall3/sitestore/eccc/crd/ccrn/obs/amap/TES/tes_methane_lite/'):

    np.seterr(divide='ignore', invalid='ignore') #we will divide by zero at some points, ignore all those warnings

    #set colours for the models for scatter plots
    colour_dict={'CESM': 'light blue', 'CanAM': 'orange', 'CMAM': 'gray', 'DEHM': 'dark blue', 'ECHAM-SALSA': 'maroon', 'EMEP-MSCW': 'green', 'GISS-E2.1': 'cyan', 'MATCH': 'dark green', 'MATCH-SALSA': 'blue', 'MRI-ESM2': 'red', 'NorESM': 'purple', 'OsloCTM': 'brown', 'UKESM1': 'magenta', 'WRF-CHEM': 'dark yellow'}

    #standardized 1x1 lat lon grid for all models and obs comparisons
    compare_lat = list(np.arange(-90., 91.))
    compare_lon = list(np.arange(0., 360.))
    nm_lat = len(compare_lat)
    nm_lon = len(compare_lon)

    plots_dir_eps = "/space/hall3/sitestore/eccc/crd/ccrn/users/sta109/eps_figs/"
    #lets read in the observational data first
    print("Reading TES files!")

    nm_tes_levels = 26 #for methane all have 26, different for other species

    tes_ch4_mnth = np.ma.zeros((12, nm_tes_levels, nm_lat, nm_lon))
    tes_pres = np.ma.zeros((nm_tes_levels, nm_lat, nm_lon))
    tes_count = np.zeros((12, nm_tes_levels, nm_lat, nm_lon))
    tes_const_vector = np.zeros((12, nm_tes_levels, nm_lat, nm_lon))
    tes_avg_matrix = np.zeros((12, nm_tes_levels, nm_tes_levels, nm_lat, nm_lon))
    tes_matrix_count = np.zeros((12, nm_tes_levels, nm_tes_levels, nm_lat, nm_lon))

    #first read in all the data from the files
    for root, dirs, files in os.walk(tes_root):
        for file in files:
            if not file.endswith(".nc"):
                continue

            td = Dataset(tes_root+file, 'r')

            tes_lat = (td.variables['Latitude'][:]).tolist()
            tes_lon = (td.variables['Longitude'][:]).tolist()
            grid_tar = (td.variables['Grid_Targets'][:]).tolist()
            grid_pres = td.variables['Grid_Pressure'][:]

            tes_ch4 = td.variables['Species'][:]
            tes_ch4 = np.ma.masked_values(tes_ch4, -999.)
            tes_ch4 = tes_ch4*10.**9
            tes_pr = td.variables['Pressure'][:]

            tes_yf = td.variables['YearFloat'][:]
            avg_kernel = td.variables['AveragingKernel'][:]
            const_vector = td.variables['ConstraintVector'][:]
            const_vector = np.ma.masked_values(const_vector, -999.)
            const_vector = const_vector*10.**9

            td.close()

            #now put on the correct grid and monthly average
            for i in range(len(grid_tar)):
                #first lets check the time
        
                #year and fraction of year that has passed
                time = tes_yf[i]
                yr = int(time)
                if str(yr) not in years:
                    break

                #what month is this?
                day = int((time-yr)*365.)
                mnth = 1 if day == 0 else date.fromordinal(day).month

                #now what grid box are we in compared to the model data?
                lt = tes_lat[i]
                ln = tes_lon[i]
                if ln < 0:
                    ln = ln+360.

                #now find the locations on the regular grid
                lon_index = bisect_left(compare_lon, ln)-1 #gives the insertion point, closest point is at i-1
                lat_index = bisect_left(compare_lat, lt)-1

                #pressure is constant throughout time at each place
                tes_pres[:,lat_index, lon_index] = tes_pr[i,:]
         
                tes_ch4_mnth[mnth-1, :, lat_index, lon_index] = np.add(tes_ch4[i,:], tes_ch4_mnth[mnth-1, :, lat_index, lon_index])

                tes_count[mnth-1, :, lat_index, lon_index] += 1

                #need to average the constraint vector and avg matrix at this point
                tes_const_vector[mnth-1, :, lat_index, lon_index] = np.add(const_vector[i, :], tes_const_vector[mnth-1, :, lat_index, lon_index])
                tes_avg_matrix[mnth-1, :, :, lat_index, lon_index] = np.add(avg_kernel[i, :, :], tes_avg_matrix[mnth-1, :, :, lat_index, lon_index])

                tes_matrix_count[mnth-1, :,:,lat_index, lon_index] += 1

    tes_ch4_mnth[:,:,:,:] = np.divide(tes_ch4_mnth[:,:,:,:], tes_count[:,:,:,:])
    tes_const_vector[:,:,:,:] = np.divide(tes_const_vector[:,:,:,:] , tes_count[:,:,:,:])
    tes_avg_matrix[:,:,:,:,:] = np.divide(tes_avg_matrix[:,:,:,:,:], tes_matrix_count[:, :,:,:,:])

    #also want an annual average for tes
    tes_annual = np.nanmean(tes_ch4_mnth, axis=0)

    if np.all(np.isnan(tes_annual)):
        print("All tes values nan???")

    #read in the data from each model, then interpolate
    multi_mdl_ch4 = {}
    multi_mdl_dif = {}
    multi_mdl_annual = {}
    processed_models = []

    for m in models:
        if m == "CMAM": 
            mdl_ch4, mdl_pres, mdl_times, mdl_lat, mdl_lon, mdl_lev = cmam_model(years=years)
        elif m == "EMEP-MSCW": 
            mdl_ch4, mdl_pres, mdl_times, mdl_lat, mdl_lon, mdl_lev = emep_model(years=years)        
        elif m == "GEOS-CHEM": 
            mdl_ch4, mdl_pres, mdl_times, mdl_lat, mdl_lon, mdl_lev = geoschem_model(years=years)
        elif m == "GISS-E2.1": 
            mdl_ch4, mdl_pres, mdl_times, mdl_lat, mdl_lon, mdl_lev = giss_model(years=years)
        elif m == "MRI-ESM2": 
            mdl_ch4, mdl_pres, mdl_times, mdl_lat, mdl_lon, mdl_lev = mri_model(years=years)
        elif m == "OsloCTM": 
            mdl_ch4, mdl_pres, mdl_times, mdl_lat, mdl_lon, mdl_lev = oslo_model(years=years)
        elif m == "UKESM1": 
            mdl_ch4, mdl_pres, mdl_times, mdl_lat, mdl_lon, mdl_lev = ukesm_model(years=years)
        else:
            print("Didn't recognize model "+m+". Can't read model files")
            continue

        #now lets regrid the data onto a 1x1 grid
        print("Regridding "+m)

        #print(mdl_lon)
        #print(mdl_lat)	
        mdl_mnth_ch4 = np.zeros((12, mdl_lev, len(compare_lat), len(compare_lon)))
        mdl_mnth_pres = np.zeros((12, mdl_lev, len(compare_lat), len(compare_lon)))
        regrid_count = np.zeros((12, mdl_lev, len(compare_lat), len(compare_lon)))
        mdl_lon = [ln+360 if ln<0  else ln for ln in mdl_lon]
        for i in range(12):
            for j in range(mdl_lev):
                f = interp2d(x=mdl_lon,y=mdl_lat,z=mdl_ch4[i,j,:,:],fill_value=-999)
                mdl_mnth_ch4[i,j,:,:] = f(compare_lon, compare_lat)
                f = interp2d(x=mdl_lon,y=mdl_lat,z=mdl_pres[i,j,:,:],fill_value=-999)		
                mdl_mnth_pres[i,j,:,:] = f(compare_lon, compare_lat)
        if np.all(np.isnan(mdl_mnth_ch4)):
            print("Regridding failed??? All nan for "+m)

        #interpolate
        print("Interpolating "+m+" to pressure levels")
        mdl_ch4_cp = np.ma.masked_all((12, nm_tes_levels, len(compare_lat), len(compare_lon)))
        for mt in range(12):
            for i in range(len(compare_lat)):
                for j in range(len(compare_lon)):
                    pr_lev = (mdl_mnth_pres[mt,:,i,j]).tolist()
                    ch4_lev = (mdl_mnth_ch4[mt,:,i,j]).tolist()
                    if m not in ("CESM","EMEP-MSCW","GEOS-CHEM"):
                        pr_lev.reverse()
                        ch4_lev.reverse()
                    tar_pr = np.ma.masked_values(tes_pres[:,i,j], -999.)
                    if np.all(tar_pr==0): continue
                    mdl_ch4_cp[mt,:,i,j] = np.interp(tar_pr, pr_lev, ch4_lev)

        if np.all(np.isnan(mdl_ch4_cp)):
            print("Something wrong in interpolation??? All nan for "+m)

        #and now we can smooth the model to the observational data
        print("Smoothing "+m)
        smooth_mdl_mnth = np.ma.masked_all((12, nm_tes_levels, len(compare_lat), len(compare_lon)))
        for mt in range(12):
            for ln in range(len(compare_lon)):
                for lt in range(len(compare_lat)):
                    log_const = np.log10(np.ma.masked_values(tes_const_vector[mt,:,lt,ln], -999.))
                    log_mdl = np.log10(mdl_ch4_cp[mt,:,lt,ln])
                    matrix_mask = np.ma.masked_values(tes_avg_matrix[mt,:,:,lt,ln], -999.) 
                    smooth_mdl_mnth[mt,:,lt,ln] = log_const+matrix_mask.dot((log_mdl-log_const))
                    smooth_mdl_mnth[mt,:,lt,ln] = np.power(10, smooth_mdl_mnth[mt,:,lt,ln])


        if np.all(np.isnan(smooth_mdl_mnth)):
            print("Smoothing didn't work??? All nan for "+m)

        #need the difference now for plotting
        mdl_dif = smooth_mdl_mnth[:,:,:,:] - tes_ch4_mnth[:,:,:,:]
        #and the annual average
        annual_mean = np.nanmean(smooth_mdl_mnth, axis=0)

        multi_mdl_dif[m] = mdl_dif
        multi_mdl_ch4[m] = smooth_mdl_mnth
        multi_mdl_annual[m] = annual_mean
        processed_models.append(m)
        print(multi_mdl_annual[m] - tes_annual)
	


    ###------------Plotting-----------------

    #now should have all data from the models
    #lets figure out the plotting configuration
    num_cols = 2 #num of columns
    num_plots = len(processed_models) + 1 #models plus one for the tes values
    num_rows = int(num_plots / num_cols) #want 4 columns
    if (num_plots % num_cols) != 0: num_rows += 1
    print("Number of rows: "+str(num_rows))
    print("Number of plots: "+str(num_plots))

 #   mnth_lookup = ['January', 'February', 'March', 'April', 'May', 'June', 'July','August', 'September', 'October', 'November', 'December']
    target_pres = [400, 600, 900]
    #target_pres = [600, 900]
    pres_index = [6,4, 2]

    for i in range(len(target_pres)):
        # Don't make monthly plots for now
        """
        for m in range(12):
            plt.clf()
            fig, axs = plt.subplots(num_rows, 3)
            #first we're going to plot just the observations
            ax = axs[0,0]
            ax.set_title("TES")
            obs_map = Basemap(projection='merc',llcrnrlat=-80,urcrnrlat=80, llcrnrlon=-180,urcrnrlon=180,lat_ts=20,resolution='c', ax=ax)
            obs_map.drawmapboundary()
            obs_map.drawcoastlines()
            lons, lats = np.meshgrid(compare_lon, compare_lat)
            pcm_tes = obs_map.pcolormesh(lons, lats, tes_ch4_mnth[m,pres_index[i]], latlon=True, cmap='YlOrRd', vmin=0, vmax=150)
            c = 0
            for nr in range(num_rows):
                for nc in range(4):
                    #don't plot over the tes values
                    if nr == 0 and nc == 0: continue
                    #if we've plotted all the models, just need to delete any extra plots
                    if c+1 >= num_plots:
                        fig.delaxes(axs[nr, nc])
                        continue
                    ax = axs[nr,nc]
                    ax.set_title(processed_models[c-2])
                    mdl_map = Basemap(projection='merc',llcrnrlat=-80,urcrnrlat=80, llcrnrlon=-180,urcrnrlon=180,lat_ts=20,resolution='c', ax=ax)
                    mdl_map.drawmapboundary(linewidth=0.5,fill_color='lightgrey')
                    mdl_map.drawcoastlines(linewidth=0.5)
                    mdl_map.fillcontinents(color='lightgrey', zorder=0)
                    plot_dif = multi_mdl_dif[processed_models[c-2]]
                    pcm = mdl_map.pcolormesh(lons, lats, plot_dif[m,pres_index[i]], latlon=True, cmap='bwr', vmin=-50, vmax=50)
                    c +=1

            #now lets place the colorbars
            fig.colorbar(pcm_tes, ax=axs[num_rows-1,:], shrink=0.6, location='bottom')
            fig.colorbar(pcm, ax=axs[num_rows-1,:], shrink=0.6, location='bottom')

            plt.savefig(plots_dir+"multi_mdl_tes_"+mnth_lookup[m]+"_"+str(target_pres[i])+"hPa_"+years[0]+"-"+years[1]+".png")
        """

        #and now the annual average
        plt.clf()
        fig, axs = plt.subplots(num_rows, num_cols)
        ax = axs[0,0]
        ax.set_title("TES",fontsize=9)
        if type=='Arctic':
            obs_map = Basemap(projection='npaeqd',boundinglat=60,lon_0=0,resolution='c', ax=ax)
        else:
            obs_map = Basemap(projection='merc',llcrnrlat=-45,urcrnrlat=85, llcrnrlon=-180,urcrnrlon=180,lat_ts=20,resolution='c', ax=ax)
        obs_map.drawmapboundary(linewidth=0.25)
        obs_map.drawcoastlines(linewidth=0.25)
        lons, lats = np.meshgrid(compare_lon, compare_lat)
        pcm_tes = obs_map.pcolormesh(lons, lats, tes_annual[pres_index[i]], latlon=True, cmap='YlOrRd', vmin=1700, vmax=2000)

        c = 0
        for nr in range(num_rows): 	# 4=number of columns
            for nc in range(num_cols):
                if nr ==0 and nc ==0: continue
                #if we've plotted all the models, just need to delete any extra plots
                if c+1 >= num_plots:
                    fig.delaxes(axs[nr, nc])
                    continue
                ax = axs[nr,nc]
                ax.set_title(processed_models[c],fontsize=7)
                if type=='Arctic':
                    mdl_map = Basemap(projection='npaeqd',boundinglat=60,lon_0=0,resolution='c', ax=ax)
                else:
                    mdl_map = Basemap(projection='merc',llcrnrlat=-45,urcrnrlat=85, llcrnrlon=-180,urcrnrlon=180,lat_ts=20,resolution='c', ax=ax)
                mdl_map.drawmapboundary(linewidth=0.25,fill_color='lightgrey')
                mdl_map.drawcoastlines(linewidth=0.25)
                mdl_map.fillcontinents(color='lightgrey', zorder=0)
                plot_dif = multi_mdl_annual[processed_models[c]] - tes_annual
                pcm = mdl_map.pcolormesh(lons, lats, plot_dif[pres_index[i]], latlon=True, cmap='bwr', vmin=-500, vmax=500)
                plot_per_dif = plot_dif/tes_annual * 100           
                print('mean of per diff for',processed_models[c],' and ',target_pres[i],' is ',np.nanmean(plot_per_dif[pres_index[i]]))
             #   test=pd.DataFrame({'a': [multi_mdl_annual[processed_models[c]][pres_index[i]]], 'b': [tes_annual[pres_index[i]]]})
             #   print('R of ',processed_models[c],' and ',pres_index[i],' is ',test['a'].astype('float64').corr(test['b'].astype('float64')))
                c +=1
        
        cbaxes = fig.add_axes([0.02, 0.1, 0.01, 0.75])
        cbaxes2 = fig.add_axes([0.92, 0.1, 0.01, 0.75]) 
        fig.colorbar(pcm_tes, orientation='vertical',ticks=[tick for tick in range(1700,2000+1,50)],cax=cbaxes,pad=0.2) 
        fig.colorbar(pcm, orientation='vertical',ticks=[tick for tick in range(-500,500+1,100)],cax=cbaxes2,pad=0.2)
	
        fig.subplots_adjust(wspace=0)
        if type == 'Arctic':
            plt.savefig(plots_dir+"ch4_Arctic_mdl_tes_annual_"+str(target_pres[i])+"hPa_"+years[0]+"-"+years[1]+".png")
            plt.savefig(plots_dir_eps+"ch4_Arctic_mdl_tes_annual_"+str(target_pres[i])+"hPa_"+years[0]+"-"+years[1]+".eps")
        else:
            plt.savefig(plots_dir+"ch4_multi_mdl_tes_annual_"+str(target_pres[i])+"hPa_"+years[0]+"-"+years[1]+".png") 
            plt.savefig(plots_dir_eps+"ch4_multi_mdl_tes_annual_"+str(target_pres[i])+"hPa_"+years[0]+"-"+years[1]+".eps") 
	  

if __name__ == "__main__":
    #multi_model_plots(type='Arctic',years=['2008', '2009'], models=['CMAM', 'EMEP-MSCW', 'GEOS-CHEM', 'GISS-E2.1', 'MRI-ESM2', 'OsloCTM', 'UKESM1'])
    #multi_model_plots(type='global',years=['2008', '2009'], models=['CMAM', 'EMEP-MSCW', 'GEOS-CHEM', 'GISS-E2.1', 'MRI-ESM2', 'OsloCTM', 'UKESM1'])
    #multi_model_plots(type='Arctic',years=['2014', '2015'], models=['CMAM', 'EMEP-MSCW', 'GEOS-CHEM', 'GISS-E2.1', 'MRI-ESM2', 'OsloCTM', 'UKESM1'])
    multi_model_plots(type='global',years=['2014', '2015'], models=['CMAM', 'EMEP-MSCW', 'GEOS-CHEM', 'GISS-E2.1', 'MRI-ESM2', 'OsloCTM', 'UKESM1'])
    
#'CESM','CMAM','DEHM','EMEP-MSCW','GEOS-CHEM','GISS-E2.1','MATCH','MATCH-SALSA','MRI-ESM2','OsloCTM','UKESM1'

    
