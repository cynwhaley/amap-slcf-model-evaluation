"""
tes_multi-model_o3_plots.py
Author: Tahya Weiss-Gibbons
January 2020
tahya.weissgibbons@gmail.com

Takes in a list of models and a specified time period and compares model data to TES ozone data in that time period.
"""
import os
import sys
import csv
import argparse
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import re
from datetime import date, datetime
from bisect import bisect_left
from netCDF4 import Dataset, num2date
from scipy.interpolate import interpn, interp2d, interp1d
from mpl_toolkits.basemap import Basemap

def cesm_model(years=['2014', '2015'], root_mdl ='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/CESM/'):
    print("Reading in CESM data...")

    mdl_files = {'o3': 'CESM_type0_o3_'+years[0]+'_'+years[1]+'_3h.nc', 'pres': 'CESM_type0_ps_'+years[0]+'_'+years[1]+'_3h.nc'}
    ab_file = '/space/hall3/sitestore/eccc/crd/ccrn/users/rcw001/AMAP/hyam_hybm_P0_CESM.nc'

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = (mf.variables['lon'][:]).tolist()
    mdl_lat = (mf.variables['lat'][:]).tolist()
    try:
        mdl_lev = (mf.variables['lvl'][:]).tolist()
    except KeyError:
        mdl_lev = (mf.variables['lev'][:]).tolist()
    mdl_vmro3 = mf.variables['o3'][:]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)
    nm_lev = len(mdl_lev)

    mdl_mnth_o3 = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_mnth_pres = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    pres_count = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))

    #now read in the surface pressure from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_ps = mf.variables['ps'][:]
    mf.close()
    
    # get a & b values to calculate 3D pressure
    mf = Dataset(ab_file,'r')
    mdl_ap = mf.variables['hyam'][:]
    mdl_b = mf.variables['hybm'][:]
    mf.close()

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        mdl_mnth_o3[m,:,:,:] = np.add(mdl_vmro3[i,:,:,:], mdl_mnth_o3[m,:,:,:])
        mnth_count[m,:,:,:] +=1
        for j in range(nm_mdl_lat):
            for k in range(nm_mdl_lon):
                tmp_pres = (mdl_ap*100000 + mdl_b*mdl_ps[i,j,k])*0.01 #convert from Pa to hPa
                mdl_mnth_pres[m,:, j,k] = np.add(tmp_pres, mdl_mnth_pres[m,:,j,k])
                pres_count[m,:,j,k] += 1	

    mdl_mnth_o3[:,:,:,:] = np.divide(mdl_mnth_o3[:,:,:,:] ,mnth_count[:,:,:,:])
    mdl_mnth_pres[:,:,:,:] = np.divide(mdl_mnth_pres[:,:,:,:], pres_count[:,:,:,:])
    return mdl_mnth_o3, mdl_mnth_pres, times, mdl_lat, mdl_lon, nm_lev

def cmam_model(years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/CMAM/'):
    print("Reading in CMAM data...")
    mdl_files = {'o3': 'vmro3_month_CMAM_AMAP2020-SD_r1i1p1_199001-201812.nc'}

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = (mf.variables['lon'][:]).tolist()
    mdl_lat = (mf.variables['lat'][:]).tolist()
    mdl_lev = (mf.variables['lev'][:]).tolist()
    mdl_vmro3 = mf.variables['vmro3'][:]
    o3_units = mf.variables['vmro3'].getncattr('units')

    if o3_units == "mole mole-1": #equivalent to volume ozone per volume air from ideal gas law
        mdl_vmro3 = mdl_vmro3*(10**9) #convert to ppbv

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    #need to find pressure from formula p = ap + b*ps at every level
    mdl_ap = mf.variables['ap'][:]
    mdl_b = mf.variables['b'][:]
    mdl_ps = mf.variables['ps'][:]

    mf.close()

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)
    nm_lev = len(mdl_lev)

    mdl_mnth_o3 = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_mnth_pres = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    pres_count = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        mdl_mnth_o3[m,:,:,:] = np.add(mdl_vmro3[i,:,:,:], mdl_mnth_o3[m,:,:,:])
        mnth_count[m,:,:,:] +=1
        for j in range(nm_mdl_lat):
            for k in range(nm_mdl_lon):
                tmp_pres = (mdl_ap + mdl_b*mdl_ps[i,j,k])*0.01 #convert to hpa
                mdl_mnth_pres[m,:, j,k] = np.add(tmp_pres, mdl_mnth_pres[m,:,j,k])
                pres_count[m,:,j,k] += 1

    mdl_mnth_o3[:,:,:,:] = np.divide(mdl_mnth_o3[:,:,:,:], mnth_count[:,:,:,:])
    mdl_mnth_pres[:,:,:,:] = np.divide(mdl_mnth_pres[:,:,:,:], pres_count[:,:,:,:])
    return mdl_mnth_o3, mdl_mnth_pres, times, mdl_lat, mdl_lon, nm_lev

def dehm_model(years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/DEHM/'):
    print("Reading in DEHM data....")

    mdl_files = {'o3': 'DEHM_type0_o3_1990_2018.nc', 'pres': 'DEHM_type0_pres_1990_2018.nc'}

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = (mf.variables['lon'][:]).tolist()
    mdl_lat = (mf.variables['lat'][:]).tolist()
    try:
        mdl_lev = (mf.variables['lvl'][:]).tolist()
    except KeyError:
        mdl_lev = (mf.variables['lev'][:]).tolist()
    mdl_vmro3 = mf.variables['o3'][:]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)
    nm_lev = len(mdl_lev)

    mdl_mnth_o3 = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_mnth_pres = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_pres = mf.variables['pres'][:]

    #check the units on the pressure 
    pres_units = mf.variables['pres'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf.close()

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        mdl_mnth_o3[m,:,:,:] = np.add(mdl_vmro3[i,:,:,:], mdl_mnth_o3[m,:,:,:])
        mdl_mnth_pres[m,:,:,:] = np.add(mdl_pres[i,:,:,:], mdl_mnth_pres[m,:,:,:])
        mnth_count[m,:,:,:] +=1

    mdl_mnth_o3[:,:,:,:] = np.divide(mdl_mnth_o3[:,:,:,:] ,mnth_count[:,:,:,:])
    mdl_mnth_pres[:,:,:,:] = np.divide(mdl_mnth_pres[:,:,:,:], mnth_count[:,:,:,:])
    return mdl_mnth_o3, mdl_mnth_pres, times, mdl_lat, mdl_lon, nm_lev

def emep_model(years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/EMEP-MSCW/'):
    print("Reading in emep data....")

    mdl_files = {'o3': 'EMEP-MSCW_tp0_v04_o3_3hour_'+years[0]+'_'+years[1]+'.nc', 'pres': 'EMEP-MSCW_tp0_v04_ps_3hour_'+years[0]+'_'+years[1]+'.nc'}
    ab_file = '/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/EMEP-MSCW/emep_a_b.csv'
    
    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = np.array((mf.variables['lon'][:]).tolist())
    mdl_lon = [ln+360 if ln<0 else ln for ln in mdl_lon]
    mdl_lat = (mf.variables['lat'][:]).tolist()
    try:
        mdl_lev = (mf.variables['lvl'][:]).tolist()
    except KeyError:
        mdl_lev = (mf.variables['lev'][:]).tolist()
    mdl_vmro3 = mf.variables['o3'][:]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
   # time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)
    nm_lev = len(mdl_lev)

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_ps = mf.variables['ps'][:]

    mf.close()

    #also need the a and b coordinates to calcualte the pressure at each level
    #info in csv in format lev a b
    #the pressure is from the formula a+b*ps
    with open(ab_file, 'r') as f:
        headers = f.readline()
        lines = f.readlines()

    mdl_a = []
    mdl_b =[]
    for r in lines:
        t = r.split(',')
        mdl_a.append(float(t[0].strip()))
        mdl_b.append(float(t[1].strip()))

    mdl_a = np.asarray(mdl_a)
    mdl_b = np.asarray(mdl_b)

    times = num2date(mdl_time, units=time_units, calendar='gregorian')

    mdl_mnth_o3 = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_mnth_pres = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    pres_count = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        mdl_mnth_o3[m,:,:,:] = np.add(mdl_mnth_o3[m, :,:,:], mdl_vmro3[i,:,:,:])
        mnth_count[m,:,:,:] += 1
        for j in range(nm_mdl_lat):
            for k in range(nm_mdl_lon):
                tmp = mdl_b*mdl_ps[i,j,k]
                tmp_pres = (mdl_a + tmp)*0.01 #convert to hpa
                mdl_mnth_pres[m,:,j,k] = np.add(tmp_pres, mdl_mnth_pres[m,:,j,k])
                pres_count[m,:,j,k] += 1

    mdl_mnth_o3[:,:,:,:] = np.divide(mdl_mnth_o3[:,:,:,:] ,mnth_count[:,:,:,:])
    mdl_mnth_pres[:,:,:,:] = np.divide(mdl_mnth_pres[:,:,:,:], pres_count[:,:,:,:])
    
    return mdl_mnth_o3, mdl_mnth_pres, times, mdl_lat, mdl_lon, nm_lev
    

def geoschem_model(years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/GEOS-CHEM/'):
    print("Reading in GEOS-Chem data...")

    mdl_files = {'o3': 'GEOS-CHEM_type0_o3_'+years[0]+'_'+years[1]+'.nc', 'pres': 'GEOS-CHEM_type0_pres_'+years[0]+'_'+years[1]+'.nc'}

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = np.array((mf.variables['lon'][:]).tolist())
    mdl_lon = [ln+360 if ln<0 else ln for ln in mdl_lon]
    mdl_lat = (mf.variables['lat'][:]).tolist()
    mdl_vmro3 = mf.variables['o3'][:]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('long_name')
    time_cal = mf.variables['time'].getncattr('calendar')
    nm_lev = mf.dimensions['lev'].size

    mf.close()

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)

    mdl_mnth_o3 = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_mnth_pres = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    pmnth_count = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_pres = mf.variables['pres'][:]
    p_time = mf.variables['time'][:]
    ptime_units = mf.variables['time'].getncattr('long_name')
    ptime_cal = mf.variables['time'].getncattr('calendar')
    
    #check the units on the pressure
    pres_units = mf.variables['pres'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf.close()

    times = num2date(mdl_time, units=time_units, calendar=time_cal)
    ptimes = num2date(p_time, units=ptime_units, calendar=ptime_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        mdl_mnth_o3[m,:,:,:] = np.add(mdl_vmro3[i,:,:,:], mdl_mnth_o3[m,:,:,:])
        mnth_count[m,:,:,:] +=1

    for t in ptimes:
        if str(t.year) not in years:
            continue
        i = list(ptimes).index(t) 
        m = t.month - 1
        mdl_mnth_pres[m,:,:,:] = np.add(mdl_pres[i,:,:,:], mdl_mnth_pres[m,:,:,:])
        pmnth_count[m,:,:,:] +=1

    mdl_mnth_o3[:,:,:,:] = np.divide(mdl_mnth_o3[:,:,:,:] ,mnth_count[:,:,:,:])
    mdl_mnth_pres[:,:,:,:] = np.divide(mdl_mnth_pres[:,:,:,:], pmnth_count[:,:,:,:])
    return mdl_mnth_o3, mdl_mnth_pres, times, mdl_lat, mdl_lon, nm_lev
    
def giss_model(years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/GISS-modelE-OMA/GISS_reformatted//'):
    print("Reading in GISS-modelE-OMA data...")

    mdl_files = {'o3': 'GISS-modelE-OMA_type0_o3_NoNudge_reformatted.nc', 'pres': 'GISS-modelE-OMA_type0_pres_NoNudge_reformatted.nc'}

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = np.array((mf.variables['lon'][:]).tolist())
    mdl_lon = [ln+360 if ln<0 else ln for ln in mdl_lon]
    mdl_lat = (mf.variables['lat'][:]).tolist()
    try:
        mdl_lev = (mf.variables['lvl'][:]).tolist()
    except KeyError:
        mdl_lev = (mf.variables['lev'][:]).tolist()
    mdl_vmro3 = mf.variables['o3'][:]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)
    nm_lev = len(mdl_lev)

    mdl_mnth_o3 = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_mnth_pres = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_pres = mf.variables['pres'][:]

    #check the units on the pressure 
    pres_units = mf.variables['pres'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf.close()

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        mdl_mnth_o3[m,:,:,:] = np.add(mdl_vmro3[i,:,:,:], mdl_mnth_o3[m,:,:,:])
        mdl_mnth_pres[m,:,:,:] = np.add(mdl_pres[i,:,:,:], mdl_mnth_pres[m,:,:,:])
        mnth_count[m,:,:,:] +=1

    mdl_mnth_o3[:,:,:,:] = np.divide(mdl_mnth_o3[:,:,:,:] ,mnth_count[:,:,:,:])
    mdl_mnth_pres[:,:,:,:] = np.divide(mdl_mnth_pres[:,:,:,:], mnth_count[:,:,:,:])
    return mdl_mnth_o3, mdl_mnth_pres, times, mdl_lat, mdl_lon, nm_lev

def match_model(years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/MATCH/'):
    print("Reading in MATCH model data...")

    mdl_files = {'o3': ['MATCH_Type0_o3_'+years[0]+'.nc', 'MATCH_Type0_o3_'+years[1]+'.nc'], 'pres': ['MATCH_Type0_pres_'+years[0]+'.nc', 'MATCH_Type0_pres_'+years[1]+'.nc']}

    #first lets read in the info from the o3 file
    mf1 = Dataset(root_mdl+mdl_files['o3'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['o3'][1], 'r')
    mdl_lon = (mf1.variables['lon'][:]).tolist()
    mdl_lat = (mf1.variables['lat'][:]).tolist()
    mdl_lev = (mf1.variables['lev'][:]).tolist()
    mdl_vmro3 = np.concatenate((mf1.variables['o3'][:], mf2.variables['o3'][:]))

    mdl_time = np.concatenate((mf1.variables['time'][:], mf2.variables['time'][:]))
    time_units = mf1.variables['time'].getncattr('units')
    time_cal = mf1.variables['time'].getncattr('calendar')

    mf1.close()
    mf2.close()

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)
    nm_lev = len(mdl_lev)

    mdl_mnth_o3 = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_mnth_pres = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))

    #now read in the data from the pressure file
    mf1 = Dataset(root_mdl+mdl_files['pres'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['pres'][1], 'r')
    mdl_pres = np.concatenate((mf1.variables['pres'][:], mf2.variables['pres'][:]))

    #check the units on the pressure 
    pres_units = mf1.variables['pres'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf1.close()
    mf2.close()

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        mdl_mnth_o3[m,:,:,:] = np.add(mdl_vmro3[i,:,:,:], mdl_mnth_o3[m,:,:,:])
        mdl_mnth_pres[m,:,:,:] = np.add(mdl_pres[i,:,:,:], mdl_mnth_pres[m,:,:,:])
        mnth_count[m,:,:,:] +=1

    mdl_mnth_o3[:,:,:,:] = np.divide(mdl_mnth_o3[:,:,:,:] ,mnth_count[:,:,:,:])
    mdl_mnth_pres[:,:,:,:] = np.divide(mdl_mnth_pres[:,:,:,:], mnth_count[:,:,:,:])
    return mdl_mnth_o3, mdl_mnth_pres, times, mdl_lat, mdl_lon, nm_lev

def matchsalsa_model(years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/MATCH-SALSA/'):
    print("Reading in MATCH model data...")
																					      
    mdl_files = {'o3': ['MATCH-SALSA_type0_o3_'+years[0]+'.nc', 'MATCH-SALSA_type0_o3_'+years[1]+'.nc'], 'pres': ['MATCH-SALSA_type0_pres_'+years[0]+'.nc', 'MATCH-SALSA_type0_pres_'+years[1]+'.nc']}
    match_ab_file = '/space/hall3/sitestore/eccc/crd/ccrn/users/sta109/AMAP_plots/tes_scripts/match-salsa_ab.csv'

    #first lets read in the info from the o3 file															          
    mf1 = Dataset(root_mdl+mdl_files['o3'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['o3'][1], 'r')															          
    mdl_lon = (mf1.variables['lon'][:]).tolist()															   
    mdl_lat = (mf1.variables['lat'][:]).tolist()															          
    mdl_lev = (mf1.variables['lev'][:]).tolist()															          
    mdl_vmro3 = np.concatenate((mf1.variables['o3'][:], mf2.variables['o3'][:]))											          
																					          
    mdl_time = np.concatenate((mf1.variables['time'][:], mf2.variables['time'][:]))															          
																					          
    mf1.close() 																			          
    mf2.close()
																					          
    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)																		          
    nm_lev = len(mdl_lev)																		         

    #now read in the data from the pressure file
    #info in csv in format lev a b
    #the pressure is from the formula a+b*ps
    with open(match_ab_file, 'r') as f:
        headers = f.readline()
        lines = f.readlines()

    mdl_a = []
    mdl_b =[]
    for r in lines:
        t = r.split('|')
        if(len(t) != 3): continue
        mdl_a.append(float(t[1].strip()))
        mdl_b.append(float(t[2].strip()))

    mdl_a = np.asarray(mdl_a)
    mdl_b = np.asarray(mdl_b)

    #now read in the surface pressure
    mf1 = Dataset(root_mdl+mdl_files['pres'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['pres'][1], 'r')

    mdl_ps = np.concatenate((mf1.variables['pres'][:], mf2.variables['pres'][:]))
    
 #   #check the units on the pressure 																	          
 #   pres_units = mf1.variables['pres'].getncattr('units')														          
 #   if pres_units == 'Pa' or pres_units == "kg m-1 s-2":														          
 #       mdl_pres = mdl_pres*0.01
    
    mf1.close()
    mf2.close()
 																					          
    mdl_mnth_o3 = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_mnth_pres = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    pres_count = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))	
																					          
    #now average over just the interested years 															          
    for t in mdl_time:																			              
        if str(t)[:4] not in years:																	        	  
            continue																			              
        i = list(mdl_time).index(t)																	              
        m = int(str(t)[4:6])-1																			              
        mdl_mnth_o3[m,:,:,:] = np.add(mdl_mnth_o3[m,:,:,:], mdl_vmro3[i,:,:,:])
        mnth_count[m,:,:,:] += 1
        for j in range(nm_mdl_lat):
            for k in range(nm_mdl_lon):
                tmp = mdl_b*mdl_ps[i,j,k]
                tmp_pres = (mdl_a + tmp)*0.01 #convert to hpa
                mdl_mnth_pres[m,:,j,k] = np.add(tmp_pres, mdl_mnth_pres[m,:,j,k])
                pres_count[m,:,j,k] += 1																	        	  
																					        	      
    mdl_mnth_o3[:,:,:,:] = np.divide(mdl_mnth_o3[:,:,:,:] ,mnth_count[:,:,:,:]) 											        	      
    mdl_mnth_pres[:,:,:,:] = np.divide(mdl_mnth_pres[:,:,:,:], pres_count[:,:,:,:])											        	      

  #  print('match-salsa pres vector=',mdl_mnth_pres[8,:,10,10])
  #  print('match-salsa O3 vector=',mdl_mnth_o3[8,:,10,10])
  #  print('are they opposite eachother? - yes')

    return mdl_mnth_o3, mdl_mnth_pres, mdl_time, mdl_lat, mdl_lon, nm_lev

def mri_model(years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/MRI-ESM/'):
    print("Reading in MRI-ESM data...")
    
  #  mdl_files = {'o3': 'MRI-ESM_type0_o3_'+years[0]+'-'+years[1]+'.nc', 'pres': 'MRI-ESM_type0_ps_T42L80_128x32NH_3hr_'+years[0]+'-'+years[1]+'.nc'}
  # use the monthly files instead so that code runs more quickly:
    mdl_files = {'o3': 'MRI-ESM_type0_o3_1990-2015.nc', 'pres': 'MRI-ESM_type0_ps_T42L80_mon_1990-2015.nc'}

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = (mf.variables['lon'][:]).tolist()
    mdl_lat = (mf.variables['lat'][:]).tolist()
    mdl_lev = (mf.variables['lev'][:]).tolist()
    mdl_vmro3 = mf.variables['o3'][:]
    o3_units = mf.variables['o3'].getncattr('units')

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')
    
    mf.close()
    
    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)
    nm_lev = len(mdl_lev)
    
    mdl_mnth_o3 = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_mnth_pres = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    pres_count = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    
    #now read in the data from the pressure file
    #need to find pressure from formula p = a*p0 + b*ps at every level
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')    
    mdl_p0 = mf.variables['p0'][:]
    mdl_a = mf.variables['a'][:]
    mdl_b = mf.variables['b'][:]
    mdl_ps = mf.variables['ps'][:]
    
    mf.close() 
    
    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        mdl_mnth_o3[m,:,:,:] = np.add(mdl_vmro3[i,:,:,:], mdl_mnth_o3[m,:,:,:])
        mnth_count[m,:,:,:] +=1
        for j in range(nm_mdl_lat):
            for k in range(nm_mdl_lon):
                tmp_pres = (mdl_a*mdl_p0 + mdl_b*mdl_ps[i,j,k])*0.01 #convert to hpa
                mdl_mnth_pres[m,:, j,k] = np.add(tmp_pres, mdl_mnth_pres[m,:,j,k])
                pres_count[m,:,j,k] += 1

    mdl_mnth_o3[:,:,:,:] = np.divide(mdl_mnth_o3[:,:,:,:], mnth_count[:,:,:,:])
    mdl_mnth_pres[:,:,:,:] = np.divide(mdl_mnth_pres[:,:,:,:], pres_count[:,:,:,:])
    return mdl_mnth_o3, mdl_mnth_pres, times, mdl_lat, mdl_lon, nm_lev
    	
def oslo_model(years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/OsloCTM/'):
    print("Reading in OsloCTM model data...")

    mdl_files = {'o3': ['OsloCTM_type0_o3_monthly_'+years[0]+'.nc', 'OsloCTM_type0_o3_monthly_'+years[1]+'.nc'], 'pres': ['OsloCTM_type0_pres_monthly_'+years[0]+'.nc', 'OsloCTM_type0_pres_monthly_'+years[1]+'.nc']}

    #first lets read in the info from the o3 file
    mf1 = Dataset(root_mdl+mdl_files['o3'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['o3'][1], 'r')
    mdl_lon = (mf1.variables['lon'][:]).tolist()
    mdl_lat = (mf1.variables['lat'][:]).tolist()
    mdl_lev = (mf1.variables['lev'][:]).tolist()
    mdl_vmro3 = np.concatenate((mf1.variables['o3'][:], mf2.variables['o3'][:]))

    mdl_time = np.concatenate((mf1.variables['time'][:], mf2.variables['time'][:]))
    #weren't specified in o3 files
    #taken from the pressure file, assuming the same
    time_units = "days since 2001-01-01 00:00:00"
    time_cal = "Julian"

    mf1.close()
    mf2.close()

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)
    nm_lev = len(mdl_lev)

    mdl_mnth_o3 = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_mnth_pres = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))

    #now read in the data from the pressure file
    mf1 = Dataset(root_mdl+mdl_files['pres'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['pres'][1], 'r')
    mdl_pres = np.concatenate((mf1.variables['pres'][:], mf2.variables['pres'][:]))

    #check the units on the pressure 
    pres_units = mf1.variables['pres'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf1.close()
    mf2.close()

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        mdl_mnth_o3[m,:,:,:] = np.add(mdl_vmro3[i,:,:,:], mdl_mnth_o3[m,:,:,:])
        mdl_mnth_pres[m,:,:,:] = np.add(mdl_pres[i,:,:,:], mdl_mnth_pres[m,:,:,:])
        mnth_count[m,:,:,:] +=1

    mdl_mnth_o3[:,:,:,:] = np.divide(mdl_mnth_o3[:,:,:,:] ,mnth_count[:,:,:,:])
    mdl_mnth_pres[:,:,:,:] = np.divide(mdl_mnth_pres[:,:,:,:], mnth_count[:,:,:,:])
    return mdl_mnth_o3, mdl_mnth_pres, times, mdl_lat, mdl_lon, nm_lev

def ukesm_model(years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/UKESM1/'):
    print("Reading in UKESM1 model data...")

    mdl_files = {'o3': 'UKESM1_type0_monthly_ozone_volume_mixing_ratio_'+years[0]+'_'+years[1]+'.nc', 'pres': 'UKESM1_type0_monthly_air_pressure_'+years[0]+'_'+years[1]+'.nc'}

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = (mf.variables['longitude'][:]).tolist()
    mdl_lat = (mf.variables['latitude'][:]).tolist()
    mdl_lev = (mf.variables['level_height'][:]).tolist()
    mdl_vmro3 = mf.variables['ozone_volume_mixing_ratio'][:]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    nm_mdl_lat = len(mdl_lat)
    nm_mdl_lon = len(mdl_lon)
    nm_lev = len(mdl_lev)

    mdl_mnth_o3 = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_mnth_pres = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_pres = mf.variables['air_pressure'][:]

    #check the units on the pressure 
    pres_units = mf.variables['air_pressure'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf.close()

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        mdl_mnth_o3[m,:,:,:] = np.add(mdl_vmro3[i,:,:,:], mdl_mnth_o3[m,:,:,:])
        mdl_mnth_pres[m,:,:,:] = np.add(mdl_pres[i,:,:,:], mdl_mnth_pres[m,:,:,:])
        mnth_count[m,:,:,:] +=1

    mdl_mnth_o3[:,:,:,:] = np.divide(mdl_mnth_o3[:,:,:,:] ,mnth_count[:,:,:,:])
    mdl_mnth_pres[:,:,:,:] = np.divide(mdl_mnth_pres[:,:,:,:], mnth_count[:,:,:,:])
    return mdl_mnth_o3, mdl_mnth_pres, times, mdl_lat, mdl_lon, nm_lev

def wrfchem_model(years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/WRF-CHEM/'):
    print("Reading in WRF-CHEM model data...")

    mdl_files = {'o3': 'WRF-CHEM_type0_o3.nc', 'pres': 'WRF-CHEM_type0_pres.nc'}
    
    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = (mf.variables['XLONG'][:]).tolist()
    mdl_lat = (mf.variables['XLAT'][:]).tolist()
    mdl_vmro3 = mf.variables['o3'][:]

    mdl_time = mf.variables['Times'][:]

    nm_mdl_lon = mf.dimensions['west_east'].size
    nm_mdl_lat = mf.dimensions['south_north'].size

    mf.close()

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_pres = mf.variables['pres'][:]
    nm_lev = mf.dimensions['bottom_top'].size

    #check the units on the pressure 
    pres_units = mf.variables['pres'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf.close()

    mdl_mnth_o3 = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mdl_mnth_pres = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))
    mnth_count = np.zeros((12, nm_lev, nm_mdl_lat, nm_mdl_lon))

    #convert the times
    rep = {'b': '', "'": "", ' ': '', "\n": "", '[': "", ']': ""} #for formating strings
    rep = dict((re.escape(k), v) for k, v in rep.items())
    pattern = re.compile("|".join(rep.keys()))

    times = []
    for i in range(24):
        t = np.array2string(mdl_time[i])
        new_t = pattern.sub(lambda m: rep[re.escape(m.group(0))], t)
        #now convert to a datetime object
        d = datetime.strptime(new_t, '%Y-%m-%d_%H:%M:%S')
        times.append(d)
        

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        mdl_mnth_o3[m,:,:,:] = np.add(mdl_vmro3[i,:,:,:], mdl_mnth_o3[m,:,:,:])
        mdl_mnth_pres[m,:,:,:] = np.add(mdl_pres[i,:,:,:], mdl_mnth_pres[m,:,:,:])
        mnth_count[m,:,:,:] +=1

    mdl_mnth_o3[:,:,:,:] = np.divide(mdl_mnth_o3[:,:,:,:] ,mnth_count[:,:,:,:])
    mdl_mnth_pres[:,:,:,:] = np.divide(mdl_mnth_pres[:,:,:,:], mnth_count[:,:,:,:])
    return mdl_mnth_o3, mdl_mnth_pres, times, mdl_lat, mdl_lon, nm_lev

def multi_model_plots(map_type='global',models=['CESM','CMAM','DEHM','EMEP-MSC-W','GEOS-CHEM','GISS-E2.1','MATCH','MATCH-SALSA','MRI-ESM2','OsloCTM','UKESM1','WRF-CHEM'], years=['2014', '2015']):

    tes_root='/space/hall3/sitestore/eccc/crd/ccrn/obs/amap/TES/tes_ozone_lite/'
    plots_dir='/space/hall3/sitestore/eccc/crd/ccrn/users/rcw001/AMAP/png_figs/' 
    plots_dir_eps = "/space/hall3/sitestore/eccc/crd/ccrn/users/rcw001/AMAP/eps_figs/"

    np.seterr(divide='ignore', invalid='ignore') #we will divide by zero at some points, ignore all those warnings
    start = datetime.now()

    #standardized vertical grid (on 100 hPa pressure levels) and 1x1 lat lon grid for all models and obs comparisons
   # nvg=[float(n) for n in range(1000,0,-100)]
    nvg = [ 925.0, 850.0, 800.0, 750.0, 700.0, 650.0, 600.0, 550.0, 500.0, 450.0, 400.0, 350.0, 300.0, 250.0, 200.0, 175.0, 150.0, 125.0, 110.0, 100.0]
    print(nvg)
    compare_lat = list(np.arange(-90., 91.))
    compare_lon = list(np.arange(0., 360.))
    nm_lat = len(compare_lat)
    nm_lon = len(compare_lon)


    #lets read in the observational data first
    print("Reading TES files!")

    # in the TES netCDF file, look for "Grid_Pressure =", this is the number of vertical levels
    nm_tes_levels = 26 #for ozone, 26 levels. Different for other species

    tes_o3_mnth = np.zeros((12, nm_tes_levels, nm_lat, nm_lon))
    tes_pres = np.zeros((nm_tes_levels, nm_lat, nm_lon))
    tes_count = np.zeros((12, nm_tes_levels, nm_lat, nm_lon))
    tes_const_vector = np.zeros((12, nm_tes_levels, nm_lat, nm_lon))
    tes_avg_matrix = np.zeros((12, nm_tes_levels, nm_tes_levels, nm_lat, nm_lon))
    tes_matrix_count = np.zeros((12, nm_tes_levels, nm_tes_levels, nm_lat, nm_lon))

    nvg_o3_mnth = np.ma.zeros((12, len(nvg), nm_lat, nm_lon))
    nvg_tes_count = np.zeros((12, len(nvg), nm_lat, nm_lon))
    nvg_const_vector = np.ma.zeros((12, len(nvg), nm_lat, nm_lon))
    nvg_avg_matrix = np.ma.zeros((12, len(nvg), len(nvg), nm_lat, nm_lon))
    nvg_tes_matrix_count = np.zeros((12, len(nvg), len(nvg), nm_lat, nm_lon))

    #first read in all the data from the files
    for root, dirs, files in os.walk(tes_root):
        for file in files:
            if not file.endswith(".nc"):
                continue

            td = Dataset(tes_root+file, 'r')

            tes_lat = (td.variables['Latitude'][:]).tolist()
            tes_lon = (td.variables['Longitude'][:]).tolist()
            grid_tar = (td.variables['Grid_Targets'][:]).tolist()
            grid_pres = td.variables['Grid_Pressure'][:]
	
            # TES time only a function of lat/lon ("grid targets") in TES lite files.    
            tes_time = td.variables['Time'][:]
            time_units = td.variables['Time'].getncattr('Units')
            time_cal = td.variables['Time'].getncattr('Calendar')
            #tes_UThour = td.variables['UT_Hour'][:]  # UTC hour at location. useful for matching to 3-hourly model files in the future

            tes_o3 = np.ma.array(td.variables['Species'][:])
            tes_o3 = np.ma.masked_values(tes_o3, np.nan)
            tes_o3 = tes_o3*10.**9
            #print(np.ma.MaskedArray.min(tes_o3))
            tes_pr = td.variables['Pressure'][:]
            #print(tes_pr)
            #sys.exit()
            tes_yf = td.variables['YearFloat'][:]
            avg_kernel = td.variables['AveragingKernel'][:]
            const_vector = np.ma.array(td.variables['ConstraintVector'][:])
            const_vector = np.ma.masked_values(const_vector, np.nan)
            const_vector = const_vector*10.**9

            td.close()

            #now put on the correct grid and monthly average
            for i in range(len(grid_tar)):
                #first lets check the time
        
                #year and fraction of year that has passed
                time = tes_yf[i]
                yr = int(time)
                if str(yr) not in years:
                    break

                #what month is this?
                day = int((time-yr)*365.)
                mnth = 1 if day == 0 else date.fromordinal(day).month

                #now what grid box are we in compared to the model data?
                lt = tes_lat[i]
                ln = tes_lon[i]
                if ln < 0:
                    ln = ln+360.

                #now find the locations on the regular grid
                lon_index = bisect_left(compare_lon, ln)-1 #gives the insertion point, closest point is at i-1
                lat_index = bisect_left(compare_lat, lt)-1

                #pressure is constant throughout time at each place
                tes_pres[:,lat_index, lon_index] = tes_pr[i,:]
 
               # vertically interpolate TES O3 from native pressure levels to nvg pressure levels
                tes_o3_mnth[mnth-1, :, lat_index, lon_index] = np.add(tes_o3[i,:], tes_o3_mnth[mnth-1, :, lat_index, lon_index])
                f = interp1d(tes_pres[:,lat_index, lon_index],tes_o3_mnth[mnth-1, :, lat_index, lon_index],kind='linear',bounds_error=False,fill_value=np.nan) # fill_value=-999
                nvg_o3_mnth[mnth-1, :, lat_index, lon_index] = f(nvg) 
                nvg_tes_count[mnth-1, :, lat_index, lon_index] += 1

              # vertically interpolate the TES a priori vector to nvg levels
                tes_const_vector[mnth-1, :, lat_index, lon_index] = np.add(const_vector[i, :], tes_const_vector[mnth-1, :, lat_index, lon_index])
                f = interp1d(tes_pres[:,lat_index, lon_index],tes_const_vector[mnth-1, :, lat_index, lon_index],kind='linear',bounds_error=False,fill_value=np.nan)
                nvg_const_vector[mnth-1, :, lat_index, lon_index] = f(nvg) #np.interp(nvg,np.flip(tes_pres[:,lat_index, lon_index]),np.flip(tes_const_vector[mnth-1, :, lat_index, lon_index]))

              # vertically interpolate the TES averaging kernel to nvg levels
                tes_avg_matrix[mnth-1, :, :, lat_index, lon_index] = np.add(avg_kernel[i, :, :], tes_avg_matrix[mnth-1, :, :, lat_index, lon_index])
                f = interp2d(tes_pres[:,lat_index, lon_index],tes_pres[:,lat_index, lon_index],tes_avg_matrix[mnth-1, :, :, lat_index, lon_index],kind='linear',bounds_error=False,fill_value=np.nan)
                nvg_avg_matrix[mnth-1, :, :, lat_index, lon_index] = f(nvg,nvg)
                nvg_tes_matrix_count[mnth-1, :,:,lat_index, lon_index] += 1
                tes_matrix_count[mnth-1, :,:,lat_index, lon_index] += 1

   #need to average the constraint vector and avg matrix at this point
    nvg_o3_mnth = np.ma.masked_values(nvg_o3_mnth, np.nan)  
    nvg_const_vector = np.ma.masked_values(nvg_const_vector, np.nan)
    nvg_avg_matrix = np.ma.masked_values(nvg_avg_matrix, np.nan)
    nvg_o3_mnth[:,:,:,:] = np.divide(nvg_o3_mnth[:,:,:,:], nvg_tes_count[:,:,:,:])
    nvg_const_vector[:,:,:,:] = np.divide(nvg_const_vector[:,:,:,:] , nvg_tes_count[:,:,:,:])
    nvg_avg_matrix[:,:,:,:,:] = np.divide(nvg_avg_matrix[:,:,:,:,:], nvg_tes_matrix_count[:, :,:,:,:])

    #also want an annual average for tes
    #print(np.ma.MaskedArray.min(nvg_o3_mnth))
    tes_annual = np.nanmean(nvg_o3_mnth, axis=0)
    print("finished nvg calc",datetime.now()-start)
    #sys.exit()

    if np.all(np.isnan(tes_annual)):
        print("All tes values nan???")

    #read in the data from each model, then interpolate
    multi_mdl_o3 = {}
    multi_mdl_dif = {}
    multi_mdl_annual = {}
    processed_models = []

    for m in models:
        if m == "CESM": 
            mdl_o3, mdl_pres, mdl_times, mdl_lat, mdl_lon, mdl_lev = cesm_model(years=years)
        elif m == "CMAM": 
            mdl_o3, mdl_pres, mdl_times, mdl_lat, mdl_lon, mdl_lev = cmam_model(years=years)
        elif m == "DEHM": 
            mdl_o3, mdl_pres, mdl_times, mdl_lat, mdl_lon, mdl_lev = dehm_model(years=years)
        elif m == "EMEP-MSC-W": 
            mdl_o3, mdl_pres, mdl_times, mdl_lat, mdl_lon, mdl_lev = emep_model(years=years)        
        elif m == "GEOS-Chem": 
            mdl_o3, mdl_pres, mdl_times, mdl_lat, mdl_lon, mdl_lev = geoschem_model(years=years)
        elif m == "GISS-E2.1": 
            mdl_o3, mdl_pres, mdl_times, mdl_lat, mdl_lon, mdl_lev = giss_model(years=years)
        elif m == "MATCH": 
            mdl_o3, mdl_pres, mdl_times, mdl_lat, mdl_lon, mdl_lev = match_model(years=years)
        elif m == "MATCH-SALSA": 
            mdl_o3, mdl_pres, mdl_times, mdl_lat, mdl_lon, mdl_lev = matchsalsa_model(years=years)
        elif m == "MRI-ESM2": 
            mdl_o3, mdl_pres, mdl_times, mdl_lat, mdl_lon, mdl_lev = mri_model(years=years)
        elif m == "OsloCTM": 
            mdl_o3, mdl_pres, mdl_times, mdl_lat, mdl_lon, mdl_lev = oslo_model(years=years)
        elif m == "UKESM1": 
            mdl_o3, mdl_pres, mdl_times, mdl_lat, mdl_lon, mdl_lev = ukesm_model(years=years)
        elif m == "WRF-Chem": 
            mdl_o3, mdl_pres, mdl_times, mdl_lat, mdl_lon, mdl_lev = wrfchem_model(years=years)
        else:
            print("Didn't recognize model "+m+". Can't read model files")
            continue

        #now lets regrid the modelled values onto a 1x1 grid
        print("Regridding "+m)
        mdl_lon = [ln+360 if ln<0  else ln for ln in mdl_lon]
        mdl_mnth_o3 = np.zeros((12, mdl_lev, len(compare_lat), len(compare_lon)))
        mdl_mnth_pres = np.zeros((12, mdl_lev, len(compare_lat), len(compare_lon)))
        regrid_count = np.zeros((12, mdl_lev, len(compare_lat), len(compare_lon)))

        for i in range(12):
            for j in range(mdl_lev):
                f = interp2d(x=mdl_lon,y=mdl_lat,z=mdl_o3[i,j,:,:],fill_value=np.nan)
                mdl_mnth_o3[i,j,:,:] = f(compare_lon, compare_lat)
                f = interp2d(x=mdl_lon,y=mdl_lat,z=mdl_pres[i,j,:,:],fill_value=np.nan)		
                mdl_mnth_pres[i,j,:,:] = f(compare_lon, compare_lat)
        if np.all(np.isnan(mdl_mnth_o3)):
            print("Regridding failed??? All nan for "+m)

        # vertical interpolate model to nvg levels
        print("Interpolating "+m+" to pressure levels")
        mdl_o3_cp = np.ma.masked_all((12, len(nvg), len(compare_lat), len(compare_lon)))
        for mt in range(12):
            for i in range(len(compare_lat)):
                for j in range(len(compare_lon)):
                    pr_lev = (mdl_mnth_pres[mt,:,i,j]).tolist()
                    o3_lev = (mdl_mnth_o3[mt,:,i,j]).tolist()
                    if m not in ("CESM","EMEP-MSC-W"):
                        pr_lev.reverse()
                        o3_lev.reverse()
                    if m == "MATCH-SALSA":
                        pr_lev.reverse()  # pressure was ordered in opposite direction as O3.
                    mdl_o3_cp[mt,:,i,j] = np.interp(nvg, pr_lev, o3_lev)

        if np.all(np.isnan(mdl_o3_cp)):
            print("Something wrong in interpolation??? All nan for "+m)

        #and now we can smooth the model to the observational data
        print("Smoothing "+m)
        smooth_mdl_mnth = np.ma.masked_all((12, len(nvg), len(compare_lat), len(compare_lon)))
        for mt in range(12):
            for ln in range(len(compare_lon)):
                for lt in range(len(compare_lat)):
                    log_const = np.log10(np.ma.masked_values(nvg_const_vector[mt,:,lt,ln], np.nan))  
                    log_mdl = np.log10(mdl_o3_cp[mt,:,lt,ln])
                    matrix_mask = np.ma.masked_values(nvg_avg_matrix[mt,:,:,lt,ln], np.nan)  
                    smooth_mdl_mnth[mt,:,lt,ln] = log_const+matrix_mask.dot((log_mdl-log_const))
                    smooth_mdl_mnth[mt,:,lt,ln] = np.power(10, smooth_mdl_mnth[mt,:,lt,ln])

        if np.all(np.isnan(smooth_mdl_mnth)):
            print("Smoothing didn't work??? All nan for "+m)

        #need the difference now for plotting
        mdl_dif = smooth_mdl_mnth[:,:,:,:] - nvg_o3_mnth[:,:,:,:]
        #and the annual average
        annual_mean = np.nanmean(smooth_mdl_mnth, axis=0)

        multi_mdl_dif[m] = mdl_dif
        multi_mdl_o3[m] = smooth_mdl_mnth
        multi_mdl_annual[m] = annual_mean
        processed_models.append(m)
      #  print('annual model minus annual measurement:',multi_mdl_annual[m] - tes_annual)
        print(m," finished processing",datetime.now()-start)
  
    
    ###------------Plotting-----------------

    #now should have all data from the models
    #lets figure out the plotting configuration
    num_plots = len(processed_models) + 1 #models plus one for the tes values
    num_rows = int(num_plots / 4) #want 4 columns
    if (num_plots % 4) != 0: num_rows += 1
   # print("Number of rows: "+str(num_rows))
   # print("Number of plots: "+str(num_plots))

    mnth_lookup = ['January', 'February', 'March', 'April', 'May', 'June', 'July','August', 'September', 'October', 'November', 'December']
    target_pres = [400, 600, 925]
    pres_index = [nvg.index(p) for p in target_pres]
    x=[0,3,6,9] # Just Jan, Apr, Jul, and Oct
    for i in range(len(target_pres)):
        for m in x:
            plt.clf()
            fig, axs = plt.subplots(num_rows, 4)
            #first we're going to plot just the observations
            ax = axs[0,0]
            ax.set_title("TES")
            obs_map = Basemap(projection='merc',llcrnrlat=-80,urcrnrlat=80, llcrnrlon=-180,urcrnrlon=180,lat_ts=20,resolution='c', ax=ax)
            obs_map.drawmapboundary()
            obs_map.drawcoastlines()
            lons, lats = np.meshgrid(compare_lon, compare_lat)
            pcm_tes = obs_map.pcolormesh(lons, lats, tes_o3_mnth[m,pres_index[i]], latlon=True, cmap='YlOrRd', vmin=0, vmax=150)
            c = 0
            for nr in range(num_rows):
                for nc in range(4):
                    #don't plot over the tes values
                    if nr == 0 and nc == 0: continue
                    #if we've plotted all the models, just need to delete any extra plots
                    if c+1 >= num_plots:
                        fig.delaxes(axs[nr, nc])
                        continue
                    ax = axs[nr,nc]
                    ax.set_title(processed_models[c-2])
                    mdl_map = Basemap(projection='merc',llcrnrlat=-45,urcrnrlat=85, llcrnrlon=-180,urcrnrlon=180,lat_ts=20,resolution='c', ax=ax)
                    mdl_map.drawmapboundary(linewidth=0.5,fill_color='lightgrey')
                    mdl_map.drawcoastlines(linewidth=0.5)
                    mdl_map.fillcontinents(color='lightgrey', zorder=0)
                    plot_dif = multi_mdl_dif[processed_models[c-2]]
                    pcm = mdl_map.pcolormesh(lons, lats, plot_dif[m,pres_index[i]], latlon=True, cmap='bwr', vmin=-50, vmax=50)
                    c +=1

            #now lets place the colorbars
            fig.colorbar(pcm_tes, ax=axs[num_rows-1,:], shrink=0.6, location='bottom')
            fig.colorbar(pcm, ax=axs[num_rows-1,:], shrink=0.6, location='bottom')

            plt.savefig(plots_dir+"multi_mdl_tes_"+mnth_lookup[m]+"_"+str(target_pres[i])+"hPa_"+years[0]+"-"+years[1]+".png")

        #and now the annual average
        plt.clf()
        fig, axs = plt.subplots(num_rows, 4)
        ax = axs[0,0]
        ax.set_title("TES",fontsize=9)
        if map_type=='Arctic':
            obs_map = Basemap(projection='npaeqd',boundinglat=60,lon_0=0,resolution='c', ax=ax)
        else:
            obs_map = Basemap(projection='merc',llcrnrlat=-45,urcrnrlat=85, llcrnrlon=-180,urcrnrlon=180,lat_ts=20,resolution='c', ax=ax)
        obs_map.drawmapboundary(linewidth=0.25)
        obs_map.drawcoastlines(linewidth=0.25)
        lons, lats = np.meshgrid(compare_lon, compare_lat)
        pcm_tes = obs_map.pcolormesh(lons, lats, tes_annual[pres_index[i]], latlon=True, cmap='YlOrRd', vmin=30, vmax=100)

        c = 0
        for nr in range(num_rows): 	# 4=number of columns
            for nc in range(4):
                if nr ==0 and nc ==0: continue
                #if we've plotted all the models, just need to delete any extra plots
                if c+1 >= num_plots:
                    fig.delaxes(axs[nr, nc])
                    continue
                ax = axs[nr,nc]
                ax.set_title(processed_models[c],fontsize=7)
                if map_type=='Arctic':
                    mdl_map = Basemap(projection='npaeqd',boundinglat=60,lon_0=0,resolution='c', ax=ax)
                else:
                    mdl_map = Basemap(projection='merc',llcrnrlat=-45,urcrnrlat=85, llcrnrlon=-180,urcrnrlon=180,lat_ts=20,resolution='c', ax=ax)
                mdl_map.drawmapboundary(linewidth=0.25,fill_color='lightgrey')
                mdl_map.drawcoastlines(linewidth=0.25)
                mdl_map.fillcontinents(color='lightgrey', zorder=0)
                plot_dif = multi_mdl_annual[processed_models[c]] - tes_annual
                pcm = mdl_map.pcolormesh(lons, lats, plot_dif[pres_index[i]], latlon=True, cmap='bwr', vmin=-50, vmax=50)
                plot_per_dif = plot_dif/tes_annual * 100           
                print('mean of diff for',processed_models[c],' and ',target_pres[i],' is ',np.nanmean(plot_dif[pres_index[i]]))
                c +=1
        
        cbaxes = fig.add_axes([0.02, 0.1, 0.01, 0.75])
        cbaxes2 = fig.add_axes([0.92, 0.1, 0.01, 0.75]) 
        fig.colorbar(pcm_tes, orientation='vertical',ticks=[40,60,80,100],cax=cbaxes,pad=0.2) 
        fig.colorbar(pcm, orientation='vertical',ticks=[-50,-25,0,25,50],cax=cbaxes2,pad=0.2)
	
        fig.subplots_adjust(wspace=0)
        if map_type == 'Arctic':
            plt.savefig(plots_dir+"o3_Arctic_mdl_tes_annual_"+str(target_pres[i])+"hPa_"+years[0]+"-"+years[1]+".png")
            plt.savefig(plots_dir_eps+"o3_Arctic_mdl_tes_annual_"+str(target_pres[i])+"hPa_"+years[0]+"-"+years[1]+".eps")
        else:
            plt.savefig(plots_dir+"o3_multi_mdl_tes_annual_"+str(target_pres[i])+"hPa_"+years[0]+"-"+years[1]+".png") 
            plt.savefig(plots_dir_eps+"o3_multi_mdl_tes_annual_"+str(target_pres[i])+"hPa_"+years[0]+"-"+years[1]+".eps")


    ###------------region calculations-----------------
    mid_lat =[]
    arctic_lat = []
    print(np.ma.MaskedArray.min(tes_annual))
    midlat_start = 30
    midlat_end = 60
    arctic_start = 61  
    
    for i in range(len(nvg)):
        arr=[]
        arr_artic = []
        pres = nvg[i]
        arr.append(pres)
        arr_artic.append(pres)
        val_tes = np.nanmean(tes_annual[i,compare_lat.index(midlat_start):compare_lat.index(midlat_end),:])
        arr.append(val_tes)
        val_tes_artic = np.nanmean(tes_annual[i,compare_lat.index(arctic_start):,:])
        arr_artic.append(val_tes_artic)
        for m in processed_models:
            plot_dif = multi_mdl_annual[m] - tes_annual
            plot_per_dif = plot_dif/tes_annual * 100
            #print(plot_per_dif[i].shape)
            val = np.nanmean(plot_per_dif[i][compare_lat.index(midlat_start):compare_lat.index(midlat_end),:])
            arr.append(val)
            arc_val = np.nanmean(plot_per_dif[i][compare_lat.index(arctic_start):,:])
            arr_artic.append(arc_val)
         
        mid_lat.append(arr)
        arctic_lat.append(arr_artic)
	    
    #print(mid_lat)
    cols = ['Pressure','TES']
    for m in processed_models: cols.append(str(m))
    midlat_df = pd.DataFrame(mid_lat, columns = cols)
    midlat_df.to_csv('/space/hall3/sitestore/eccc/crd/ccrn/users/rcw001/AMAP/csv_files/TES_midlat_'+years[0]+'_'+years[1]+'.csv', index=False)
    arctic_df = pd.DataFrame(arctic_lat, columns = cols)
    arctic_df.to_csv('/space/hall3/sitestore/eccc/crd/ccrn/users/rcw001/AMAP/csv_files/TES_arctic_'+years[0]+'_'+years[1]+'.csv', index=False)
 

if __name__ == "__main__":
    #multi_model_plots(map_type='Arctic',years =['2014', '2015'], models = ['CESM','CMAM','DEHM','EMEP-MSC-W','GEOS-Chem','GISS-E2.1','MATCH','MATCH-SALSA','MRI-ESM2','OsloCTM','UKESM1',"WRF-Chem"])
    #multi_model_plots(map_type='global',years =['2014', '2015'], models = ['CESM','CMAM','DEHM','EMEP-MSC-W','GEOS-Chem','GISS-E2.1','MATCH','MATCH-SALSA','MRI-ESM2','OsloCTM','UKESM1',"WRF-Chem"])
    #multi_model_plots(map_type='Arctic',years =['2008', '2009'], models = ['CESM','CMAM','DEHM','EMEP-MSC-W','GEOS-Chem','GISS-E2.1','MATCH','MATCH-SALSA','MRI-ESM2','OsloCTM','UKESM1'])
    multi_model_plots(map_type='global',years =['2008', '2009'], models = ['CESM','CMAM','DEHM','EMEP-MSC-W','GEOS-CHEM','GISS-E2.1','MATCH','MATCH-SALSA','MRI-ESM2','OsloCTM','UKESM1'])
   # half of the models at a time:
   # multi_model_plots(map_type='global',years =['2008', '2009'], models = ['CMAM','GISS-E2.1','MRI-ESM2','OsloCTM','UKESM1','WRF-Chem'])
    # 'CESM','CMAM','DEHM','EMEP-MSC-W','UKESM1'
    # 'GEOS-Chem','GISS-E2.1','MATCH','MATCH-SALSA','MRI-ESM2','OsloCTM'
    
