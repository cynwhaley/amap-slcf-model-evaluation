from netCDF4 import Dataset
import numpy as np

#location of old file
old_netcdf_dir = '/fs/site1/dev/eccc/crd/ccrn/users/cmr209/AMAP/GISS-modelE-OMA/'
#and whatever you want the new file to be called
new_netcdf_dir = 'GISS_reformatted/'

varb = ['abs550aer', 'alti', 'bcsnow', 'biogenic_emissions', 'ch4', 'clt', 'co', 'concbc', 'concnh4', 'concno3', 'concso4', 'drybc', 'dryno3', 'dryso4', 'hno3', 'hno4', 'iwp', 'lwp', 'n2o5', 'nmvocs', 'no', 'no2', 'no3', 'o3', 'od550aer', 'pan', 'pm25bc', 'pm25cm', 'pm25nh4', 'pm25no3', 'pm25oa', 'pm25so4', 'pm25ss', 'pr', 'pres', 'prw', 'ps', 'temp', 'wetbc', 'wetno3', 'wetso4']

name_format = 'GISS-modelE-OMA_type0_'

for var in varb:

    old_netcdf = old_netcdf_dir+name_format+var+'.nc'
    new_netcdf = new_netcdf_dir+name_format+var+'_reformatted.nc'

    old_mdl = Dataset(old_netcdf, 'r')
    new_mdl = Dataset(new_netcdf, 'w')


    #read in all the variables and see which ones have a month or year dependance
    var_convert = []
    for varname in old_mdl.variables.keys():
        var = old_mdl.variables[varname]
        if 'month' in var.dimensions and 'year' in var.dimensions:
            var_convert.append(varname)


     #read in the month and year variables and convert to day count

    years = (old_mdl.variables['year'][:]).tolist()
    months = (old_mdl.variables['month'][:]).tolist()
        
    t = []

    mnth_len = np.array([31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31])
    #now want to start the days count on 1850-01-01
    time_array = np.zeros((len(years), len(months)))
    for yr in years:
        #assume 365 days in a year
        base_count = (yr*365) - (1850*365)
        for mnth in months:
            #now each monthly average day count is halfway through the month
            d_count = mnth_len[:int(mnth)-1].sum()
            c = base_count+d_count+(mnth_len[int(mnth)-1])/2
            t.append(c)
            yi = years.index(yr)
            mi = months.index(mnth)
            time_array[yi, mi] = c


    #now create a new time variable

    time = new_mdl.createDimension("time", len(t))
    times = new_mdl.createVariable("time", 'float64', ("time"))
    #want to set the attributes for time
    setattr(times, 'long_name', 'time')
    setattr(times, 'units', 'days since 1850-1-01')
    setattr(times, 'calendar', '365_day')

    times[:] = t

    #copy the dimensions except for the old month year
    for dimname, dim in old_mdl.dimensions.items():
        #check to make sure not copying over old time dimensions
        if dimname == 'month' or dimname == 'year': continue

        new_mdl.createDimension(dimname, len(dim))

    #now we want to change all the variables that rely on month and year to time
    for var in var_convert:
        v = old_mdl.variables[var][:]
        ncvar = old_mdl.variables[var]
        #assuming that first two dimensions are month year
        #should change this for other models to make more general
        old_shape = list(v.shape)
        new_shape = tuple([len(t)] + old_shape[2:])
        new_var = np.zeros(new_shape)
        for i in range(len(years)):
            for j in range(len(months)):
                new_time = time_array[i,j]
                ti = t.index(new_time)
                new_var[ti] = v[j,i]
        #and then we just need to change the new dimensions
        old_dim = list(ncvar.dimensions)
        new_dim = tuple(['time'] + old_dim[2:])
        #check what the fill value should be
        fill = ncvar._FillValue
        var_new = new_mdl.createVariable(var, ncvar.dtype, new_dim, fill_value = fill)
        #now copy over the variable attributes
        for attname in ncvar.ncattrs():
            if attname == '_FillValue': continue
            setattr(var_new, attname, getattr(ncvar, attname))

        var_new[:] = new_var[:]
    

    #Now we are goign to copy over all the other variables and info into the new netcdf file

    #set the global attributes
    for attname in old_mdl.ncattrs():
        setattr(new_mdl, attname, getattr(old_mdl, attname))

    #copy all the other variables
    for varname, ncvar in old_mdl.variables.items():
        #check if this is one of the converted variables
        if varname in var_convert: continue
        if varname == 'month' or varname == 'year': continue

        var = new_mdl.createVariable(varname, ncvar.dtype, ncvar.dimensions)
        #copy the variable attributes
        for attname in ncvar.ncattrs():
            setattr(var, attname, getattr(ncvar, attname))

        #copy the actual variable data over
        var[:] = ncvar[:]

    old_mdl.close()
    new_mdl.close()
