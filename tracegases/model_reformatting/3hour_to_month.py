#convert the 3 hourly alti data to monthly alti data
#save as new netCDF file

from netCDF4 import Dataset
from netCDF4 import num2date, date2num
import numpy as np
from datetime import datetime

#location of old file
old_netcdf_dir = '/fs/site1/dev/eccc/crd/ccrn/users/cmr209/AMAP/EMEP-MSCW/'
change_var = ['concbc', 'concoa', 'concso4']
#old_netcdf_dir = '/user/nphome1/009/Documents/new_mdl/'

for var in change_var:
        old_netcdf = 'EMEP-MSCW_tp0_v03_'+var+'_3hour_2008_2009.nc'
        new_netcdf = 'EMEP-MSCW_tp0_v03_'+var+'_monthly_2008_2009.nc'

        old_mdl = Dataset(old_netcdf_dir+old_netcdf, 'r')
        new_mdl = Dataset(new_netcdf, 'w')

        time = old_mdl.variables['time'][:]
        time_units = old_mdl.variables['time'].getncattr('units')
        #time_cal = old_mdl.variables['time'].getncattr('calendar')
        #now lets figure out the months and get average times
        times = num2date(time, units=time_units, calendar='gregorian')

        new_times = set()
        for t in times:
            y = t.year
            m = t.month
            new_times.add((y, m))

        new_times = list(new_times)
        new_times.sort(key=lambda tup: (tup[0], tup[1]))

        #lets make a new averaged alti variable
        old_alti = old_mdl.variables[var][:]
        lon_dim= old_mdl.dimensions['lon'].size
        lat_dim = old_mdl.dimensions['lat'].size
        lev_dim = old_mdl.dimensions['lev'].size
        old_time_len = old_mdl.dimensions['time'].size

        old_time_units = old_mdl.variables['time'].getncattr('units')

        new_alti = np.zeros((len(new_times), lev_dim, lat_dim, lon_dim))
        count = np.zeros((len(new_times), lev_dim, lat_dim, lon_dim))
        #now lets do the average
        for i in range(old_time_len):
            t = times[i]
            ym = (t.year, t.month)
            j = new_times.index(ym)
            new_alti[j,:,:,:] = np.add(old_alti[i,:,:,:], new_alti[j,:,:,:])
            count[j,:,:,:] += 1

        new_alti = np.divide(new_alti, count)

        #now create a new time variable

        time_new = new_mdl.createDimension("time", len(new_times))
        times_new = new_mdl.createVariable("time", 'float64', ("time"))
        #want to set the attributes for time
        #setattr(times_new, 'long_name', 'Time')
        #setattr(times_new, 'units', old_time_units)
        setattr(times_new, 'calendar', 'gregorian') # this is just for emep
        #setattr(times_new, 'axis', 'T')
        #setattr(times_new, 'standard name', 'time')
        for attname in old_mdl.variables['time'].ncattrs():
            if attname == 'numberofrecords': continue
            setattr(times_new, attname, getattr(old_mdl.variables['time'], attname)) 

        #figure out the times as num
        num_times = []
        for i in range(len(new_times)):
            do = datetime(new_times[i][0], new_times[i][1], 1)
            num_times.append(date2num(do, units=time_units, calendar='gregorian'))

        times_new[:] = num_times

        #copy the dimensions except for the old month year
        for dimname, dim in old_mdl.dimensions.items():
            #check to make sure not copying over old time dimensions
            if dimname == 'time' : continue
            new_mdl.createDimension(dimname, len(dim))

        #lets make the new averaged variable
        ncvar = old_mdl.variables[var]
        #old_fill = old_mdl.variables[var]._FillValue
        var_new = new_mdl.createVariable(var, "float64", ("time", "lev", "lat", "lon"))
        #set attributes
        for attname in ncvar.ncattrs():
               if attname == '_FillValue': continue
               setattr(var_new, attname, getattr(ncvar, attname))

        var_new[:] = new_alti


        #Now we are going to copy over all the other variables and info into the new netcdf file

        #set the global attributes
        for attname in old_mdl.ncattrs():
            setattr(new_mdl, attname, getattr(old_mdl, attname))

        #copy all the other variables
        for varname, ncvar in old_mdl.variables.items():
            #check if this is one of the converted variables
            if varname == 'time' or varname == var: continue

            var = new_mdl.createVariable(varname, ncvar.dtype, ncvar.dimensions)
            #copy the variable attributes
            for attname in ncvar.ncattrs():
                setattr(var, attname, getattr(ncvar, attname))

            #copy the actual variable data over
            var[:] = ncvar[:]

        old_mdl.close()
        new_mdl.close()
