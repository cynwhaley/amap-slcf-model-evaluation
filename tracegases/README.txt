Plot creation and comparision of AMAP SCLF models to numerous trace gas observational datasets. These python scripts were developed by Tahya Weiss-Gibbons, Sujay Manish Damani, and Cynthia Whaley (correspondance: cynthia.whaley@ec.gc.ca) from Environment and Climate Change Canada for the 2021 AMAP SLCF assessment report and subsequent journal publications.


**Note: All scripts mentioned here should be run in a conda environment, in order to have the correct versions and packages. 
The environment build file is conda_build.txt and instructions on how to setup a conda environment on the supercomputer can be found in conda_setup.txt


MOPITT

Creates global plot comparisions of CO concentrations at different pressure levels. 


TES

Creates global maps of ozone and methane VMRs different pressure levels after interpolating models and measurements to a 1x1 horizontal grid, and a given vertical grid.


OZONE, NO2, and CO SURFACE COMPARISONS

Looks at surface ozone, NO2, and CO VMRs for North America, Europe and Asia using the CSN, CMDL, NAPS, EU Environmental site data, and Chinese Environmental agency site data datasets. Also includes some single station measurements.


METHANE SURFACE COMPARISONS

Looks at the surface concentrations of methane from World Data Centre Greenhouse Gas measurements.
 
 
OZONESONDES

Ozone profiles for various Arctic sites. ozonesondes.py compares monthly model output to monthly averaged ozonesondes (from WOUDC). ozonesondes_3hr.py compared the closest-in-time output from 3-hourly models and then does the monthly averaging. And ozonesonde_longTS.py just extracts vertical profiles at ozonesonde locations from the models that provided 1990-2015 output. This one doesn't compare to measurements.
 

AIRCRAFT

These are model plotting scripts for vertical profiles of O3, CO, NO2, and PAN. They are meant for comparison to the ATOM aircraft measurements, though the AMAP SLCF models were for 2014-15, and the ATOM campaign was in 2016-17.


MODEL REFORMATTING SCRIPTS

3hour_to_month.py

Converts 3 hourly model data to monthly averages. Takes in the file name format of the 3 houlry files, the directory and which variables to convert. Will output the new monthly files in the current working directory.

model_time_conversion.py

Converts the model time variables from multiple variables (ie, year and month) to one time variable. 
Times are given as a single float, as days from 01-01-1850. 
Takes in the variables to convert, the file name format, and the directory of the old model files. 
Outputs to a specified directory.

level_extraction_3hr.py

Takes the vertical profiles of models at certain station locations and outputs to text files.
Model extraction code written for MRI-ESM and MATCH. 
Model extraction code can be adapted from TES or MOPITT extraction code, following the format for multipl species,
in the currently written code.



