"""
ozonesondes.py
Based on TES program by
Author: Tahya Weiss-Gibbons
January 2020
tahya.weissgibbons@gmail.com
Updates by:
Author: Cyndi Whaley
April 2021
cynthia.whaley@canada.ca

Takes in a list of models and a specified time period and compares model data to WOUDC ozonesonde data in that time period.
"""
import os
import sys
import csv
import argparse
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
from io import StringIO
import re
from datetime import date, datetime
from bisect import bisect_left
from netCDF4 import Dataset, num2date
from scipy.interpolate import interpn, interp2d
from mpl_toolkits.basemap import Basemap


def cesm_model(wc_lat=74.6973,wc_lon=-94.8297,years=['2014', '2015']):
    print("Reading in CESM data...")
    root_mdl ='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/CESM/'
    mdl_files = {'o3': 'CESM_type0_o3_'+years[0]+'_'+years[1]+'_3h.nc', 'pres': 'CESM_type0_ps_'+years[0]+'_'+years[1]+'_3h.nc'}
    ab_file = '/space/hall3/sitestore/eccc/crd/ccrn/users/rcw001/AMAP/hyam_hybm_P0_CESM.nc'

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = np.array(mf.variables['lon'][:])
    mdl_lat = np.array(mf.variables['lat'][:])
    
    # determine the indices of the given wc_lat and wc_lon
    if wc_lon < 0:
        wc_lon= wc_lon+360
    latind=np.abs(wc_lat-mdl_lat).argmin()
    lonind=np.abs(wc_lon-mdl_lon).argmin()
    print('latind, lonind=',latind,lonind)
    
    try:
        mdl_lev = (mf.variables['lvl'][:]).tolist()
    except KeyError:
        mdl_lev = (mf.variables['lev'][:]).tolist()

    mdl_vmro3 = mf.variables['o3'][:,:,latind,lonind]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    nm_lev = len(mdl_lev)
    nm_year = 2 

    # get a & b values to calculate 3D pressure
    mf = Dataset(ab_file,'r')
    mdl_ap = mf.variables['hyam'][:]
    mdl_b = mf.variables['hybm'][:]
    mf.close()
    
    # get surface pressure
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_ps = mf.variables['ps'][:,latind,lonind]
    mf.close()

    # calculate the vertical profile of modelled pressure:
    mdl_pres = np.zeros((len(mdl_time),nm_lev))
    for i in range(len(mdl_time)):
        mdl_pres[i,:] = (mdl_ap*100000 + mdl_b*mdl_ps[i])*0.01 #convert from Pa to hPa

    mdl_mnth_o3 = np.zeros((nm_year, 12, nm_lev))
    mdl_mnth_pres = np.zeros((nm_year, 12, nm_lev))
    mnth_count = np.zeros((nm_year, 12, nm_lev))

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year-int(years[0])
        mdl_mnth_o3[y,m,:] = np.add(mdl_vmro3[i,:], mdl_mnth_o3[y,m,:])
        mdl_mnth_pres[y,m,:] = np.add(mdl_pres[i,:], mdl_mnth_pres[y,m,:])
        mnth_count[y,m,:] +=1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:] ,mnth_count[:,:,:])
    mdl_mnth_pres[:,:,:] = np.divide(mdl_mnth_pres[:,:,:], mnth_count[:,:,:])
    
    return mdl_mnth_o3, mdl_mnth_pres, times, nm_lev

def cmam_model(wc_lat=74.6973,wc_lon=-94.8297,years=['2014', '2015']):
    print("Reading in CMAM data...")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/CMAM/'
    mdl_files = {'o3': 'vmro3_month_CMAM_AMAP2020-SD_r1i1p1_199001-201812.nc'}

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = np.array(mf.variables['lon'][:])
    mdl_lat = np.array(mf.variables['lat'][:])
    
    # determine the indices of the given wc_lat and wc_lon
    if wc_lon < 0:
        wc_lon= wc_lon+360
    latind=np.abs(wc_lat-mdl_lat).argmin()
    lonind=np.abs(wc_lon-mdl_lon).argmin()
    print('latind, lonind=',latind,lonind)
    
    mdl_lev = (mf.variables['lev'][:]).tolist()
    mdl_vmro3 = mf.variables['vmro3'][:,:,latind,lonind]
    o3_units = mf.variables['vmro3'].getncattr('units')

    if o3_units == "mole mole-1": #equivalent to volume ozone per volume air from ideal gas law
        mdl_vmro3 = mdl_vmro3*(10**9) #convert to ppbv

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    #need to find pressure from formula p = ap + b*ps at every level
    mdl_ap = mf.variables['ap'][:]
    mdl_b = mf.variables['b'][:]
    mdl_ps = mf.variables['ps'][:,latind,lonind]

    mf.close()

    nm_lev = len(mdl_lev)
    nm_year = 2

    mdl_mnth_o3 = np.zeros((nm_year,12, nm_lev))
    mdl_mnth_pres = np.zeros((nm_year,12, nm_lev))
    mnth_count = np.zeros((nm_year,12, nm_lev))
    pres_count = np.zeros((nm_year,12, nm_lev))

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year-int(years[0])
        mdl_mnth_o3[y,m,:] = np.add(mdl_vmro3[i,:], mdl_mnth_o3[y,m,:])
        mnth_count[y,m,:] +=1
        tmp_pres = (mdl_ap + mdl_b*mdl_ps[i])*0.01 #convert to hpa
        mdl_mnth_pres[y,m,:] = np.add(tmp_pres, mdl_mnth_pres[y,m,:])
        pres_count[y,m,:] += 1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:], mnth_count[:,:,:])
    mdl_mnth_pres[:,:,:] = np.divide(mdl_mnth_pres[:,:,:], pres_count[:,:,:])

    return mdl_mnth_o3, mdl_mnth_pres, times, nm_lev

def dehm_model(wc_lat=74.6973,wc_lon=-94.8297,years=['2014', '2015']):
    print("Reading in DEHM data....")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/DEHM/'
    mdl_files = {'o3': 'DEHM_type0_o3_1990_2018.nc', 'pres': 'DEHM_type0_pres_1990_2018.nc'}

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = np.array(mf.variables['lon'][:])
    mdl_lat = np.array(mf.variables['lat'][:])
    try:
        mdl_lev = (mf.variables['lvl'][:]).tolist()
    except KeyError:
        mdl_lev = (mf.variables['lev'][:]).tolist()

    # determine the indices of the given wc_lat and wc_lon
    if wc_lon < 0:
        wc_lon= wc_lon+360
    latind=np.abs(wc_lat-mdl_lat).argmin()
    lonind=np.abs(wc_lon-mdl_lon).argmin()
    print('latind, lonind=',latind,lonind)
    
    mdl_vmro3 = mf.variables['o3'][:,:,latind,lonind]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    nm_lev = len(mdl_lev)
    nm_year = 2

    mdl_mnth_o3 = np.zeros((nm_year,12, nm_lev))
    mdl_mnth_pres = np.zeros((nm_year,12, nm_lev))
    mnth_count = np.zeros((nm_year,12, nm_lev))

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_pres = mf.variables['pres'][:,:,latind,lonind]

    #check the units on the pressure 
    pres_units = mf.variables['pres'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf.close()

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year-int(years[0])
        mdl_mnth_o3[y,m,:] = np.add(mdl_vmro3[i,:], mdl_mnth_o3[y,m,:])
        mdl_mnth_pres[y,m,:] = np.add(mdl_pres[i,:], mdl_mnth_pres[y,m,:])
        mnth_count[y,m,:] +=1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:] ,mnth_count[:,:,:])
    mdl_mnth_pres[:,:,:] = np.divide(mdl_mnth_pres[:,:,:], mnth_count[:,:,:])

    return mdl_mnth_o3, mdl_mnth_pres, times, nm_lev

def emep_model(wc_lat=74.6973,wc_lon=-94.8297,years=['2014', '2015']):
    print("Reading in emep data....")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/EMEP-MSCW/'
    mdl_files = {'o3': 'EMEP-MSCW_tp0_v04_o3_3hour_'+years[0]+'_'+years[1]+'.nc', 'pres': 'EMEP-MSCW_tp0_v04_ps_3hour_'+years[0]+'_'+years[1]+'.nc'}
    ab_file = '/space/hall3/sitestore/eccc/crd/ccrn/users/sta109/AMAP_plots/tes_scripts/emep_a_b.csv'
    
    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = np.array(mf.variables['lon'][:])
    mdl_lat = np.array(mf.variables['lat'][:])
    try:
        mdl_lev = (mf.variables['lvl'][:]).tolist()
    except KeyError:
        mdl_lev = (mf.variables['lev'][:]).tolist()
    
    # determine the indices of the given wc_lat and wc_lon
    if wc_lon < 0:
        wc_lon= wc_lon+360
    latind=np.abs(wc_lat-mdl_lat).argmin()
    lonind=np.abs(wc_lon-mdl_lon).argmin()
    print('latind, lonind=',latind,lonind)
    
    mdl_vmro3 = mf.variables['o3'][:,:,latind,lonind]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
   # time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    nm_year = 2
    nm_lev = len(mdl_lev)

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_ps = mf.variables['ps'][:,latind,lonind]

    mf.close()

    #also need the a and b coordinates to calcualte the pressure at each level
    #info in csv in format lev a b
    #the pressure is from the formula a+b*ps
    with open(ab_file, 'r') as f:
        headers = f.readline()
        lines = f.readlines()

    mdl_a = []
    mdl_b =[]
    for r in lines:
        t = r.split(',')
        mdl_a.append(float(t[0].strip()))
        mdl_b.append(float(t[1].strip()))

    mdl_a = np.asarray(mdl_a)
    mdl_b = np.asarray(mdl_b)

    times = num2date(mdl_time, units=time_units, calendar='gregorian')

    mdl_mnth_o3 = np.zeros((nm_year,12, nm_lev))
    mdl_mnth_pres = np.zeros((nm_year,12, nm_lev))
    mnth_count = np.zeros((nm_year,12, nm_lev))
    pres_count = np.zeros((nm_year,12, nm_lev))

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year-int(years[0])
        mdl_mnth_o3[y,m,:] = np.add(mdl_mnth_o3[y,m,:], mdl_vmro3[i,:])
        mnth_count[y,m,:] += 1
        tmp = mdl_b*mdl_ps[i]
        tmp_pres = (mdl_a + tmp)*0.01 #convert to hpa
        mdl_mnth_pres[y,m,:] = np.add(tmp_pres, mdl_mnth_pres[y,m,:])
        pres_count[y,m,:] += 1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:] ,mnth_count[:,:,:])
    mdl_mnth_pres[:,:,:] = np.divide(mdl_mnth_pres[:,:,:], mnth_count[:,:,:])
    
    return mdl_mnth_o3, mdl_mnth_pres, times, nm_lev
    

def geoschem_model(wc_lat=74.6973,wc_lon=-94.8297,years=['2014', '2015']):
    print("Reading in GEOS-Chem data...")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/GEOS-CHEM/'
    mdl_files = {'o3': 'GEOS-CHEM_type0_o3_'+years[0]+'_'+years[1]+'.nc', 'pres': 'GEOS-CHEM_type0_pres_'+years[0]+'_'+years[1]+'.nc'}

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = np.array(mf.variables['lon'][:])
    mdl_lon = [ln+360 if ln<0 else ln for ln in mdl_lon]
    mdl_lon = np.array(mdl_lon)
    mdl_lat = np.array(mf.variables['lat'][:])
    
    # determine the indices of the given wc_lat and wc_lon
    if wc_lon < 0:
        wc_lon= wc_lon+360
    latind=np.abs(wc_lat-mdl_lat).argmin()
    lonind=np.abs(wc_lon-mdl_lon).argmin()
    print('latind, lonind=',latind,lonind) 
    
    mdl_vmro3 = mf.variables['o3'][:,:,latind,lonind]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('long_name')
    time_cal = mf.variables['time'].getncattr('calendar')
    nm_lev = mf.dimensions['lev'].size

    mf.close()
    
    nm_year = 2

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_pres = mf.variables['pres'][:,:,latind,lonind]
    p_time = mf.variables['time'][:]
    ptime_units = mf.variables['time'].getncattr('long_name')
    ptime_cal = mf.variables['time'].getncattr('calendar')
    
    #check the units on the pressure
    pres_units = mf.variables['pres'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf.close()

    times = num2date(mdl_time, units=time_units, calendar=time_cal)
    ptimes = num2date(p_time, units=ptime_units, calendar=ptime_cal)

    mdl_mnth_o3 = np.zeros((nm_year, 12, nm_lev))
    mdl_mnth_pres = np.zeros((nm_year, 12, nm_lev))
    mnth_count = np.zeros((nm_year, 12, nm_lev))
    pmnth_count = np.zeros((nm_year, 12, nm_lev))

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year-int(years[0])
        mdl_mnth_o3[y,m,:] = np.add(mdl_vmro3[i,:], mdl_mnth_o3[y,m,:])
        mnth_count[y,m,:] +=1

    for t in ptimes:
        if str(t.year) not in years:
            continue
        i = list(ptimes).index(t) 
        m = t.month - 1
        yr = t.year
        y = t.year-int(years[0])
        mdl_mnth_pres[y,m,:] = np.add(mdl_pres[i,:], mdl_mnth_pres[y,m,:])
        pmnth_count[y,m,:] +=1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:] ,mnth_count[:,:,:])
    mdl_mnth_pres[:,:,:] = np.divide(mdl_mnth_pres[:,:,:], pmnth_count[:,:,:])

    return mdl_mnth_o3, mdl_mnth_pres, times, nm_lev
    
def giss_model(wc_lat=74.6973,wc_lon=-94.8297,years=['2014', '2015']):
    print("Reading in GISS-modelE-OMA data...")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/GISS-modelE-OMA/GISS_reformatted/'
    mdl_files = {'o3': 'GISS-modelE-OMA_type0_o3_NCEP_reformatted.nc', 'pres': 'GISS-modelE-OMA_type0_pres_NCEP_reformatted.nc'}

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = np.array(mf.variables['lon'][:])
    mdl_lat = np.array(mf.variables['lat'][:])
    try:
        mdl_lev = (mf.variables['lvl'][:]).tolist()
    except KeyError:
        mdl_lev = (mf.variables['lev'][:]).tolist()

    # determine the indices of the given wc_lat and wc_lon
    if wc_lon < 0:
        wc_lon= wc_lon+360
    latind=np.abs(wc_lat-mdl_lat).argmin()
    lonind=np.abs(wc_lon-mdl_lon).argmin()
    print('latind, lonind=',latind,lonind) 
         
    mdl_vmro3 = mf.variables['o3'][:,:,latind,lonind]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    nm_year = 2
    nm_lev = len(mdl_lev)

    mdl_mnth_o3 = np.zeros((nm_year, 12, nm_lev))
    mdl_mnth_pres = np.zeros((nm_year, 12, nm_lev))
    mnth_count = np.zeros((nm_year, 12, nm_lev))

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_pres = mf.variables['pres'][:,:,latind,lonind]

    #check the units on the pressure 
    pres_units = mf.variables['pres'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf.close()

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year-int(years[0])
        mdl_mnth_o3[y,m,:] = np.add(mdl_vmro3[i,:], mdl_mnth_o3[y,m,:])
        mdl_mnth_pres[y,m,:] = np.add(mdl_pres[i,:], mdl_mnth_pres[y,m,:])
        mnth_count[y,m,:] +=1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:] ,mnth_count[:,:,:])
    mdl_mnth_pres[:,:,:] = np.divide(mdl_mnth_pres[:,:,:], mnth_count[:,:,:])

    return mdl_mnth_o3, mdl_mnth_pres, times, nm_lev

def match_model(wc_lat=74.6973,wc_lon=-94.8297,years=['2014', '2015']):
    print("Reading in MATCH model data...")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/MATCH/'
    mdl_files = {'o3': ['MATCH_Type0_o3_'+years[0]+'.nc', 'MATCH_Type0_o3_'+years[1]+'.nc'], 'pres': ['MATCH_Type0_pres_'+years[0]+'.nc', 'MATCH_Type0_pres_'+years[1]+'.nc']}

    #first lets read in the info from the o3 file
    mf1 = Dataset(root_mdl+mdl_files['o3'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['o3'][1], 'r')
    mdl_lon = np.array(mf1.variables['lon'][:])
    mdl_lat = np.array(mf1.variables['lat'][:])
    
    # determine the indices of the given wc_lat and wc_lon
    if wc_lon < 0:
        wc_lon= wc_lon+360
    latind=np.abs(wc_lat-mdl_lat).argmin()
    lonind=np.abs(wc_lon-mdl_lon).argmin()
    print('latind, lonind=',latind,lonind) 
    
    mdl_lev = (mf1.variables['lev'][:]).tolist()
    mdl_vmro3 = np.concatenate((mf1.variables['o3'][:,:,latind,lonind], mf2.variables['o3'][:,:,latind,lonind]))

    mdl_time = np.concatenate((mf1.variables['time'][:], mf2.variables['time'][:]))
    time_units = mf1.variables['time'].getncattr('units')
    time_cal = mf1.variables['time'].getncattr('calendar')

    mf1.close()
    mf2.close()

    nm_lev = len(mdl_lev)
    nm_year = 2

    mdl_mnth_o3 = np.zeros((nm_year, 12, nm_lev))
    mdl_mnth_pres = np.zeros((nm_year, 12, nm_lev))
    mnth_count = np.zeros((nm_year, 12, nm_lev))

    #now read in the data from the pressure file
    mf1 = Dataset(root_mdl+mdl_files['pres'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['pres'][1], 'r')
    mdl_pres = np.concatenate((mf1.variables['pres'][:,:,latind,lonind], mf2.variables['pres'][:,:,latind,lonind]))

    #check the units on the pressure 
    pres_units = mf1.variables['pres'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf1.close()
    mf2.close()

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year-int(years[0])
        mdl_mnth_o3[y,m,:] = np.add(mdl_vmro3[i,:], mdl_mnth_o3[y,m,:])
        mdl_mnth_pres[y,m,:] = np.add(mdl_pres[i,:], mdl_mnth_pres[y,m,:])
        mnth_count[y,m,:] +=1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:] ,mnth_count[:,:,:])
    mdl_mnth_pres[:,:,:] = np.divide(mdl_mnth_pres[:,:,:], mnth_count[:,:,:])

    return mdl_mnth_o3, mdl_mnth_pres, times, nm_lev

def matchsalsa_model(wc_lat=74.6973,wc_lon=-94.8297,years=['2014', '2015']):
    print("Reading in MATCH model data...")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/MATCH-SALSA/'																					      
    mdl_files = {'o3': ['MATCH-SALSA_type0_o3_'+years[0]+'.nc', 'MATCH-SALSA_type0_o3_'+years[1]+'.nc'], 'pres': ['MATCH-SALSA_type0_pres_'+years[0]+'.nc', 'MATCH-SALSA_type0_pres_'+years[1]+'.nc']}
    match_ab_file = '/space/hall3/sitestore/eccc/crd/ccrn/users/sta109/AMAP_plots/tes_scripts/match-salsa_ab.csv'

    #first lets read in the info from the o3 file															          
    mf1 = Dataset(root_mdl+mdl_files['o3'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['o3'][1], 'r')															          
    mdl_lon = np.array(mf1.variables['lon'][:])															   
    mdl_lat = np.array(mf1.variables['lat'][:])															          
    
    # determine the indices of the given wc_lat and wc_lon
    if wc_lon < 0:
        wc_lon= wc_lon+360
    latind=np.abs(wc_lat-mdl_lat).argmin()
    lonind=np.abs(wc_lon-mdl_lon).argmin()
    print('latind, lonind=',latind,lonind) 
    
    mdl_lev = (mf1.variables['lev'][:]).tolist()															          
    mdl_vmro3 = np.concatenate((mf1.variables['o3'][:,:,latind,lonind], mf2.variables['o3'][:,:,latind,lonind]))											          
																					          
    mdl_time = np.concatenate((mf1.variables['time'][:], mf2.variables['time'][:]))															          
																					          
    mf1.close() 																			          
    mf2.close()
																					          
    nm_year = 2																		          
    nm_lev = len(mdl_lev)																		         

    #now read in the data from the pressure file
    #info in csv in format lev a b
    #the pressure is from the formula a+b*ps
    with open(match_ab_file, 'r') as f:
        headers = f.readline()
        lines = f.readlines()

    mdl_a = []
    mdl_b =[]
    for r in lines:
        t = r.split('|')
        if(len(t) != 3): continue
        mdl_a.append(float(t[1].strip()))
        mdl_b.append(float(t[2].strip()))

    mdl_a = np.asarray(mdl_a)
    mdl_b = np.asarray(mdl_b)

    #now read in the surface pressure
    mf1 = Dataset(root_mdl+mdl_files['pres'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['pres'][1], 'r')

    mdl_ps = np.concatenate((mf1.variables['pres'][:,latind,lonind], mf2.variables['pres'][:,latind,lonind]))
    
    mf1.close()
    mf2.close()
 																					          
    mdl_mnth_o3 = np.zeros((nm_year, 12, nm_lev))
    mdl_mnth_pres = np.zeros((nm_year, 12, nm_lev))
    mnth_count = np.zeros((nm_year, 12, nm_lev))
																					          
    #now average over just the interested years 															          
    for t in mdl_time:																			              
        if str(t)[:4] not in years:																	        	  
            continue																			              
        i = list(mdl_time).index(t)																	              
        m = int(str(t)[4:6])-1
        yr = int(str(t)[:4])
        y = yr - int(years[0])																			              
        mdl_mnth_o3[y,m,:] = np.add(mdl_mnth_o3[y,m,:], mdl_vmro3[i,:])
        mnth_count[y,m,:] += 1
        tmp = mdl_b*mdl_ps[i]
        tmp_pres = (mdl_a + tmp)*0.01 #convert to hpa
        mdl_mnth_pres[y,m,:] = np.add(tmp_pres, mdl_mnth_pres[y,m,:])
																					        	      
    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:] ,mnth_count[:,:,:]) 											        	      
    mdl_mnth_pres[:,:,:] = np.divide(mdl_mnth_pres[:,:,:], mnth_count[:,:,:])											        	      

    return mdl_mnth_o3, mdl_mnth_pres, mdl_time, nm_lev

def mri_model(wc_lat=74.6973,wc_lon=-94.8297,years=['2014', '2015']):
    print("Reading in MRI-ESM data...")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/MRI-ESM/'    
    mdl_files = {'o3': 'MRI-ESM_type0_o3_'+years[0]+'-'+years[1]+'.nc', 'pres': 'MRI-ESM_type0_ps_T42L80_128x32NH_3hr_'+years[0]+'-'+years[1]+'.nc'}

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = np.array(mf.variables['lon'][:])
    mdl_lat = np.array(mf.variables['lat'][:])
    mdl_lev = (mf.variables['lev'][:]).tolist()
    
    # determine the indices of the given wc_lat and wc_lon
    if wc_lon < 0:
        wc_lon= wc_lon+360
    latind=np.abs(wc_lat-mdl_lat).argmin()
    lonind=np.abs(wc_lon-mdl_lon).argmin()
    print('latind, lonind=',latind,lonind) 
    
    mdl_vmro3 = mf.variables['o3'][:,:,latind,lonind]
    o3_units = mf.variables['o3'].getncattr('units')

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')
    
    mf.close()
    
    nm_year = 2
    nm_lev = len(mdl_lev)
    
    mdl_mnth_o3 = np.zeros((nm_year, 12, nm_lev))
    mdl_mnth_pres = np.zeros((nm_year, 12, nm_lev))
    mnth_count = np.zeros((nm_year, 12, nm_lev))
    pres_count = np.zeros((nm_year, 12, nm_lev))
    
    #now read in the data from the pressure file
    #need to find pressure from formula p = a*p0 + b*ps at every level
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')    
    mdl_p0 = mf.variables['p0'][:]
    mdl_a = mf.variables['a'][:]
    mdl_b = mf.variables['b'][:]
    mdl_ps = mf.variables['ps'][:,latind,lonind]
    
    mf.close() 
    
    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year-int(years[0])
        mdl_mnth_o3[y,m,:] = np.add(mdl_vmro3[i,:], mdl_mnth_o3[y,m,:])
        mnth_count[y,m,:] +=1
        tmp_pres = (mdl_a*mdl_p0 + mdl_b*mdl_ps[i])*0.01 #convert to hpa
        mdl_mnth_pres[y,m,:] = np.add(tmp_pres, mdl_mnth_pres[y,m,:])
        pres_count[y,m,:] += 1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:], mnth_count[:,:,:])
    mdl_mnth_pres[:,:,:] = np.divide(mdl_mnth_pres[:,:,:], pres_count[:,:,:])

    return mdl_mnth_o3, mdl_mnth_pres, times, nm_lev
    	
def oslo_model(wc_lat=74.6973,wc_lon=-94.8297,years=['2014', '2015']):
    print("Reading in OsloCTM model data...")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/OsloCTM/'
    mdl_files = {'o3': ['OsloCTM_type0_o3_monthly_'+years[0]+'.nc', 'OsloCTM_type0_o3_monthly_'+years[1]+'.nc'], 'pres': ['OsloCTM_type0_pres_monthly_'+years[0]+'.nc', 'OsloCTM_type0_pres_monthly_'+years[1]+'.nc']}

    #first lets read in the info from the o3 file
    mf1 = Dataset(root_mdl+mdl_files['o3'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['o3'][1], 'r')
    mdl_lon = np.array(mf1.variables['lon'][:])
    mdl_lat = np.array(mf1.variables['lat'][:])
    
    # determine the indices of the given wc_lat and wc_lon
    if wc_lon < 0:
        wc_lon= wc_lon+360
    latind=np.abs(wc_lat-mdl_lat).argmin()
    lonind=np.abs(wc_lon-mdl_lon).argmin()
    print('latind, lonind=',latind,lonind)     
    
    mdl_lev = (mf1.variables['lev'][:]).tolist()
    mdl_vmro3 = np.concatenate((mf1.variables['o3'][:,:,latind,lonind], mf2.variables['o3'][:,:,latind,lonind]))

    mdl_time = np.concatenate((mf1.variables['time'][:], mf2.variables['time'][:]))
    #weren't specified in o3 files
    #taken from the pressure file, assuming the same
    time_units = "days since 2001-01-01 00:00:00"
    time_cal = "Julian"

    mf1.close()
    mf2.close()

    nm_year = 2
    nm_lev = len(mdl_lev)

    mdl_mnth_o3 = np.zeros((nm_year,12, nm_lev))
    mdl_mnth_pres = np.zeros((nm_year,12, nm_lev))
    mnth_count = np.zeros((nm_year,12, nm_lev))

    #now read in the data from the pressure file
    mf1 = Dataset(root_mdl+mdl_files['pres'][0], 'r')
    mf2 = Dataset(root_mdl+mdl_files['pres'][1], 'r')
    mdl_pres = np.concatenate((mf1.variables['pres'][:,:,latind,lonind], mf2.variables['pres'][:,:,latind,lonind]))

    #check the units on the pressure 
    pres_units = mf1.variables['pres'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf1.close()
    mf2.close()

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year - int(years[0])
        mdl_mnth_o3[y,m,:] = np.add(mdl_vmro3[i,:], mdl_mnth_o3[y,m,:])
        mdl_mnth_pres[y,m,:] = np.add(mdl_pres[i,:], mdl_mnth_pres[y,m,:])
        mnth_count[y,m,:] +=1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:] ,mnth_count[:,:,:])
    mdl_mnth_pres[:,:,:] = np.divide(mdl_mnth_pres[:,:,:], mnth_count[:,:,:])

    return mdl_mnth_o3, mdl_mnth_pres, times, nm_lev

def ukesm_model(wc_lat=74.6973,wc_lon=-94.8297,years=['2014', '2015']):
    print("Reading in UKESM1 model data...")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/UKESM1/'
    mdl_files = {'o3': 'UKESM1_type0_monthly_ozone_volume_mixing_ratio_'+years[0]+'_'+years[1]+'.nc', 'pres': 'UKESM1_type0_monthly_air_pressure_'+years[0]+'_'+years[1]+'.nc'}

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = np.array(mf.variables['longitude'][:])
    mdl_lat = np.array(mf.variables['latitude'][:])
    mdl_lev = (mf.variables['level_height'][:]).tolist()
    
    # determine the indices of the given wc_lat and wc_lon
    if wc_lon < 0:
        wc_lon= wc_lon+360
    latind=np.abs(wc_lat-mdl_lat).argmin()
    lonind=np.abs(wc_lon-mdl_lon).argmin()
    print('latind, lonind=',latind,lonind)     
    
    mdl_vmro3 = mf.variables['ozone_volume_mixing_ratio'][:,:,latind,lonind]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    nm_year = 2
    nm_lev = len(mdl_lev)

    mdl_mnth_o3 = np.zeros((nm_year, 12, nm_lev))
    mdl_mnth_pres = np.zeros((nm_year, 12, nm_lev))
    mnth_count = np.zeros((nm_year, 12, nm_lev))

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_pres = mf.variables['air_pressure'][:,:,latind,lonind]

    #check the units on the pressure 
    pres_units = mf.variables['air_pressure'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf.close()

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year - int(years[0])
        mdl_mnth_o3[y,m,:] = np.add(mdl_vmro3[i,:], mdl_mnth_o3[y,m,:])
        mdl_mnth_pres[y,m,:] = np.add(mdl_pres[i,:], mdl_mnth_pres[y,m,:])
        mnth_count[y,m,:] +=1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:] ,mnth_count[:,:,:])
    mdl_mnth_pres[:,:,:] = np.divide(mdl_mnth_pres[:,:,:], mnth_count[:,:,:])

    return mdl_mnth_o3, mdl_mnth_pres, times, nm_lev

def wrfchem_model(wc_lat=74.6973,wc_lon=-94.8297,years=['2014', '2015']):
    print("Reading in WRF-Chem model data...")
    root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/WRF-CHEM/'
    mdl_files = {'o3': 'WRF-CHEM_type0_o3.nc', 'pres': 'WRF-CHEM_type0_pres.nc'}

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = np.array(mf.variables['XLONG'][:])
    mdl_lat = np.array(mf.variables['XLAT'][:])

    # determine the indices of the given wc_lat and wc_lon
    if wc_lon < 0:
        wc_lon= wc_lon+360
    latind=np.abs(wc_lat-mdl_lat).argmin()
    lonind=np.abs(wc_lon-mdl_lon).argmin()
    print('latind, lonind=',latind,lonind) 

    mdl_vmro3 = mf.variables['o3'][:,:,latind,lonind]

    mdl_time = mf.variables['Times'][:]

  #  nm_mdl_lon = mf.dimensions['west_east'].size
  #  nm_mdl_lat = mf.dimensions['south_north'].size

    mf.close()

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_pres = mf.variables['pres'][:,:,latind,lonind]
    nm_lev = mf.dimensions['bottom_top'].size

    #check the units on the pressure 
    pres_units = mf.variables['pres'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf.close()

    nm_year = 2
    mdl_mnth_o3 = np.zeros((nm_year, 12, nm_lev))
    mdl_mnth_pres = np.zeros((nm_year, 12, nm_lev))
    mnth_count = np.zeros((nm_year, 12, nm_lev))

    #convert the times
    rep = {'b': '', "'": "", ' ': '', "\n": "", '[': "", ']': ""} #for formating strings
    rep = dict((re.escape(k), v) for k, v in rep.items())
    pattern = re.compile("|".join(rep.keys()))

    times = []
    for i in range(24):
        t = np.array2string(mdl_time[i])
        new_t = pattern.sub(lambda m: rep[re.escape(m.group(0))], t)
        #now convert to a datetime object
        d = datetime.strptime(new_t, '%Y-%m-%d_%H:%M:%S')
        times.append(d)
        
    #now average over just the interested years
    for t in times:
        if str(t.year) not in years:
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year - int(years[0])
        mdl_mnth_o3[y,m,:] = np.add(mdl_vmro3[i,:], mdl_mnth_o3[y,m,:])
        mdl_mnth_pres[y,m,:] = np.add(mdl_pres[i,:], mdl_mnth_pres[y,m,:])
        mnth_count[y,m,:] +=1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:] ,mnth_count[:,:,:])
    mdl_mnth_pres[:,:,:] = np.divide(mdl_mnth_pres[:,:,:], mnth_count[:,:,:])

    return mdl_mnth_o3, mdl_mnth_pres, times, nm_lev

def multi_model_plots(sites="Resolute",models=['CESM','CMAM','DEHM','EMEP-MSCW','GEOS-Chem','GISS-E2.1','MATCH','MATCH-SALSA','MRI-ESM2','OsloCTM','UKESM1','WRF-Chem'], years=['2014', '2015'], plots_dir='/space/hall3/sitestore/eccc/crd/ccrn/users/rcw001/AMAP/png_figs/'):
    
    # Obs directories:
    if years==['2008', '2009']:
        wc_root='/space/hall3/sitestore/eccc/crd/ccrn/obs/amap/WOUDC/'
    else:
        wc_root='/space/hall3/sitestore/eccc/crd/ccrn/obs/amap/WOUDC_2/'
	
    np.seterr(divide='ignore', invalid='ignore') #we will divide by zero at some points, ignore all those warnings

    #constant pressure levels which data is interpolated to
    cpres = [ 925.0, 850.0, 800.0, 750.0, 700.0, 650.0, 600.0, 550.0, 500.0, 450.0, 400.0, 350.0, 300.0, 250.0, 200.0, 175.0, 150.0, 125.0, 110.0, 100.0]
    nm_cpres = len(cpres)
    
    plots_dir_eps = "/space/hall3/sitestore/eccc/crd/ccrn/users/rcw001/AMAP/eps_figs/"
    
    #lets read in the observational data first
    print("Reading ozonesonde files")

    wc_cpres_profiles = []
    site_name = []
    lat = []
    lon = []
    obsyear = []
    obsmonth = []
    obsday = []
    avglat = []
    avglon = []

    directory = os.path.join(wc_root, sites)
    print('processing observations for '+sites)
    h=0
    for root, dirs, files in os.walk(directory):
        for file in files:
            filelow=file.lower()
            if not filelow.endswith(".csv"):
                continue

            with open(wc_root+sites+'/'+file, 'r') as wc:
                text = wc.read()

            blocks = text.split('\n\n')

            #Extracting the pressure profile
            profile = [i for i in blocks if i.startswith('#PROFILE')]
            profile = profile[0][8:]
            profile = profile.replace("* Tropopause", "")

            data = StringIO(profile)

            try:
                wc_profile = np.genfromtxt(data, delimiter=',', names=True)
            except(ValueError):
                print("genfromtxt failed")
                print(file)
                continue
            wc_pres = []
            wc_o3 = []

            for row in wc_profile:
                wc_pres.append(row[0])
                wc_o3.append(row[1])

            #now want to interpolate to constant pressure levels (interp requires increasing p values)
            wc_pres.reverse()
            wc_o3.reverse()
            cpres.reverse()
            wc_cp_o3 = np.interp(cpres, wc_pres, wc_o3)
            cpres.reverse() # reverse again to get back to original
            wc_cp_o3=np.flip(wc_cp_o3,axis=0)		
		
            # convert O3 partial pressure (Px, given in mPa) to mixing ratio (Cx) in ppbv:
            # Cx=Px/P*1E9, where P in hPa, and Px in mPa, which is 1E-5 factor; hence 1E4 factor below
            wc_cp_o3 = np.divide(wc_cp_o3,cpres)
            wc_cp_o3 = wc_cp_o3 * 1.E4
		
            #wc_cpres_profiles.append(wc_cp_o3) # this appends each profile end to end
            if h==0:
                wc_cpres_profiles=wc_cp_o3
            else:
                wc_cpres_profiles=np.vstack((wc_cpres_profiles,wc_cp_o3)) # append each profile as a new row
            h=h+1
	                
            #Where is this data coming from? And what time&date was the data taken at?
            stat = [i for i in blocks if i.startswith('#PLATFORM')]
            loc = [i for i in blocks if i.startswith('#LOCATION')]
            time_stamp = [i for i in blocks if i.startswith('#TIMESTAMP')]
            
            loc= loc[0][9:]
            time_stamp = time_stamp[0][10:]
            stat = stat[0][9:]
            loc = loc.split()
            time_stamp = time_stamp.split()
            stat = stat.split()

            loc_headers = loc[0].split(',')
            time_headers = time_stamp[0].split(',')
            stat_headers = stat[0].split(',')

            date_index = time_headers.index('Date')
            dte = time_stamp[1].split(',')[date_index]
            dte = dte.split('-')

            if dte[0] not in years:
                print("Opps, wrong date?")
                print(dte[0])
                print(type(dte[0]))
                continue

            lat_index = loc_headers.index('Latitude')
            lon_index = loc_headers.index('Longitude')
            lat.append(loc[1].split(',')[lat_index])
            lon.append(loc[1].split(',')[lon_index])

            obsyear.append(dte[0])
            obsmonth.append(dte[1])
            obsday.append(dte[2])
 
            stat_index = stat_headers.index('Name')
            site_name.append(stat[1].split(',')[stat_index])

        # create a datetime object from the observations
        d = {'year': obsyear, 'month': obsmonth, 'day': obsday}
        tmp = pd.DataFrame(d, columns = ['year','month','day']) 
        obstime = pd.to_datetime(tmp[['year', 'month', 'day']])
    #    print('obstime at creation=',obstime)

        # some sights have slightly different lat & lon for the same site.  
        # here take the average to keep for the lat & lon for one site/location
        # first change the list of strings into a list of numbers:
        latNum = [float(s) for s in lat]
        lonNum = [float(s) for s in lon]
        # then compute the avg of the numbers in the list:
        if len(latNum)>0:
            avglat=sum(latNum)/len(latNum)
        else:
            avglat=lat
        if len(lonNum)>0:
            avglon=sum(lonNum)/len(lonNum)
        else:
            avglon=lon
		    
    wc_lat=round(float(avglat),2)
    wc_lon=round(float(avglon),2) 
    
## OLD weird method for calculating the monthly mean.       
#    wc_mnth_count = np.zeros((12))
#     
#    for k in range(len(lat)):
#        m = int(month[k])-1
#        wc_mnth_count[m] = wc_mnth_count[m]+1
#
#    wc_cp_mnth_o3 = np.zeros((12, len(cpres), nmlat, nmlon))
#    for i in range(len(lat)):
#        j = 0
#        k = 0
#        m = int(month[i])-1
#        prof = wc_cpres_profiles[i]
#        wc_cp_mnth_o3[m,:,j,k] = np.add(wc_cp_mnth_o3[m,:,j,k], prof)
#
#    for m in range(12):
#        mnth = (m+1)
#        mnth = str(mnth) if mnth > 10 else '0'+str(mnth)
#        mnth_count = month.count(mnth)
#        for i in range(nmlat):
#            if (wc_mnth_count[m,i,i] > 2):
#                wc_cp_mnth_o3[m,:,i,i] = np.divide(wc_cp_mnth_o3[m,:,i,i], wc_mnth_count[m,i,i])

    # last thing is to do monthly and annual means for plotting the measurements:
    print('dimensions of wc_cpres_profiles=',np.shape(wc_cpres_profiles))
    wc_cp_mnth_o3 = np.zeros((2, 12, nm_cpres)) # 2 years, 12 months
    wc_mnth_count = np.zeros((2, 12, nm_cpres))
    for t in obstime:
        i = list(obstime).index(t)
        m = t.month-1
        y = t.year-int(years[0])
        wc_cp_mnth_o3[y,m,:] = np.add(wc_cpres_profiles[i,:], wc_cp_mnth_o3[y,m,:])
        wc_mnth_count[y,m,:] +=1
    # the monthly avg
    wc_cp_mnth_o3[:,:,:] = np.divide(wc_cp_mnth_o3[:,:,:], wc_mnth_count[:,:,:])	 
    # the annual average
    wc_tmp = np.nanmean(wc_cp_mnth_o3,axis=1) # first average the 12 months together into one
    wc_annual = np.nanmean(wc_tmp, axis=0) # then average the 2 years together into one.
    
    if np.all(np.isnan(wc_annual)):
        print("All WOUDC values nan???")

  #  exit()

    #read in the data from each model, then interpolate
    multi_mdl_o3 = {}
    multi_mdl_o3_seas = {}
    multi_mdl_annual = {}
    processed_models = []

    for m in models:
        if m == "CESM": 
            mdl_o3, mdl_pres, mdl_times, mdl_lev = cesm_model(wc_lat=wc_lat,wc_lon=wc_lon,years=years)
        elif m == "CMAM": 
            mdl_o3, mdl_pres, mdl_times, mdl_lev = cmam_model(wc_lat=wc_lat,wc_lon=wc_lon,years=years)
        elif m == "DEHM": 
            mdl_o3, mdl_pres, mdl_times, mdl_lev = dehm_model(wc_lat=wc_lat,wc_lon=wc_lon,years=years)
        elif m == "EMEP-MSCW": 
            mdl_o3, mdl_pres, mdl_times, mdl_lev = emep_model(wc_lat=wc_lat,wc_lon=wc_lon,years=years)        
        elif m == "GEOS-Chem": 
            mdl_o3, mdl_pres, mdl_times, mdl_lev = geoschem_model(wc_lat=wc_lat,wc_lon=wc_lon,years=years)
        elif m == "GISS-E2.1": 
            mdl_o3, mdl_pres, mdl_times, mdl_lev = giss_model(wc_lat=wc_lat,wc_lon=wc_lon,years=years)
        elif m == "MATCH": 
            mdl_o3, mdl_pres, mdl_times, mdl_lev = match_model(wc_lat=wc_lat,wc_lon=wc_lon,years=years)
        elif m == "MATCH-SALSA": 
            mdl_o3, mdl_pres, mdl_times, mdl_lev = matchsalsa_model(wc_lat=wc_lat,wc_lon=wc_lon,years=years)
        elif m == "MRI-ESM2": 
            mdl_o3, mdl_pres, mdl_times, mdl_lev = mri_model(wc_lat=wc_lat,wc_lon=wc_lon,years=years)
        elif m == "OsloCTM": 
            mdl_o3, mdl_pres, mdl_times, mdl_lev = oslo_model(wc_lat=wc_lat,wc_lon=wc_lon,years=years)
        elif m == "UKESM1": 
            mdl_o3, mdl_pres, mdl_times, mdl_lev = ukesm_model(wc_lat=wc_lat,wc_lon=wc_lon,years=years)
        elif m == "WRF-Chem": 
            mdl_o3, mdl_pres, mdl_times, mdl_lev = wrfchem_model(wc_lat=wc_lat,wc_lon=wc_lon,years=years)
        else:
            print("Didn't recognize model "+m+". Can't read model files")
            continue
	
        #  Now vertically interpolate the model profiles to cpres levels
        nm_years = 2
        mdl_mnth_o3_tmp = np.zeros((nm_years, 12, mdl_lev))
        mdl_mnth_o3_int = np.zeros((nm_years, 12, len(cpres)))
        print("Matching location of "+m+" to obs and then interpolated to pressure levels")
        for mn in range(12):
            for yr in range(nm_years):
                mdl_mnth_o3_tmp[yr,mn,:] = mdl_o3[yr,mn,:]
                # Now interpolate onto cpres levelsp    
                tmp_o3 = (mdl_mnth_o3_tmp[yr,mn,:]).tolist()
                tmp_plev = (mdl_pres[yr, mn, :]).tolist()
                if m not in ("CESM","EMEP-MSCW"):  # "GEOS-Chem"
                    tmp_plev.reverse()
                    tmp_o3.reverse()		    
                if m == "MATCH-SALSA":
                    tmp_plev.reverse()  # pressure was ordered in opposite direction as O3.
                mdl_mnth_o3_int[yr,mn,:] = np.interp(cpres, tmp_plev, tmp_o3)		    
			
        if np.all(np.isnan(mdl_mnth_o3_int)):
            print("Something wrong in interpolation??? All nan for "+m)

        # the annual average
        annual_tmp = np.nanmean(mdl_mnth_o3_int, axis=1) # first avg the 12 months together
        annual_mean = np.nanmean(annual_tmp, axis=0) # then avg the 2 years together.

        ###----------Seasonal Averages--------

        #lets define the seasons as dec-jan-feb, march-april-may, june-july-aug, sept-oct-nov

        #first the model averages
        djf_mdl = np.add(mdl_mnth_o3_int[:,11,:], mdl_mnth_o3_int[:,0,:])
        djf_mdl = np.add(djf_mdl, mdl_mnth_o3_int[:,1,:])
        mam_mdl = np.add(mdl_mnth_o3_int[:,2,:], mdl_mnth_o3_int[:,3,:])
        mam_mdl = np.add(mam_mdl, mdl_mnth_o3_int[:,4,:])
        jja_mdl = np.add(mdl_mnth_o3_int[:,5,:], mdl_mnth_o3_int[:,6,:])
        jja_mdl = np.add(jja_mdl, mdl_mnth_o3_int[:,7,:])
        son_mdl = np.add(mdl_mnth_o3_int[:,8,:], mdl_mnth_o3_int[:,9,:])
        son_mdl = np.add(son_mdl, mdl_mnth_o3_int[:,10,:])

        djf_mdl = np.divide(djf_mdl, 3)
        mam_mdl = np.divide(mam_mdl, 3)
        jja_mdl = np.divide(jja_mdl, 3)
        son_mdl = np.divide(son_mdl, 3)

        seas_model = np.stack((djf_mdl, mam_mdl, jja_mdl, son_mdl))
        seas_model = np.nanmean(seas_model, axis=1)   # get the average of the 2 years.

        multi_mdl_o3[m] = mdl_mnth_o3_int
        multi_mdl_o3_seas[m] = seas_model
        multi_mdl_annual[m] = annual_mean
        processed_models.append(m)

    #now the observations
    djf_obs = np.add(wc_cp_mnth_o3[:,11,:], wc_cp_mnth_o3[:,0,:])
    djf_obs = np.add(djf_obs, wc_cp_mnth_o3[:,1,:])
    mam_obs = np.add(wc_cp_mnth_o3[:,2,:], wc_cp_mnth_o3[:,3,:])
    mam_obs = np.add(mam_obs, wc_cp_mnth_o3[:,4,:])
    jja_obs = np.add(wc_cp_mnth_o3[:,5,:], wc_cp_mnth_o3[:,6,:])
    jja_obs = np.add(jja_obs, wc_cp_mnth_o3[:,7,:])
    son_obs = np.add(wc_cp_mnth_o3[:,8,:], wc_cp_mnth_o3[:,9,:])
    son_obs = np.add(son_obs, wc_cp_mnth_o3[:,10,:])

    djf_obs = np.divide(djf_obs, 3)
    mam_obs = np.divide(mam_obs, 3)
    jja_obs = np.divide(jja_obs, 3)
    son_obs = np.divide(son_obs, 3)

    seas_obs = np.stack((djf_obs, mam_obs, jja_obs, son_obs))
    seas_obs = np.nanmean(seas_obs, axis=1)  # get the average of the 2 years.


    ###------------Plotting-----------------
    #set colours for the models for scatter plots
    colour_dict={'CESM': 'lightblue', 'CanAM': 'orange', 'CMAM': 'gray', 'DEHM': 'darkblue', 'ECHAM-SALSA': 'maroon', 'EMEP-MSCW': 'limegreen', 'GEOS-Chem': 'gold', 'GISS-E2.1': 'cyan', 'MATCH': 'darkgreen', 'MATCH-SALSA': 'blue', 'MRI-ESM2': 'red', 'NorESM': 'purple', 'OsloCTM': 'brown', 'UKESM1': 'magenta', 'WRF-Chem': 'darkgoldenrod'}
    
    seas_lookup = ['DJF', 'MAM', 'JJA', 'SON']
    
    #first plot the profile and difference
    plt.clf()
    fig, axs = plt.subplots(1,2)
    
    wc_ann = (wc_annual[:]).tolist()
    wc_ann.reverse()
    cpres_rev = cpres
    cpres.reverse()
	
    axs[0].plot(wc_ann, cpres_rev, c='k', label='WOUDC Obs',linewidth=2.0)
    axs[0].set_title("Annual mean O3 Profile for "+str(years[0])+"-"+str(years[1])+" "+sites)
        
    axs[0].set_ylim(950, 200)
    axs[0].set_ylabel('Pressure (hPa)')
    axs[0].set_yscale('log')
    axs[0].set_xscale('log')
    axs[0].set_xlabel('O3 (ppbv)')
        
    zero = np.zeros(len(cpres)) # 20 vertical levels
    axs[1].plot(zero, cpres_rev, c='k', linestyle='dashed')
    axs[1].set_yscale('log')
    axs[1].set_ylim(950,200)
    axs[1].set_yticklabels([''])
     
    c=0
    for m in models:
        mod=multi_mdl_annual[m]  # change to annual mean from models
        mod=(mod[:]).tolist() # change to annual mean
        mod.reverse()
        plot_dif = (np.array(mod) - np.array(wc_ann))/np.array(wc_ann)*100  # wc_jan for monthly 

        axs[0].plot(mod, cpres_rev, c=colour_dict[m],label=m,linewidth=0.5)
        axs[1].plot(plot_dif, cpres_rev,c=colour_dict[m])
        c=c+1      
    axs[0].legend(loc='lower right',fontsize='x-small')	
    axs[1].set_xlim(-100, 100)
    axs[1].set_title("Percent Difference")
    axs[1].set_xlabel('Difference (%)')
	
    fig.savefig(plots_dir+"o3sonde_annual"+years[0]+"-"+years[1]+"_"+sites+".png")
    fig.savefig(plots_dir_eps+"o3sonde_annual"+years[0]+"-"+years[1]+"_"+sites+".eps")
	
    #### Now the seasonal plots
    for j in range(4):
        plt.clf()
        fig, axs = plt.subplots(1,2)
    
        wc_seas = (seas_obs[j,:]).tolist()
        wc_seas.reverse()
         #   cpres_rev = cpres
         #   cpres.reverse()
	
        axs[0].plot(wc_seas, cpres_rev, c='k', label='WOUDC Obs',linewidth=2.0)
        axs[0].set_title(seas_lookup[j]+" avg O3 Profile for "+str(years[0])+"-"+str(years[1])+" "+sites)
        axs[0].set_ylim(950, 200)
        axs[0].set_ylabel('Pressure (hPa)')
        axs[0].set_yscale('log')
        axs[0].set_xscale('log')
        axs[0].set_xlabel('O3 (ppbv)')
        
        zero = np.zeros(len(cpres)) # 19 vertical levels
        axs[1].plot(zero, cpres_rev, c='k', linestyle='dashed')
        axs[1].set_yscale('log')
        axs[1].set_ylim(950,200)
        c=0
        for m in models:
            mod=multi_mdl_o3_seas[m]
            mod=(mod[j,:]).tolist()
            mod.reverse()
            plot_dif = (np.array(mod) - np.array(wc_seas))/np.array(wc_seas)*100  # wc_ann for annual
            axs[0].plot(mod, cpres_rev, c=colour_dict[m],label=m,linewidth=0.5)
            axs[1].plot(plot_dif, cpres_rev,c=colour_dict[m])
            c=c+1      
        axs[0].legend(loc='lower right',fontsize='x-small')	
        axs[1].set_xlim(-100, 100)
        axs[1].set_title("Percent Difference")
        axs[1].set_xlabel('Difference (%)')

        fig.savefig(plots_dir+"o3sonde_"+seas_lookup[j]+years[0]+"-"+years[1]+"_"+sites+".png")
        fig.savefig(plots_dir_eps+"o3sonde_"+seas_lookup[j]+years[0]+"-"+years[1]+"_"+sites+".eps")

        ####

if __name__ == "__main__":
    multi_model_plots(sites="Resolute",years =['2014', '2015'], models = ['CESM','CMAM','DEHM','EMEP-MSCW','GEOS-Chem','GISS-E2.1','MATCH','MATCH-SALSA','MRI-ESM2','OsloCTM','UKESM1',"WRF-Chem"])
  #  multi_model_plots(sites="Resolute",years =['2014', '2015'], models = ['GEOS-Chem'])
 
    #other sites include: 'Alert', 'Eureka', 'Resolute', 'Churchill', 'Lerwick', 'NyAlesund' (not Barrow b/c it only has 1 month of data)
    # compared to the 3-hourly analysis, only additional model seems to be OsloCTM.
    
