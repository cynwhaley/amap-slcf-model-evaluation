"""
ozonesondes_longTS.py
Based on TES program by
Author: Tahya Weiss-Gibbons
January 2020
tahya.weissgibbons@gmail.com
Updates by:
Author: Cyndi Whaley
April 2021
cynthia.whaley@canada.ca

Takes in a list of models and a specified time period and compares model data to WOUDC ozonesonde data in that time period. 
This *_longTS version is to run only on models that submitted 1990-2015 time series, and to create a datafile of the mmm for Jens to analyse.
1990-2015 models are: CMAM, DEHM, EMEP-MSC-W, GISS-E2.1 (1995-), MRI-ESM2, UKESM1
"""
import os
import sys
import csv
import argparse
import numpy as np
import pandas as pd
import statistics as st
import matplotlib
import matplotlib.pyplot as plt
from io import StringIO
import re
from datetime import date, datetime
from bisect import bisect_left
from netCDF4 import Dataset, num2date
from scipy.interpolate import interpn, interp2d
from mpl_toolkits.basemap import Basemap

def cmam_model(years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/CMAM/',wc_lat=80.053,wc_lon=-86.42):
    print("Reading in CMAM data...")
    mdl_files = {'o3': 'vmro3_month_CMAM_AMAP2020-SD_r1i1p1_199001-201812.nc'}

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = np.array(mf.variables['lon'][:])
    mdl_lat = np.array(mf.variables['lat'][:])
    
    # determine the indices of the given wc_lat and wc_lon
    if wc_lon < 0:
        wc_lon= wc_lon+360
    latind=np.abs(wc_lat-mdl_lat).argmin()
    lonind=np.abs(wc_lon-mdl_lon).argmin()
    print('latind, lonind=',latind,lonind)
    
    mdl_lev = (mf.variables['lev'][:]).tolist()
    
    # get O3 only at the given lat and lon
    mdl_vmro3 = mf.variables['vmro3'][:,:,latind,lonind]    # vmro3(time, lev, lat, lon) 
    o3_units = mf.variables['vmro3'].getncattr('units')
   # print('mdl_vmro3=',mdl_vmro3[10,:])
    
    if o3_units == "mole mole-1": #equivalent to volume ozone per volume air from ideal gas law
        mdl_vmro3 = mdl_vmro3*(10**9) #convert to ppbv

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    #need to find pressure from formula p = ap + b*ps at every level
    mdl_ap = mf.variables['ap'][:]
    mdl_b = mf.variables['b'][:]
    mdl_ps = mf.variables['ps'][:,latind,lonind]   # ps(time, lat, lon)
   # print('mdl_ps=',mdl_ps[:])

    mf.close()

    nm_lev = len(mdl_lev)
    nm_year = 21 # between 1995 and 2015

    mdl_mnth_o3 = np.zeros((nm_year, 12, nm_lev))
    mdl_mnth_pres = np.zeros((nm_year, 12, nm_lev))
    mnth_count = np.zeros((nm_year, 12, nm_lev))
    pres_count = np.zeros((nm_year, 12, nm_lev))

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if int(t.year) not in range(int(years[0]),(int(years[1])+1),1):
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year-1995 # y = the year index. 1994 would be at index 0, 1995 at index 1, ... 2015 at index 20.
        mdl_mnth_o3[y,m,:] = np.add(mdl_vmro3[i,:], mdl_mnth_o3[y,m,:])
        mnth_count[y,m,:] +=1
        tmp_pres = (mdl_ap + mdl_b*mdl_ps[i])*0.01 #convert to hpa
        mdl_mnth_pres[y,m,:] = np.add(tmp_pres, mdl_mnth_pres[y,m,:])
        pres_count[y,m,:] += 1
    
  #  print('mdl_mnth_o3 at 10th year, 2nd month=',mdl_mnth_o3[10,2,:])
    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:], mnth_count[:,:,:])
    mdl_mnth_pres[:,:,:] = np.divide(mdl_mnth_pres[:,:,:], pres_count[:,:,:])
    return mdl_mnth_o3, mdl_mnth_pres, nm_lev # times, mdl_lat, mdl_lon,

def dehm_model(years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/DEHM/',wc_lat=80.053,wc_lon=-86.42):
    print("Reading in DEHM data....")

    mdl_files = {'o3': 'DEHM_type0_o3_1990_2018.nc', 'pres': 'DEHM_type0_pres_1990_2018.nc'}

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = np.array(mf.variables['lon'][:])
    mdl_lat = np.array(mf.variables['lat'][:])
    try:
        mdl_lev = (mf.variables['lvl'][:]).tolist()
    except KeyError:
        mdl_lev = (mf.variables['lev'][:]).tolist()
    
    # determine the indices of the given wc_lat and wc_lon
    if wc_lon < 0:
        wc_lon= wc_lon+360
    latind=np.abs(wc_lat-mdl_lat).argmin()
    lonind=np.abs(wc_lon-mdl_lon).argmin()
    print('latind, lonind=',latind,lonind)
    
    mdl_vmro3 = mf.variables['o3'][:,:,latind,lonind] # time, lev, lat, lon

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    nm_lev = len(mdl_lev)
    nm_year = 21 # between 1995 and 2015

    mdl_mnth_o3 = np.zeros((nm_year, 12, nm_lev))
    mdl_mnth_pres = np.zeros((nm_year, 12, nm_lev))
    mnth_count = np.zeros((nm_year, 12, nm_lev))

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_pres = mf.variables['pres'][:,:,latind,lonind]

    #check the units on the pressure 
    pres_units = mf.variables['pres'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf.close()

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if int(t.year) not in range(int(years[0]),(int(years[1])+1),1):
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year-1995
        mdl_mnth_o3[y,m,:] = np.add(mdl_vmro3[i,:], mdl_mnth_o3[y,m,:])
        mdl_mnth_pres[y,m,:] = np.add(mdl_pres[i,:], mdl_mnth_pres[y,m,:])
        mnth_count[y,m,:] +=1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:] ,mnth_count[:,:,:])
    mdl_mnth_pres[:,:,:] = np.divide(mdl_mnth_pres[:,:,:], mnth_count[:,:,:])
    return mdl_mnth_o3, mdl_mnth_pres, nm_lev # times, mdl_lat, mdl_lon,

def emep_model(years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/EMEP-MSCW/',wc_lat=80.053,wc_lon=-86.42):
    print("Reading in emep data....")

    mdl_files = {'o3': 'EMEP-MSCW_tp0_v04_o3_3hour_'+years[0]+'_'+years[1]+'.nc', 'pres': 'EMEP-MSCW_tp0_v04_ps_3hour_'+years[0]+'_'+years[1]+'.nc'}
    ab_file = '/space/hall3/sitestore/eccc/crd/ccrn/users/sta109/AMAP_plots/tes_scripts/emep_a_b.csv'
    
    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = np.array(mf.variables['lon'][:])
    mdl_lat = np.array(mf.variables['lat'][:])
    try:
        mdl_lev = (mf.variables['lvl'][:]).tolist()
    except KeyError:
        mdl_lev = (mf.variables['lev'][:]).tolist()
      
    # determine the indices of the given wc_lat and wc_lon
    if wc_lon < 0:
        wc_lon= wc_lon+360
    latind=np.abs(wc_lat-mdl_lat).argmin()
    lonind=np.abs(wc_lon-mdl_lon).argmin()
    print('latind, lonind=',latind,lonind)    
    
    mdl_vmro3 = mf.variables['o3'][:,:,latind,lonind]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
   # time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    nm_lev = len(mdl_lev)
    nm_year = 21 # between 1995 and 2015

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_ps = mf.variables['ps'][:,latind,lonind]

    mf.close()

    #also need the a and b coordinates to calcualte the pressure at each level
    #info in csv in format lev a b
    #the pressure is from the formula a+b*ps
    with open(ab_file, 'r') as f:
        headers = f.readline()
        lines = f.readlines()

    mdl_a = []
    mdl_b =[]
    for r in lines:
        t = r.split(',')
        mdl_a.append(float(t[0].strip()))
        mdl_b.append(float(t[1].strip()))

    mdl_a = np.asarray(mdl_a)
    mdl_b = np.asarray(mdl_b)

    times = num2date(mdl_time, units=time_units, calendar='gregorian')

    mdl_mnth_o3 = np.zeros((nm_year, 12, nm_lev))
    mdl_mnth_pres = np.zeros((nm_year, 12, nm_lev))
    mnth_count = np.zeros((nm_year, 12, nm_lev))
    pres_count = np.zeros((nm_year, 12, nm_lev))

    #now average over just the interested years
    for t in times:
        if int(t.year) not in range(int(years[0]),(int(years[1])+1),1):
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year-1995
        mdl_mnth_o3[y,m,:] = np.add(mdl_mnth_o3[y,m,:], mdl_vmro3[i,:])
        mnth_count[y,m,:] += 1

        tmp = mdl_b*mdl_ps[i]
        tmp_pres = (mdl_a + tmp)*0.01 #convert to hpa
        mdl_mnth_pres[y,m,:] = np.add(tmp_pres, mdl_mnth_pres[y,m,:])
        pres_count[y,m,:] += 1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:], mnth_count[:,:,:])
    mdl_mnth_pres[:,:,:] = np.divide(mdl_mnth_pres[:,:,:], mnth_count[:,:,:])
    
    return mdl_mnth_o3, mdl_mnth_pres, nm_lev # times, mdl_lat, mdl_lon,
    
    
def giss_model(years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/GISS-modelE-OMA/GISS_reformatted/',wc_lat=80.053,wc_lon=-86.42):
    print("Reading in GISS-modelE-OMA data...")

    mdl_files = {'o3': 'GISS-modelE-OMA_type0_o3_NCEP_reformatted.nc', 'pres': 'GISS-modelE-OMA_type0_pres_NCEP_reformatted.nc'}

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = np.array(mf.variables['lon'][:])
    mdl_lat = np.array(mf.variables['lat'][:])
    try:
        mdl_lev = (mf.variables['lvl'][:]).tolist()
    except KeyError:
        mdl_lev = (mf.variables['lev'][:]).tolist()
    
    # determine the indices of the given wc_lat and wc_lon
    if wc_lon < 0:
        wc_lon= wc_lon+360
    latind=np.abs(wc_lat-mdl_lat).argmin()
    lonind=np.abs(wc_lon-mdl_lon).argmin()
    print('latind, lonind=',latind,lonind) 
    
    mdl_vmro3 = mf.variables['o3'][:,:,latind,lonind]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    nm_lev = len(mdl_lev)
    nm_year = 21 # between 1995 and 2015

    mdl_mnth_o3 = np.zeros((nm_year, 12, nm_lev))
    mdl_mnth_pres = np.zeros((nm_year, 12, nm_lev))
    mnth_count = np.zeros((nm_year, 12, nm_lev))

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_pres = mf.variables['pres'][:,:,latind,lonind]

    #check the units on the pressure 
    pres_units = mf.variables['pres'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf.close()

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if int(t.year) not in range(int(years[0]),(int(years[1])+1),1):
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year-1995
        mdl_mnth_o3[y,m,:] = np.add(mdl_vmro3[i,:], mdl_mnth_o3[y,m,:])
        mdl_mnth_pres[y,m,:] = np.add(mdl_pres[i,:], mdl_mnth_pres[y,m,:])
        mnth_count[y,m,:] +=1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:] ,mnth_count[:,:,:])
    mdl_mnth_pres[:,:,:] = np.divide(mdl_mnth_pres[:,:,:], mnth_count[:,:,:])
    return mdl_mnth_o3, mdl_mnth_pres, nm_lev # times, mdl_lat, mdl_lon,

def mri_model(years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/MRI-ESM/',wc_lat=80.053,wc_lon=-86.42):
    print("Reading in MRI-ESM data...")
    
    mdl_files = {'o3': 'MRI-ESM_type0_o3_1990-'+years[1]+'.nc', 'pres': 'MRI-ESM_type0_ps_T42L80_mon_1990-2015.nc'}

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = np.array(mf.variables['lon'][:])
    mdl_lat = np.array(mf.variables['lat'][:])
    mdl_lev = (mf.variables['lev'][:]).tolist()

    # determine the indices of the given wc_lat and wc_lon
    if wc_lon < 0:
        wc_lon= wc_lon+360
    latind=np.abs(wc_lat-mdl_lat).argmin()
    lonind=np.abs(wc_lon-mdl_lon).argmin()
    print('latind, lonind=',latind,lonind) 

    mdl_vmro3 = mf.variables['o3'][:,:,latind,lonind]
    o3_units = mf.variables['o3'].getncattr('units')

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')
    
    mf.close()

    nm_lev = len(mdl_lev)
    nm_year = 21
    
    mdl_mnth_o3 = np.zeros((nm_year, 12, nm_lev))
    mdl_mnth_pres = np.zeros((nm_year, 12, nm_lev))
    mnth_count = np.zeros((nm_year, 12, nm_lev))
    pres_count = np.zeros((nm_year, 12, nm_lev))
    
    #now read in the data from the pressure file
    #need to find pressure from formula p = a*p0 + b*ps at every level
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')    
    mdl_p0 = mf.variables['p0'][:]
    mdl_a = mf.variables['a'][:]
    mdl_b = mf.variables['b'][:]
    mdl_ps = mf.variables['ps'][:,latind,lonind]
    
    mf.close() 
    
    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    #now average over just the interested years
    for t in times:
        if int(t.year) not in range(int(years[0]),(int(years[1])+1),1):
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year-1995
        mdl_mnth_o3[y,m,:] = np.add(mdl_vmro3[i,:], mdl_mnth_o3[y,m,:])
        mnth_count[y,m,:] +=1
        tmp_pres = (mdl_a*mdl_p0 + mdl_b*mdl_ps[i])*0.01 #convert to hpa
        mdl_mnth_pres[y,m,:] = np.add(tmp_pres, mdl_mnth_pres[y,m,:])
        pres_count[y,m,:] += 1

    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:], mnth_count[:,:,:])
    mdl_mnth_pres[:,:,:] = np.divide(mdl_mnth_pres[:,:,:], pres_count[:,:,:])
    return mdl_mnth_o3, mdl_mnth_pres, nm_lev # times, mdl_lat, mdl_lon,
    	

def ukesm_model(years=['2014', '2015'], root_mdl='/space/hall3/sitestore/eccc/crd/ccrn/users/cmr209/AMAP/UKESM1/',wc_lat=80.053,wc_lon=-86.42):
    print("Reading in UKESM1 model data...")

    mdl_files = {'o3': 'UKESM1_type0_monthly_ozone_volume_mixing_ratio_1990_'+years[1]+'.nc', 'pres': 'UKESM1_type0_monthly_air_pressure_1990_2015_V1.nc'}

    #first lets read in the info from the o3 file
    mf = Dataset(root_mdl+mdl_files['o3'], 'r')
    mdl_lon = np.array(mf.variables['longitude'][:])
    mdl_lat = np.array(mf.variables['latitude'][:])
    mdl_lev = (mf.variables['level_height'][:]).tolist()

    # determine the indices of the given wc_lat and wc_lon
    if wc_lon < 0:
        wc_lon= wc_lon+360
    latind=np.abs(wc_lat-mdl_lat).argmin()
    lonind=np.abs(wc_lon-mdl_lon).argmin()
    print('latind, lonind=',latind,lonind) 

    mdl_vmro3 = mf.variables['ozone_volume_mixing_ratio'][:,:,latind,lonind]

    mdl_time = mf.variables['time'][:]
    time_units = mf.variables['time'].getncattr('units')
    time_cal = mf.variables['time'].getncattr('calendar')

    mf.close()

    #now read in the data from the pressure file
    mf = Dataset(root_mdl+mdl_files['pres'], 'r')
    mdl_pres = mf.variables['air_pressure'][:,:,latind,lonind]

    #check the units on the pressure 
    pres_units = mf.variables['air_pressure'].getncattr('units')
    if pres_units == 'Pa' or pres_units == "kg m-1 s-2":
        mdl_pres = mdl_pres*0.01

    mf.close()

    times = num2date(mdl_time, units=time_units, calendar=time_cal)

    nm_lev = len(mdl_lev)
    nm_year = 21

    mdl_mnth_o3 = np.zeros((nm_year, 12, nm_lev)) # 1995-2015 is 21 years
    mdl_mnth_pres = np.zeros((nm_year,12, nm_lev))
    mnth_count = np.zeros((nm_year,12, nm_lev))

    #now make a data frame with year, month, O3 values at all vertical levels
    for t in times:
        if int(t.year) not in range(int(years[0]),int(years[1]),1):
            continue
        i = list(times).index(t)
        m = t.month-1
        yr = t.year
        y = t.year-1995
        mdl_mnth_o3[y,m,:] = np.add(mdl_vmro3[i,:], mdl_mnth_o3[y,m,:])
        mdl_mnth_pres[y,m,:] = np.add(mdl_pres[i,:], mdl_mnth_pres[y,m,:])
        mnth_count[y,m,:] +=1
    
    x=t.year
    print('year range',x)
    mdl_mnth_o3[:,:,:] = np.divide(mdl_mnth_o3[:,:,:] ,mnth_count[:,:,:])
    mdl_mnth_pres[:,:,:] = np.divide(mdl_mnth_pres[:,:,:], mnth_count[:,:,:])
    return mdl_mnth_o3, mdl_mnth_pres, nm_lev # times, mdl_lat, mdl_lon,
  

def multi_model_plots(sites="Eureka",wc_lat=80.053,wc_lon=-86.42,models=['CMAM','DEHM','EMEP-MSCW','GISS-E2.1','MRI-ESM2','UKESM1'], years=['1995', '2015']):
    
    # Don't have or need obs for the whole 1995-2015 time period b/c this program is just to produce a data file for Jens to analyse.
	
    np.seterr(divide='ignore', invalid='ignore') #we will divide by zero at some points, ignore all those warnings

    #constant pressure levels which data is interpolated to
    cpres = [ 925.0, 850.0, 800.0, 750.0, 700.0, 650.0, 600.0, 550.0, 500.0, 450.0, 400.0, 350.0, 300.0, 250.0, 200.0, 175.0, 150.0, 125.0, 110.0, 100.0]
    nm_cpres = len(cpres)

    plots_dir='/space/hall3/sitestore/eccc/crd/ccrn/users/rcw001/AMAP/png_figs/'
    #plots_dir_eps = "/space/hall3/sitestore/eccc/crd/ccrn/users/rcw001/AMAP/eps_figs/"

    #read in the data from each model, then interpolate
    multi_mdl_o3 = {}
    multi_mdl_o3_seas = {}
    multi_mdl_annual = {}
    processed_models = []

    for m in models:
        print(m)
	
        if m == "CMAM": 
            mdl_o3, mdl_pres, mdl_lev = cmam_model(years=years,wc_lat=wc_lat,wc_lon=wc_lon)
        elif m == "DEHM": 
            mdl_o3, mdl_pres, mdl_lev = dehm_model(years=years,wc_lat=wc_lat,wc_lon=wc_lon)
        elif m == "EMEP-MSC-W": 
            mdl_o3, mdl_pres, mdl_lev = emep_model(years=years,wc_lat=wc_lat,wc_lon=wc_lon)        
        elif m == "GEOS-Chem": 
            mdl_o3, mdl_pres, mdl_lev = geoschem_model(years=years,wc_lat=wc_lat,wc_lon=wc_lon)
        elif m == "GISS-E2.1": 
            mdl_o3, mdl_pres, mdl_lev = giss_model(years=years,wc_lat=wc_lat,wc_lon=wc_lon)
        elif m == "MRI-ESM2": 
            mdl_o3, mdl_pres, mdl_lev = mri_model(years=years,wc_lat=wc_lat,wc_lon=wc_lon)
        elif m == "OsloCTM": 
            mdl_o3, mdl_pres, mdl_lev = oslo_model(years=years,wc_lat=wc_lat,wc_lon=wc_lon)
        elif m == "UKESM1": 
            mdl_o3, mdl_pres, mdl_lev = ukesm_model(years=years,wc_lat=wc_lat,wc_lon=wc_lon)
        else:
            print("Didn't recognize model "+m+". Can't read model files")
            continue
	
        #  Now vertically interpolate the model profiles
        nm_years = 21
        mdl_mnth_o3_tmp = np.zeros((nm_years, 12, mdl_lev))
        mdl_mnth_o3_int = np.zeros((nm_years, 12, len(cpres)))
        print("Matching location of "+m+" to obs and then interpolated to pressure levels")
        for mn in range(12):
            for yr in range(nm_years):
                mdl_mnth_o3_tmp[yr,mn,:] = mdl_o3[yr,mn,:]
                # Now interpolate onto cpres levels    
                tmp_o3 = (mdl_mnth_o3_tmp[yr,mn,:]).tolist()
                tmp_plev = (mdl_pres[yr, mn, :]).tolist()
                if m not in ("EMEP-MSC-W"):
                    tmp_plev.reverse()
                    tmp_o3.reverse()
                mdl_mnth_o3_int[yr,mn,:] = np.interp(cpres, tmp_plev, tmp_o3)		    
	
    #    if np.all(np.isnan(mdl_mnth_cp_o3)):
    #        print("Something wrong in interpolation??? All nan for "+m)

     #   print('mdl_mnth_o3_int, 10th year, 2nd month=',mdl_mnth_o3_int[10,2,:])
	
        multi_mdl_o3[m] = mdl_mnth_o3_int
        processed_models.append(m)

    ###------------Plotting-----------------
    #set colours for the models for scatter plots
    colour_dict={'CMAM': 'gray', 'DEHM': 'darkblue', 'EMEP-MSC-W': 'limegreen', 'GISS-E2.1': 'cyan', 'MRI-ESM2': 'red', 'UKESM1': 'magenta'}
    
    
    # try plotting just the first year (1995)
    cpres_rev = cpres
    cpres.reverse()
	
    for i in range(12):
        target_month = i
      
        #first plot the profile and difference
        plt.clf()
        fig, axs = plt.subplots(1,1)
    
        axs.set_title("O3 Profile for "+str(target_month)+" "+str(years[0])+" "+sites)
        axs.set_ylim(950, 100)
        axs.set_ylabel('Pressure (hPa)')
        axs.set_yscale('log')
        axs.set_xscale('log')
        axs.set_xlabel('O3 (ppbv)')
      
        c=0
        for m in models:
            mod=multi_mdl_o3[m]
            mod=(mod[0,target_month,:]).tolist()
         #   print('mod=',mod)
         #   mod.reverse()
            axs.plot(mod, cpres_rev, c=colour_dict[m],label=m)
            c=c+1      
        axs.legend(loc='lower right',fontsize='x-small')	
	
        fig.savefig(plots_dir+"o3sonde_monthly_"+str(years[0])+"_"+str(target_month)+"_"+sites+".png") 
    
    
    # ---- Calculate the multi-model *median* for each vertical level --- #
    rows = 21*12
    cols = 2+len(cpres)
    medi = np.array([0.0]*len(cpres))
    arr = [[0]*cols]*rows
    y=0
    n=0
    for yr in range(1995,2016):
        m=0
        for mn in range(1,13):
            l=0
            for lev in cpres:
                val=[]
                for mo in models:
                    mod=multi_mdl_o3[mo]   
                    val=np.append(val,mod[y,m,l])	# the o3 value at yr, mn, lev for model m
                medi[l]=st.median(val)			# the multi-model median (mmm) at yr, mn, lev
                l=l+1
            m=m+1 
          #  print('yr,mn,medi=',yr,mn,medi)
            tmp = np.append(medi, yr)	# put the corresponding yr and mn before the vector of mmm at yr, mn (all levs)
            tmp = np.append(tmp, mn)
            arr[n] = tmp		# row-append, so that each row is for a new month
            n=n+1
        y=y+1
	
   # print('arr=',arr[:,:])
    # Now write data to a csv file. columns should be: year, month, O3_925, O3_850, ...O3_100.
    np.savetxt('/space/hall3/sitestore/eccc/crd/ccrn/users/rcw001/AMAP/csv_files/mmm_o3_sondes_'+sites+years[0]+"-"+years[1]+'.csv',arr,delimiter=',')  


 ####

if __name__ == "__main__":
    multi_model_plots(sites="Lerwick",wc_lat=60.1530,wc_lon=-1.1493,years =['1995', '2015'], models = ['CMAM', 'DEHM','GISS-E2.1','MRI-ESM2', 'UKESM1'])
  #  multi_model_plots(sites="Alert",wc_lat=82.4508,wc_lon=-62.5072,years =['1995', '2015'], models = ['CMAM'])

    # EMEP-MSC-W only provided surface-level O3, thus can not be included here.
    #others include: 'Alert', 'Eureka', 'Resolute', 'Churchill', 'Lerwick', 'NyAlesund' (not Barrow b/c it only has 1 month of data)
    # also: Esrange, Hurdal, Sodankyla       
    
