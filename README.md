# AMAP SLCF model evaluation

This repository is for AMAP SLCF model evaluation programs that were used to produce figures and analysis for the AMAP SLCF 2021 assessment report, as well as subsequent journal publications.

Subdirectories include:

- Trace gas model evaluation code for comparisons to surface monitoring stations, ozonesondes, TES, & MOPITT (written in python, by Tahya Weiss-Gibbons, Sujay Manish Damani, and Cyndi Whaley)
- Aerosol model evaluation code (written in NCL, by Rashed Mahmood)
- BC aircraft model evaluation code (written in ferret, by Barbara Winter)
- ACE-FTS trace gas model evaluation code (written in python, by Laura Saunders)
- possibly also: BC & SO4 deposition evaluation code (by Sabine Eckhardt)

If you wish to use or develop these programs, please alert the author of the program to discuss co-authorship/acknowledgements.
